/*! $URL$
*/

#ifndef __IMU_SPECCMDS_H__
#define __IMU_SPECCMDS_H__

/* Define special functions */

#define SPECCMD_IMU_REVISION_DATA         2000
#define SPECCMD_IMU_READ_SERIALNUM        2001

#define SPECCMD_IMU_SC_POWER_OFF          2030
#define SPECCMD_IMU_TOTAL_POWER_OFF       2032

#define SPECCMD_IMU_PING_CMD              2100
#define SPECCMD_IMU_COUNT_CMD             2101

#define SPECCMD_IMU_FLASH_INIT_CMD        2900
#define SPECCMD_IMU_FLASH_ERASE_CMD       2901
#define SPECCMD_IMU_FLASH_WRITE_CMD       2902
#define SPECCMD_LAST_IMU_CMD              2999

#endif