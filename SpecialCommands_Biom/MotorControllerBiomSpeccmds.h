/*! $URL$
*/

#ifndef __MC_SPECCMDS_H__
#define __MC_SPECCMDS_H__

#define SPECCMD_MC_PING_CMD               1000
#define SPECCMD_MC_COUNT_CMD              1001

#define SPECCMD_MC_START_LDR_CMD          1080
#define SPECCMD_MC_START_APP_CMD          1081
#define SPECCMD_MC_VERIFY_APP_CMD         1082
#define SPECCMD_MC_FLASH_INIT_CMD         1900
#define SPECCMD_MC_FLASH_ERASE_CMD        1901
#define SPECCMD_MC_FLASH_WRITE_CMD        1902
#define SPECCMD_LAST_MC_CMD               1999

#define MC_SET_ENC_POSITION               1107
#define MC_SET_BATTERY_OFFSET             1108
#define MC_GET_DATA                       1111
#define MC_SET_MOTOR_TORQ                 1114
#define MC_SET_MODE_AND_CURRENT           1115
#define MC_GET_CURRENT_OFFSET             1116
#define MC_SET_FILTER                     1117
#define MC_SET_ELEC_FUSE                  1122
#define MC_SET_TRAJ_CTRL                  1315
#define MC_SET_TRAJ_GOAL_POS              1318
#define MC_ZERO_ANKLE_ANGLE               1323 //zero / set ankle angle
#define MC_SET_IMPED                      1331
#define MC_SET_IMPED_AND_DISABLE_FORCEFIELD       1731

#define MC_SET_IMPED_ENABLE               1333
#define MC_SET_IMPED_OFFSET               1335
#define MC_ZERO_VIRTENC_IMPED_OFFSET      1337
#define MC_SET_IMPED_STATE_AND_PARAMS     1361  //new "atomic" command for H2 state machine
#define MC_GET_SAFETY_IMPED               1340
#define MC_SET_SAFETY_IMPED               1341
#define MC_SM_DEBUG                       1411
#define MC_SM_GET_STATE                   1420
#define MC_SM_SET_STATE                   1421
#define MC_SM_SET_ANKLE_ERROR             1422
#define MC_GET_CURRENT_LIMIT              1430
#define MC_SET_CURRENT_LIMIT              1431
#define MC_GET_TORQUE_LIMIT               1432
#define MC_SET_TORQUE_LIMIT               1433

#define MC_GET_FORCEFIELD_PARAMS          1440
#define MC_SET_FORCEFIELD_PARAMS          1441

#define MC_FORCEFIELD_DEBUG               1442
#define MC_SET_FORCEFIELD_ENABLE          1443
#define MC_GET_GLOBAL_FORCEFIELD_ENABLE   1444
#define MC_SET_GLOBAL_FORCEFIELD_ENABLE   1445

#define MC_SET_FORCEFIELD_PARAMS_DAMP     1447

#define MC_RESET_FORCEFIELD_PARAMS        1448
// options for above
#define MC_FF_ENC_THRUST   1
#define MC_FF_HALL_K3      2
#define MC_FF_HALL_THRUST  3

#define MC_SET_ONE_SIDE_IMPED_PLUS_TORQUE 1461
#define MC_SET_ONE_SIDE_IMPED_PLUS_TORQUE_AND_ENABLE_FORCEFIELD 1761

#define MC_SET_FIXED_ENC_qVq              1464
#define MC_SET_qVq_qVd                    1466
#define MC_SET_AUDIO_SM_PARAMS            1471
#define MC_START_AUDIO_SEQ                1477
#define MC_CMD_SET_CALIBRATION_TABLE      1499

// CEB removed 12-7-2010, no longer used
// #define MC_SET_CURRENT_OFFSET_TABLE         1671   // see current_offset_table.h for details
// #define MC_SET_CURRENT_OFFSET_TABLE_BLOCK   1673   // uses a special data format

typedef enum {
  MC_GET_DATA_MUX_ID_TYPE0,
  MC_GET_DATA_MUX_ID_TYPE1,
  MC_GET_DATA_MUX_ID_TYPE2,
  MC_GET_DATA_MUX_ID_TYPE3,
  MC_GET_DATA_MUX_ID_TYPE4,
  MC_GET_DATA_MUX_ID_TYPE5,
} MC_GET_DATA_MUX_ID_T;


#define SPECCMD_MC_CALB_TABLE_ERASE_FLASH        1775
#define SPECCMD_MC_CALB_TABLE_SAVE_TO_FLASH      1776
#define SPECCMD_MC_CALB_TABLE_ERASE_ALL_FLASH    1777


#endif