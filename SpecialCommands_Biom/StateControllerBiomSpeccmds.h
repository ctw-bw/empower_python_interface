#ifndef __SPECCMDS_H__
#define __SPECCMDS_H__

//////////////////////////////////////////////////////////////////
//
//            STATE CONTROLLER SPECIAL COMMANDS
//
//////////////////////////////////////////////////////////////////


#define SPECCMD_SC_PING_CMD                     0
#define SPECCMD_SC_COUNT_CMD                    1

#define SPECCMD_CALENDAR_LOG_DEBUG              50
#define SPECCMD_CALENDAR_LOG_TRIGGER_PFF_STEP   51
#define SPECCMD_CALENDAR_LOG_TRIGGER_DB_STEP    52
#define SPECCMD_CALENDAR_LOG_TRIGGER_WSM_PARAM_SAVE  53

#define SPECCMD_INVOKE_LOADER_CMD               80
#define SPECCMD_RESTART_CMD                     81
#define SPECCMD_STANDBY_CMD                     82
#define SPECCMD_FLASH_INIT_CMD                  90
#define SPECCMD_FLASH_ERASE_CMD                 91
#define SPECCMD_FLASH_PROG_CMD                  92
#define SPECCMD_FLASH_SHUTDOWN_CMD              93
#define SPECCMD_SET_TIME_CMD                    100
#define SPECCMD_GET_TIME_CMD                    101

typedef struct {
  UINT32_T year;
  UINT32_T month;
  UINT32_T day;
  UINT32_T weekday;
  UINT32_T hour;
  UINT32_T min;
  UINT32_T sec;
  UINT32_T lowbat;
}SPECCMD_TIME_T;

#define SPECCMD_SET_RTC_REG_CMD                 102
#define SPECCMD_GET_RTC_REG_CMD                 103

#define SPECCMD_DEBUG_VAR_CMD                   104
#define SPECCMD_BIOM_COM_DEBUG                  105

#define SPECCMD_UNKNOWN_106                     106

#define SPECCMD_READ_MOTOR_SENSORS              111

typedef struct {
  UINT32_T unused_0;
  UINT32_T unused_1;
  UINT32_T unused_2;
  UINT32_T AnkAngRaw;
  UINT32_T MotPosition;
  UINT32_T MotorRpm;
}SPECCMD_MOTOR_SENSORS_T;


#define SPECCMD_READ_MOTOR_THERMAL              117
#define SPECCMD_SET_MOTOR_TLIMITS               118
typedef struct {
  UINT32_T Rmot;
  UINT32_T Tw;
  UINT32_T Tc;
  UINT32_T Th;
}SPECCMD_MOTOR_THERMALS_T;

#define SPECCMD_SET_AC_ENABLE                   123
typedef struct {
  UINT32_T ac_enable;
}SPECCMD_SET_AC_ENABLE_T;

#define SPECCMD_WSM_GET_FEATURE_BITS            126 // get statemachine debug level
#define SPECCMD_WSM_SET_FEATURE_BITS            127 // set statemachine debug level

#define SPECCMD_READ_RELAXED_PARAMS             136
#define SPECCMD_WRITE_RELAXED_PARAMS            137

typedef struct {
  UINT32_T fast_cycle_count;
  UINT32_T exit_hardstop_torq;
}SPECCMD_RELAXED_PARAMS_T;

#define SPECCMD_READ_SC_MFG_EEPROM              138 // read memory from MFG eeprom
#define SPECCMD_WRITE_SC_MFG_EEPROM_BYTE        139 // write 1 byte at addr to MFG eeprom
#define SPECCMD_READ_SC_EEPROM                  140 // read memory from APP eeprom
#define SPECCMD_WRITE_SC_EEPROM_BYTE            141 // write 1 byte at addr to APP eeprom

#define SPECCMD_READ_STEP_COUNT                 142
#define SPECCMD_RESET_STEP_COUNT                143
#define SPECCMD_INC_STEP_COUNT                  145

#define SPECCMD_WRITE_KEY_VALUE  77

typedef struct {
  UINT32_T walking_step_count;
  UINT32_T total_step_count;
  UINT32_T trip_odometer;
}SPECCMD_STEP_COUNT_T;

#define SPECCMD_CMD_D                           144

#define SPECCMD_TELEMETRY_DATAPORT_SET          157
#define SPECCMD_TELEMETRY_DATAPORT_GET          156

#define SPECCMD_GET_SHARED_OBJ                  162
#define SPECCMD_GET_SHARED_OBJ_HEADER_STR       166  // return the address of the shared object header string

#define SPECCMD_DEMUX_STATE_GET                 172
#define SPECCMD_DEMUX_STATE_SET                 173  // force demux statemachine into state

#define SPECCMD_GET_EEPROM_KEY_VALUE            214
#define SPECCMD_SET_EEPROM_KEY_VALUE            215
#define SPECCMD_GET_EEPROM_TABLE_SIZE           216
#define SPECCMD_DELETE_EEPROM_KEY               217

#define SPECCMD_MC_CMD_SET_COM_ENABLE           225

typedef struct {
  UINT32_T ram_addr;
  UINT32_T table_size;
  UINT32_T eeprom_base_addr;
  UINT32_T version;
}SPECCMD_CMD_D_T;

#define SPECCMD_GET_BATT_LIMITS                 248

typedef struct {
  UINT32_T low_voltage_sd_mv;
  UINT32_T batt_volts_mv;
  UINT32_T batt_volts_mv_f;
  UINT32_T batt_volts_mv_20mS_f;
  UINT32_T batt_volts_mv_lvshdn_test;
}SPECCMD_GET_BATT_LIMITS_T;

#define SPECCMD_SET_BATT_LIMITS                 249

#define SPECCMD_HW_ID_SERNUM                    250

typedef struct {
  UINT32_T ecode_state_fixed;
  UINT32_T ankle_ser_num;
}SPECCMD_HW_ID_SERNUM_T;

#define SPECCMD_BOOT_STATUS                     252

typedef struct {
  UINT32_T run_level;
  UINT32_T feature_bits;          //DC 12-11-13 DEPRECATED, left in for backwards table compatibility
  UINT32_T ecode_state_calb;
  UINT32_T ecode_state_fixed;
  UINT32_T ecode_motor_calb;
  UINT32_T ecode_motor_fixed;     //DC 12-11-13 DEPRECATED, left in for backwards table compatibility
  UINT32_T ecode_walking_param;
  UINT32_T ecode_toestrike_param; //AB 1-15-13 DEPRECATED, left in for backwards table compatibility
  UINT32_T ecode_calbstop_enc_obj;//DC 12-11-13 DEPRECATED, left in for backwards table compatibility
  UINT32_T diag_ecode;
} SPECCMD_BOOT_STATUS_T;

#define SPECCMD_MOTOR_TEST_OBSOLETE             256

#define SPECCMD_LOW_BATT_BUZZ_SET               259
#define SPECCMD_LOW_BATT_BUZZ_READ              260
typedef struct {
  UINT32_T unused;
  UINT32_T amplitude;
  UINT32_T freq;
  UINT32_T enable;
  UINT32_T save_settings;
} SPECCMD_LOW_BATT_BUZZ_T;

#define SPECCMD_VERY_LOW_BATT_BUZZ_TEST         263
#define SPECCMD_LOW_BATT_BUZZ_TEST              265

#define SPECCMD_LOW_BATT_ALERT_STATUS           268
typedef struct {
  UINT32_T low_bat_alert_enabled;
  UINT32_T low_bat_alert_max_strength;
  UINT32_T low_bat_alert_offset_mv;
  UINT32_T very_low_bat_alert_offset_mv;
  UINT32_T bat_alert_status;
  UINT32_T bat_alert_tics;
  UINT32_T bat_alert_period;
  UINT32_T bat_alert_duration;
  UINT32_T bat_alert_duration12;
} SPECCMD_LOW_BATT_ALERT_STATUS_T;

#define SPECCMD_WSM_RUNLEVEL_READ               304  // read run level
#define SPECCMD_WSM_RUNLEVEL_WRITE              305  // write run level

#define SPECCMD_DEMUX_DEBUG_READ                420

#define SPECCMD_MC_CAL_TABLE_DEBUG_READ         430
#define SPECCMD_MC_CMD_DEBUG_READ               440
#define SPECCMD_MC_CMD_DEBUG_CLEAR              441


#define SPECCMD_SC_IMU_COMM_DEBUG               442

typedef struct {
  UINT32_T lastCommand;
  UINT32_T commEnabled;
  UINT32_T outstandingCommandType;
  UINT32_T lastSendErrCode;
  UINT32_T lastReceiveErrCode;
  UINT32_T errorCount;
  UINT32_T dataStaleCount;
  UINT32_T defaultCommand;
  UINT32_T missCount;
  UINT32_T maxMissCount;
} SPECCMD_IMU_COMM_DEBUG0_T;

typedef struct {
  UINT32_T lastCommand;
  UINT32_T commEnabled;
  UINT32_T outstandingCommandType;
  UINT32_T lastSendErrCode;
  UINT32_T lastReceiveErrCode;
  UINT32_T errorCount;
  UINT32_T rcvErrorCount;
  UINT32_T sendErrorCount;
  UINT32_T dataStaleCount;
  UINT32_T overrunCount;
} SPECCMD_IMU_COMM_DEBUG1_T;


#define SPECCMD_SENSORS_DEBUG                   450

typedef struct {
  UINT32_T comm_stale_cnt;
  UINT32_T max_comm_stale_cnt;
  UINT32_T mc_sm_state;
  UINT32_T tkmot_mode;
  UINT32_T last_motor_energy;
  UINT32_T last_motor_vi_energy;
  UINT32_T est_full_battery_life_sec;
  UINT32_T peak_motor_current_last_step_qIq;
  UINT32_T total_comm_stale_cnt;
} SPECCMD_SENSORS_DEBUG_T;

#define SPECCMD_MOTORTEMP_PERFORM_DIAG_TEST     473

#define SPECCMD_POWER_CONTROL_DEBUG             480

typedef struct {
  UINT32_T state;
  UINT32_T unused1;
  UINT32_T shutdown_req_flag;
  UINT32_T shutdown_ack_flag;
  UINT32_T unused4;
  UINT32_T unused5;
  UINT32_T batt_volt_update_count;
  UINT32_T batt_voltage_mv_f;
  UINT32_T batt_voltage_mv;
  UINT32_T rtc_pin_val : 1;
  UINT32_T unused9a : 1;
  UINT32_T mc_pwr_en_pin_val : 1;
  UINT32_T sc_pwr_en_pin_val : 1;
  UINT32_T unusedbits: 28;
}SPECCMD_POWER_CONTROL_DEBUG_T;

#define SPECCMD_POWER_CONTROL_STATE_SET         481
#define SPECCMD_POWER_CONTROL_SHUTDOWN_REQ      483

#define SPECCMD_PDA_GET_SUBJECT_WEIGHT          500

typedef struct {
  UINT32_T unused_0;
  UINT32_T subject_weight_centi_lbs;
} SPECCMD_SUBJECT_WEIGHT_T;

#define SPECCMD_PDA_SET_SUBJECT_PARAMS          501

typedef struct {
  UINT32_T unused_0;
 UINT32_T subject_weight_centi_lbs;
} SPECCMD_SUBJECT_PARAMS_T;

#define SPECCMD_PDA_GET_TUNING_PARAMS           502
#define SPECCMD_PDA_SET_NORMALIZED_PARAMS       503
#define SPECCMD_PDA_GET_ENGINEERING_PARAMS      504
#define SPECCMD_PDA_SET_ENGINEERING_PARAMS      505

#define SPECCMD_STREAMING_MEM_XFER_START        507
#define SPECCMD_STREAMING_MEM_XFER_DEBUG        508

#define SPECCMD_PDA_GET_PHONE_VERSION           510 // return internal value of phone version
#define SPECCMD_PDA_SET_PHONE_VERSION           511 // set phone version
#define SPECCMD_PDA_GET_MINIMUM_PHONE_VERSION   512 // read back minimum phone version firmware requires


#define SPECCMD_KITCHEN_MODE_TEST_DEBUG         520 // kitchen mode support testing
#define SPECCMD_KITCHEN_MODE_TEST_SET           521 // set KM paramaeters
#define SPECCMD_KITCHEN_MODE_TEST_SINGLE_STEP   523 // single step the kitchen mode processing alg.
#define SPECCMD_KITCHEN_MODE_TEST_WRITE_QUEUE   525 //

#define SPECCMD_DASHBOARD_POP_ENTRY             910 // read from (pop) dashboard queue
#define SPECCMD_DASHBOARD_FLUSH                 911 // flush dashboard queue
#define SPECCMD_DASHBOARD_DEBUG                 912 // return dashboard debug info
#define SPECCMD_DASHBOARD_RESTART               915 // force a restart of dashboard

#define SPECCMD_MFG_SHUTDOWN_DISABLE_DEBUG      920
#define SPECCMD_SET_LOW_VOLTAGE_MFG_TEST_FLAG   921

// DEBUG - for testing only.  To be removed
#define SPECCMD_GET_ANKLE_ANGLE_CHANGE_THRESH   930
#define SPECCMD_SET_ANKLE_ANGLE_CHANGE_THRESH   931
#define SPECCMD_BATTERY_ALERT_PROCESS_DEBUG     932
#define SPECCMD_SET_HS_TORQUE_RATE_THRESH       933
#define SPECCMD_UNITTEST_CAUSE_MC_REBOOT        935

#define SPECCMD_COMPARE_SS_TORQUE_BUFFERS       940

#define SPECCMD_GET_BUZZ_STIFFNESS              950
#define SPECCMD_SET_BUZZ_STIFFNESS              951

#define SPECCMD_GET_BUZZ_RAMP_DURATION          960
#define SPECCMD_SET_BUZZ_RAMP_DURATION          961

#define SPECCMD_FLOATING_POINT_TEST             973

// These define the ranges of special commands used for routing decision made by the SC
#define SPECCMD_LAST_SC_CMD                     999
#define SPECCMD_LAST_MC_CMD                     1999
#define SPECCMD_LAST_IMU_CMD                    2999


#endif /* __SPECCMDS_H__ */