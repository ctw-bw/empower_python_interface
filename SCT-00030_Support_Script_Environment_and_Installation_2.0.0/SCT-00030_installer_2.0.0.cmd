echo off
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: SCT-00030 version 2.0.0
:: This script installs Python 2.6 for communication with the BiOM, Python 2.7
:: for communication with the emPOWER, and Python 3.6 for pyLauncher and future
:: development. It also installs all necessary 2.6 and 2.7 extensions for 
:: communicating with and developing for both platforms. 
::
:: This script attempts to aquire administrator privileges.
:: 
:: Copyright 2018, BionX Medical Technologies. 
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

set "PY_INSTALLER_PATH=%~dp0py_installer"
set "PYTHON_26_REMOVAL_LOG=%~dp0python26_removal_log.txt"
set "PYTHON_27_REMOVAL_LOG=%~dp0python27_removal_log.txt"
set "LOGFILE=%~dp0main_install_log.txt"
set "PYTHON_26_INSTALL_LOG=%~dp0python26_install_log.txt"
set "PYTHON_27_INSTALL_LOG=%~dp0python27_install_log.txt"
set "PYTHON_27_PIP=C:\Python27\Scripts\pip.exe"
setlocal EnableDelayedExpansion

:::::::::::::::::::::::::::::::::::::::::::
:: Main Function
:::::::::::::::::::::::::::::::::::::::::::

:: Check for permissions
>nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"

:: If error flag set, we do not have admin.
IF %errorlevel% NEQ 0 (
    echo Requesting administrative privileges...
    goto :UACPrompt
) ELSE ( goto :gotAdmin )
goto :eof

:::::::::::::::::::::::::::::::::::::::::::
:: End of Linear Execution
:::::::::::::::::::::::::::::::::::::::::::

:UACPrompt
    echo set UAC = CreateObject^("Shell.Application"^) > "%temp%\getadmin.vbs"
    echo UAC.ShellExecute "%~s0", "", "", "runas", 1 >> "%temp%\getadmin.vbs"

    "%temp%\getadmin.vbs"
    exit /B
goto :eof

:gotAdmin
    if exist "%temp%\getadmin.vbs" ( del "%temp%\getadmin.vbs" )
    pushd "%CD%"
    CD /D "%~dp0"
    call :clearLogFiles
    call :checkDirectory
    call :uninstallations
    call :installations
goto :eof

:clearLogFiles
    :: Clear log files from previous installs
    if exist %LOGFILE% ( del %LOGFILE% )
    if exist %PYTHON_26_REMOVAL_LOG% ( del %PYTHON_26_REMOVAL_LOG% )
    if exist %PYTHON_27_REMOVAL_LOG% ( del %PYTHON_27_REMOVAL_LOG% )
    if exist %PYTHON_26_INSTALL_LOG% ( del %PYTHON_26_INSTALL_LOG% )
    if exist %PYTHON_27_INSTALL_LOG% ( del %PYTHON_27_INSTALL_LOG% )
    if exist %VERSIONFILE% ( del %VERSIONFILE% )
goto :eof

:checkDirectory
    :: Python36 does not install correctly if located in Downloads.
    set "directoryError="
    for %%a in ( 
        "Python36 does not install correctly when the installer"
        "is located in DOWNLOADS. Please move the ENTIRE folder to DOCUMENTS."
        ) do ( set directoryError=!directoryError!%%~a )
    rem echo directoryError = !directoryError! >> %LOGFILE% 2>&1

    echo.%CD% | find /I "Down">nul && (
        set "directoryPathMessage=Found 'Down' in Directory Path"
        echo !directoryPathMessage! >> %LOGFILE% 2>&1
        call :PrintAndLog "%directoryError%"
        goto :error
        ) || (
        set "directoryPathMessage=Did Not Find 'Down' in Directory Path"
        echo !directoryPathMessage!  >> %LOGFILE% 2>&1
        )
goto :eof

:uninstallations
    :: Uninstall everything first. Very aggressive approach.

    :: Uninstall Python 2.6.6
    echo Uninstalling Python 2.6.6
    start /wait msiexec.exe /x "%PY_INSTALLER_PATH%\python-2.6.6.msi" /QN ^
        /L*V %PYTHON_26_REMOVAL_LOG%
    echo Done uninstalling 2.6, status: %ERRORLEVEL%

    :: Uninstall Python 2.7.14
    echo Uninstalling Python 2.7.14
    start /wait msiexec.exe /x "%PY_INSTALLER_PATH%\python-2.7.14.msi" /QN ^
        /L*V %PYTHON_27_REMOVAL_LOG%
    echo Done uninstalling 2.7.14, status: %ERRORLEVEL%

    :: Uninstall Python 3.6.4
    echo Uninstalling Python 3.6.4
    "%PY_INSTALLER_PATH%\python-3.6.4.exe" /uninstall /quiet
    echo Done uninstalling 3.6.4, status: %ERRORLEVEL%

    :: Uninstall Python Launcher
    echo Uninstalling Python Launcher
    wmic product where name="Python Launcher" call uninstall >> %LOGFILE% 2>&1
    echo Done uninstalling Python Launcher

    :: Delete Windows File Associations and File Types
    set "REGISTRY_KEYS="
    for %%a in ( 
        ".py .pyc .pyd .pyo .pyw .pyz .pyzw"
        "Python Python.ArchiveFile Python.CompiledFile Python.Extension"
        "Python.File Python.NoConArchiveFile Python.NoConFile"
        ) do ( set REGISTRY_KEYS=!REGISTRY_KEYS!%%~a )
    rem echo REGISTRY_KEYS = %REGISTRY_KEYS% >> %LOGFILE% 2>&1
    for %%a in ( %REGISTRY_KEYS% ) do ( 
        call :deleteRegistryKey "%%a" )
goto :eof

:installations
    :: Install Python 2.6.6
    echo Installing Python 2.6.6
    start /wait msiexec.exe /i "%PY_INSTALLER_PATH%\python-2.6.6.msi" /QN ^
        WHICHUSERS=ALL ADDLOCAL=ALL REMOVE=Extensions ^
        /L*V %PYTHON_26_INSTALL_LOG%
    echo Done installing 2.6.6, status: %ERRORLEVEL%

    :: Install Python 2.7.14
    echo Installing Python 2.7.14
    start /wait msiexec.exe /i "%PY_INSTALLER_PATH%\python-2.7.14.msi" /QN ^
        WHICHUSERS=ALL ADDLOCAL=ALL REMOVE=Extensions,PrependPath ^
        /L*V %PYTHON_27_INSTALL_LOG%
    echo Done installing 2.7.14, status: %ERRORLEVEL%

    :: Install Python 3.6.4 - options located at:
    ::  https://docs.python.org/3/using/windows.html#installing-without-ui
    echo Installing Python 3.6.4
    "%PY_INSTALLER_PATH%\python-3.6.4.exe" /quiet ^
        InstallAllUsers=1 TargetDir=C:\Python36 AssociateFiles=1
    echo Done installing 3.6.4, status: %ERRORLEVEL%

    :: Install plugins for for Python 2.7
    call :setup27

    :: Install plugins for Python 2.6
    call :setup26

    call :PrintAndLog "Installation Complete"
    call :PrintAndLog "PROCEED TO SCT-00030_instructions.docx : Appendix D"
    pause
goto :eof

:deleteRegistryKey
    call :PrintAndLog "Deleting Registry Key %~1"
    REG DELETE HKCR\%~1 /f >> %LOGFILE% 2>&1
goto :eof

:PrintAndLog
    echo %~1
    echo -------%~1 >> %LOGFILE% 
goto :eof

:setup27
    ::::::::: Lists of things to install for Python 2.7
    set "PYTHON_27_PACKAGES="
    for %%a in ( 
        "numpy scipy PyBluez pyserial ply pycparserext pycparser"
        "XlsxWriter six python_dateutil pyparsing pytz cycler"
        "functools32 matplotlib Phidgets pypubsub wxPython"
        ) do ( set PYTHON_27_PACKAGES=!PYTHON_27_PACKAGES!%%~a )
    rem echo PYTHON_27_PACKAGES = %PYTHON_27_PACKAGES% >> %LOGFILE% 2>&1

    :: The order of installation matters, because components have dependencies!
    :: Part 1 are the packages for talking to an ankle
    set "PART1_WHEEL_LIST_27="
    for %%a in ( 
        "numpy-1.13.1-cp27-none-win32.whl"
        "scipy-0.19.1-cp27-cp27m-win32.whl"
        "PyBluez-0.22-cp27-none-win32.whl"
        ) do ( set PART1_WHEEL_LIST_27=!PART1_WHEEL_LIST_27!%%~a )
    rem echo PART1_WHEEL_LIST_27 = %PART1_WHEEL_LIST_27% >> %LOGFILE% 2>&1
    set "PART1_PACKAGE_LIST_27=pyserial-2.7"

    :: Part 2 are the packages for getting logs from an ankle
    set "PART2_PACKAGE_LIST_27=ply-3.4 pycparser-2.10 pycparserext-2013.2"

    :: Part 3 are the packages for parsing logs from an ankle
    set "PART3_WHEEL_LIST_27="
    for %%a in ( 
        "XlsxWriter-0.9.8-py2.py3-none-any.whl"
        "six-1.10.0-py2.py3-none-any.whl"
        "python_dateutil-2.6.1-py2.py3-none-any.whl"
        "pyparsing-2.2.0-py2.py3-none-any.whl"
        "pytz-2017.2-py2.py3-none-any.whl"
        "cycler-0.10.0-py2.py3-none-any.whl"
        ) do ( set PART3_WHEEL_LIST_27=!PART3_WHEEL_LIST_27!%%~a )
    rem echo PART3_WHEEL_LIST_27 = %PART3_WHEEL_LIST_27% >> %LOGFILE% 2>&1
    set "PART3_PACKAGE_LIST_27=functools32-3.2.3-2"
    set "PART3_WHEEL_LIST2_27=matplotlib-2.0.2-cp27-cp27m-win32.whl"

    :: Part 4 are the packages for running calibration
    set "PART4_PACKAGE_LIST_27=Phidgets-2.1.8 PyPubSub-3.3.0"
    set "PART4_WHEEL_LIST_27=wxPython-4.0.2-cp27-cp27m-win32.whl"

    set "SUPPORT_DIR=%~dp0py27_support"

    :: Remove all existing versions of specified packages.
    for %%a in ( %PYTHON_27_PACKAGES% ) do (
        call :removeExistingPackages "%%a" "2.7")

    :: Install stuff for do_command, robo_config, wifi_fast
    for %%a in ( %PART1_WHEEL_LIST_27% ) do ( 
        call :installWheel "%SUPPORT_DIR%\%%a" "2.7" )
    for %%a in ( %PART1_PACKAGE_LIST_27% ) do ( 
        call :installPackage "%SUPPORT_DIR%\%%a" "2.7" )

    :: Install stuff for getting logs
    for %%a in ( %PART2_PACKAGE_LIST_27% ) do ( 
        call :installPackage "%SUPPORT_DIR%\%%a" "2.7" )

    :: Install stuff for parsing logs
    for %%a in ( %PART3_WHEEL_LIST_27% ) do ( 
        call :installWheel "%SUPPORT_DIR%\%%a" "2.7" )
    for %%a in ( %PART3_PACKAGE_LIST_27% ) do ( 
        call :installPackage "%SUPPORT_DIR%\%%a" "2.7" )
    for %%a in ( %PART3_WHEEL_LIST2_27% ) do ( 
        call :installWheel "%SUPPORT_DIR%\%%a" "2.7" )

    :: Install stuff for calibration
    for %%a in ( %PART4_PACKAGE_LIST_27% ) do ( 
        call :installPackage "%SUPPORT_DIR%\%%a" "2.7" )
	for %%a in ( %PART4_WHEEL_LIST_27% ) do ( 
        call :installWheel "%SUPPORT_DIR%\%%a" "2.7" )

goto :eof

:setup26
    ::::::::: Lists of things to install for Python 2.6
    :: The order of installation matters, because components have dependencies!
    set "PACKAGE_LIST_26=paramiko-1.7.7.1 pyserial-2.5"
    set "EXECUTABLE_LIST_26="
    for %%a in ( 
        "numpy-1.6.2.win32-py2.6.exe"
        "pywin32-214.win32-py2.6.exe"
        "scipy-0.11.0-win32-superpack-python2.6.exe"
        "wxPython2.8-win32-unicode-2.8.12.0-py26.exe"
        ) do ( set EXECUTABLE_LIST_26=!EXECUTABLE_LIST_26!%%~a )
    rem echo EXECUTABLE_LIST_26 = %EXECUTABLE_LIST_26% >> %LOGFILE% 2>&1

    set "SUPPORT_DIR=%~dp0py26_support"

    :: Install stuff for do_command, LTF, TF-050
    for %%a in ( %PACKAGE_LIST_26% ) do ( 
        call :installPackage "%SUPPORT_DIR%\%%a" "2.6" )
    for %%a in ( %EXECUTABLE_LIST_26% ) do ( 
        call :installExecutable "%SUPPORT_DIR%\%%a" "2.6" )
goto :eof

:removeExistingPackages
    call :PrintAndLog "Removing Existing Python %~2 Package %~1"
    IF %~2 == 2.7 ( %PYTHON_27_PIP% uninstall %~1 --yes --quiet ^
        --disable-pip-version-check >> %LOGFILE% 2>&1 )
goto :eof

:installWheel
    :: Most python packages are wheels, and they are installed with PIP.
    call :PrintAndLog "Installing Python %~2 Wheel %~1"
    IF %~2 == 2.7 ( %PYTHON_27_PIP% install %~1 ^
        --disable-pip-version-check >> %LOGFILE% 2>&1 )
    IF %ERRORLEVEL% NEQ 0 ( goto :error )
goto :eof

:installPackage
    :: Some packages aren't wheels; they are self-compiling and installing.
    call :PrintAndLog "Installing Python %~2 Package %~1"
    pushd "%~1"
    IF %ERRORLEVEL% NEQ 0 ( goto :error )
    py -%~2 setup.py install >> %LOGFILE% 2>&1
    IF %ERRORLEVEL% NEQ 0 ( goto :error )
    popd
goto :eof

:installExecutable
    :: Some packages are executables; they are self-compiling and installing.
    call :PrintAndLog "Installing Python %~2 Executable %~1"
    start "" /wait %~1
    IF %ERRORLEVEL% NEQ 0 ( goto :error )
goto :eof

:error
    popd
    echo =======================================================
    echo ENVIRONMENT SETUP ERROR
    echo please see %LOGFILE% for details
    echo =======================================================
    endlocal
    pause
    :: This will show up as a syntax error.
    :: This is a convenient way of stepping out of the entire batch execution
    ::  without exiting the command line. 
    ()
goto :eof
