#!/usr/bin/env python
    
import sys, time, os, struct, string
import python_paths_default  # test for user's python_paths.py and update paths
import python_paths as pyenv
import zipfile  # CEB finds several discrempancyies with docs
    # if zip doesnt exist - then 'a' does NOT create
    # if file is opened with 'a' and the archive is empty - then adding a file to it causes an error
    # thus to create a zip file - try to open it with 'a' - if exception - open with 'w'
    # if opened with 'w' then add a file to it.
    # documentation also states that path names are relative to the archive root -
    # seems to be relative to CWD - thus a files are opened with their path, and then a pathless archive name is used
    
from python_utils import class_address_helper
from comm_lib import serial_port_helper
from comm_lib.wifi_lib import gen_matlab, class_read_varlist, class_fancy_list_output, read_v3_packet, class_read_v1_packet, class_read_v2_packet,  udp_matlab
        
import bz2       # about bz2 http://www.python.org/doc/2.5.2/lib/node301.html    # why bz2 http://warp.povusers.org/ArchiverComparison/
import argparse #used for command line options. 

try :
    import comm_lib.wifi_lib.class_smart_size_logger as CSSL
    import platform
    import serial;
    
    if 1 or os.name == "nt" :
        USE_KEYBOARD = True
        import python_utils.keyboard as keyboard;
        keybd_obj = keyboard.keyboard();
        # import msvcrt;
    else :
        USE_KEYBOARD = False

except :
    print sys.path
    sys.exit(-1);


import getopt, sys, time    

class c_struct :
    pass

class wifi_options :
        PACKET_ENDIAN = '>'
        GUI_MATLAB = 1
        GUI_PYTHON = 2
        PACKET_V1 = 1 
        PACKET_V2 = 2
        PACKET_V3 = 3
        BASE_IP_ADDRESS = "192.168.192."
         
        def __init__(self, args) :
            parser = argparse.ArgumentParser();
            parser.add_argument("-i", "--ip_address", dest="ip_address", default=None, help=" IP address of the SWIFI");
            parser.add_argument("--lantron_port", dest="lantron_port", type=int, default=10001, help="lantron port number for SWIFI connections.");
            parser.add_argument("-p", "--com_port", dest="com_port", default=None, type=int, help="COM PORT number for serial port connection");
            parser.add_argument("-B", dest="device_string", type=str, help="MAC or Linux device name");
            parser.add_argument("-P", "--matlab_port", dest="matlab_port", default= 9999, type=int, help="matlab/ rtplot port number" );
            parser.add_argument("-H", "--matlab_host", default='127.0.0.1', help="matlab IP address")
            parser.add_argument("-f", "--log_file_name", dest="wifilog_filename", default=None, help="log file name")
            parser.add_argument("-F", action="store_true", default=False, dest="swifi_flush", help="flush SWIFI")
            parser.add_argument("-V", "--vlist_file", dest="var_list_filename", default= "dataport3h", help="variable type list")
            parser.add_argument("-t", "--timeout_seconds", dest="application_timeout_secs", type=int, default=None , help="set a timer in seconds for when the wifi_fast program will automatically terminate regardless of connection status.")   
            parser.add_argument("-G", "--gui_choice" , dest="gui_version", default = wifi_options.GUI_MATLAB, help="choose 1 for Matlab, 2 for Python.")   
            parser.add_argument("-b", "--baud_rate", dest="baudrate", type=int, default=1250000, help="baurdrate when using serial port (default is 115200)");
            parser.add_argument("--packet_version", dest="packet_version", type=int, default=2, help="Packet version to parse. Defaults to 2 (used in legacy BiOM and Tango). Use '3' for Battery 2.X.");
           
            
            # parser.disable_interspersed_args()
            args = parser.parse_args()
            
            # make a struct from the options dictionary
            opt = c_struct();
            for k,v in args.__dict__.iteritems() :
                opt.__dict__[k] = v
                #print k,v
            self.opt = opt;
            if opt.lantron_port:
                print "PORT",opt.lantron_port                
            if opt.ip_address:
                print "using  IP ", opt.ip_address
        
            #Legacy options:
            wifi_options.PAcket_version = opt.packet_version
            
            opt.AH = class_address_helper.address_helper(190,240, wifi_options.BASE_IP_ADDRESS);
            
     
                    
                    
            
    
    
            
            
            
            
        
    
    
    


if __name__ == "__main__":
    opt = wifi_options(sys.argv).opt; 
    
    
    FILES_PER_HOUR = 4
    
    VLIST_REL_PATH = "vlist/"
    WIFILOG_REL_PATH = "../wifilog/"
    

    
    ZIP_FILE_DEBUG = False
    
    """
    - Receive data packets from ankle over serial port.
    - Each data packet has header/footer, 8 32-bit floats, and 2 asc-text chars
    - Parse and (re)syncronize to the packet.
    - Output the text channel to the local console
    -build a UDP packet from the 8 data channels, send them to HOST/PORT
    -characters entered on the local console are sent out over the serial port
    - the space character moves between modes that turn / on console printing and
    output of the UDP packet
    """
    
    
    
    SAMPLES_PER_PACKET = 20
    
    
    
    # UDP_HOST = '127.0.0.1';
    # UDP_PORT = 9999;
    
    
    
    # Binary escape sequences for WIP4a and SC
    STOP_FUP_PWM = 191;
    START_FUP_PWM = 190;
    
    
    
    
    # Binary escape sequences for WIP4a and SC
    FLUSH_CMD = 200;
    UNLOCK_CMD = 201;
    LOCK_CMD = 202;
    NO_MIT_COMM = 203;
    RESET_IDLE = 204;
    DISABLE_SELF_IDLE_RESET = 220;
    ENABLE_SELF_IDLE_RESET = 221;
    RESET_SWIFI = 222;
    
    ESC_STR_START_PWM =  [0xee, 211, 223, 227, 229, 197, 199, STOP_FUP_PWM];
    ESC_STR_STOP_PWM =  [0xee, 211, 223, 227, 229, 197, 199, START_FUP_PWM];
    
    ESC_STR_UNLOCK =  [0xee, 211, 223, 227, 229, 197, 199, UNLOCK_CMD];
    ESC_STR_LOCK =  [0xee, 211, 223, 227, 229, 197, 199, LOCK_CMD];
    ESC_STR_FLUSH =  [0xee, 211, 223, 227, 229, 197, 199, FLUSH_CMD];
    ESC_STR_NO_MIT_COMM =  [0xee, 211, 223, 227, 229, 197, 199, NO_MIT_COMM];
    ESC_STR_RESET_IDLE =  [0xee, 211, 223, 227, 229, 197, 199, RESET_IDLE];
    ESC_STR_RESET_SWIFI =  [0xee, 211, 223, 227, 229, 197, 199, DISABLE_SELF_IDLE_RESET];
    ESC_STR_RESET_SWIFI =  [0xee, 211, 223, 227, 229, 197, 199, ENABLE_SELF_IDLE_RESET];
    ESC_STR_RESET_SWIFI =  [0xee, 211, 223, 227, 229, 197, 199, RESET_SWIFI];
    
    
    
    time.sleep(0.5);
    
    ##########################################################################################
    
    
    
    if opt.com_port :
        sph = serial_port_helper.serial_port_helper()
        OS_COM_PORT = sph.get_os_serial_port_name(opt.com_port);
        logging_file_robot_string = "COM:%s" % OS_COM_PORT
        print "USE WINDOWS SERIAL COM:", OS_COM_PORT
       
    elif opt.ip_address :
        LANTRON_HOST = opt.ip_address;
        logging_file_robot_string = LANTRON_HOST
        print "USE TCP ", LANTRON_HOST
        
    elif os.name != 'nt':
        print 'SERIAL LINUX'
        # Discover the USB Serial Ports to use
        sph = serial_port_helper.serial_port_helper()
        mpd_command_portNum = int(sph.get_port_to_use())
        opt.com_port = mpd_command_portNum + 1
        #Note: assumes linux setup hardcoded telemetry to Port mpdcommand + 1.        
        OS_COM_PORT = sph.get_os_serial_port_name(opt.com_port);
        logging_file_robot_string = OS_COM_PORT
        print "USE LINUX SERIAL COM:", OS_COM_PORT        
    else :
            print "Specifiy a COMM port or IP address"
            sys.exit()
    
    
    # DATA_PER_SAMPLE = opt.var_count;
    # DATA_SIZE = opt.var_width;
        
    # DATA_PACKET_LENGTH = DATA_PER_SAMPLE * DATA_SIZE;
    
    
    # sys = platform.system()
    
    
    
    
    
    
    
    
    
    ##########################################################################################
    def write_header(fout) :
        log_file_header_string, header_field_count,date_field_number = make_log_file_header_string()    
        # print "write header called"
        fout.write(log_file_header_string+'\n');
    
    ##########################################################################################
    
    class swifi_periodic_logger :
    # file_per_hours sets the interval of splitting log files.
        def __init__ (self, default_path, base_fname, files_per_hour=6, ZIP_ARCHIVE_FNAME = None, ZIP_FIRST_FILE=None) :
            self.nice_gen = CSSL.nice_interval_generator(None, files_per_hour);
            self.fout = None
            self.next_test_counter = 0;
            self.TEST_COUNT = 0
            self.file_count = 0;
            self.capture_file_at_exit = None;
    
    
            self.working_filename = None;
            self.base_fname = base_fname;
            
    
            t = time.localtime()
            date_str = time.strftime("%y%m%d",t);
            time_str = time.strftime("%H%M",t);
            
            
            (self.filepath, self.filename) = os.path.split(self.base_fname);
            (self.shortname, self.extension) = os.path.splitext(self.filename)
            if len(self.filepath.strip()) == 0 :
                self.filepath=default_path
    
            if ZIP_ARCHIVE_FNAME : 
                (zip_path, zip_filename) = os.path.split(ZIP_ARCHIVE_FNAME);
                (zip_shortname, zip_extension) = os.path.splitext(zip_filename)
                zip_file_base = "%s_%s_%s.zip" % (zip_shortname, date_str, time_str);
            
                self.zip_archive_fname = zip_file_base;         
            
                self.zip_filename = os.path.join(self.filepath, zip_file_base);
    
                try :
                    zf = zipfile.ZipFile(self.zip_filename,'a', zipfile.ZIP_DEFLATED, True);
                    # zf = zipfile.ZipFile(self.zip_filename,'a', zipfile.ZIP_STORED, True);            
                    print "appended existing zipfile : ", self.zip_filename;            
                except :
                    zf = zipfile.ZipFile(self.zip_filename,'w', zipfile.ZIP_DEFLATED, True);
                    print "created new zipfile : ", self.zip_filename;
    
                if ZIP_FIRST_FILE :
                    (path, archive_name) = os.path.split(ZIP_FIRST_FILE);
                    zf.write(ZIP_FIRST_FILE, archive_name)
                zf.close()        
    
            print
            print
            print "path=%s name=%s short=%s ext=%s " % (self.filepath, self.filename, self.shortname, self.extension)
            print
            print
            self.reopen()
    
        def __del__(self) :
            print "__del__ swifi_periodic_logger called";
            if self.fout :
                self.fout.close(); 
                self.add_file_to_zip();
                if self.capture_file_at_exit :
                    self.add_file_to_zip(self.capture_file_at_exit)
    
        def close(self) :
            pass
    
        def set_capture_file_at_exit(self, file) :
            self.capture_file_at_exit = file;
    
        def add_file_to_zip(self, add_filename = None) :
            # append the file just closed to the archive
            
                    
            zf = zipfile.ZipFile(self.zip_filename,'a', zipfile.ZIP_DEFLATED, True);
            # zf = zipfile.ZipFile(self.zip_filename,'a', zipfile.ZIP_STORED, True);
    
            if add_filename :
                full_name = os.path.join(self.filepath, add_filename)
                (zip_path, archive_name) = os.path.split(add_filename);
            else :
                full_name = os.path.join(self.filepath, self.working_filename);
                archive_name = self.working_filename;
    
            if ZIP_FILE_DEBUG :
                print "adding to zipfile : ", archive_name, full_name
    
                
            zf.write(full_name, archive_name);
            zf.close()
            os.remove(full_name);        
    
        def reopen(self) :
            
            # calculate file name
            t = time.localtime()
            date_str = time.strftime("%y%m%d",t);
            time_str = time.strftime("%H%M%S",t);
            
            if ZIP_FILE_DEBUG :
                print
                print
                print "REOPEN CALLED", self.file_count
    
            if self.fout :
    
                # might want to write a footer
                self.fout.close(); # close present log file
                
                if self.zip_archive_fname :
                    self.add_file_to_zip()
                
            self.file_count += 1;
            
            fname = "%s_%05d_%s_%s%s" % (self.shortname, self.file_count, date_str, time_str, self.extension);
            self.working_filename = fname;
            full_name = os.path.join(self.filepath, fname);
    
            # open next file
            self.fout = open(full_name, "w");
            
            # write the header
            write_header(self.fout)
            if ZIP_FILE_DEBUG :
                print full_name
                print
                print
                print        
            
    
        def append(self, data) :
            if self.fout :
                self.next_test_counter += 10;
                if self.next_test_counter > self.TEST_COUNT :
                    self.next_test_counter = 0;
                    if self.nice_gen.test_minutes() :
                        self.reopen()
                self.fout.write(data)
    
            
                                
                
                
    
    
    
    
    ##########################################################################################
    """ parse a file that has a variable name and type list """
    fname = opt.var_list_filename
    
    
    if len(fname) < 6 or fname[-6:] != '.vlist' :
        print "appending .vlist to file : %s " % fname
        fname += '.vlist';
        
    vlist_fname = os.path.join(VLIST_REL_PATH, fname)
    vlist_obj = class_read_varlist.read_varlist(vlist_fname)
    
    print "OBJECT KEYS:",vlist_obj.__dict__.keys()
    
    
    
    
    struct_str = vlist_obj.make_struct_string(ENDIAN= '>') #Wifi-fast data is always big-endian.
    
    DATA_PACKET_LENGTH = vlist_obj.get_length();
    # struct_str = struct_str[0] + "bb" + struct_str[1:]   # for the text char stream
    print "using struct string :",struct_str, DATA_PACKET_LENGTH
    
    
    varlist = vlist_obj.get_varlist()                        # get a varlist which is a list of dict (use as a named list)
    
    # fmt_list = [fmt for fmt in vlist_obj.print_format_list]
    # FLO = class_fancy_list_output.fancy_list_output(fmt_list);
    FLO = class_fancy_list_output.fancy_list_output()
    
    # create the graphics config file for matlab.
    fname = 'auto_ui_%d.txt' % opt.matlab_port;
    print "Creating matlab info file ",fname
    fout = open(fname,'w');
    for v in varlist  :
        # print v.keys()
    
        str = "%s, %s, %s" % (v['varname'], v['offset'], v['scale'])
        
    
        # print str
        fout.write(str + '\n');
    
        FLO.append(v['format'], v['scale'], v['offset'], v['varname']);
    
        print "%4s  %10.5g  %5.5g   %-30s" % (v['format'], v['scale'], v['offset'], v['varname'])
               
    fout.close()
    
    FLO.append("%d", label="milliseconds");
    FLO.append("%d", label="packet_number");
    FLO.append("%d", label="text_int");
    
    print "FLO format string",FLO.print_format_str 
    
    # sys.exit(0);
    
    # print len(fmt_list), len(varlist),fmt_list
    # sys.exit()
    
    def make_log_file_header_string() :
        name_list = [v['varname'] for v in varlist]        # strip out the variable names from the list
    
        name_list.append('milliseconds')   # see fmt_list.append
        name_list.append('packet_number')
        name_list.append('text_int_value')    
        header_field_count = len(name_list)
    
        date_field = time.strftime('Date_%y%m%d_%H%M%S')
        name_list.append(date_field);
        date_field_number = len(name_list)
        
        log_file_header_string  = '\n'.join(name_list)
        return log_file_header_string, header_field_count, date_field_number
    
    
    
    # log_file_header_string  = ',\t'.join(name_list)
    # log_file_header_string  = '\n'.join(name_list)
    log_file_header_string, header_field_count,date_field_number = make_log_file_header_string()
    # print "LOG FILE HEADER STRING :",log_file_header_string
    
    format_list = [v['format'] for v in varlist]        # strip out the variable names from the list
    format_list.append('%d')
    
    
    
        
        
    
    ##########################################################################################
    """ make file names used for logging"""
    TBL = string.maketrans(".","_");
    
    ipa = logging_file_robot_string.translate(TBL);
    
    TBL = string.maketrans("()-.","____");
    
    time_str = time.strftime("%y%m%d_%H%M%S");
    
    if opt.wifilog_filename == None :
        print
        print
        print "You need to provide a filename for logging.  Example '-f h22_001_walkabout' "
        print "If you don't wish to log the session, then use '-f xx'"
        print
        sys.exit();
    elif opt.wifilog_filename[0:2] == "XX" or opt.wifilog_filename[0:2] == "xx" :
        ENABLE_LOGGING = False;
    else :
        ENABLE_LOGGING = True;
        
    if (opt.wifilog_filename) :
        user_fname = opt.wifilog_filename.translate(TBL);
    else :
        user_fname = opt.wifilog_filename.translate(TBL);
        fname= "%s_IP%s" % ("wifilog", ipa)
    
    
    
    
    fname=  os.path.join(WIFILOG_REL_PATH,"%s_%s.bin6" % (user_fname, time_str))
    bin6_fname = fname;
    realtime_fname= os.path.join(WIFILOG_REL_PATH, "%s_%s.txt.bz2" % (user_fname, time_str))
    new_bzip2_fname= os.path.join(WIFILOG_REL_PATH, user_fname);
    unpack_fname = "%s_%s.txt" % (user_fname, time_str)  # the old way
    unpack_fname = user_fname
    # mat_loader_fname = os.path.join(WIFILOG_REL_PATH, "wifiload_%s_%s.m" % (user_fname, time_str))
    mat_loader_fname = os.path.join(WIFILOG_REL_PATH, "wifiload_%s.m" % (user_fname))
    mat_loader_fname_3 = os.path.join(WIFILOG_REL_PATH, "wifiload.m")
    mat_output_fname = "%s_%s.mat" % (user_fname, time_str)
        
    
    if ENABLE_LOGGING : 
        # gen_matlab.gen_matlab_2(mat_loader_fname, unpack_fname, mat_output_fname, header_field_count, date_field_number)
        gen_matlab.gen_matlab_3(mat_loader_fname_3, unpack_fname, mat_output_fname, header_field_count, date_field_number)
    
    
    ##########################################################################################
    """ open a realtime converted file log"""
    
    
    if ENABLE_LOGGING :
        # log_bz2 = CSSL.logger(new_bzip2_fname, ".txt.bz", write_header, None, True);
        # pfn = CSSL.period_filename_generator("", log_bz2.reopen);
        # nice_gen = CSSL.nice_interval_generator(pfn.open_next_file, 6)
        # log_bin6 = CSSL.logger("tmp/ltf_log_bin", ".bin6");
        # pfn = CSSL.period_filename_generator("", log_bin6, log_bz2);
        if opt.wifilog_filename : 
            SPL = swifi_periodic_logger(WIFILOG_REL_PATH, opt.wifilog_filename+".txt", FILES_PER_HOUR, opt.wifilog_filename, mat_loader_fname_3);
            # print SPL, bin6_fname
            # SPL.set_capture_file_at_exit(bin6_fname);
            SPL.set_capture_file_at_exit(None);
        else:
            ENABLE_LOGGING = False
        
        
    
        
            
    
    
        
    ##########################################################################################
    
    if opt.gui_version  == wifi_options.GUI_MATLAB :
        
        print "matlab plotting sent to ", opt.matlab_host, opt.matlab_port
        udp_mat = udp_matlab.SendToGraph(host=[opt.matlab_host], port=opt.matlab_port);
        udp_mat.SetStructUnpackStr(struct_str)    
                
    
    
    
        
    
    
    ##########################################################################################
    if wifi_options.PAcket_version == wifi_options.PACKET_V2 :
        udp_mat.enable_pcnt_diff = False
        if opt.com_port :
            wifi = class_read_v2_packet.ReadFromAnkle(0, 0 , DATA_PACKET_LENGTH, "error_log.txt", OPEN = 0, USE_SERIAL=1);
            wifi.sp = serial.Serial(OS_COM_PORT, opt.baudrate, timeout=0.2, rtscts=0)
    
            print opt.baudrate
        else :
            wifi = class_read_v2_packet.ReadFromAnkle(LANTRON_HOST , opt.lantron_port, DATA_PACKET_LENGTH, "error_log.txt");
            print DATA_PACKET_LENGTH
    
    
    
    elif wifi_options.PAcket_version == wifi_options.PACKET_V3 :
        wifi_options.PACKET_ENDIAN = '<'   
        udp_mat.enable_pcnt_diff = False
        if opt.com_port :
            wifi = read_v3_packet.ReadFromAnkle(0, 0 , DATA_PACKET_LENGTH, "error_log.txt", OPEN = 0, USE_SERIAL=1);
            wifi.sp = serial.Serial(OS_COM_PORT, opt.baudrate, timeout=0.2, rtscts=0)
    
            print opt.baudrate
        else :
            wifi = read_v3_packet.ReadFromAnkle(LANTRON_HOST , opt.lantron_port, DATA_PACKET_LENGTH, "error_log.txt");
            print DATA_PACKET_LENGTH
    
    
    
    wifi.SendList(ESC_STR_RESET_IDLE);
    time.sleep(0.1);
    
    
    if ENABLE_LOGGING : 
        # enable / disable binary logging
        print "Log to : ", fname
        wifi.OpenBinLog(fname);
        pass
    else :
        print "Not logging";
        print
    
    
    
    
    ##########################################################################################
    quiet = 0;
    pause = 0;
    
    
    
    
    wifi.Timeout(1.0);
    
    if (opt.swifi_flush) :
        print
        print "Sending WIP4a SRAM flush"
        print
        wifi.SendList(ESC_STR_FLUSH);
    
    
    
    last_time = time.time();
    packet_count = 0;
    
    if opt.gui_version == wifi_options.GUI_PYTHON :
        plot.step2();
    
    ENABLE_GUI_UPDATE = True;
    
    print
    print "WIFI_FAST.PY"
    print 
    
    application_starttime = time.time()
    if opt.application_timeout_secs != None:
        print "Application will stop automatically in ", opt.application_timeout_secs, "seconds."
      
    while (opt.application_timeout_secs == None) or (time.time() - application_starttime < opt.application_timeout_secs):
    
        now_time = time.time();
        """
        now_time = time.time();
        if (now_time - last_time > 3) :
            # print wifi.sp, wifi.timeout_cnt;
            if (wifi.timeout_cnt == 0) :
                wifi.SendList(ESC_STR_RESET_IDLE);
            last_time = now_time;
        """
        
        if (pause == 0) :
            pk,text_int_value = wifi.ProcessBuff();
            if (pk) :
                #print "LEN",len(pk), type(pk)
            
                packet_count = packet_count + 1
    
                data_list = vlist_obj.vlist_unpack(pk, ENDIAN= wifi_options.PACKET_ENDIAN)   # transform the binary data packet into a python list  (or sequence???)
                #print data_list
                
                tm = int(round(time.clock() * 1000))
                output_list = data_list + (tm, packet_count, text_int_value)     # append to the list the time in mS, the packet count, and a 16 bit INT that represents 0 to 2 ASCII chars
                
                scaled_data = FLO.scale_data(output_list)
                output_string = FLO.make_string_from_data(scaled_data) # make a nicer output string. new class with scale,offset 3-9-2010
                
                if ENABLE_LOGGING :
                    SPL.append(output_string + '\n');
    
    
                if (ENABLE_GUI_UPDATE == True) :
                    if opt.gui_version == wifi_options.GUI_PYTHON :
                        plot.AddPacket_v2(data_list)
                    
                    elif opt.gui_version == wifi_options.GUI_MATLAB : 
                        udp_mat.AddPacket_v03(data_list);
                        pass
    
    
        if USE_KEYBOARD :
            (raw_ch, esc, fun, ch) = keybd_obj.getch_packed();
            """
            print "got ", raw_ch, esc, fun, ch,
            if (raw_ch) :
                print ord(raw_ch);
                print "E:%d F:%d " % (esc, fun)
            else :
                print
            """
            if (raw_ch == '\x1b') :
                print "!!!!!!!!!!!  RAW_CH == ESC.  CALLING BREAK"
                break;
    
        else :
            (raw_ch, esc, fun, ch) = (0,0,0,0)
    
        # wifi.send(chr(0x55) * 10)
    
    
    
        KEY_CAP = False;
        if (fun == 2) :
            KEY_CAP = True;
            # print "ENABLE", ENABLE_GUI_UPDATE
            if ENABLE_GUI_UPDATE  :
                ENABLE_GUI_UPDATE = False
            else :
                ENABLE_GUI_UPDATE = True
                    
        if (fun) :
            KEY_CAP = True;
            
            if (fun == 9) : # reset swifi
                wifi.SendList(ESC_STR_RESET_SWIFI);
                print
                print "Sending RESET SWIFI"
                print
    
            if (fun == 6) : # reset idle timer
                print
                print "Sending RESET IDLE"
                print
                wifi.SendList(ESC_STR_RESET_IDLE);
            if (fun == 50) : # ALT F5 flush WIP4
                print
                print "Sending WIP4a SRAM flush"
                print
                wifi.SendList(ESC_STR_FLUSH);
            if (fun == 4) : # unlock
                print
                print "Sending UNLOCK"
                print
                wifi.SendList(ESC_STR_UNLOCK);            
    
            if (fun == 46) : # start fup PWM   ALT F1
                print
                print "Sending start fup pwm"
                print
                wifi.SendList(ESC_STR_START_PWM);            
            if (fun == 47) : # stop fup PWM   ALT F2
                print
                print "Sending stop fup pwm"
                print
                wifi.SendList(ESC_STR_STOP_PWM);            
    
    
            if (fun == 54) : # ALT F9 reconnect TCP
                print
                print "TCP DISCONNECT"
                wifi.Disconnect();
                time.sleep(0.5);
                print
                wifi.Connect();
                print
    
    
        if (ch and KEY_CAP == False) :
            wifi.SendChar(ch)
            wifi.AppendUserChar(ch);
            
            
                
    
    wifi.close();
    # ext_ctrl.close();
    udp_mat.close();
    
    if ENABLE_LOGGING :
        fout.close();
    
    try :
        # print "Adding .bin6 file to archive"
        # SPL.add_file_to_zip(bin6_fname);
        SPL.set_capture_file_at_exit(None);
        SPL.close();
    except :
        pass
    

