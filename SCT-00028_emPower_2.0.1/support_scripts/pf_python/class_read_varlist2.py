
import sys, struct

class read_varlist :

    def __init__ (self, fname, ENDIAN=1) :

        self.varlist = [];
        self.make_typedict()
        self.struct_string = ''
        self.data_label_string = ''
        self.data_label_list = []
        self.print_format_string = ''
        self.print_format_list = []
        self.data_units_list = []
        self.data_units_string = ''
        self.data_length = 0;
        self.endian = ENDIAN
        self.readfile(fname)



    def readfile(self, fname) :
        try :
            fin = open(fname, "r");
        except :
            print "varlist : unable to open ", fname
            sys.exit(-1)
            return 

        self.varlist = [];
        for ln in fin :
            ln = ln.strip();
            
            if not ln or ln[0] == '#' :
                continue;

            ndx = ln.find('#');
            if ndx >= 0 :
                list = ln[0:ndx].split();
            else :
                list = ln.split();                

            len_list = len(list)
            # print list
            if len_list >= 2 :
                d = {} ;
                vtype = list[0].lower();
                d['type'] = vtype;
                self.data_units_list.append(vtype)
                d['varname'] =  list[1]
                self.data_label_list.append(list[1])
                offset = 0;
                scale = 1.0;

                if len_list >= 3 :
                    scale = eval(list[2]);
                if len_list >= 4 :
                    offset = eval(list[3]);


                if self.types_d.has_key(vtype) :
                    fmtlist = self.types_d[vtype]
                    d['struct'] = fmtlist[0];  # struct pack/unpack conversion char
                    d['format'] = fmtlist[1];  # printf conversion char
                    self.print_format_list.append(fmtlist[1])
                    d['offset'] = offset 
                    d['scale'] = scale
                    self.varlist.append(d);
                else :
                    print "unknown type",list
            else :
                print "unknown format",list
        
        self.data_label_string = ", ".join(map(str, self.data_label_list))
        self.data_units_string = ", ".join(map(str, self.data_units_list))
        self.print_format_string = " ".join(self.print_format_list)
        self.struct_string = self.make_struct_string(self.endian)
        self.data_length = struct.calcsize(self.struct_string)
        fin.close()



    def get_data_label_string(self) :
        return self.data_label_string



    def get_data_unit_string(self) :
        return self.data_units_string


    def get_print_format_string(self) :
        return self.print_format_string

            
    def get_varlist(self) :
        return self.varlist;

    def get_varlist_map(self) :
        vlist_map = dict()
        for i, v in zip(range(0,1000), self.varlist) :
            name = v['varname']
            vlist_map[name] = i
        return vlist_map

    def make_struct_string(self, ENDIAN=1) :
        """ build a struct string to unpack data"""
        d = {} ;
        d['struct'] = '~'
        self.varlist.append(d);
        
        last_str_char = ''
        count = 1;
        if (ENDIAN == 1) :
            struct_str = '>'
        else :
            struct_str = '<'
            
        for v in self.varlist :
            str_char = v['struct'];
            if (str_char == last_str_char) :
                # print str_char, count
                count = count + 1
            else :
                # print "add", last_str_char, count
                if (count > 1) :
                    str = "%d%c" % (count, last_str_char)
                    count = 1;                    
                else :
                    str = last_str_char;
                struct_str = struct_str + str
                
            last_str_char = str_char

        self.data_length = struct.calcsize(struct_str);
        self.struct_string = struct_str;
        # print struct_str

        self.varlist = self.varlist[:-1]   # remove extra item added
        return struct_str

    def get_length(self) :
        return self.data_length;


                
            
        

        
    def make_typedict(self) :
        self.types_d = {};
        # self.types_d['float'] = ["f", "%f"]
        # self.types_d['float32_t'] = ["f", "%f"]
        self.types_d['float'] = ["f", "%g"]
        self.types_d['float32_t'] = ["f", "%g"]
        
        self.types_d['int'] = ["h", "%d"]
        self.types_d['int16_t'] = ["h", "%d"]
        self.types_d['int16'] = ["h", "%d"]    
        self.types_d['unsigned int'] = ["H", "%d"]
        self.types_d['uint16_t'] = ["H", "%d"]
        self.types_d['uint16'] = ["H", "%d"]
        self.types_d['long'] = ["l", "%d"]
        self.types_d['int32_t'] = ["l", "%d"]
        self.types_d['int32'] = ["l", "%d"]    
        self.types_d['unsigned long'] = ["L", "%d"]
        self.types_d['uint32_t'] = ["L", "%d"]
        self.types_d['uint32'] = ["L", "%d"]    
        self.types_d['uint8_t'] = ["B", "%d"]
        self.types_d['uint8'] = ["B", "%d"]
        self.types_d['byte'] = ["B", "%d"]        
        self.types_d['int8_t'] = ["b", "%d"]
        self.types_d['int8'] = ["b", "%d"]
        self.types_d['char'] = ["B", "%c"]
        


        

if __name__ == "__main__" :
    obj = read_varlist("swifi.vlist");
    print obj.make_struct_string()    
    vl = obj.get_varlist()
    if (1) :
        for v in vl :
            print v
