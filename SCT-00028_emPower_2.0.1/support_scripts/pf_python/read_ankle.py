import sys, traceback

sys.path.insert(0,'support')

# import class_read_v2_packet as dataport

import class_wifi as dataport
import class_process_v2_packet as pk_process
import class_read_varlist2


import keyboard;

import time
import struct
import serial_port_helper

import threading


LANTRON_PORT = 10001;
class class_read_ankle () :
    
    def __init__(self, UDP_DEST=None, UDP_PORT=None, BIN_LOG_FNAME=None,
                 USE_CURRENT_TIME = False) :

        # self.live_parsed_data_output_helpper(vlist_fname)

        # Default timestamp struct string
        self.useCurrentTime = USE_CURRENT_TIME
        self.timestamp_struct_string = ">q"
        self.time_of_last_good_packet = 0
        self.packet_count = 0;
        self.first_timestamp = None

        self.start_log();
        self.stop_log();        

        self.wifi = dataport.class_wifi()

        if (BIN_LOG_FNAME) :
            self.wifi.start_bin_log(BIN_LOG_FNAME)

        # self.text_output = pk_process.text_output();
        try :
            #self.text_output = pk_process.udp_text_output(UDP_DEST, UDP_PORT);
            self.text_output = None

        
            # self.text_output.connect_udp('127.0.0.1', 9990);    # send messages to port 9990
            # try to send some data to see if it breaks
            #for i in range(0,5) :
            #    self.text_output.writeln("UDP %d\n" % i)
            #    time.sleep(0.01)
                
        except :
            traceback.print_exc();
            
            print
            print "UDP TEXT OUTPUT DESTINATION REFUSES DATA"
            print            
            self.text_output = None
            # print sys.exc_info()

            
        self.pp = pk_process.process_v2_packet(data_packet_len=40, data_source=self.wifi, text_logger=self.text_output, error_logger=None) ;
        
        self.ENABLE_GUI_UPDATE = True;
        
        
        
        self.kb = keyboard.keyboard();
        self.TERMINATE = None;
        
        # Binary escape sequences for WIP4a and SC
        self.FLUSH_CMD = 200;
        self.UNLOCK_CMD = 201;
        self.LOCK_CMD = 202;
        self.NO_MIT_COMM = 203;
        self.RESET_IDLE = 204;
        self.DISABLE_SELF_IDLE_RESET = 220;
        self.ENABLE_SELF_IDLE_RESET = 221;
        self.RESET_SWIFI = 222;

        self.ESC_STR_UNLOCK =  [0xee, 211, 223, 227, 229, 197, 199, self.UNLOCK_CMD];
        self.ESC_STR_FLUSH =  [0xee, 211, 223, 227, 229, 197, 199, self.FLUSH_CMD];
        self.ESC_STR_RESET_IDLE =  [0xee, 211, 223, 227, 229, 197, 199, self.RESET_IDLE];
        self.ESC_STR_DISABLE_SELF_IDLE_RESET =  [0xee, 211, 223, 227, 229, 197, 199, self.DISABLE_SELF_IDLE_RESET];
        self.ESC_STR_ENABLE_SELF_IDLE_RESET =  [0xee, 211, 223, 227, 229, 197, 199, self.ENABLE_SELF_IDLE_RESET];
        self.ESC_STR_RESET_SWIFI =  [0xee, 211, 223, 227, 229, 197, 199, self.RESET_SWIFI];

        self.set_user_callback_function(None)


    def __del__(self) :
        print "read_ankle.__del__"
        self.close()

    def stop(self) :
        self.first_timestamp = None

    """
    def start_udp_matlab(self, host, port):
        self.udp_mat = udp_matlab.SendToGraph(host, port);
        self.udp_mat.SetStructUnpackStr(self.struct_str)
    """

    def start_log (self) :
        self.packet_log = []   # reset the log
        self.enable_packet_log = True;


    
    def stop_log (self) :
        self.enable_packet_log = False
        d = dict(packet_list = self.packet_log, packet_count = self.packet_count)
        return d

    




    def live_parsed_data_output_helpper(self, vlist_fname) :
        """ Read the variable list file the specifies data format of the WIFI packet """
        """ parse a file that has a variable name and type list """
        
        self.vlist_obj = class_read_varlist2.read_varlist(vlist_fname)
        self.struct_str = self.vlist_obj.make_struct_string()
        self.timestamp_struct_string = self.struct_str[:1] + "q"
        self.DATA_PACKET_LENGTH = self.vlist_obj.get_length();
        #print "using struct string :",self.struct_str, self.DATA_PACKET_LENGTH

        """
        #####################

        # the remainder is for creating a text log file

        vl = self.vlist_obj.get_varlist()    # get a varlist which is a list of dict (use as a named list)
        
        name_list = [v['varname'] for v in vl]        # strip out the variable names from the list

        self.variable_name_list = [n for n in name_list];  # make a copy
        
        date_field = time.strftime('Date_%y%m%d_%H%M%S')
        
        name_list.append('milliseconds')
        name_list.append('packet_number')

        self.header_field_count = len(name_list)
        name_list.append(date_field);
        date_field_number = len(name_list)

        # log_file_header_string  = ',\t'.join(name_list)
        self.log_file_header_string  = '\n'.join(name_list)
        
        # print log_file_header_string

        self.format_list = [v['format'] for v in vl]        # strip out the variable names from the list
        format_list.append('%d')

        """


    def connect_serial(self, port, baudrate) :
        self.wifi.connect_serial(port, baudrate) 


    def connect_tcp(self, HOST, PORT=LANTRON_PORT) :
        """ make the connection to the LANTRON """        
        self.wifi.connect_tcp(HOST, PORT);
        




    
    
    def SendList(self, list) :
        self.wifi.SendList(list);

    def send_flush(self,quiet=False) :
        if quiet == False :
            print
            print "Sending WIP4a SRAM flush"
            print
        self.SendList(self.ESC_STR_FLUSH);

    

    def send_unlock(self) :
        print
        print "Sending UNLOCK"
        print
        self.SendList(self.ESC_STR_UNLOCK);
        
    def send_reset_swifi(self) :
        print
        print "Sending RESET SWIFI"
        print
        self.SendList(self.ESC_STR_RESET_SWIFI);
        
    def send_reset_idle_timer(self) :
        print
        print "Sending RESET IDLE"
        print
        self.SendList(self.ESC_STR_RESET_IDLE);                        




                        
    def keyboard_ui(self) :
        (raw_ch, esc, fun, ch) = self.kb.getch_packed();
        
        if (raw_ch == '\x1b') :
                self.TERMINATE = True;
            

        if (raw_ch) :
            # print "E:%d F:%d " % (esc, fun)
            pass 

        KEY_CAP = False;
        if (fun == 2 or raw_ch == ' ') :
            KEY_CAP = True;
            # print "ENABLE", ENABLE_GUI_UPDATE

            if self.ENABLE_GUI_UPDATE  :
                self.ENABLE_GUI_UPDATE = False
            else :
                self.ENABLE_GUI_UPDATE = True
                    
        if (fun) :
            KEY_CAP = True;
            if (fun == 9) : # reset swifi
                        self.send_reset_swifi()

            if (fun == 6) : # reset idle timer
                        self.send_reset_idle_timer()

            if (fun == 50) : # ALT F5 flush WIP4
                        self.send_flush()

            if (fun == 4) : # unlock
                self.send_unlock()

            if (fun == 54) : # ALT F9 reconnect TCP
                print
                print "TCP DISCONNECT"
                self.wifi.Disconnect();
                time.sleep(0.5);
                print
                self.wifi.Connect();
                print
            
            """
            if (fun == 1) :
                fname = "wifilog/Will_Bordon_track_log_%s.bin" % time.strftime("%Y_%m_%d_%H.%M.%S");
                wifi.OpenBinLog(fname);
            """

            if (ch and KEY_CAP == False) :
                self.wifi.SendChar(ch)
                self.text_output.AppendUserChar(ch);
                
        return self.TERMINATE
        
    def close(self) :
        self.wifi.close();


    
            

    def main_iteration(self) :
        if (self.first_timestamp == None) :
            if (self.useCurrentTime) :
                self.first_timestamp = 0
            else :
                self.first_timestamp = time.time()

        pk = self.pp.ReadAndProcessBuff();
        if (pk) :
            raw_tm = time.clock()
            self.time_of_last_good_packet = time.time()
            time_delta = self.time_of_last_good_packet - self.first_timestamp

            self.packet_count += 1
            #self.time_of_last_good_packet = self.packet_count
            
            #data_list = struct.unpack(self.struct_str, pk)
            #pk_dict = dict(rx_time = raw_tm, count = self.packet_count, data = data_list, raw_data=pk)
            
            #pk_dict = dict(rx_time = raw_tm, count = self.packet_count, raw_data=pk)
            # Build the buffer to pass back
            pk_dict = struct.pack(self.timestamp_struct_string, int(time_delta * 1000000.0)) + pk
            #pk_dict = pk

            if self.enable_packet_log :   # this is the in memory log
                # self.packet_log.append(data_list);
                self.packet_log.append(pk);     
                if len(self.packet_log) > 10000 :   # keep the list at a reasonable length
                    del self.packet_log[0:1000]
                    

            if self.user_callback :
                self.user_callback(pk_dict)
                

            # output_list = data_list + (now_time, self.packet_count)
            #tm = int(raw_tm * 1000)
            #output_list = data_list + (tm, self.packet_count)
            #pk_dict = output_list
            #print str(data_list)
            #output_string = str(output_list)[1:-1]
            #print output_string

            """
            if (self.ENABLE_GUI_UPDATE == True) :
                # print "add packet", len(pk)
                if (self.udp_mat) :
                    self.udp_mat.AddPacket_v02(pk);
                if (self.py_gui_plot) :
                    py_gui_plot.AddPacket_v2(data_list)
            """
        else : 
            pk_dict = None;                

        return pk_dict
    
    def set_user_callback_function(self, func=None) :
        self.user_callback = func;
    





from threading import Thread
class swifi_thread(Thread) :
    def __init__(self, swifi) :
        Thread.__init__(self)
        self.swifi = swifi;
        self.terminate = False;
        
        self.time_last_cycle = 0

    def __del__ (self) :
        print "swifi_thread.__del__"
        self.terminate = True
    
        
    def run(self) :
        while 1 :
            self.time_last_cycle = time.time()
            pk_dict = self.swifi.main_iteration();
            if (pk_dict) :
                print "[",pk_dict,"]"
            terminate = self.swifi.keyboard_ui();
            
            if (terminate) :
                break;
            if self.terminate :
                break
        # self.read_ankle.wifi.Disconnect()
        self.terminate = True


    def is_terminated(self) :
        return self.terminate
        
    def stop(self) :
        self.terminate = True;
        



        

if __name__ == "__main__" :
    UDP_HOST = '127.0.0.1';
    UDP_PORT = 9999;

    LANTRON_HOST = '192.168.192.3';
    LANTRON_PORT = 10001;

    SERIAL_PORT = 1
    SERIAL_BAUD_RATE = 1250000
    # SERIAL_BAUD_RATE = 115200

    sph = serial_port_helper.serial_port_helper()
    OS_COM_PORT = sph.get_os_serial_port_name(SERIAL_PORT);
    # logging_file_robot_string = "COM:%s" % OS_COM_PORT
    print "USE SERIAL COM:", OS_COM_PORT


    # vlist_fname = 'pvf.vlist'
    #vlist_fname = '../vlist/lifefix.vlist'
    #vlist_fname = '../vlist/dataport8.vlist'
    vlist_fname = '../vlist/calibrate.vlist'
    # vlist_fname = 'imu2.vlist'

    
    read_ankle = class_read_ankle(vlist_fname);

    # print read_ankle.variable_name_list

    read_ankle.connect_serial(OS_COM_PORT, SERIAL_BAUD_RATE)
    # read_ankle.start_udp_matlab(UDP_HOST, UDP_PORT);
    read_ankle.live_parsed_data_output_helpper(vlist_fname)

    swifi_d = swifi_thread(read_ankle);
    swifi_d.start();
    while swifi_d.isAlive() :
        print "*", 
        swifi_d.join(1.0)
        
    read_ankle.wifi.Disconnect()

    import subprocess
    subprocess.call('tset', shell=True);        
