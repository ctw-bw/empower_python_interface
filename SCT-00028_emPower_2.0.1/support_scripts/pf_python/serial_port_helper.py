#!/usr/bin/env python
import os, sys
import glob
import serial

class serial_port_helper() :

    # def __init__(self, *args, **kwargs) :                                                                             
    # self.sp = serial.__init__(self, *args, **kwargs);                                                                 


    def __init__(self, port=None) :
        self.LINUX_OS_NAME = "posix"
        self.WIN_OS_NAME = "nt"
        self.debug = 0;


        self.OS_WINDOWS = (os.name == self.WIN_OS_NAME)
        self.OS_LINUX = (os.name == self.LINUX_OS_NAME)

        if (self.OS_LINUX) :
            self.OS_DEVICE_PATH = "/dev"
            self.FILE_SPEC_LIST = ['ttyUSB*','ttySIO*'];
            self.SERIAL_DEV_SYMLINK_DIR = "/dev/serial/by-id"

        port_name = self.get_os_serial_port_name(port);


    def get_os_serial_port_name(self, port=None) :
        port_name = None;

        if (port != None) :
            if self.OS_LINUX :
                if type(port) == type(1) :
                    port_name = "/dev/ttyUSB%d" % port;
                else  :
                    port_name = port
            if self.OS_WINDOWS :
                port_name = port-1;

        return port_name;


    def winnt_find_serial_ports(self,start=1, stop=39) :

        port_list = [];

        if (os.name != self.WIN_OS_NAME) :
            print "Invalid function for this OS", os.name
        else :

            for com_port in range(start, stop) :
                try :
                    sp = serial.Serial(com_port-1, timeout=0.001)
                    d = dict( name="COM%d:"%com_port, device=com_port)
                    port_list.append(d)
                    sp.close();
                except :
                    # print sys.exc_info();
                    # print com_port, "failed"
                    pass

        return port_list;


    def linux_find_serial_ports(self, path=None) :
        # look for serial ports in /dev/ttyUSB* ttySIO*                                                                 
        # also there may be a /dev/serial/by_id listing                                                                 

        dev_list = [];
        dev_list_2 = [];
        if os.name != self.LINUX_OS_NAME :
            print "Invalid function for this OS", os.name
        else :
            for file_spec in self.FILE_SPEC_LIST :
                full_path = os.path.join(self.OS_DEVICE_PATH, file_spec);
                devices = glob.glob(full_path);
                dev_list.append(devices);

            if (self.debug >= 1) :
                print "Method 1 found devices : ", dev_list

            if (self.debug>=3) :
                print self.SERIAL_DEV_SYMLINK_DIR
            full_path = os.path.join(self.SERIAL_DEV_SYMLINK_DIR,  "*");

            if (self.debug>=3) :
                print "FP", full_path
            device_names = glob.glob(full_path);
            if (self.debug>=3) :
                print "device names", device_names

            for dev_name in device_names :
                sym_link_target = os.readlink(dev_name);
                device = os.path.split(sym_link_target);
                if (self.debug>=3) :
                    print "DEVICE", device[-1]
                full_dev_path = os.path.join(self.OS_DEVICE_PATH, device[-1]);
                d = dict(name=dev_name, device=full_dev_path)
                dev_list_2.append(d);
                if (self.debug>=2) :
                    print d

                # print "%-20s  :: %s " % (device, dev_name)                                                            

                # path_to_dev = os.path.join(os.path.dirname(full_path), device)                                        


            if (self.debug >= 1) :
                print "Method 2 found devices : "
                print dev_list_2

            if (len(dev_list_2)) :
                return_list = dev_list_2;
            else :
                return_list = dev_list;

            return return_list;





    def get_port_list(self) :
        port_list = None;
        if (self.OS_LINUX == True) :
            port_list = self.linux_find_serial_ports();

        if (self.OS_WINDOWS == True) :
            port_list = self.winnt_find_serial_ports(start=1, stop=39);

        return port_list


    def output_port_list(self) :
        lst = self.get_port_list();
        print
        for l in lst :
            print l
        print
        return lst
        

    def get_port_to_use(self) :
        s = self.get_port_list()
        l = []
        for i in s :
            # Filter for PSER USBs THIS IS HEAVILY TTY-USB DEVICE DEPENDENT !!!
            s2 = i['device']
            if s2.find('usb-FTDI_Dual_RS232-HS') :
                l += i['device'][-1]
        l.sort()
        if len(l) :
            return l[0]
        else :
            return None


if __name__ == "__main__" :
    s = serial_port_helper();
    # s.linux_find_serial_ports();                                                                                      
    lst = s.get_port_list();
    for l in lst :
        print l
    
    # print s.get_os_serial_port_name("/dev/tty3")








