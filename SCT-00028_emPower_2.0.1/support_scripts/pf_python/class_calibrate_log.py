#!/usr/bin/env python

class class_calibrate_log :
    """
    This class handles logging of calibration data to a log file .
    """
    def __init__(self):
        self.filename = ''
        self.out = None
        self.cycle_count = 0
        self.cycle_count_fmt = "%u "
        self.cycle_count_string = "cycleCount"
        pass

    def open(self, filename) :
        self.filename = filename
        self.out = open(filename, 'w')
        self.cycle_count = 0

    def close(self) :
        if (self.out != None) :
            self.out.close()
            self.out = None
            self.filename = ''

    def format_count(self, str) :
        count = 0
        for i in str :
            if (i[0:1] == '%') :
                count += 1
        return count

    def list_formatter(self, header_list) :
        str = ""
        for item in header_list :
            if (self.header_count > 1) :
                self.out.write(", ")
            str += "(%d)%s" % (self.header_count, item.replace(" ", "_"))
            self.header_count += 1
        return str

    def write_header(self, data_source) :
        """
        Write the header to the log file.
        """
        self.header_count = 1
        self.out.write("#")
        self.out.write(self.list_formatter(self.cycle_count_string.split()))
        firstItem = True
        for item in data_source :
            if (firstItem) :
                self.out.write(self.list_formatter(item.get_record_data_labels_list()))
                firstItem = False
            else :
                #self.out.write(", ")
                self.out.write(self.list_formatter(item.get_record_data_labels_list()))
        self.out.write("\n")
        firstItem = True
        # Write out the units
        self.out.write("#")
        for item in data_source :
            if (firstItem) :
                self.out.write(item.get_record_data_units_string())
                firstItem = False
            else :
                #self.out.write(", ")
                self.out.write(item.get_record_data_units_string())
        self.out.write("\n")
        firstItem = True
        # Write out the units
        self.out.write("#")
        for item in data_source :
            if (firstItem) :
                self.out.write(item.get_record_print_format_string())
                firstItem = False
            else :
                #self.out.write(" ")
                self.out.write(item.get_record_print_format_string())
        self.out.write("\n")

    def write_data(self, data_source, num_records_requested) :
        """
        Write all the items from each data source to a log file.
        """
        item_number = 0
        self.out.write(self.cycle_count_fmt % self.cycle_count)
        for item in data_source :
            self.write_item(item_number, item, num_records_requested)
            item_number += 1
        self.out.write("\n")

    def write_item(self, item_number, item, num_records_requested) :
        """
        Write the values of an item to the log file.
        """
        (n, header_list, record_list) = item.read_as_list(num_records_requested)
        # FIXME: add aggregation
        if (item_number == 0) :
            # Handle the Copley
            if (n) :
                # Write the values to the log file
                for rec in record_list :
                    self.out.write(item.get_record_print_format_string() % rec)
                    self.out.write(" ")
            else :
                format_count = self.format_count(item.get_record_print_format_string())
                for i in range(format_count) :
                    self.out.write("NaN ")
        elif (item_number == 1) :
            # Handle the loadcell
            if (n) :
                # Write the values to the log file
                for rec in record_list :
                    self.out.write(item.get_record_print_format_string() % rec)
                    self.out.write(" ")
            else :
                format_count = self.format_count(item.get_record_print_format_string())
                for i in range(format_count) :
                    self.out.write("NaN ")
        elif (item_number == 2) :
            # Handle the ankle
            if (n) :
                # Write the values to the log file
                for rec in record_list :
                    self.out.write(item.get_record_print_format_string() % rec)
                    self.out.write(" ")
            else :
                format_count = self.format_count(item.get_record_print_format_string())
                for i in range(format_count) :
                    self.out.write("NaN ")

    def get_cycle_count(self) :
        return self.cycle_count

    def set_cycle_count(self, cycle_count) :
        self.cycle_count = cycle_count
