

class UDP_to_Matlab :

    def __init__ (self, ip='127.0.0.1', port=9999, num_samples=20) :
        self.UDP_HOST = ip;
        self.UDP_PORT = port;
        
        import socket
        self.s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM);
        self.s.connect((self.UDP_HOST, self.UDP_PORT));

        self.SAMPLES_PER_PACKET = num_samples;

        self.data = ""
        
        self.count = 0;
        self.d_list = range(0, self.SAMPLES_PER_PACKET);

    def __del__(self) :
        self.s.close()

    def update(self, data) :
        
        f_str_lst = [struct.pack(">f", f) for f in data];

        self.data += ''.join(f_str_lst);

        self.count += 1;

        if (self.count == self.SAMPLES_PER_PACKET) :
            self.s.send(self.data);
            self.count = 0
            self.data = ""


        


            

import struct
import time
if __name__=="__main__":
    udp = UDP_to_Matlab();


    for i in range(0,10000) :
        d = [i*10+v for v in range(0,8)];
        d.insert(0,">8H");
        str = apply(struct.pack, d);
        # print len(str),
        udp.update(str);
        time.sleep(0.01);

        
    
