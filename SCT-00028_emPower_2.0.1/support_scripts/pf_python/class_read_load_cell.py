import struct, sys
import time

from ReadLoadCell import *

import numpy


# class read_load_cell (ReadLoadCell) :
class read_load_cell :
    def __init__ (self, sample_rate_hz=1000, max_num_records=1024) :
        
        rlc = ReadLoadCell(max_num_records, 1000000 / sample_rate_hz)
        # print "setting sable rate to : ", sample_rate_hz
        # time.sleep(1.0)
        
        self.rlc = rlc;
        self.record_length = rlc.get_record_length()
        self.header_length = rlc.get_header_length() 
        self.buffer_size = self.header_length + self.record_length * max_num_records;
        self.so_buffer = chr(1) * (self.buffer_size);

        self.header_data_labels = rlc.get_header_data_labels()
        self.header_data_labels_list = rlc.get_header_data_labels().split(', ')
        self.header_unpack_string = rlc.get_header_unpack_string()
        self.header_print_format_string = rlc.get_header_print_format_string()


        self.record_data_labels = rlc.get_record_data_labels()
        self.record_data_labels_list = rlc.get_record_data_labels().split(', ')
        
        self.record_unpack_string = rlc.get_record_unpack_string()
        self.record_print_format_string = rlc.get_record_print_format_string()
        self.record_print_format_list = rlc.get_record_print_format_string().split()
        
        self.record_data_units_string =  rlc.get_record_data_units()
        self.record_data_units_list =  rlc.get_record_data_units().split(', ')
        
        print         self.record_data_labels_list
        print         self.record_data_units_list
        
        self.rlc.start()
        self.NDX_OF_ITEMS_READ = 2
        
        pass

    def __del__ (self) :
        """ stop the read load cell application """
        self.rlc.stop();
        self.rlc.close();
        


    def read(self, num_records_requested) :   # override SO read for now.
        n = self.rlc.read(self.so_buffer, num_records_requested)
        print "items read : ",n
        return n

    def header_to_dict(self) :
        """ turn the last read buffer header into a dictionary """
        d = dict()
        
        header_list = struct.unpack(self.header_unpack_string, self.so_buffer[:self.header_length])
        

        for name, value in zip(self.header_data_labels_list, header_list) :
            d[name] = value

        return d
        
    def get_first_record(self) :
        """ from the last read buffer, turn everything read into a list of records """
        
        n = self.rlc.read(self.so_buffer, 1000);

        # header_list = struct.unpack(self.header_unpack_string, self.so_buffer[:self.header_length])
        # records_to_decode = header_list[self.NDX_OF_ITEMS_READ]

        if n : 
            rec_num = 0
            record_offset = self.header_length + rec_num * self.record_length
            buff = self.so_buffer[record_offset : record_offset + self.record_length]
            record = struct.unpack(self.record_unpack_string, buff)
            # print "dat:", record
        else :
            record = 0;

        return n, record


    def make_record_list(self) :
        

        header_list = struct.unpack(self.header_unpack_string, self.so_buffer[:self.header_length])
        records_to_decode = header_list[self.NDX_OF_ITEMS_READ]

        # print "RTD:",records_to_decode
        
        rec_list = []        
        for rec_num in xrange(0, records_to_decode) :
            record_offset = self.header_length + rec_num * self.record_length
            buff = self.so_buffer[record_offset : record_offset + self.record_length]
            record = struct.unpack(self.record_unpack_string, buff)
            rec_list.append(record)

        return rec_list


    def read_as_list(self, num_records_requested) :
        n = self.rlc.read(self.so_buffer, num_records_requested)
        # print "got ",n
        if (n) :
            rec_list = self.make_record_list()
            return rec_list

    def read_as_numpy(self, num_records_requested) :
        n = self.rlc.read(self.so_buffer, num_records_requested)
        # print "got ",n
        if (n) :
            rec_list = self.make_record_list()
            return numpy.array(rec_list);

        



    def read_obj(self, num_records_requested) :
        """ read and return as a documented object """
        n = self.rlc.read(self.so_buffer, num_records_requested)
        
        d = self.header_to_dict()
        
        d['data_labels'] = self.record_data_labels_list 
        d['record_print_format'] = self.record_print_format_list 
        d['record_units'] = self.record_data_units_list

        
        d['record_list'] = self.make_record_list()

        return d

    def record_to_dict(self, record) :
        d = dict()
        for name, value in zip(self.record_data_labels_list,  record):
            d[name] = value
        return d

    def record_to_plist(self, record) :
        """ converts a record - which is a list - into a list of printable strings """
        fmt_list = self.record_print_format_list
        plist = [fmt % val for (fmt,val) in zip(fmt_list, record)]
        return plist

    def flush(self) :
        return self.rlc.clear()

import simp_udp_to_matlab as matplot

if __name__ == "__main__" :
    rec_list = []



    
    if 0 :  # for log to file and sending to matlab
        if 0 :
            flog_jr3_fname = "dynamic_log.txt"
            flog_jr3 = open(flog_jr3_fname, "w");
        else : 
            flog_jr3 = None

        rlc = read_load_cell(1000, 2000);
        udp = matplot.UDP_to_Matlab(ip='192.168.192.15', port=9990, num_samples=20);
        
        while 1 :
            time.sleep(0.05);        
            rec_list = rlc.read_as_list(1000);

            for rec in rec_list :
                udp.update(rec[1:])

                str_list = [ ("%5.1f" % m) for m in rec[1:] ]
                str_2 = ' '.join(str_list);
                if flog_jr3 : 
                    flog_jr3.write(str_2 + "\n");
            print str_2                
            if flog_jr3 :
                flog_jr3.flush()



        
    rlc = read_load_cell(20, 200);        

    while 1 :

        rec_list = rlc.read_as_list(1);
        if rec_list :
            # print rec_list[0][2]
            print rec_list[0][0:4]            

    for cnt in xrange(0,100) :
        if rlc.rlc.size() > 2:
            # rlc.debug_read(10) 

            if (0) :
                print "cnt = ", rlc.read(10)
                rec_list = rlc.make_record_list()

            if (1) :
                rec_list = rlc.read_as_list(10)
                print "len rec list : ", len(rec_list)

            for rec in rec_list :
                plist = rlc.record_to_plist(rec)
                print ' '.join(plist)

            if (0) :
                d = rlc.read_obj(3)
                print d.keys()
                print d['items_read']

                rec_list = d['record_list']
                for rec in rec_list : 
                      d = rlc.record_to_dict(rec)
                      print d
                  
            
        time.sleep(0.01)

        
    print rlc.record_unpack_string;

    """
    try :
        rlc.rlc.stop();
    except :
        print "python stop error"

    try :
        rlc.rlc.close();
    except :
        print "python close error"

    """    
