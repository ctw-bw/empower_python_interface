#!/usr/bin/env python

import os
import python_paths_default
import read_ankle
import serial_port_helper
import time
import Queue
import threading
import struct
import class_read_varlist2

class read_from_ankle(threading.Thread) :
    """
    This class handles logic and threading for reading data from an ankle
    """
    def __init__(self, max_records, sample_rate, swifi) :
        self.pgrp = os.getpgrp()   # This is the process group identifier
        print "pgrp=", self.pgrp
        self.swifi = swifi;
        self.que_obj = Queue.Queue(max_records)
        self.sample_rate = sample_rate / 1000000.0
        self.dwell_time = 0
        self.settle_time = 0
        self.terminate = False;
        self.error_status = 0
        self.mutex = threading.Lock()
        self.struct_string = ''
        self.DATA_PACKET_LENGTH = 0
        self.data_label_string = ''
        self.data_units_string = ''
        self.data_print_format_string = ''
        self.time_last_cycle = 0
        # Header variables
        self.header_unpack_string = "=3L"
        self.header_print_format_string = "%lu %lu %lu"
        self.header_print_format_list = self.header_print_format_string.split()
        self.header_data_labels_string = "items_available, error_status_code, items_read"
        self.header_data_labels_list = self.header_data_labels_string.split(', ')
        self.header_data_units_string = "uint, uint, uint"
        self.header_data_units_list = self.header_data_units_string.split(', ')
        threading.Thread.__init__(self)

    def __del__ (self) :
        print "swifi_thread.__del__"
        self.terminate = True

    def close(self) :
        self.Terminate = True
    
    def handler_USR1(self, signum, frame):
        self.sig1_count += 1        
        #dt = time.time()- self.time_start        
        print "&&&&& SIGUSR1:", self.sig1_count, dt
        # self.time_1.append(  int(1000 * dt))
        self.sig1 = True

        #if self.quiet == False :
        print 'CB1 Signal handler called with signal', signum

    def start(self) :
        threading.Thread.start(self)
        return 0

    
    

    def live_parsed_data_output_helpper(self, vlist_fname) :
        """ Read the variable list file the specifies data format of the WIFI packet """
        """ parse a file that has a variable name and type list """
        
        self.vlist_obj = class_read_varlist2.read_varlist(vlist_fname)
        self.struct_string = self.vlist_obj.make_struct_string()
        self.struct_string = self.struct_string[:1] + "q" + self.struct_string[1:]
        self.DATA_PACKET_LENGTH = self.vlist_obj.get_length();
        self.data_label_string = "sample time, " +self.vlist_obj.get_data_label_string()
        self.data_label_list = self.data_label_string.split(', ')
        self.data_units_string = "sec, " + self.vlist_obj.get_data_unit_string()
        self.data_print_format_string = "%lu " +self.vlist_obj.get_print_format_string()
        self.header_length = 8 * 3  # 8 bytes, 3 elements
        print "using struct string :",self.struct_string, self.DATA_PACKET_LENGTH

    def PushData(self, item) :
        """
        Put item on the queue, blocked if a read is occurring.
        Pop off the oldest element if the queue is full.
        """
        retVal = 1
        self.mutex.acquire()

        try :
            self.que_obj.put_nowait(item)
        except :
            self.que_obj.get_nowait()
            self.que_obj.task_done()
            self.que_obj.put_nowait(item)
            retVal = 0
            #print "queue size=",self.que_obj.qsize()
        self.mutex.release()
        return retVal

    def read(self, num_records_requested) :
        """
        Read 'count' elements from the queue. If there a fewer elements than
        'count', return the number of elements. While reading, 'put' is blocked.
        """
        self.mutex.acquire()
        rec_list = []
        record_count = 0
        try :
            for i in range(num_records_requested) :
                elem = self.que_obj.get_nowait()
                rec_list.append(alem)
                record_count = record_count + 1
                self.que_obj.task_done()
        except :
            pass

        
        header_string = struct.pack("=3L",
                                    self.que_obj.qsize(),
                                    self.error_status,
                                    record_count)
        self.mutex.release()
        return record_count, header_string, rec_list

    def read_as_list(self, num_records_requested) :
        """
        Read 'count' elements from the queue. If there a fewer elements than
        'count', return the number of elements. While reading, 'put' is blocked.
        """
        self.mutex.acquire()
        rec_list = []
        record_count = 0
        try :
            for i in range(num_records_requested) :
                elem = self.que_obj.get_nowait()
                record = struct.unpack(self.struct_string, elem)
                rec_list.append(record)
                record_count = record_count + 1
                self.que_obj.task_done()
        except :
            pass

        
        header_list = (self.que_obj.qsize(), self.error_status, record_count)
        self.mutex.release()
        return record_count, header_list, rec_list

    def read_as_numpy(self, num_records_requested) :
        (n, header_list, rec_list) = self.read_as_list(num_records_requested)
        return n, header_list, numpy.array(rec_list);

        



    def read_obj(self, num_records_requested) :
        """ read and return as a documented object """
        (n, header_list, record_list) = self.read_as_list(num_records_requested)
        
        d = self.header_to_dict()
        
        d['data_labels'] = self.record_data_labels_list 
        d['record_print_format'] = self.record_print_format_list 
        d['record_units'] = self.record_data_units_list

        
        d['record_list'] = self.make_record_list()

        return d

    def record_to_dict(self, record) :
        d = dict()
        for name, value in zip(self.record_data_labels_list, record):
            d[name] = value
        return d

    def record_to_plist(self, record) :
        """ converts a record - which is a list - into a list of printable strings """
        fmt_list = self.record_print_format_list
        plist = [fmt % val for (fmt,val) in zip(fmt_list, record)]
        return plist

    def clear(self) :
        (n, header_list, rec_list) = self.read_as_list(self.max_records)
        # FIXME: use a better return value
        return 0

    def flush(self) :
        return self.clear()
            
    def run(self) :
        terminate = False
        while 1 :
            self.time_last_cycle = time.time()
            pk_dict = self.swifi.main_iteration();
            if (pk_dict) :
                self.PushData(pk_dict)
            #terminate = self.swifi.keyboard_ui();
            
            if (terminate) :
                break;
            if self.terminate :
                break

            sleep_time = (time.time() - self.time_last_cycle) % self.sample_rate
            if (sleep_time == 0) :
                sleep_time = self.sample_rate
            time.sleep(sleep_time)
        # self.read_ankle.wifi.Disconnect()
        self.terminate = True
        self.swifi.stop()

    def is_terminated(self) :
        return self.terminate
        
    def stop(self) :
        self.terminate = True;

    def get_header_length(self) :
        return self.header_length

    def get_header_unpack_string(self) :
        return self.header_unpack_string
 
    def get_header_print_format_string(self) :
        return self.header_print_format_string
 
    def get_header_print_format_list(self) :
        return self.header_print_format_list

    def get_header_data_labels_string(self) :
        return self.header_data_labels_string

    def get_header_data_labels_list(self) :
        return self.header_data_labels_list

    def get_header_data_units_string(self) :
        return self.header_data_units_string

    def get_header_data_units_list(self) :
        return self.header_data_units_list

    def get_record_length(self) :
        return self.DATA_PACKET_LENGTH

    def get_record_unpack_string(self) :
        return self.struct_string

    def get_record_print_format_string(self) :
        return self.data_print_format_string

    def get_record_print_format_list(self) :
        return self.data_print_format_list

    def get_record_data_labels_string(self) :
        return self.data_label_string

    def get_record_data_labels_list(self) :
        return self.data_label_list

    def get_record_data_units_string(self) :
        return self.data_units_string

    def get_record_data_units_list(self) :
        return self.data_units_list

if __name__ == "__main__" :
    UDP_HOST = '127.0.0.1';
    UDP_PORT = 9999;

    LANTRON_HOST = '192.168.192.3';
    LANTRON_PORT = 10001;

    SERIAL_PORT = 1
    SERIAL_BAUD_RATE = 1250000
    # SERIAL_BAUD_RATE = 115200

    sph = serial_port_helper.serial_port_helper()
    OS_COM_PORT = sph.get_os_serial_port_name(SERIAL_PORT);
    # logging_file_robot_string = "COM:%s" % OS_COM_PORT
    print "USE SERIAL COM:", OS_COM_PORT


    # vlist_fname = 'pvf.vlist'
    vlist_fname = './vlist/dataport3d.vlist'
    #vlist_fname = './vlist/dataport8.vlist'

    
    ra = read_ankle.class_read_ankle(vlist_fname);

    # print read_ankle.variable_name_list

    ra.connect_serial(OS_COM_PORT, SERIAL_BAUD_RATE)
    # read_ankle.start_udp_matlab(UDP_HOST, UDP_PORT);
    ra.live_parsed_data_output_helpper(vlist_fname)

    sample_rate_hz = 500
    rfa = read_from_ankle(100, 1000000 / sample_rate_hz, ra)
    rfa.live_parsed_data_output_helpper(vlist_fname)

    rfa.start()
    time.sleep(3)
    for i in range(100) :
        time.sleep(0.1)
        buff = []
        (n, header_list, rec_list) = rfa.read_as_list(2)
        if (n) :
            print "count=", n, "res=", rec_list
    rfa.stop()
    rfa.join()
    """

    swifi_d = swifi_thread(read_ankle);
    swifi_d.start();
    while swifi_d.isAlive() :
        print "*", 
        swifi_d.join(1.0)
        
    read_ankle.wifi.Disconnect()
    """
    import subprocess
    subprocess.call('tset', shell=True);        
