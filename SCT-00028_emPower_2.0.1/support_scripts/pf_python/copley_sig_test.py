#!/usr/bin/env python

import signal, os, time, sys







class handle_signals() :
    def __init__(self, quiet = True) :
        self.sig1=False
        self.sig2=False
        self.sig1_count = 0;
        self.sig2_count = 0;
        self.time_start = time.time()
        self.time = []
        self.time_1 = []        

        signal.signal(signal.SIGUSR1, self.handler_USR1)
        signal.signal(signal.SIGUSR2, self.handler_USR2)
        print "Registered signal handlers"
        self.quiet = quiet
        pass
    
    def __del__(self) :
        print "handle signals __del__ called"
        # signal.signal(signal.SIGUSR1, signal.SIG_IGN);
        # signal.signal(signal.SIGUSR2, signal.SIG_IGN);
    
    def handler_USR1(self, signum, frame):
        self.sig1_count += 1        
        dt = time.time()- self.time_start        
#        print "&&&&& SIGUSR1:", self.sig1_count, dt
        # self.time_1.append(  int(1000 * dt))
        self.sig1 = True

        if self.quiet == False :
            print 'CB1 Signal handler called with signal', signum


    def handler_USR2(self, signum, frame):
        self.sig2_count += 1        
        dt = time.time()- self.time_start
#        print "&&&&& SIGUSR2:", self.sig2_count, dt
        # self.time.append(  int(1000 * dt))
        self.sig2 = True

        if self.quiet == False :
            print 'CB2 Signal handler called with signal', signum


    def send_signal_usr1(self, copley) :
        self.time_start = time.time()
        self.time = []
        self.sig2_count = 0;
        copley.send_child_signal_USR1()

    def wait_for_ack2(self, timeout = 10.0) :
        rcd = True
        start = time.time();
        
        while self.sig2 == False :
            if (time.time() - start) > timeout :
                break;
            time.sleep(1.0)
            
        if self.sig2 == False :
            rcd = False
            print "wait for ack2 timeout : wait_for_ack2"

        self.sig2 = False;

        return rcd

    def wait_for_usr_signals(self, timeout = 10.0) :
        """ wait for either USR1 or USR2, and return an ID of the signal we responsed too"""
        sig_id = [];
        start = time.time()

        last1 = self.sig1_count
        last2 = self.sig2_count
        while self.sig2 == False and self.sig1 == False :
            if (time.time() - start) > timeout :
                break;
            time.sleep(1.0)

        # print last1, last2, self.sig1_count, self.sig2_count
        if self.sig1_count != last1 :
            self.sig1 = False;            
            sig_id.append(1)

        if self.sig2_count != last2 :
            self.sig2 = False;            
            sig_id.append(2)

        return sig_id
        
        
        
        

if __name__ == "__main__" :
    #    import class_copley_control
    
    pvt_fname = 'ceb_pvt_third_amp.txt'
    ECAT_PATH = "/home/edana/loadcell/"
    
    hs = handle_signals(False);

    for i in xrange(0,20) :
        rcd = hs.wait_for_usr_signals(timeout = 3.0);
        print rcd
    
    if (0) :
        copley = class_copley_control.copley_control(EXE_PATH=ECAT_PATH);
        copley.debug = 5

        print "before ", hs.sig2_count, hs.time    
        rcd = copley.move_with_pvt_file_dd(pvt_fname, cycles=3, WAIT=False, OUTPUT=False)


        rcd = hs.wait_for_ack2()
        time.sleep(0.3)
        print "after ", hs.sig2_count, hs.time    

        while 1 :
            print
            print 
            print "START"
            hs.send_signal_usr1(copley)

            rcd = hs.wait_for_ack2()
            if (rcd) :
                rcd = hs.wait_for_ack2()
                time.sleep(0.3)

            if rcd == False :
                print "ACK timeout"
                sys.exit(5)

            if (0) :
                for i in range(0,10) :
                    print "cnt ", i, hs.sig2_count, hs.time
                    time.sleep(0.3)
            else :
                print "times ", hs.sig2_count, hs.time

            print "FIN SLEEP"

    print "FIN FIN"
