#!/usr/bin/env python

import class_c_api_reader
import python_paths_default
import python_paths

from CopleyWrapperCpp import * 

class class_read_copley(class_c_api_reader.class_c_api_reader) :
    def __init__ (self, max_num_records=1024, sample_rate_hz=1000) :
        self.c_api_obj = ReadCopley(max_num_records, sample_rate_hz)
        class_c_api_reader.class_c_api_reader.__init__(self,
                                                       self.c_api_obj,
                                                       max_num_records,
                                                       sample_rate_hz)

    def setAmp(self, amp) :
        self.c_api_obj.setAmp(amp)
