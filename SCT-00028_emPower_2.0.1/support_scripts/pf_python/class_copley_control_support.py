#!/usr/bin/env python

SVN_SUB_REV_STR = "$Revision: 11029 $"

LTF_REV_C = True

import class_read_copley
import class_read_loadcell
import os
import signal
import time

import python_paths_default
import python_paths
from CopleyWrapperCpp import * 

class class_copley_control_support() :
    def __init__(self, ecat, read_copley, read_loadcell, sample_time_length,
                 quiet = True) :
        self.ecat = ecat
        self.sample_freq = 500
        self.read_copley = read_copley
        self.read_loadcell = read_loadcell
        self.sample_time_length = sample_time_length
        self.sig1 = False
        self.sig2 = False
        self.quiet = quiet

        # Initalize a paramater block
        self.params = MotorOperationParams().create()
        self.ecat.setParams(self.params)
        self.params.setParam("ppid", os.getpid())
        self.params.setParam("initial_position", 0)
        self.params.setParam("final_position", 0)
        self.params.setParam("accel", 6.0)
        self.params.setParam("decel", 6.0)

        if LTF_REV_C :
            self.params.setParam("rpm",  85) # Rev C 85 ~= 84.92 ~= 100 / 1.1776
        else :
            self.params.setParam("rpm", 100) # Rev A

        self.params.setParam("passes", 2)
        self.params.setParam("mode", "p2p")
        self.params.setParam("safety_protocol", 2)

        self.old_sighandler_USR1 = signal.signal(signal.SIGUSR1, self.handler_USR1)
        self.old_sighandler_USR2 = signal.signal(signal.SIGUSR2, self.handler_USR2)
    
    def handler_USR1(self, signum, frame):
        #print "*&&&&& SIGUSR1:"
        # self.time_1.append(  int(1000 * dt))
        self.sig1 = True
        self.sig2 = False
        if self.quiet == False :
            print 'USR1 Signal handler called with signal', signum


    def handler_USR2(self, signum, frame):
        #print "&&&&& SIGUSR2:"
        # self.time.append(  int(1000 * dt))
        self.sig2 = True
        if self.quiet == False :
            print 'USR2 Signal handler called with signal', signum

    def getMotorPos_N(self, newtons) :
        self.params.setParam("final_position", 600)
        n = self.read_loadcell.start()
        print "lc_start=", n
        n = self.read_copley.start()
        print "lc_start=", n
        motorPos = None
        fz = None
        time.sleep(1)

        while (1) :
            self.ecat.start()
            try :
                (motorPos, fz) = self.findMotorPos_N(newtons)
            except Exception, e :
                # Stop the Copley
                self.read_copley.stop()
                # Stop the Loadcell
                self.read_loadcell.stop()
                self.remove_signal_handlers()
                self.ecat.stop()
                raise Exception, e

            if (newtons - fz <= 0.0) :
                # Stop the Copley
                self.read_copley.stop()
                # Stop the Loadcell
                self.read_loadcell.stop()
                self.remove_signal_handlers()
                self.ecat.stop()
                return motorPos, fz

            if (newtons - fz > 200.0) :
                new_finalpos = self.params.getParam_int("final_position") + 2000

            elif (newtons - fz > 150.0) :
                new_finalpos = self.params.getParam_int("final_position") + 1000

            elif (newtons - fz > 100.0) :
                new_finalpos = self.params.getParam_int("final_position") + 500

            elif (newtons - fz > 25.0) :
                new_finalpos = self.params.getParam_int("final_position") + 150

            else :
                new_finalpos = self.params.getParam_int("final_position") + 50

            self.params.setParam("final_position", new_finalpos)

            if (not self.quiet) :
                print "new_final_pos=", new_finalpos

    def findMotorPos_N(self, newtons) :
        motorPos = None
        fz = None
        maxFz = None
        finalMotorPos = None
        max_num_records = self.read_loadcell.max_size()
        time.sleep(1)
        while (1) :
            if (self.sig1) :
                (n, header_list, record_list) = self.read_loadcell.read_as_list(max_num_records)
                if (n) :
                    rec_dict = self.read_loadcell.record_to_dict(record_list[n - 1])
                    fz = rec_dict["Fz"]
                    if (maxFz == None) :
                        maxFz = fz
                    # Read the data from the Copley
                    (n, header_list, record_list) = self.read_copley.read_as_list(max_num_records)
                    if (n) :
                        rec_dict = self.read_copley.record_to_dict(record_list[n - 1])
                        #print rec_dict
                        motorPosFlag = rec_dict["MotorPosition Flag"]
                        if (motorPosFlag)  :
                            motorPos = rec_dict["MotorPosition"]

                    if (fz >= maxFz) :
                        maxFz = fz
                        finalMotorPos = motorPos

                    # If we have attained our goal, get the motor position
                    # if available
                    if (maxFz >= newtons) :
                        # Wait for the cycle to complete
                        while (not self.sig2) :
                            time.sleep(0.01)
                        break;

            if (self.sig2) :
                self.sig2 = False
                break;

            time.sleep(self.sample_time_length)

        if (finalMotorPos == None) :
            raise Exception("Failed to read Motor Position")

        if (not self.quiet) :
            print "motorPos=", finalMotorPos, " fz=", maxFz

        return finalMotorPos, maxFz

    def remove_signal_handlers(self) :
        signal.signal(signal.SIGUSR1, self.old_sighandler_USR1)
        signal.signal(signal.SIGUSR2, self.old_sighandler_USR2)
