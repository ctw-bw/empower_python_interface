
import os, time, sys

if os.name == 'nt' :
    import msvcrt;

if os.name == 'posix' :
    sys.path.insert(0,'support')
    import py_getch 
    sys.path.pop(0)


class keyboard :
    def __init__ (self) :
        self.last_char = " ";


        if os.name == 'nt' :
            ESC = chr(224);
            FUN = chr(0);
        
            self.ESC = ESC;
            self.FUN = FUN;



        if os.name == 'posix' :
            self.ESC = chr(27)
            self.FUN = None;
            self.getch = py_getch.py_getch(BLOCK=False, ECHO=False, TIMEOUT=0.0)

    def __del__(self) :
        # print "__del__ (keyboard.py)"
        pass

    

        
    if os.name == 'nt' :    
        def getch(self) :
            r_char = [];
            if msvcrt.kbhit() :
                ch = msvcrt.getch();

                if (self.last_char == self.ESC or self.last_char == self.FUN) :
                    r_char = self.last_char + ch;

                elif (ch != self.ESC and ch != self.FUN) :
                    r_char = ch;

                self.last_char = ch;
            return r_char

        def getch_packed(self) :
            ch = self.getch();
            esc = 0;
            fun = 0;
            char = "";

            if (ch and ch[0] == self.ESC) :
                esc = ord(ch[1]);
            elif (ch and ch[0] == self.FUN) :
                fun = ord(ch[1]) - 58;
            elif (ch) :
                char = ch;

            return (ch, esc, fun, char);

                
    if os.name == 'posix' :
        def getch(self) :
            ch = self.getch()
            return ch;
        
        def getch_packed(self) :
            ch = self.getch()

            esc = None;
            fun = None;
            char = ""
            # under linux - and using a TTY - special keys appear to be encoder as 3-5 byte sequences
            # from my memory - 
            # this first is ESC (27), followed by 2 of chr(91) '[', followed by the ID char
            # the escape sequence is 27 91
        
            
            return (ch, esc, fun, char);

        
if __name__ == "__main__" :

    
    k = keyboard();


    while 1 :
        ch = k.getch_packed();
        if (ch[0]) :
            print ch

    for i in range(0,200) :
        ch=k.getch();
        if (ch and ch[0] == k.ESC) :
            print "ESC ", ord(ch)
        elif (ch and ch[0] == k.FUN) :
            print "FUN", ord(ch)
        elif (ch) :
            print ch, ord(ch), i
        time.sleep(0.1)



            

