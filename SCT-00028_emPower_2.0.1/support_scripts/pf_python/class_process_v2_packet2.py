


import time, os
import string
import struct
import crc8





SYNC_CHAR_1 = '!';    # 0x21
SYNC_CHAR_2_V2 = chr(0x42); # 0x42


SYNC_CHAR_3 = '#';    # 0x23
SYNC_STR = SYNC_CHAR_1 + SYNC_CHAR_2_V2;




class process_v2_packet() :
    def __init__ (self, data_packet_len, data_source, text_logger=None, error_logger=None) :
        """
        data source : is a file descripter - and needs a read method.
        text_logger : file descriper that is used to send the text output.
        """ 
        
        self.TBL = string.maketrans("","");                    
        self.error_fw = error_logger;
        self.packet_len = data_packet_len + 5 + 4;
        self.data_source = data_source;
        self.text_output = text_logger;

        self.buff = "";
        self.fast_text = "";
        self.pk_cnt  = 0;
        self.buff_list = [];
        
        # self.user_char_str = "";
        self.timeout_cnt = 0;
        self.do_text_output = True;

        self.packet_number = 0


                
    def ReadAndProcessBuff(self) :
        # print "buff len IN", len(self.buff)            
        ln = len(self.buff)
        if (len(self.buff) < self.packet_len) :
            ln = self.data_source.read(self.packet_len - ln);
            # print "LEN LN", len(ln) 
            if (len(ln) > 0) :
                self.buff = ''.join( [self.buff, ln]);
#        if (len(self.buff) < 512) :
#            ln = self.data_source.read(512);
#            # print "LEN LN", len(ln) 
#            if (len(ln) > 0) :
#                self.buff = ''.join( [self.buff, ln]);


        # print "buff len 2IN", len(self.buff)


        #print "buff len X ", len(self.buff),"packet_len=",self.packet_len
        
        ndx = self.buff.find(SYNC_STR, 0);
        if (ndx == -1) :
            #print "case -1"
            ln = self.data_source.read(self.packet_len);
            #print "LEN LN", len(ln) 
            if (len(ln) > 0) :
                self.buff = ''.join( [self.buff, ln]);
                ndx = self.buff.find(SYNC_STR, 0);
                #print "ndx=",ndx,", self.buff_len=",len(self.buff) 
                if (ndx > 0) :
                    self.buff = self.buff[ndx:]
                    ndx = 0
                
        if (ndx > 0) :
            #print "case 0"
            ln = self.data_source.read(ndx);
            #print "LEN LN", len(ln) 
            if (len(ln) > 0) :
                self.buff = ''.join( [self.buff[ndx:], ln]);
                ndx = 0
            else :
                return []
        
        if (len(self.buff) < self.packet_len) :
            #print "broken"
            return [];

        if (ndx == 0) :
                #print "self.buff_len=",len(self.buff)
                header = struct.unpack(">HBBH", self.buff[0:6]);
                """
                print
                print                    
                print ["%x" % h for h in header]
                """
                # 0 : sync header
                # 1 : packet ID
                # 2 : data section length
                # 3 : composite word w/stream_id:3, stream:len:3, mon_var_id:10


                stream_id = header[3]  & 7
                stream_len = (header[3] >> 3) & 7
                mon_var_id = (header[3]>>6) & 0x3ff;
                header_length = 6;
                mon_var_length = 0;

                text_offset = header_length;
                mon_var_offset = text_offset + stream_len;
                data_offset = mon_var_offset + mon_var_length;

                # print stream_id, stream_len, mon_var_id



                crc = crc8.calc_upd_crc(self.buff[: self.packet_len-1])
                #print "len=",len(self.buff)," packet_len=",self.packet_len
                eop_char = self.buff[self.packet_len-1];
                #print "crc=", crc, "eop=", ord(eop_char)
                crc = ord(eop_char)

                if (ord(eop_char) == crc) :                
                        self.pk_cnt = self.pk_cnt + 1;

                        txt = self.buff[text_offset : mon_var_offset];
                        if len(txt) == 2:  # a zero length txt segment is possible
                            val = struct.unpack('>h',txt);
                        else :
                            val = 0;
                            
                        mon_var_buff = self.buff[mon_var_offset : data_offset];


                        self.data = self.buff[data_offset : self.packet_len - 1];
                        self.raw_data = self.buff[: self.packet_len ]; 

                        self.buff = self.buff[self.packet_len :];

                        # text stream process
                        #
                        self.fast_text = ''.join(  [self.fast_text, txt] );
                        ndx = self.fast_text.find(chr(0x0a));
                        if (ndx == -1) :
                                ndx = self.fast_text.find(chr(0x0d));


                        ll = len(self.fast_text);

                        if (ll > 1300) :
                            ndx = 1300;
                        if (ndx >=0) :
                            txt = self.fast_text[:ndx];

                            if (self.text_output) :
                                pn_str = "-- PN:%d TM:%4.2f " %(self.packet_number, self.packet_number/500.)

                                self.text_output.writeln(txt.translate(self.TBL,chr(0))+pn_str)

                            self.fast_text = self.fast_text[ndx+1:];

                        # print "exit : ", len(self.buff)
                        self.packet_number += 1
                        return self.data;
                    
                else :

                        str1 = "CRC : %2x %2x " % (ord(eop_char), crc)
                        val_list = [('%2x' % ord(val) ) for val in self.buff[0: self.packet_len+10]];
                        str2 = ' '.join(val_list);

                        str3 = time.strftime("%b%d %y %H:%M:%S  ", time.localtime());
                        str4 = " PC:%3x BL:%3x" % (self.pk_cnt, len(self.buff));
                        if (self.error_fw) :
                                self.error_fw.write(str3 + str1 + str2 + str4 + '\n');

                        self.buff = self.buff[2:];
                        ndx = self.buff.find(SYNC_STR, 0);



        # if ndx != 0 - then we get here.

        ### process missing sync
        str1 = "SYNC: %2x :: " % ndx
        str2 = "";
        # print [hex(ord(ch)) for ch in self.buff[0:50]]
        # print [hex(ord(ch)) for ch in SYNC_STR]


        if (ndx == -1) :
                ll = len(self.buff);
                if ( ll > 200) :
                        self.buff = self.buff[ ll-50 :]


        if (ndx > 0) :
                self.buff = self.buff[ndx:];
                val_list = [('%2x' % ord(val) ) for val in self.buff[0: self.packet_len+1]];
                str2 = ' '.join(val_list);

        str4 = " PC:%3x BL:%3x" % (self.pk_cnt, len(self.buff));

        # print str1, str2, str4
        str3 = time.strftime("%b%d %y %H:%M:%S  ", time.localtime());

        if (self.error_fw) :
            self.error_fw.write(str3+str1 + str2 + str4 + '\n');

        return [];



import sys
class text_output () :
    """ example class that will output text stripped from the packet."""
    def __init__ (self, log_fname=None) :
        self.user_char_str = "";
        
        if log_fname :
            self.fout = open(log_fname,"w")
        else :
            self.fout = None

    def close(self) :
        if self.fout :
            self.fout.close()

    def AppendUserChar(self, ch) :
        """ local keyboard input can be added here, and printed at the end of the line"""
        self.user_char_str += ch;

    def write(self, str) :
        sys.stdout.write(str)
        if self.fout :
            self.fout.write(str)


    def writeln(self, str) :
        wstr = str+ "[%s]\n" % self.user_char_str
        sys.stdout.write(wstr);
        if (self.fout) :
            self.fout.write(wstr)
        self.user_char_str = "";        



import socket;
class udp_text_output() :
    def __init__ (self, HOST=None, PORT=None) :
        self.user_char_str = "";
        self.soc = None;
        self.host = HOST;
        self.port = PORT;
        self.err_msg_cnt = 0;

        try : 
            if (HOST and PORT) :
                self.connect_udp();
            print "UDP CONNECTED"    
            self.writeln("UDP connected")
        except :
            print
            print "**** UDP CONNECTION FAILED"
            print            
        

    def connect_udp(self, host=None, port=None) :
        if (host) :
            self.host = host
        if (port) :
            self.port = port

        print self.host, self.port
        self.soc = socket.socket(socket.AF_INET, socket.SOCK_DGRAM);
        self.soc.connect((self.host, self.port));

    def close(self) :
        self.soc.close();

    def AppendUserChar(self, ch) :
        self.user_char_str += ch;


    def writeln(self, str) :

        if self.soc :
            self.soc.send(str+ "[%s]\n" % self.user_char_str)
            self.user_char_str = "";
        else :
            if self.err_msg_cnt < 10 :
                print "udp_text_output - socket not open"
            elif self.err_msg_cnt == 110 :
                print "100xudp_text_output - socket not open"
                self_err_msg_cnt = 10;

            self.err_msg_cnt += 1





def get_targeted_values_from_string(ln, list) :
    d = {}
    l = []
    for str in list :
        pos = ln.find(str)
        if (pos>=0) :
            val_pos = pos + len(str)
            sp = ln[val_pos:].split()
            # print pos, val_pos, str, sp[0]
            val = float(sp[0])
        else :
            val = -1
        d[str] = val
        l.append(val);
    d['LIST'] = l
    return d
            
            


        
        
        

if __name__ == "__main__" :
    fname = "v2_packet_capture.bin"
    fname = "X:\iWalk LLC\data\tim_bedard-tuning-aclimation-2009_oct13\tim_study2_091013_170550.bin6"


    fname = "X:tim_tuning_091013_165628.bin6"
    fname = "X:tim_study2_091013_170550.bin6"

    fd = open(fname, 'rb')

    print os.path.splitext(fname)
    (filename, ext) = os.path.splitext(fname)
    fname_out = filename + "_textstream.txt"
    fname_parse_out = filename + "_parsed.txt"    
    print fname_out

    

    eo = text_output();    
    to = text_output(fname_out);

    # data_packet_len, input FD, text output FD, error_log FD
    pp = process_v2_packet(40, fd, to, eo) ;

    last_fd_pos = 0;
    stop_cnt = 0
    # for i in range(0,10) :
    # fd.seek(3000000);
    while 1 : 
        pk = pp.ReadAndProcessBuff();

        fd_pos = fd.tell()

        if (last_fd_pos == fd_pos) :
            # print fd_pos, last_fd_pos, stop_cnt
            stop_cnt += 1
            if stop_cnt > 50 :
                break
        else :
            stop_cnt = 0


        last_fd_pos = fd_pos


    fd.close();
    to.close()


    #####################
    fin  = open(fname_out, 'r')
    fout = open(fname_parse_out, 'w')
    target_list = ["RM I:", "VB", "STEP:", "PN:", "TM:"]
    first = True
    for ln in fin :
        print ln
        values = get_targeted_values_from_string(ln, target_list)
        if first :
            str = '  ;  '.join(target_list) + "\n"
            fout.write(str)
            first = False
            
        str = ''.join ( ["%s " % val for val in values['LIST'] ] )+ "\n"
        fout.write(str)
