import socket
import time
import string
import sys

import struct
sys.path.append('..')
import crc8
# import serial

SYNC_CHAR_1 = '!';    # 0x21
SYNC_CHAR_2_V2 = chr(0x42); # 0x42


SYNC_CHAR_3 = '#';    # 0x23
SYNC_STR = SYNC_CHAR_1 + SYNC_CHAR_2_V2;

TBL = string.maketrans("","");


##########################################################################################
class ReadFromAnkle:
    """
    open / manage a serial port
    look for sync chars, resyncronize, and build a data packet
    return completed packets.
    """

    def __init__(self, HOST, PORT, data_packet_len, error_log_fname, OPEN=1, USE_SERIAL =0):

        self.buff = "";
        self.fast_text = "";
        self.pk_cnt  = 0;
        self.buff_list = [];
        self.packet_len = data_packet_len + 5 + 4;
        self.user_char_str = "";
        self.bin_logger_fw = []
        self.timeout_cnt = 0;
        self.error_fw = []
        self.quiet = 0;
        self.do_text_output = 1;

        self.USE_SERIAL = USE_SERIAL;
        if (OPEN) :
            if (error_log_fname) :
                self.OpenErrorLog(error_log_fname)
            else :
                self.error_fw = []

            if (self.USE_SERIAL) :
                self.sp = serial.Serial(13, 115200 * 2, timeout=0.2, rtscts=0)
                time.sleep(1.0);
                return 
            else :
                self.Connect(HOST, PORT);
            print "Packet length %d " % self.packet_len
            print
            print


                
    def Disconnect(self) :
            self.sp.close();
            
    def Connect(self, HOST='', PORT=0, timeout = 1.0) :

            if (HOST) :
                self.tcp_addr = (HOST, PORT);
            
            print "Connect to IP %s PORT %s ..." % self.tcp_addr,
            
            try_cnt = 4;
            while try_cnt > 0 :
                try :
                    self.sp.close();
                except :
                    pass;
            
                try :
                    self.sp = socket.socket(socket.AF_INET, socket.SOCK_STREAM);
                    self.Timeout(timeout);
                    self.sp.connect(self.tcp_addr);
                    print "Success"
                    try_cnt = 0;
                    break;
                except :
                    print "Retry",
                    self.sp.close();
                    time.sleep(1.0);
                    try_cnt = try_cnt - 1;
                


            str3 = time.strftime("%b%d %y %H:%M:%S  ", time.localtime());
            str1 = "OPEN: IP %s PORT %s  : packet_len %d" % (HOST, PORT , self.packet_len);
            if (self.error_fw) :
                self.error_fw.write(str3 + str1 + '\n');                        
                
    def Timeout(self, timeout) :
                if (self.USE_SERIAL == 0) :
                    self.sp.settimeout(timeout);

    def send(self,ch) :
            if (self.USE_SERIAL) :
                self.sp.write(ch)
            else :
                # print "sending ",ch
                self.sp.send(ch)
            
    def SendChar(self,ch) :
        # self.sp.write(ch)
        # print
        # print "sending ",ch
        self.send(ch)

    def SendStrSlow(self, str) :
            for ch in str :
                self.send(ch)
                print ch, 
                time.sleep(0.05);


    def SendList(self, list) :
            s = ''.join([chr(val) for val in list])
            # print s
            self.send(s);
            return 
            for val in list :
                # print "SEND", hex(val), val
                self.send( chr(val))
                # time.sleep(0.03);

    def Open_LogWifiText(self, fname) :
        self.log_wifi_text = open(fname,'w');
        
    def OpenErrorLog(self, fname = 'error_log.txt') :
        self.error_fw = open('error_log.txt','a');
        """
        http://docs.python.org/library/logging.html
        self.logger = logging.getLogger("logger.txt");
        formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s");
        self.logger.addHandler(ch)
        """

    def OpenBinLog(self, fname = 'wifi_log.bin') :
        try :
            self.bin_logger_fw.close();
        except :
            pass
        try :
            self.bin_logger_fw = open(fname,'wb');
            print "Open : %s" % fname
        except :
            print "FAILED  open : %s" % fname

        def close(self) :
            self.Disconnect();            
            try :
                self.bin_logger_fw.close();
            except :
                pass

            try :
                self.error_fw.close();
            except :
                pass


                
        
    def AppendUserChar(self, ch) :
            self.user_char_str = self.user_char_str + ch;

        def flush_soc(self) :
            try :
                ln = self.sp.recv(100);
                return 1
            except :
                pass
            return 0
            
            
        def DoBlockRead(self) :

            try :
                if (self.USE_SERIAL) :
                    ln = self.sp.read(512);
                    # print len(ln),
                    self.timeout_cnt = 0;
                else :
                    ln = self.sp.recv(512);
                    self.timeout_cnt = 0;
            except :
                self.timeout_cnt += 1;
                if (self.timeout_cnt == 1) :
                    print "DBR timeout ", 
                print self.timeout_cnt, 
                ln = ""
                if (self.timeout_cnt >= 9) :
                    self.Connect();

                
            if (len(ln) > 0) :
                self.buff = ''.join( [self.buff, ln]);
                # print len(ln), len(buff)
                if (self.bin_logger_fw) :
                    self.bin_logger_fw.write(ln);



    def DoFileRead(self) :

        ln = self.sp.read(512);
        ll = len(ln), 
        if (ll > 0) :
            self.buff = ''.join( [self.buff, ln]);
        return ll


    def ProcessBuff(self) :
        if (len(self.buff) < 512) :
            self.DoBlockRead();
            # ll = self.DoFileRead();
                
            
        if (len(self.buff) < self.packet_len) :
            return [];

        ndx = self.buff.find(SYNC_STR, 0);

        
        if (ndx == 0) :
            header = struct.unpack(">HBBH", self.buff[0:6]);
            # print ["%x" % h for h in header]
            # 0 : sync header
            # 1 : packet ID
            # 2 : data section length
            # 3 : composite word w/stream_id:3, stream:len:3, mon_var_id:10
            stream_id = header[3]  & 7
            stream_len = (header[3] >> 3) & 7
            mon_var_id = (header[3]>>6) & 0x3ff;
            header_length = 6;
            mon_var_length = 0;
            
            text_offset = header_length;
            mon_var_offset = text_offset + stream_len;
            data_offset = mon_var_offset + mon_var_length;
            
            # print stream_id, stream_len, mon_var_id
            

            
            crc = crc8.calc_upd_crc(self.buff[: self.packet_len-1])
            eop_char = self.buff[self.packet_len-1];
            # if (eop_char == SYNC_CHAR_3 or ord(eop_char) == crc) :
            if (ord(eop_char) == crc) :                
                self.pk_cnt = self.pk_cnt + 1;

                # txt = self.buff[6:8];
                txt = self.buff[text_offset : mon_var_offset];
                val = struct.unpack('>h',txt);
                mon_var_buff = self.buff[mon_var_offset : data_offset];

                if (0 and val[0]) :
                    sys.stdout.write(txt.translate(TBL,chr(0)))

                # self.data = self.buff[8 : self.packet_len - 1];
                self.data = self.buff[data_offset : self.packet_len - 1];
                self.raw_data = self.buff[: self.packet_len ]; 

                self.buff = self.buff[self.packet_len :];

                # text stream process
                #
                self.fast_text = ''.join(  [self.fast_text, txt] );
                ndx = self.fast_text.find(chr(0x0a));
                if (ndx == -1) :
                    ndx = self.fast_text.find(chr(0x0d));
                
                
                ll = len(self.fast_text);
                
                if (ll > 1300) :
                    ndx = 1300;
                if (ndx >=0) :
                    txt = self.fast_text[:ndx];
                    # pn_v = ord(self.data[8]) * 256 + ord(self.data[9]);
                    # print hex(pn_v), hex(self.pk_cnt), txt.translate(TBL,chr(0));
                    
                    # print hex(len(self.buff)), hex(self.pk_cnt), txt.translate(TBL,chr(0)),
                    if (self.do_text_output) :
                        print txt.translate(TBL,chr(0)),
                        print "[%s]" % self.user_char_str;
                    self.user_char_str = "";

                    self.fast_text = self.fast_text[ndx+1:];


                return self.data;
            else :

                str1 = "CRC : %2x %2x " % (ord(eop_char), crc)
                val_list = [('%2x' % ord(val) ) for val in self.buff[0: self.packet_len+10]];
                str2 = ' '.join(val_list);
                print str1, str2
                str3 = time.strftime("%b%d %y %H:%M:%S  ", time.localtime());
                str4 = " PC:%3x BL:%3x" % (self.pk_cnt, len(self.buff));
                if (self.error_fw) :
                    self.error_fw.write(str3 + str1 + str2 + str4 + '\n');
                    self.error_fw.flush();                
                time.sleep(0.010);
                self.buff = self.buff[2:];
                ndx = self.buff.find(SYNC_STR, 0);
                


        ### process missing sync
        str1 = "SYNC: %2x " % ndx
        str2 = "";
        print [hex(ord(ch)) for ch in self.buff[0:50]]
        print [hex(ord(ch)) for ch in SYNC_STR]
        

        if (ndx == -1) :
            ll = len(self.buff);
            if ( ll > 200) :
                self.buff = self.buff[ ll-50 :]
                

        if (ndx > 0) :
            self.buff = self.buff[ndx:];
            val_list = [('%2x' % ord(val) ) for val in self.buff[0: self.packet_len+1]];
            str2 = ' '.join(val_list);

        str4 = " PC:%3x BL:%3x" % (self.pk_cnt, len(self.buff));
        
        print str1, str2, str4
        str3 = time.strftime("%b%d %y %H:%M:%S  ", time.localtime());
                
        if (self.error_fw) :
                    self.error_fw.write(str3+str1 + str2 + str4 + '\n');
                    self.error_fw.flush();
                    
        time.sleep(0.010);
        
        return [];



