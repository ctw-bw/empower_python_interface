#!/usr/bin/env python
'''
Created on Oct 13, 2015

@author: ekurz
'''

from __future__ import print_function

import python_paths_default
import python_paths
import pfcmd
import ankle_commands
import calb_tests
import sys
import traceback

try:
    import USB_RLY_Phidgets
except Exception as e: 
    print("WARNING! Not able to import driver for USB relay")
    print(e)
import time
import ConfigParser as cp
import ordereddict
import os
import serial_port_helper
import datetime
import robo_config
import loggHandeler
#if os.name != 'nt':
#    import calb_springs
import wx
import shutil
import select
if os.name == 'nt':
    import msvcrt

# TIMEOUT_SECONDS_LONG = 5
STARTUP_COMM_DELY_DUE_TO_BLUETOOTH_INIT = 7.0 ### Bluetooth init in Tango means that comm is inaccessible up to 5 seconds after power up of unit. 
COMM_NO_RESPONSE_TIMEOUT_SECONDS = .25
TIMEOUT_SECONDS_POWER_CYCLE = .25

# def ask_for_power_cycle(pf, port, baudrate, log):
#     pf.disconnect()    
#     time.sleep(TIMEOUT_SECONDS_POWER_CYCLE)
#     print('power cycle ankle', file=log)
#     prompt("Press Enter to continue...",log)
#     print('Waiting for Ankle Bluetooth Comm to finish...', file=log)
#     time.sleep(STARTUP_COMM_DELY_DUE_TO_BLUETOOTH_INIT)
#     pf.connect(port, baudrate,COMM_NO_RESPONSE_TIMEOUT_SECONDS)
#     pf.flush(); #Flush anything in comport from Radio
#     print('Reconnected to Ankle.', file=log)
            
def ask_for_power_cycle(pf, port, baudrate, log):
    print('reseting UUT',file=log)
    pf.do_command(ankle_commands.s_command_type(), ankle_commands.s_micro_reset())
    pf.disconnect() 
    time.sleep(STARTUP_COMM_DELY_DUE_TO_BLUETOOTH_INIT)
    pf.connect(port, baudrate,COMM_NO_RESPONSE_TIMEOUT_SECONDS)
    try:
        upTime = pf.do_command(ankle_commands.s_command_type(),ankle_commands.s_timers_get())
        upTime = upTime.get(ankle_commands.timers_upTime())
    except:
        time.sleep(3)
        upTime = pf.do_command(ankle_commands.s_command_type(),ankle_commands.s_timers_get())
        upTime = upTime.get(ankle_commands.timers_upTime())
    print(upTime, file=log)
    if upTime > 7:
        print('reset failed',file = log)
        return False
    print('reset complete',file =log)
    return True
    
def testFailed(cfg,log,test,CalibrationFileOut):
    print('#'*100, file=log)
    print('Test failed',file=log)
    print(test, file=log)
    print('#'*100)
    cfg.set('mfg_table', 'result', 'Fail')
    commitConfigFileLocally(cfg, CalibrationFileOut, log)

def turnOn(relay,pf,cp, baudrate,ankleRl, timeout_seconds = COMM_NO_RESPONSE_TIMEOUT_SECONDS):
    relay.turn_on(ankleRl)
    print('Waiting for Ankle Bluetooth Comm to finish...', file=log)
    time.sleep(STARTUP_COMM_DELY_DUE_TO_BLUETOOTH_INIT)
    pf.connect(cp, baudrate,timeout_seconds); 
    #pf.flush(); #Flush anything in comport from Radio
    #upTime = pf.do_command(ankle_commands.s_command_type(),ankle_commands.s_timers_get())
    #upTime = upTime.get(ankle_commands.timers_upTime())
    #print(upTime, file=log)
    #if upTime > 7:
    #    print('Relay turned on and Com reconnected to Ankle failed.',file = log)
    #    return False
    #print('Relay turned on and Com reconnected to Ankle.', file=log)
    return True
    

def powercycle(rl,pf,cp, baudrate):
    pf.disconnect()
    rl.turn_all_off()
    time.sleep(TIMEOUT_SECONDS_POWER_CYCLE)
    return turnOn(rl, pf, cp,baudrate)

def workflowSetTS(cfgObj) :
    """set the datetime of the last update to the configuration"""
    d = datetime.datetime.now()
    cfgObj.set('mfg_table', 'last_update', d.strftime('%Y%m%d_%H%M%S'))

def commitConfigFileLocally(cfg,fn,logger) :
    """commit the current configuration to local file, including timestamp"""
    workflowSetTS(cfg)

    with open(fn,'w') as fh :
        cfg.write(fh)
        fh.flush()
    
def checkSN(sn,logger):
    """take in a serial number and confirm with the operator that the SN is correct"""
    print('UUT SN is: ',sn, file = logger)
    val = prompt("Press Enter if SN is correct otherwise put in new SN: ",logger)
#     print('#')
#     print(val,file=logger)
#     print('#')
    import re 
    pattern = re.compile('[0-9][0-9][0-9][0-9][0-9][0-9][0-9]')
    while not pattern.match(val) and ''!=val:
        val = prompt("Press Enter if SN is correct otherwise put in new SN: ",logger)
    if val == '':
        return sn
    else :
        return val
def getOP(logger):
    '''
    get the operator id
    '''
    val = prompt('Enter operator ID then press enter: ',logger)
#     print('#')
#     print(val,file=logger)
#     print('#')
    while val == '':
        val = prompt('Enter operator ID then press enter: ',logger)
    return val
def or_of_boolean_vector_and(a,b) :
    """Logic utility function"""
    result = list()
    for (x,y) in zip(a,b) :
        result.append(x and y)
    return True in result

def prompt(promptStr, loggerInstance) :
    """Prompt user for input and log the interaction"""
    
    sys.stdout.flush()
    if os.name == 'nt':
        while msvcrt.kbhit():
            msvcrt.getch()
    else:
        sys.stdin.flush()
        while len(select.select([sys.stdin.fileno()], [], [], 0.0)[0])>0:
            os.read(sys.stdin.fileno(), 4096)
    rsp = raw_input(promptStr)
    if loggerInstance :
        loggerInstance.logInfo(promptStr + rsp)

    return rsp

def promptWithNote(promptStr, relayControler, relayNumber, loggerInstance) :
    """this is to get the test operator back to the fixture and get them to press enter so that the test can continue"""
    
    try:
        while len(select.select([sys.stdin.fileno()], [], [], 0.0)[0])>0:
            os.read(sys.stdin.fileno(), 4096)
        print(promptStr, file = loggerInstance)
        relayControler.turn_on(relayNumber)
        relaystatus = True
        switchTime = time.time()+15
        while len(select.select([sys.stdin.fileno()], [], [],0.0)[0]) == 0:
            if relaystatus:
                if switchTime < time.time():
                    relayControler.turn_off(relayNumber)
                    relaystatus = False
                    switchTime = time.time()+30
            else:
                if switchTime < time.time():
                    relayControler.turn_on(relayNumber)
                    relaystatus = True
                    switchTime = time.time()+15
            time.sleep(0.5)
        relayControler.turn_off(relayNumber)
        rsp = raw_input()
    #     rsp = raw_input(promptStr)
        if loggerInstance :
            loggerInstance.logInfo(promptStr + rsp)

        return rsp
    except:
        relayControler.turn_on(relayNumber)
        time.sleep(15)
        relayControler.turn_off(relayNumber)
        return 'error with prompts'

if __name__ == '__main__':
    tr = True
    opt_parser = calb_tests.calb_test_options(sys.argv);
    opt = opt_parser.opt;

    ankleCon = pfcmd.PFCmd(0,0)
    cfg = cp.ConfigParser(dict_type=ordereddict.OrderedDict)
    cfg.optionxform=str
    cfg.read(os.path.join('testPrograms','template_ankle_tables.ini'))
    log = loggHandeler.logger_class(time.strftime('log_%y%m%d_%H%M%S.txt'))
    testReportStr=''
    calbT = calb_tests.BiOMCalbScripts(ankleCon,None,cfg,log,opt.baudrate)
    rl = None
    app = wx.App(False)
    
    cfg.set('mfg_table', 'workflow', opt.workflow)
    cfg.set('mfg_table', 'move_only',opt.move)
    
    if os.name != 'nt':
        rl = USB_RLY_Phidgets.relay()
    
        # Discover the USB Serial Ports to use
        sph = serial_port_helper.serial_port_helper()
        # Note: detects PSER movement from 0,1 to 2,3 (Linux)
        portNumStr = sph.get_port_to_use()
        if portNumStr == None :
            print('USB Serial Port NOT FOUND... Exiting',file=log)
            exit(-1)
        else :
            portNum = int(portNumStr)
            MPD_COMMAND_SERIAL_PORT = portNum
            DATAPORT_SERIAL_PORT    = portNum + 1
    
        MPD_COMMAND_COM_PORT = sph.get_os_serial_port_name(MPD_COMMAND_SERIAL_PORT);
        print("Using MPD COMMAND SERIAL COM:" + str(MPD_COMMAND_COM_PORT),file=log)
        turnOn(rl, ankleCon,MPD_COMMAND_COM_PORT, opt.baudrate,int(cfg.get('mfg_table','relay_power')))
        port = MPD_COMMAND_COM_PORT
    else:
        #On a Windows laptop, don't use LTF USB relay, use input option port.  
        port = opt.port
        #Wait in case we just turned on ankle. 
        print('Waiting for Ankle Bluetooth Comm to finish...', file=log)
        time.sleep(STARTUP_COMM_DELY_DUE_TO_BLUETOOTH_INIT)
        ankleCon.connect(opt.port, opt.baudrate,COMM_NO_RESPONSE_TIMEOUT_SECONDS);  
        ankleCon.flush(); #Flush anything in comport from Radio
    
    isFullMode      = opt.workflow.lower() == 'full'
    isCalbstopMode  = opt.workflow.lower() == 'calb'
    isSpringMode    = opt.workflow.lower() == 'spring'
    isTVMode        = opt.workflow.lower() == 'tv'
    if opt.workflow != 'full' and opt.workflow != 'calb' and opt.workflow != 'spring' and opt.workflow != 'tv':
        tr = False
        print('workflow unrecegnized',file=log)
        testReportStr = testReportStr + 'workflow: %s not recognized \n'%opt.workflow.lower()
    else: testReportStr = testReportStr + 'workflow: %s\n'%opt.workflow.lower()
    
    if opt.anyfw: testReportStr = testReportStr +  'Firmware check was overwritten \n'
    if not opt.move: testReportStr = testReportStr + 'calibration was move only \n'
    
    modeVector =[isFullMode,isCalbstopMode,isSpringMode,isTVMode, opt.move and not(isTVMode or isSpringMode)]
    
    print(modeVector,file=log)
    #                                        F  c  K  T  W
    #                                        u  a  3  o  r
    #                                        l  l  K  r  i
    #                                        l  b  s  q  t
    #                                        .  .  .  u  e
    #                                        .  .  .  e  .
    #                                        .  .  .  V  .
    doInishlize  = or_of_boolean_vector_and([0, 0, 0, 0, 1], modeVector)
    doCalbStop   = or_of_boolean_vector_and([1, 1, 0, 0, 0], modeVector)
    doWriteCal   = or_of_boolean_vector_and([0, 0, 0, 0, 1], modeVector)
    doTV         = or_of_boolean_vector_and([1, 1, 0, 1, 0], modeVector)
    doSpring     = or_of_boolean_vector_and([1, 0, 1, 0, 0], modeVector)
    doWriteFinal = or_of_boolean_vector_and([0, 0, 0, 0, 1], modeVector)
    
    print()
    print('docheck')
    print(doInishlize)
    print(doCalbStop)
    print(doWriteCal)
    print(doTV)
    print(doSpring)
    print(doWriteFinal)
    print()
    
    if os.name == 'nt' and doSpring :
        print('Spring values can only be calculated on a TF-002',file = log)
        exit()
    time.sleep(1)
    if not calbT.checkConnection():
        print('*'*100,file=log)
        print('unable to connect to UUT')
        exit()
    rc = robo_config.robo_config_comm(ankleCon,'SC')
    
    
    #runlevel check/set
#     print(ankleCon.do_command(ankle_commands.s_command_type(), ankle_commands.s_runlevel_set_motor()),file=log)
    #     ret = ankleCon.do_command(ankle_commands.s_command_type(), ankle_commands.s_runlevel_get())
    
#     ret = ankleCon.do_command(ankle_commands.s_command_type(), ankle_commands.s_runlevel_get())
#     print(ret,file=log)
#     while ret.get('v0') !=10:
#         ankleCon.do_command(ankle_commands.s_command_type(), ankle_commands.s_runlevel_set_motor())
#         ret = ankleCon.do_command(ankle_commands.s_command_type(), ankle_commands.s_runlevel_get())
#         print(ret)
     
    
    
    
#     run_level_set = setRunLevel(ankleCon, 10,log)
    try:
    #if True:
        if tr:
            ##################################################
            # creat cal folder and get SN for future use
            operator = getOP(log)
            SNGuess = str(calbT.getUUTSN(rc))
            SN = checkSN(SNGuess, log)
            folderName = SN+time.strftime('_calibration_%y%m%d_%H%M%S')+'_%s'%opt.workflow.lower()
            cfg.set('sys_key_shared', 'ankle_serial_number', SN)
            cfg.set('mfg_table','user_name',operator)
            if not os.path.exists(SN):
                os.mkdir(SN)
            os.mkdir(os.path.join(SN,folderName))
            log.setDir(os.path.join(SN,folderName))
            calbT.setDir(os.path.join(SN,folderName))
            if doSpring:
                cfg.set('sys_key_shared', 'dataport', '6')
            calbT.enableTable()
            calbT.setRoboConfig(rc)
            calbT.setRunMotor()
            print(rc.save_RAM_to_flash(0), file=log)
            CalibrationFileOut = os.path.join(os.getcwd(),SN,folderName,('%s_%s'%(SN,time.strftime('calbstop_tables_%y%m%d_%H%M%S.txt'))))

        ############################
        #get UUT firmware and put it in the cal file
        ########################################
        if tr:
            print('*'*75,file=log)
            print('firmware check', file=log)
            print('*'*75,file=log)
            if opt.anyfw:
                tr = True
                calbT.checkFW()
            else:
                tr = calbT.checkFW()
                testReportStr = testReportStr + 'firmware check %r\n'%tr
            if not tr: testFailed(cfg,log,'firmware check',CalibrationFileOut)
    #         fw = ankleCon.do_command('state_version', [])
    #         print(fw, file=log)
    #         cfg.set('mfg_table','fw_rev_and_CRC',fw.get('fw_rev_and_CRC'))
    #         cfg.set('mfg_table','fw_build_date',fw.get('fw_build_date'))
    #         cfg.set('mfg_table','fw_svn_revision',fw.get('svn_revision'))
        
#             CalibrationFileOut = os.path.join(os.getcwd(),SN,folderName,('%s_%s'%(SN,time.strftime('calbstop_tables_%y%m%d_%H%M%S.txt'))))
        
        if doInishlize and tr:
            #Actuator must be configured with defaults from template table:
            #toDo add check on each step
            tr1 = calbT.reseetTables()
            tr2 = calbT.saveTables()
            tr3 = calbT.timeSet()
            time.sleep(1)
            tr = tr1 and tr2 and tr3
        if tr:
            tr = ask_for_power_cycle(ankleCon, port, opt.baudrate, log)
            
        if doCalbStop and tr:
            
            com = ankleCon.do_command(ankle_commands.s_command_type(), ankle_commands.s_test_command())
            
            if tr: 
                print('*'*75,file=log)
                print('move to True North phase', file=log)
                print('*'*75,file=log)
                tr = calbT.motorPhase()
                commitConfigFileLocally(cfg, CalibrationFileOut, log)
                if not tr: testFailed(cfg,log,'move to True North phase',CalibrationFileOut)
            if tr: 
                print('*'*75,file=log)
                print('move to True North', file=log)
                print('*'*75,file=log)
                tr = calbT.moveToEnd(True)
                if not tr: testFailed(cfg,log,'move to True North',CalibrationFileOut)
            if tr :
                tr = ask_for_power_cycle(ankleCon, port, opt.baudrate, log)
            
            if tr: 
                print('*'*75,file=log)
                print('find motor offsets',file=log)
                print('*'*75,file=log)
                tr = calbT.MotorOffsets() #Motor current sensor offsets must be calibrated before motor can be commanded to phase. 
                commitConfigFileLocally(cfg, CalibrationFileOut, log)
                if not tr: testFailed(cfg,log,'find motor offsets',CalibrationFileOut)
                testReportStr = testReportStr + 'find motor offsets : %r\n'%tr
            #phase motor
            
            if tr: 
                print('*'*75,file=log)
                print('find motor phase',file = log)
                print('*'*75,file=log)
                tr = calbT.motorPhaseMiddle()
                commitConfigFileLocally(cfg, CalibrationFileOut, log)
                if not tr: testFailed(cfg,log,'find motor phase',CalibrationFileOut)
            
            if tr: 
                print('*'*75,file=log)
                print( 'find back', file = log)
                print('*'*75,file=log)
                tr = calbT.hardstopFind()
                commitConfigFileLocally(cfg, CalibrationFileOut, log)
                if not tr: testFailed(cfg,log,'find back',CalibrationFileOut)
            
            if tr: 
                print('*'*75,file=log)
                print( 'find true north',file = log)
                print('*'*75,file=log)
                tr = calbT.trueNorthFind()
                commitConfigFileLocally(cfg, CalibrationFileOut, log)
                if not tr: testFailed(cfg,log,'find true north',CalibrationFileOut)
            
            if tr: 
                print('*'*75,file=log)
                print('find polynomial relation',file = log)
                print('*'*75,file=log)
                tr = calbT.zeroTorqueFind()
                commitConfigFileLocally(cfg, CalibrationFileOut, log)
                if not tr: testFailed(cfg,log,'find polynomial relation',CalibrationFileOut) 
             
            if tr:
                testReportStr = testReportStr + 'find hardstops: %r\n'%tr
                cfg.set('sys_key_shared', 'run_level', '20')
                commitConfigFileLocally(cfg,CalibrationFileOut , log)
            
        if doWriteCal and tr:
            if not doSpring: cfg.set('sys_key_shared', 'dataport', '3')
            else:            cfg.set('sys_key_shared', 'dataport', '6')
            calbT.reseetTables()
            rc.write_all_tables_from_input_file([CalibrationFileOut], 0)
            calbT.saveTables()
            calbT.setRunAnkle()
            print(rc.save_RAM_to_flash(0), file=log)
            tr = ask_for_power_cycle(ankleCon, port, opt.baudrate, log)
            compare_fh = os.path.join(os.getcwd(),SN,folderName,('%s_%s'%(SN,time.strftime('tables_V_%y%m%d_%H%M%S.txt'))))
            c=open(compare_fh,'w')
            print(compare_fh, file=log)
            errors = rc.verify_all_tables_tango([CalibrationFileOut], 0, c)
            c.close()
            print(errors, file=log)
            if errors != 0:tr=False
            print(tr,file=log)
            
#             tr = rc.verify_all_tables_from_input_file(CalibrationFileOut, 0)
#             tr = rc.verify_tables(['sys_key_shared','AAE_cfg_shared','calb_table_shared','current_calb_shared'], CalibrationFileOut, 0, compare_fh)
            if not tr: testFailed(cfg, log, 'write cal',CalibrationFileOut)
#             ankleCon.disconnect()
            testReportStr = testReportStr + 'write cal: %r\n'%tr
        
            
        if doTV and tr:
            tr = ask_for_power_cycle(ankleCon, port, opt.baudrate, log)
        if doTV and tr:
            print('*'*75,file=log)
            print('check polynomial relation', file=log)
            print('*'*75,file=log)
            tr = calbT.zeroTorqueCheck()
            if not tr: testFailed(cfg, log, 'check polynomial relation',CalibrationFileOut)
#             ankleCon.disconnect()
            testReportStr = testReportStr + 'model verification: %r\n'%tr
            
        if doSpring and tr:
            if rl !=None: rl.turn_all_off()
            print('#'*75,file=log)
            print ('Attach A-Frame before continuing...',file=log)
            print('#'*75,file=log)
            if os.name != 'nt': promptWithNote("Operator action required. Press Enter ", rl, int(cfg.get('mfg_table','relay_buzzer')), log)
            time.sleep(5)
            prompt("Press Enter once A-Frame attached...",log)
            if rl !=None: tr =turnOn(rl, ankleCon,MPD_COMMAND_COM_PORT, opt.baudrate,int(cfg.get('mfg_table','relay_power')))
        
        if doSpring and tr:
            tr = ask_for_power_cycle(ankleCon, port, opt.baudrate, log)
        if doSpring and tr:
            print('*'*75,file=log)
            print('moving ankle to position',file=log)
            print('*'*75,file=log)
            tr = calbT.moveAFrame()
            if not tr: testFailed(cfg, log, 'moving ankle to position',CalibrationFileOut)
            testReportStr = testReportStr + 'moving ankle to position: %r\n'%tr
            
        if doSpring and tr:
            tr = ask_for_power_cycle(ankleCon, port, opt.baudrate, log)
        if doSpring and tr:
            print('*'*75,file=log)
            print('moving ankle to position',file=log)
            print('*'*75,file=log)
            tr = calbT.moveAFrame2()
            if not tr: testFailed(cfg, log, 'moving ankle to position 2',CalibrationFileOut)
            testReportStr = testReportStr + 'moving ankle to position 2: %r\n'%tr
            
        if doSpring and tr:
            print('*'*75,file=log)
            print('k2 spring calibration',file=log)
            print('*'*75,file=log)
            
            tr = calbT.findk2()
            commitConfigFileLocally(cfg, CalibrationFileOut, log)
            if not tr: testFailed(cfg, log, 'k2 spring calibration',CalibrationFileOut)
            testReportStr = testReportStr + 'k2 spring calibration: %r\n'%tr
            
        if doSpring and tr:
            print('*'*75,file=log)
            print('k3 spring calibration',file=log)
            print('*'*75,file=log)
            tr = calbT.findk3()
            cfg.set('sys_key_shared', 'dataport', '3')
            cfg.set('sys_key_shared', 'run_level','70')
            commitConfigFileLocally(cfg, CalibrationFileOut, log)
            if not tr: testFailed(cfg, log, 'k3 spring calibration',CalibrationFileOut)
            testReportStr = testReportStr + 'k3 spring calibration: %r\n'%tr
            
            
        if doWriteFinal and doSpring and tr:
            print('*'*75,file=log)
            print('coin cell check',file=log)
            print('*'*75,file=log)
            tr = calbT.checkBatteries()
            if not tr: testFailed(cfg, log, 'coin cell check',CalibrationFileOut)
            testReportStr = testReportStr + 'coin cell check: %r\n'%tr
            
        if doWriteFinal and doSpring and tr:
            rc.write_all_tables_from_input_file([CalibrationFileOut], 0)
            
            #TODO: Replace these table numbers with text table names during robo config API update...
            rc.save_RAM_to_flash(1)
            rc.save_RAM_to_flash(2)
            rc.save_RAM_to_flash(9)
            rc.save_RAM_to_flash(10)
        
        if doWriteFinal and doSpring and tr:
            calbT.setWalkAnkle()   
            rc.save_RAM_to_flash(0)
            #add reset to k3 adaptation
            calbT.reseetk3Table()
            tr = ask_for_power_cycle(ankleCon, port, opt.baudrate, log)
            if not tr: testFailed(cfg, log, 'power cycle',CalibrationFileOut)
            testReportStr = testReportStr + 'power cycle: %r\n'%tr
        if doWriteFinal and doSpring and tr:
            compare_fh = os.path.join(os.getcwd(),SN,folderName,('%s_%s'%(SN,time.strftime('tables_V_%y%m%d_%H%M%S.txt'))))
            c=open(compare_fh,'w')
            print(compare_fh)
            errors = rc.verify_all_tables_tango([CalibrationFileOut], 0, c)
            
            c.close()
            print(errors)
            if errors != 0:tr=False
            print(tr)
            if not tr: testFailed(cfg, log, 'write cal',CalibrationFileOut)
            testReportStr = testReportStr + 'write cal: %r\n'%tr
        if tr:
            print('*'*75,file=log)
            print('log check',file=log)
            print('*'*75,file=log)
            tr = calbT.ckeckLog()
            if not tr: testFailed(cfg, log, 'log check',CalibrationFileOut)
            testReportStr = testReportStr + 'log check: %r\n'%tr
        if tr:
            calbT.disableTables()
    #     if os.name != 'nt':
    #         rl.turn_all_off()
#     except calb_tests.unknownError as e:
        
    except Exception as e:
        tr = False
        print( type(e),file=log)      # the exception instance
        print( e.args,file=log)       # arguments stored in .args
        print( e,file=log)            # __str__ allows args to be printed directly
        etype, value, tb = sys.exc_info()
        n = 0
        limit = 50
        while tb is not None and (limit is None or n < limit):
            f = tb.tb_frame
            lineno = tb.tb_lineno
            co = f.f_code
            filename = co.co_filename
            name = co.co_name
            print('  File "%s", line %d, in %s' % (filename, lineno, name),file=log)
            traceback.linecache.checkcache(filename)
            line = traceback.linecache.getline(filename, lineno, f.f_globals)
            if line: print( '    ' + line.strip(),file=log)
            tb = tb.tb_next
            n = n+1
 
    finally:
        ################################
        #add safe shutdown code here
        #############################
        #     transfer cal to server
        if tr:
            testReportStr = testReportStr+'''
            
###     #    ######  ######
#  #   # #   #    #  #    #
#  #  #   #  #       #
###   ###### ######  ######
#    #     #      #       #
#    #     # #    #       #
#    #     # ######  ######
            
            '''
            cfg.set('mfg_table', 'result', 'True')
        else:
            testReportStr = testReportStr+'''
            
######   #    ### #
#       # #    #  #
#      #   #   #  #
####   #####   #  #
#     #     #  #  #
#     #     # ### #####
'''
        print('*'*100,file=log)
        print(testReportStr,file=log)
        
        log.close()
        if os.name == 'nt': 
            server = cfg.get('mfg_table', 'server_nt')
            
        else:               
            server = cfg.get('mfg_table', 'server_li')
        print("#"*100)
        print(server)
        print(os.path.exists(server))
        try:
            shutil.copytree(os.path.join(os.getcwd(),os.path.join(SN,folderName)), os.path.join(server,os.path.join(SN,folderName)))
        except:
            pass
        if os.path.exists(os.path.join(server,os.path.join(SN,folderName))):
            shutil.rmtree(os.path.join(os.getcwd(),os.path.join(SN,folderName)))
            print('files transfered to server')
        else:
            print('the calibration files were not transfered to the server')
        if os.name != 'nt':
            rl.turn_all_off()
            promptWithNote("Test completed press enter to end program", rl, int(cfg.get('mfg_table','relay_buzzer')), log)
