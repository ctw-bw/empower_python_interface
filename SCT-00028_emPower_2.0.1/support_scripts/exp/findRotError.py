'''
Created on Apr 7, 2015

@author: ekurz
'''
import glob
import calbstopAnalysis

if __name__ == '__main__':
    cals = glob.glob('\\\\pedant\\companyshare\\data\\AnkleCalibrationData\\H2_1260965\\H2_1260965_20150406_141939_full\\H2*calb.txt')
    
    for cal in cals:
        print cal
        calb = calbstopAnalysis.calbAnalysis(cal)
        calb.pollyFit()
        