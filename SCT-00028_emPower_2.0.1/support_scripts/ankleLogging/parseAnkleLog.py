"""Class to parse logs using source code structure to create template for logs and byte stream passed in. """
import os, collections, struct, datetime
from pycparser import c_ast
import c_parsing.cheader_string_formatter as header_parser

HARDCODED_ENUM_PREFIX = "LOG_RECORD_"
HARDCODED_RECORD_TYPE_PREFIX = "LOG_RECORD_"
HARDCODED_EMPOWER_TYPE_SUFFIX ="_T"
HARDCODED_EMPOWER_LOG_HEADER = "LOG_RECORD_HEADER_T"


def build_data_trees(fname_list, filepath_dependencies, base_path = os.path.dirname(__file__)):
    data_trees = {}
    full_deps = []
    for dep in filepath_dependencies:
        full_deps.append(os.path.join(os.path.abspath(base_path), dep))
    
    for fname in fname_list:
        full_fname = os.path.join(os.path.abspath(base_path), fname)
        p = header_parser.parsedHeader(full_fname, full_deps)
        data_trees[fname] = p
    return data_trees    

def generate_flattened_c_struct_formats(source_tree) :
    new_def_dict = {} 
    
    for k in source_tree.string_struct_formats.keys():
            #print k, source_tree.string_printf_formats[k]
            if not '.' in k: #complex types
                if not k.lower() in header_parser.types_d.keys(): #Ignore all predefined types like "int" and "float"
                    cformat = source_tree.string_struct_formats[k]
                    new_def_dict[k] = cformat
    return new_def_dict
    
  #Create record structure based on parser header:
    record_creator = Record_Type_Parser('LOG_RECORD_TYPE_E', source_tree.string_formats, source_tree.nodeTree, DEC_VARS = source_tree.declared_variables)
    
    return fields    

def get_empower_log_header_format(src_path):
    parsed_data = build_data_trees([src_path + 'src\\nvm\\log_records.h', src_path + 'src\\diag\\diag_log_records.h'],[src_path + '\\src', src_path + '\\inc', src_path + 'symlinked\\src\\common'])
    log_structure = parsed_data[src_path + 'src\\nvm\\log_records.h'] 
    flattened = generate_flattened_c_struct_formats(log_structure)
    return RecordTemplate(HARDCODED_EMPOWER_LOG_HEADER, -1, flattened[HARDCODED_EMPOWER_LOG_HEADER], fields = log_structure.labels[HARDCODED_EMPOWER_LOG_HEADER])
    
    
"""
Uses SVN externals mapped source code folders to build a dictionary of log record templates, accessible by type, that can be populated with bytestream from emPower ankle logs. 
"""
def build_empower_log_templates(src_path):
    parsed_data = build_data_trees([src_path + 'src\\nvm\\log_records.h', src_path + 'src\\diag\\diag_log_records.h'],[src_path + '\\src', src_path + '\\inc', src_path + 'symlinked\\src\\common'])
    log_structure = parsed_data[src_path + 'src\\nvm\\log_records.h'] 
    diag_log_structure = parsed_data[src_path + 'src\\diag\\diag_log_records.h']
    log_type_lookup = {}
    for enum in log_structure.enumeratedValues.keys():
        if enum.startswith(HARDCODED_ENUM_PREFIX):
            log_type_lookup[log_structure.enumeratedValues[enum]] = enum #We want to match data number to string.
    for enum in diag_log_structure.enumeratedValues.keys():
        if enum.startswith(HARDCODED_ENUM_PREFIX):
            log_type_lookup[diag_log_structure.enumeratedValues[enum]] = enum #We want to match data number to string.
            
    all_types_flattened = {}
    all_types_flattened.update(generate_flattened_c_struct_formats(log_structure))
    all_types_flattened.update(generate_flattened_c_struct_formats(diag_log_structure))
    
    record_templates = dict()
    for key in log_type_lookup.keys():
        type = log_type_lookup[key]+HARDCODED_EMPOWER_TYPE_SUFFIX
        if all_types_flattened.has_key(type):
            #Go through compound types to get fields, either a diag log or normal log:
                field_labels = []
                if "diag" in type.lower():
                    field_labels = diag_log_structure.labels[type]
                else:
                    field_labels = log_structure.labels[type]
                
                record_templates[key] = RecordTemplate(type, key, all_types_flattened[type], fields = field_labels)    
    return record_templates


class RecordTemplate:
    def __init__(self, name, enum, c_format, fields = []):
        self.name = name 
        self.c_format = c_format
        self.field_list = fields
        self.enum = enum


class Record:
    def __init__(self, bytes, template):
        self.bytesize = struct.calcsize(template.c_format)
        parsed = struct.unpack(template.c_format, bytes[0:self.bytesize])
        self.values = collections.OrderedDict()
        
        value_ind = 0
        
        for label_ind in range(0, len(template.field_list)):
            label = template.field_list[label_ind]
            if "[" in label and "]" in label:
                repeats = int(label[label.find("[")+ 1:label.find("]")])
                for i in range(0, repeats):
                    self.values[template.field_list[label_ind]+"_"+str(i)] = parsed[value_ind]
                    value_ind +=1
            else:
                self.values[template.field_list[label_ind]] = parsed[value_ind]
                #Add date objects. 
                if "hdr.dateTime" in template.field_list[label_ind]:
                    self.values["LogDateTime"] = '{:%Y_%m_%d %H:%M:%S}'.format(dateObj(parsed[value_ind]))
                
                if "hdr.byteCount" in template.field_list[label_ind]:
                    self.header_len = parsed[value_ind]
                value_ind +=1

    def validate(self): 
        # Check that the stored length matches what is expected 
        #todo: crc verify
        if self.bytesize == self.header_len :
            return True
        else: 
            return False
 
def dateObj(dateDelta):
    """ Returns a Python ''date'' object from time delta in seconds that starts on January 1, 2000, which is the same offset the EEPROM in the ankle uses."""
    return (datetime.timedelta(seconds=int(dateDelta))+ datetime.datetime(2000,1,1))           
 
if __name__ == "__main__" : 
    import datetime
    possible_record_templates =  build_empower_log_templates()
    print 
    header_template = get_empower_log_header_format()
    bytestream = "00001234567890abcdedddddddddddddeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeedddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddf"
    parsed_header = Record(bytestream, header_template)
    
    fake_records = []
    print possible_record_templates.keys()
    for key in possible_record_templates.keys():
        r = Record(bytestream[0:], header_template)
        r.values[r.values.keys()[0]] = key #record type is first element
        r2 = Record(bytestream[0:], possible_record_templates[r.values[r.values.keys()[0]]])
        print r2.bytesize, r2.values
        fake_records.append(r2)
    
    log_out_fname = "fake_test_records_" + ('{:%Y_%m_%d_%H%M}'.format(datetime.datetime.now()))+".csv"
    with open(log_out_fname, 'w') as f:
            for r in fake_records:
                print ",".join([(k+","+str(r.values[k])) for k in r.values.keys()])
                f.write(",".join([(k+","+str(r.values[k])) for k in r.values.keys()]))
                f.write("\n")
        