//Override stuff for the python parser of C code
#ifndef NEEDED_FOR_PARSER
#define PARSE_NEW_TABLES_FROM_EEPROM 
#define NEEDED_FOR_PARSER 'neededForParser'
#define WIN32
#define PACKED
#define DISABLE_STRAIN_GAUGE_SENSOR_CALCULATIONS
#define FAR_MEM
#define __attribute__(x)
#define __psv__
#define __prog__

/* MPD Standard data type definitions */
#ifndef MPD_TYPES
#define MPD_TYPES
typedef unsigned char  UINT8_T;
typedef unsigned short UINT16_T;
typedef unsigned long  UINT32_T;
typedef unsigned long long  UINT64_T;
typedef char           INT8_T;
typedef short          INT16_T;
typedef long           INT32_T;
typedef long long      INT64_T;
typedef unsigned char  BOOLEAN_T;
typedef float          FLOAT32_T;
typedef INT16_T        ERRCODE_T;
typedef ERRCODE_T (*FUNC_PTR_T)(void); 
#endif
#endif
