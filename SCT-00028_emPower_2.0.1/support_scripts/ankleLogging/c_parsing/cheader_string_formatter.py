""" Module that goes through a given C or H header file and flattens all declared typedefs and data structures to generate packet sizes and descriptions that can be used by the robo_config module to 
communicate between the Python scripts and the embedded system. Example usage:

>> python cheader_string_formatter.py -H ..\..\src\tables\mc_calibration_tables.c -p ..\..\src

"""


import optparse, os, sys
from pycparser import parse_file, c_ast
from pycparserext.ext_c_parser import GnuCParser
    

types_d = {};
types_d['float'] = ["f", "%f"]
types_d['double'] = ["f", "%f"]
types_d['float32_t'] = ["f", "%f"]
types_d['int'] = ["h", "%d"]
types_d['int16_t'] = ["h", "%d"]
types_d['unsigned int'] = ["H", "%d"]
types_d['uint16_t'] = ["H", "%d"]
types_d['long'] = ["l", "%d"]
types_d['int32_t'] = ["l", "%d"]
types_d['unsigned long'] = ["L", "%d"]
types_d['uint32_t'] = ["L", "%d"]
types_d['int64_t'] = ["q", "%d"]
types_d['long long'] = ["q", "%d"]
types_d['uint64_t'] = ["Q", "%d"]
types_d['unsigned long long'] = ["Q", "%d"]
types_d['uint8_t'] = ["B", "%d"]
types_d['int8_t'] = ["b", "%d"]    
types_d['char'] = ["B", "%c"]
types_d['unsigned char'] = ["B", "%c"]
types_d['byte'] = ["B", "%c"]
types_d['boolean_t'] = ["?", "%d"]
types_d['_bool'] = ["?", "%d"]
types_d['errcode_t'] = ["h", "%d"]
types_d['_int'] = ["h", "%d"]
types_d['_int16'] = ["h", "%d"]
types_d['_uint16'] = ["H", "%d"]
types_d['_int32'] = ["l", "%d"]
types_d['_uint32'] = ["L", "%d"]
types_d['_int64'] = ["q", "%d"]
types_d['_uint64'] = ["Q", "%d"]
types_d['_uint8'] = ["B", "%d"]
types_d['_int8'] = ["b", "%d"]         
types_d['void'] = ["", ""]

def header_parser_options(args) :

    usage = "usage: %prog [options]"
    parser = optparse.OptionParser();

    parser.add_option("-p", "--path", dest="firmware_path", type="str", action="append", help = "path(s) to the source directory. Can repeat the -p");
    parser.add_option("-H", "--header_file", dest="header_file_list", action="append", type="str", help = "header file to parse.  Can repeat the -H");

    (options, args) = parser.parse_args(args);

    class struct : #placeholder for options
        pass

    opt = struct();
    for k,v in options.__dict__.iteritems() :
        opt.__dict__[k] = v
        # print k,v

    return opt

CPPPATH='arm-none-eabi-gcc' 
PARSING_HEADER = os.path.abspath(os.path.dirname( __file__ )) + (r'/header_parser_utils/fake_libc_include')
DIRECTORY_IGNORE_LIST = ['.SVN','_SVN','OLD', 'DEBUG', 'EXCLUDED','DOCUMENTATION', 'PYTHON_SCRIPTS', '.METADATA', '.GIT', 'PRISTINE'];
        
"'Parses a header file using the pycparser library and returns a node tree of the parsed header file.'"
def get_node_tree(filename, INCLUDES=[], quiet=False, overridden_defs=[]):
    always_overridden =['-E',r'-D_WIN32=1', r'-DNEEDED_FOR_PARSER=neededForParser',r'-DPACKED=', r'-DDISABLE_STRAIN_GAUGE_SENSOR_CALCULATIONS=',r'-DFAR_MEM=',r'-DSemaphoreHandle_t=void *']
    included_options = []
    fileDir = os.path.dirname(filename)
    roots = []
    for i in [PARSING_HEADER] + INCLUDES: 
        dirPath = os.path.abspath(i)
        if dirPath not in included_options:
            included_options.append(r'-I'+dirPath)
        for root, dirs, files in os.walk(dirPath):
            for d in dirs:    
                up_include = d.upper();
                if up_include in DIRECTORY_IGNORE_LIST or os.path.basename(root).upper() in DIRECTORY_IGNORE_LIST:
                    if not quiet:
                        print "Excluding:", root, up_include
                else :
                    included_file = r'-I'+os.path.abspath(root + os.path.sep + d)
                    if included_file not in included_options:
                        included_options.append(included_file)
    
    if not quiet:                
        print "HEADER_PARSER.PY : number of include directories : ", len(included_options)
        if len(included_options) > 20 :
            print
            print "WARNING : possibly too many include directories"
            print
        
    
    cpp_arg_list =  always_overridden +  included_options + overridden_defs  
    if not quiet:
        print "File to parse:", filename
        print "CPPPATH=", CPPPATH
        for arg in cpp_arg_list:
            print arg
    
    
    ast = parse_file(filename, parser=GnuCParser(), use_cpp=True,
            cpp_path=CPPPATH,
            cpp_args=cpp_arg_list
            )
   
    
    if not quiet:                
        ast.show
      
    return ast
    
    
    
"' Subclass of c_ast.NodeVisitor that visits all typedef declaration nodes and creates a list of typedefs with their flattened structures'"
class build_typedef_strings_visitor(c_ast.NodeVisitor):
    def __init__(self):
        c_ast.NodeVisitor.__init__(self)
        self.enumTypedefs = [] # Dictionary that keeps us from thinking that an enum is a compound type. 
        self.enumeratedValues = {} # #Dictionary mapping all constants and enums to a value so that any array declared with one of them can be sized correctly. 
        self.typedefFlattenedDict = {}
        self.typedefFlattenedStringFormat = {}
        self.typedefTreeLevels = {} #Dictionary mapping all compound types to a list of their children in order. 
        
        self.prefix = ''; #Recursive prefix for building a namespace to nested variables: stuct.subelement.subsubelement, etc. 
        self.array_suffix = ''; #Since an array declaration is higher up on a tree branch than a type, we can store how many of a type we need for the name here: struct.element[5] e.g.
        self.array_size =1 
        self.last_enum_defined = 0 #Each loop through an enumerated declaration will add to the size of an enum so we can collect the actual number values of the enums.
        self.quiet = True
    
    def visit_FuncDecl(self, node):
        return; #don't go further down the tree in function call.
    
    def visit_Enum(self, node):
        if (self.prefix in self.typedefFlattenedDict.keys()):
            return
        
        #visit children so we can get the values of the enum:
        self.last_enum_defined= 0 
        c_ast.NodeVisitor.visit(self, node.values)
        
        #Enums are all formatted as ints
        self.typedefFlattenedDict[self.prefix] = types_d['int'][0]
        self.typedefFlattenedStringFormat[self.prefix] = types_d['int'][1]
        self.enumTypedefs.append(self.prefix)
    
    def visit_Enumerator(self, node): #Note: in this parlance, an "Enumerator" is a specific value inside an "enum"
        #print "Enumerated value:",  node.value
        if node.value is not None:
            self.last_enum_defined = int(node.value.value, 0) #Enum value was defined in code
        
        self.enumeratedValues[node.name] = self.last_enum_defined
        self.last_enum_defined +=1; #Increment for next time
        
    def visit_ArrayDecl(self, node):
        if node.dim == None: #array of undeclared size can't be put into a flattened structure so just return. 
            return 
       
        c_ast.NodeVisitor.visit(self, node.dim) #visit children so we can get the dimensions of this array. 
       
        #see if we've declared the array to be a fixed size or an enumerated size:
        if ("name" in node.dim.__dict__):
             node.dim.value = str(self.enumeratedValues[node.dim.name])
        
        self.array_suffix += "["+ node.dim.value + "]"
        self.array_size = self.array_size*int(node.dim.value)
        
        c_ast.NodeVisitor.visit(self, node.type)
            
        self.array_suffix = self.array_suffix.rstrip("["+ node.dim.value + "]")
        self.array_size = self.array_size/int(node.dim.value)
        
        
    def visit_IdentifierType(self, node): #collection of raw types that can be flattened by iterating over its contents.
        n = ' '.join(node.names)
        if n.lower().strip() in types_d.keys():
            self.typedefFlattenedDict[n] = types_d[n.lower().strip()][0]
            self.typedefFlattenedStringFormat[n] = types_d[n.lower().strip()][1]
            
        else:
            c_ast.NodeVisitor.generic_visit(self, node) #visits the children
            if not self.quiet: print "WARNING: Did not add ", self.prefix, n
    
    def visit_TypeDecl(self, node):
        if node.declname == None:
            return #not a type we need to track. 
    
        init_prefix = self.prefix
        decl_name = node.declname + self.array_suffix
        full_name = decl_name
        if len(init_prefix) != 0: 
            full_name = init_prefix  + '.' + full_name
            
        self.prefix = full_name
        c_ast.NodeVisitor.generic_visit(self, node) #visits the children
        self.prefix = init_prefix #take off what we appended at this level. 
    
    
        if isinstance(node.type, c_ast.IdentifierType): #flattened value of this TypeDecl is just the value of the IdentifierType
            subtype = (' '.join(node.type.names))
            array_prefix = ''
            if self.array_size != 1: array_prefix = str(self.array_size)
            self.typedefFlattenedDict[full_name] =  array_prefix + self.typedefFlattenedDict[subtype]
            self.typedefFlattenedStringFormat[full_name] = self.typedefFlattenedStringFormat[subtype] * self.array_size
            self.typedefTreeLevels[full_name] = [subtype] * self.array_size
            #if the prefix itself was a type , append what we just added at a lower level to the higher level flat type 
            if (init_prefix != ''):
                #Get any already created flattened typedefs and propagate them up the tree by appending them to the type:
                flat_typedef = [decl_name]
                if full_name in self.typedefTreeLevels.keys() and self.typedefTreeLevels[full_name][0] not in types_d.keys(): #compound type needs to be flattened. 
                    type_to_flatten = self.typedefTreeLevels[full_name][0]
                    if (type_to_flatten not in self.enumTypedefs): #enums are types, but not compound types, so we don't travel down the tree. 
                        flat_typedef = []
                        for a in self.typedefTreeLevels[type_to_flatten]:
                            flat_typedef.append(decl_name+"."+a)
                        
                if init_prefix not in self.typedefFlattenedDict.keys():
                    self.typedefFlattenedDict[init_prefix] = self.typedefFlattenedDict[full_name]
                    self.typedefFlattenedStringFormat[init_prefix] = self.typedefFlattenedStringFormat[full_name]
                    self.typedefTreeLevels[init_prefix] = flat_typedef
                    
                else:
                    self.typedefFlattenedDict[init_prefix] = self.typedefFlattenedDict[init_prefix] + self.typedefFlattenedDict[full_name]
                    self.typedefFlattenedStringFormat[init_prefix] = self.typedefFlattenedStringFormat[init_prefix] + self.typedefFlattenedStringFormat[full_name]
                    self.typedefTreeLevels[init_prefix] = self.typedefTreeLevels[init_prefix] + flat_typedef
                    
            
        
        

    
class parsedHeader:
    def __init__(self, header_file_to_parse, source_path):
        ast = get_node_tree(header_file_to_parse, INCLUDES=source_path, quiet=True, overridden_defs=[])
        f = build_typedef_strings_visitor()
        f.visit(ast)
        self.string_struct_formats = f.typedefFlattenedDict
        self.labels = f.typedefTreeLevels
        self.string_printf_formats = f.typedefFlattenedStringFormat
        self.typedefTreeLevels = f.typedefTreeLevels
        self.enumeratedValues = f.enumeratedValues
        
    def packed_format_string(self, typeName):
        if self.string_struct_formats.has_key(typeName):     
            return self.string_struct_formats[typeName]
        else:
            return None;     
    
            
    def declared_variable_names(self):
       l = []
       for node in self.declared_variables:
         l.append(node.name)
       return l  
    
    
if __name__ == "__main__":
    opt = header_parser_options(sys.argv)
    p = parsedHeader(opt.header_file_list[0],opt.firmware_path )
    for k in p.string_struct_formats:
        print k, p.string_struct_formats[k]
    