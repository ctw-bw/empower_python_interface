#!/usr/bin/env python

import argparse, serial, os, sys, struct, datetime #System libraries 
import pickle
import parseAnkleLog

def int_any_fmt(x):
    return int(x, 0)

class command_line_options :
    class struct :
        pass
      
    #helper for options so we can send cmd line arguments as hex or binary if we want. 
    def int_any_fmt(x):
            return int(x, 0)
            
    def __init__(self) :
        parser = argparse.ArgumentParser();
        parser.add_argument("-m", dest="make_decoder_file", type=str, default="LogTemplate_v2x", help="Parse the C code to create a new log decoder file with the name specified.");
        parser.add_argument("-P", "--PATH", dest="source_path", type=str, default="..\\..\\", help="Path to source code.");
        parser.add_argument("-D", "--debug", dest="debug_level", type=int_any_fmt, default=0, help="set debug level, not necessarly consistently implmented or used");
        self.args = parser.parse_args();
        
        # make a struct from the options dictionary
        opt = command_line_options.struct();
        for k,v in self.args.__dict__.iteritems() :
            opt.__dict__[k] = v
            # print k,v
            self.opt = opt;

        return  

    def __str__(self) :
        return class_to_str(self)

def createDecoderFile(filename, src_path):
    hdr_template = parseAnkleLog.get_empower_log_header_format(src_path)
    record_templates = parseAnkleLog.build_empower_log_templates(src_path)
    
    print "\nLog Entries: " + ",".join([(str(k)) for k in record_templates.keys()])
        
    with open(filename, 'wb') as handle:
        pickle.dump(record_templates, handle, protocol=pickle.HIGHEST_PROTOCOL)
        pickle.dump(hdr_template, handle, protocol=pickle.HIGHEST_PROTOCOL)
        handle.close()
    
    print "\nFile created: ", filename
        

if __name__ == "__main__" :

    opt_parser = command_line_options();
    opt = opt_parser.opt;

    createDecoderFile(opt.make_decoder_file, opt.source_path)
   
    