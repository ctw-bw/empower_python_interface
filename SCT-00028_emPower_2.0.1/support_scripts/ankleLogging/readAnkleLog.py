#!/usr/bin/env python

import argparse, serial, os, sys, struct, datetime #System libraries 

import python_paths_default     # test for user's python_paths.py and update paths
import python_paths as pyenv
from serial import serial_for_url
from collections import OrderedDict
from sys import stdout
sys.pfcmd_do_not_import_support = True;

import robo_config
import comm_lib.pfcmd as pfcmd
import comm_lib.pfcmd_ctrl as pfcmd_ctrl

import pickle
import parseAnkleLog
from python_utils.ByteStream import ByteStream

EMPOWER_LOG_TABLE_ABSOLUTE_ADDR = 0x10000
EMPOWER_LOG_DESCRIPTION_TABLE = "logFileSharedObj"
EMPOWER_LOG_PROPERTY_FILE_ADDR = "startAddress"
EMPOWER_LOG_PROPERTY_FILE_SIZE = "fileSize"
EMPOWER_LOG_FILE_BLOCK_SIZE = 128
EMPOWER_LOG_MEM_DESIGNATION = 4 # From source code comm/comm_mpdcmds_v2.h: HOSTCMD_SC_USR_FLASH_MEM_TYPE = 4

TABLE_HEADER_SIZE = 12

STREAMING_MEMORY_BLOCK_SIZE = 128
STREAMING_MEMORY_HEADER_SIZE = 12
STREAMING_MEMORY_SPECIAL_CMD = 507

EMPOWER_STREAMING_MEMORY_START = 0
EMPOWER_STREAMING_MEMORY_END = 0x80000 

def int_any_fmt(x):
    return int(x, 0)

class command_line_options :
    class struct :
        pass
      
    #helper for options so we can send cmd line arguments as hex or binary if we want. 
    def int_any_fmt(x):
            return int(x, 0)
            #
    def __init__(self) :
        parser = argparse.ArgumentParser();
        parser.add_argument("-B", dest="device_string", type=str, help="MAC or Linux comm port device name");
        parser.add_argument("-i", "--ip_address", dest="ip_address", help="Connect to robo via TCP to specified address or DNS name");
        parser.add_argument("-p", "--com_port", dest="com_port", type=int_any_fmt, help="communcation port for serial communcation");
        parser.add_argument("--LIN", dest="LIN_mode", action="store_true", default=False, help="Use with LIN mode comm: ignores the local echo on the RX line of whatever command was just sent out.")
        parser.add_argument("--comm_timeout_seconds", dest="timeout", default=0.5, help="serial or TCP communcations timeout (sec)");
        parser.add_argument("-L", "--log_filename", dest="bin_filename", type=str, default=None, help="Input log file (bin) to be precessed, if COMPORT is not specified.");
        parser.add_argument("-s", "--output_subdir", dest="sub_dir_name", type=str, default="LogFiles", help="Destination sub-directory for output files. Will be created if it doesn't exist.");
        parser.add_argument("-b", "--baud_rate", dest="baudrate", type=int_any_fmt, default=115200, help="baudrate for serial communcation.  default is 115.2kbaud");
        parser.add_argument("-t", "--log_template", dest="decoder_ring", type=str, default="LogTemplate_v210", help="Decoder template for log file.");
        parser.add_argument("-m", dest="multiple_files", action="store_true", default=False, help="Creates multiple CSV output files instead of a single consolidated file.");
        parser.add_argument("-D", "--debug", dest="debug_level", type=int_any_fmt, default=0, help="set debug level, not necessarly consistently implmented or used");
        parser.add_argument("--log_mem_type", dest="log_mem_type", type=int_any_fmt, default=EMPOWER_LOG_MEM_DESIGNATION, help="Set MEM_TYPE to be read out. Defaults to 4 (FLASH).")
        
        self.args = parser.parse_args();
        
        # make a struct from the options dictionary
        opt = command_line_options.struct();
        for k,v in self.args.__dict__.iteritems() :
            opt.__dict__[k] = v
            # print k,v
            self.opt = opt;

        return  

    def __str__(self) :
        return class_to_str(self)

class createLogFileTable():
    """Class that creates a log file table by reading the table entries from the stream passed in
       The first byte in the stream passed in is the base address of the log file table."""
    def __init__(self, raw_log_bytes):
        self.files = OrderedDict()
        self.files["STARTUP"]  = dict()
        self.files["DIAG"]     = dict()
        self.files["CALENDAR"] = dict()
        self.files["WALKING"]  = dict()
        num_def = len(self.files)
        
        # Skip the table header
        table_item_addr = TABLE_HEADER_SIZE 

        # Read the number of iles
        self.num_files = int(self.calc_32bit(raw_log_bytes[table_item_addr:]))
        table_item_addr +=2
       
        # If there are more files than we setup above, create addional generic tables
        if self.num_files > num_def:
            for i in range(num_def, self.num_files):
                self.files["LOG_" + str(i+1)] = dict()
        
        #Get the start address and size of each table
        for f in self.files:
            self.files[f][EMPOWER_LOG_PROPERTY_FILE_ADDR] = self.calc_32bit(raw_log_bytes[table_item_addr:])
            self.files[f][EMPOWER_LOG_PROPERTY_FILE_SIZE] = self.calc_32bit(raw_log_bytes[table_item_addr+4:])
            table_item_addr += 8

    #Converts 4 bytes into a single string value
    def calc_32bit(self, input):
        value = 0
        for idx in range(0, 4):
            value *= 256
            value += ord(input[3 - idx])
        
        return str(value)

      
class RawLogFile: 
    """Class that provides access to an EEPROM log contained in a file."""
    
    def __init__(self, logfile_name):
        chunksize=128
        s = ''
        with open(logfile_name, "rb") as f:
            while True:
                chunk = f.read(chunksize)
                if chunk:
                    s = s + chunk
                    #for b in chunk:
                    #    print '0x%02x ' % ord(b)
    
                else:
                    break
        self.binary_buffer = s
        
        self.datestring = datetime.datetime.fromtimestamp(os.stat(logfile_name).st_ctime).strftime("%Y%m%d_%H%M%S")
        
        # print "Creating step, diagnostic, and event logs:"
        #[parser_options.values.mem_address:parser_options.values.mem_address + parser_options.values.buffer_size]
        #Old log  file format: IWALKANKLE_IW_BIOM_<ANKLEID>_eeprom_<DateString>
        #Newer Log file format: H2_<4 digit serial num>_<date>_<time>_<descriptor>.<extension>
        name = os.path.basename(logfile_name).split("_")
        self.ankleId = None
        self.ankleLogDate = None
        if name[0] == 'H2':
            self.ankleId = name[1]
            self.ankleLogDate = "_".join(name[2:4])
        elif name[0] == 'IWALKANKLE':
            self.ankleId = name[3]
            self.ankleLogDate = "_".join(name[5:])
        else:
            self.ankleId = name[0]
            self.ankleLogDate = self.datestring  
    

def parseLog(bytestream, decoder_ring_file_name):
    """Parses a byte stream using the template file passed in.
       The template file must exist."""

    with open(decoder_ring_file_name, 'rb') as handle:
        possible_record_templates = pickle.load(handle)
        header_template = pickle.load(handle)

    records = []
    
    byte_num = 0
    num_records_found = 0
    
    length_of_header = 0
    while (byte_num < len(bytestream) - length_of_header): #Dont try to parse the last 12 bytes of the  

        r = parseAnkleLog.Record(bytestream[byte_num:], header_template)
        
        # length of header is 12, but let's use what we got from parsing.
        length_of_header = r.bytesize
        
        possible_record_type = r.values[r.values.keys()[0]]
        poss_rec_len = r.values[r.values.keys()[1]]
        poss_rec_crc = r.values[r.values.keys()[2]]
        
        #print possible_record_type
        #print poss_rec_len
        
        if possible_record_type in possible_record_templates: 
            r2 = parseAnkleLog.Record(bytestream[byte_num:], possible_record_templates[possible_record_type])
            
            if True == r2.validate():
                records.append(r2)
                byte_num += poss_rec_len
                num_records_found += 1
            else:
                byte_num += 1
        else :
            #print poss_rec_len
            #print possible_record_type, r2.bytesize
            byte_num += 1
    
    return records, possible_record_templates

def writeRecordsToFile(records, templates, file_handle):
    """Writesa to an opened file a list of log records.
       For each recod type included in the list, writes ONE record template header."""
       
    records_headers_written = dict()
    records_headers_written.clear()

    num = 0
    for r in records:
        first_done = False
        
        record_num = r.values["hdr.recordType"]
        record_type = str(record_num)
        
        # Add record header lines to the output file
        # Only add header if it hasn't been included yet
        if not records_headers_written.has_key(record_type):
            records_headers_written[record_type] = dict()

            # One line with event number and name
            file_handle.write(record_type)
            file_handle.write(","+templates[record_num].name)
            file_handle.write("\n")

            # One line with event record headers
            file_handle.write(record_type)

            for ev_header in r.values.keys():
                if first_done:
                    file_handle.write(","+ev_header)
                
                first_done = True
                
            file_handle.write("\n")

        # Now the record values
        file_handle.write(",".join([(str(r.values[k])) for k in r.values.keys()]))
        file_handle.write("\n")

        num += 1
    return num

def getSerialNumber(ankle_connection):
    ctrl = pfcmd_ctrl.controller(ankle_connection);
    
    serial_number = ctrl.special(4006, [0]);
    
    return serial_number['v0']


def dumpMemFromAnkle(ankle_connection, mem_type, start_addr, length):
    """
    Execute a streaming memory dump from the ankle.
    The routine sends special command to initiate download, and then waits for the requested number of bytes to be received.
    Returns the byte stream.
    """
    
    ctrl = pfcmd_ctrl.controller(ankle_connection);
    
    result = ctrl.special(STREAMING_MEMORY_SPECIAL_CMD, [mem_type, start_addr, start_addr+length]);

    ln = ''
    byte_stream = ''     
    sp = pf.comm_device
    
    num_read = 0
    
    packet_size = STREAMING_MEMORY_HEADER_SIZE + STREAMING_MEMORY_BLOCK_SIZE
    
    progress_last = 0;
    progress = 0;
    while num_read < length :
        ln = ln + sp.read(packet_size)

        ind = ln.find('\xDD\xE0')
        data_idx = ind + STREAMING_MEMORY_HEADER_SIZE
        if len(ln) >= packet_size + ind:
            
            byte_stream += ln[data_idx :data_idx + STREAMING_MEMORY_BLOCK_SIZE]
            ln = ln[ind + packet_size:]
        else :
            print "*", 
        
        num_read += STREAMING_MEMORY_BLOCK_SIZE 

        progress = ((num_read*100.0)/length)
        if (progress - progress_last) > 1:
            stdout.write("\r        Progress: %02d%%  " % round(progress))
            stdout.flush()
            progress_last = progress
    
    stdout.write("\r        Progress: 100%  \n")
    stdout.flush()
    
    return byte_stream

def logFileHandler(log_file_obj, all_log_bytes):
 
    for key in log_file_obj:
        print (" " * (16-len(key))) + key + ": " + hex(int_any_fmt(log_file_obj[key]))

    addr = int_any_fmt(log_file_obj[EMPOWER_LOG_PROPERTY_FILE_ADDR][0:])
    total_read_length = int_any_fmt(log_file_obj[EMPOWER_LOG_PROPERTY_FILE_SIZE][0:])
    
    fileBytes = all_log_bytes[addr:addr+total_read_length]
    
    logfile_records, logfile_templates = parseLog(fileBytes, opt.decoder_ring)
    return logfile_records, logfile_templates
 
def readLogFile(ankle_connection, log_property_dict, hex_output_fname = None, memory_type = EMPOWER_LOG_MEM_DESIGNATION):
    """
    Returns log in bytestream format. Returns None if communication errors prevented entire bytestream from being read. 
    """
    addr = int(log_property_dict[EMPOWER_LOG_PROPERTY_FILE_ADDR][0])
    total_read_length = int(log_property_dict[EMPOWER_LOG_PROPERTY_FILE_SIZE][0])
    min_addr = addr
    
    hex_output = None
    if (hex_output_fname != None and hex_output_fname != ""):
        hex_output = open(hex_output_fname, 'wb');
    
    ctrl = pfcmd_ctrl.controller(ankle_connection);
    block_length = EMPOWER_LOG_FILE_BLOCK_SIZE;
    
    last_addr = 0;
    trans_str = ""
    whole_bytestream = ''
    retries = 0;
    progress_last = 0;
    progress = 0;
    for buf_ndx in range(0, total_read_length, block_length) :
        if retries > 10:
            break;
        read_length = min(total_read_length-buf_ndx, block_length)
        
        result = ctrl.readwrite_mem('R', memory_type, read_length, addr, QUIET=True)
        if result is None:
            retries +=1
            
        if result and len(result) :
            if "ERROR" in result:
                print
                print "do_command:error:", result
                print                
                retries +=1
                continue
            elif "ecode" in result[0]: 
                retries +=1
                continue
                 
            else :
                log_bytes = result[0]['buff']
                if read_length > len(log_bytes): #We read out valid packet, but didn't get all the bytes we asked for. 
                    retries +=1
                    continue
                
                if (hex_output != None):
                    hex_output.write(log_bytes)
                whole_bytestream += log_bytes #process whole log at the end.
                #print str(bytearray(log_bytes)).encode('hex').upper()
                addr += len(log_bytes)
                progress = ((addr-min_addr)*100.0/total_read_length)
                if (progress - progress_last) > 1:
                    stdout.write("\r        Progress: %02d%%  " % round(progress))
                    stdout.flush()
                    progress_last = progress
    stdout.write("\r        Progress: 100%  \n")
    stdout.flush()
    
    if (hex_output != None):
        hex_output.close();    

    if retries <= 10:
        return whole_bytestream
    else:
        return None
 
def listLogFiles(ankle_connection):
    configurator = robo_config.robo_config_comm(ankle_connection)
    configurator.disable_imu()

    #Read out log file contents:
    log_descriptor = configurator.read_tables([EMPOWER_LOG_DESCRIPTION_TABLE], opt.debug_level)
    log_files = OrderedDict()
    for file_property in log_descriptor.section_dict[EMPOWER_LOG_DESCRIPTION_TABLE].keys():
        file_name = file_property.split(".")[0]
        file_attr = ".".join(file_property.split(".")[1:])
        
        if not log_files.has_key(file_name):
            log_files[file_name] = dict()
        
        log_files[file_name][file_attr] = log_descriptor.section_dict[EMPOWER_LOG_DESCRIPTION_TABLE][file_property]
    #print log_files
    
    return log_files
        
    
if __name__ == "__main__" :
    
    opt_parser = command_line_options();
    opt = opt_parser.opt;

    pf = pfcmd.PFCmd();

    # Check for template file
    if not os.path.exists(opt.decoder_ring):
        print "\nTemplate file \"%s\" does not exist!!!" % opt.decoder_ring
        sys.exit()
        
    # Make destination sub-directory if it does not exist
    if not os.path.exists(opt.sub_dir_name):
        os.mkdir(opt.sub_dir_name)
    
    # First check if we have connection option specified.
    # If so, we will be reading the logs directly from the ankle
    if opt.device_string :
        dev = os.path.join("/dev", opt.device_string);
        print dev
        pf.connect(dev, opt.baudrate);
        pf.set_serial_timeout(opt.timeout);
        direct_connect = True
    elif opt.com_port :
        print "SERIAL"
        pf.connect(opt.com_port, opt.baudrate);
        pf.set_serial_timeout(opt.timeout);
        direct_connect = True
    elif os.name != 'nt':
        print 'SERIAL LINUX'
          # Discover the USB Serial Ports to use
        sph = serial_port_helper.serial_port_helper()
          # Note: detects PSER movement from 0,1 to 2,3 (Linux)
        portNumStr = sph.get_port_to_use()
        if portNumStr == None :
            print('USB Serial Port NOT FOUND... Exiting')
            exit(-1)
        else :
            portNum = int(portNumStr)
            MPD_COMMAND_SERIAL_PORT = portNum
         
        MPD_COMMAND_COM_PORT = sph.get_os_serial_port_name(MPD_COMMAND_SERIAL_PORT);
        print("Using MPD COMMAND SERIAL COM:" + str(MPD_COMMAND_COM_PORT))
        port = MPD_COMMAND_COM_PORT
        pf.connect(port, opt.baudrate);
        pf.set_serial_timeout(opt.timeout);

    elif opt.bin_filename != None:
        
        if not os.path.isfile(opt.bin_filename):
            print "\nFile \"%s\" does not exist!!!" % opt.bin_filename
            sys.exit()
        
        logfile_name = opt.bin_filename

        # We have a binary file specified
        # Read the raw log file
        rawLog = RawLogFile(logfile_name)
        log_bytes_to_parse = rawLog.binary_buffer

        serial_num = rawLog.ankleId
        file_date_str = rawLog.ankleLogDate

        logsToRead = createLogFileTable(log_bytes_to_parse[EMPOWER_LOG_TABLE_ABSOLUTE_ADDR:])
    
        print "\nTable File Count: ",  logsToRead.num_files
        print " "
    
        # Option for individual files
        if opt.multiple_files:
            for desiredFile in logsToRead.files:
        
                print "\n[" + desiredFile + "]"
                records, rec_templates = logFileHandler(logsToRead.files[desiredFile], log_bytes_to_parse)
                
                log_out_fname = str(serial_num) + "_log_file_" + desiredFile +  "_" + file_date_str + ".csv"
                log_out_fname = opt.sub_dir_name + os.sep + log_out_fname
    
                with open(log_out_fname, 'w') as f:
                    num_records = writeRecordsToFile(records, rec_templates, f)
                    
                print "   Records Found:", num_records
                print "Log File Created:", log_out_fname
            
        #Default file option: a single consolidated file
        else:
            
            log_out_fname = str(serial_num) + "_log_file_" + file_date_str + ".csv"
            log_out_fname = opt.sub_dir_name + os.sep + log_out_fname
    
            with open(log_out_fname, 'w') as f:
                num_records = 0
                
                for desiredFile in logsToRead.files:
                    
                    print "\n[" + desiredFile + "]"
                    records, rec_templates = logFileHandler(logsToRead.files[desiredFile], log_bytes_to_parse)
     
                    num_records += writeRecordsToFile(records, rec_templates, f)
                
            print 
            print "   Records Found:", num_records
            print "Log File Created:", log_out_fname

        sys.exit()
    else:
        print "\nYou must specify a COMPORT (-p) or a LOGFILE (-L)."
        sys.exit()
    
    
    # Direct-connection download
    if opt.LIN_mode:
        pf.ignore_rx_of_local_echo = True;
        pf.quiet = True

    serial_num = getSerialNumber(pf)

    logsToRead = listLogFiles(pf)
    del logsToRead["robo_hdr"]
    del logsToRead["fileCount"]

    log_out_fname = str(serial_num) + "_log_file" + ('_{:%Y%m%d_%H%M%S}'.format(datetime.datetime.now()))+".csv"
    log_out_fname = opt.sub_dir_name + os.sep + log_out_fname
    
    with open(log_out_fname, 'w') as f:

        num_records = 0
        for desiredFile in logsToRead:

            print "\n[" + desiredFile + "]"
            for key in logsToRead[desiredFile]:
                print (" " * (16-len(key))) + key + ": " + hex(logsToRead[desiredFile][key][0])

            fileBytes = readLogFile(pf, logsToRead[desiredFile])
            records, record_templates = parseLog(fileBytes, opt.decoder_ring)
            #log_out_fname = str(serial_num) + "_" + desiredFile + ('_{:%Y%m%d_%H%M%S}'.format(datetime.datetime.now()))+".csv"

            records_hdr_wr = dict()
            first_done = False

            for r in records:
                first_done = False
                num_records += 1
                
                record_num = r.values["hdr.recordType"]
                record_type = str(record_num)
                
                # Add record header lines to the output file
                if not records_hdr_wr.has_key(record_type):
                    records_hdr_wr[record_type] = dict()

                    # One line with event number and name
                    f.write(record_type)
                    f.write(","+record_templates[record_num].name)

                    # One line with event record headers
                    f.write("\n")
                    f.write(record_type)
                    for ev_num in r.values.keys():
                        if first_done:
                            f.write(","+str(ev_num))
                        
                        first_done = True
                        
                    f.write("\n")

                # Now the record values
                f.write(",".join([(str(r.values[k])) for k in r.values.keys()]))
                f.write("\n")

    print
    print "   Records Found:", num_records
    print "Log File Created:", log_out_fname
