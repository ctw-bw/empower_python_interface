import time

class ErrorLogger :
    def __init__ (self, fname) :
        self.error_log_fw = open(fname, 'a');
        self.log("Open %s" % fname);
        # self.last_str = ""
        self.last_t_str = ""                
        self.intr = 0;
        print self.last_str


    def log(self, str) :
        t_str = time.strftime("%b%d %y %H:%M:%S  ", time.localtime());
        """
            if (str == self.last_str) :
                self.iter = self.iter + 1;
            else:
                if (self.iter) :
                    w_str = "%s: last message repeated %d times\n" % (t_str, self.iter);
                    self.error_log_fw.write(w_str);                    
                self.iter = 0;
        """
        self.last_str = str;
        
        w_str = "%s:%s\n" % (t_str, str)
        self.error_log_fw.write(w_str);
        self.error_log_fw.flush();        
        
    def write(self, str) :
            self.last_str = str;
            self.error_log_fw.write(str + "\n");
            self.error_log_fw.flush();        

    def close(self) :
        self.error_log_fw.close();

