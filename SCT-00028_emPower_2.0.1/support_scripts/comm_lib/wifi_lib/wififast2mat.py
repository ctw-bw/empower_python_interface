

import sys
import os
import glob
import optparse
import traceback

    
import load_wififast_data

import zipfile
try:
  import scipy.io
except:
  print 'WARNING: YOU DO NOT HAVE SCIPY INSTALLED. MATLAB EXPORT FUNCTIONS WILL NOT WORK UNTIL YOU INSTALL SCIPY'
  


def file_exists(fname) :
    try :
        fin = open(fname)
        fin.close()
        rcd = True
    except :
        rcd = False;

    return rcd;


def process_list(file_list) :
        
    for fname in file_list :
        head, tail = os.path.split(fname)
        base, ext = os.path.splitext(tail)
        # print
        if ext.upper() == ".TXT" :
            try :
                fin = open(fname,'r')
                rcd = is_wififast_txt_file(fin);
                fin.close()                                                    
                if not rcd :
                    print "not wififast file.  skip : ", fname
                else :
                    mat_fname  = os.path.join(head, base +  ".mat");                    
                    if not file_exists(mat_fname) :
                        fin = open(fname,'r')                    
                        print "processing :",fname,
                        data = load_wififast_data.load_wififast_data(fin)
                        fin.close()                                    
                        print "        saveing as : ", mat_fname
                        scipy.io.savemat(mat_fname, {'data':data}, long_field_names=True, oned_as='row');
                    else :
                        print "skiping as mat file exists :", mat_fname
                
            except :
                print "Error processing : ", fname
                traceback.print_stack()
        elif ext.upper() == ".ZIP" :

            print "zip file : ",fname
            try :
                zp = zipfile.ZipFile(fname);
                dir_list = zp.namelist()
            except :
                print "Error opening zip file : ", fname
                continue

            for zip_fname in dir_list :
                zip_base, zip_ext = os.path.splitext(zip_fname)
                if zip_ext.upper() == ".TXT" :
                    print "processing :",zip_fname ,
                    if 1 : # try :
                        fin = zp.open(zip_fname,'r')
                        rcd = is_wififast_txt_file(fin);
                        fin.close()                                                    
                        if not rcd :
                            print "not wififast file.  skip : ", fname
                        else :
                            mat_fname  = os.path.join(head, zip_base +  ".mat");
                            if not file_exists(mat_fname) :                            
                                fin = zp.open(zip_fname,'r')                            
                                data = load_wififast_data.load_wififast_data(fin)
                                fin.close()                                                                                
                                print "        saveing as : ", mat_fname
                                scipy.io.savemat(mat_fname, {'data':data}, long_field_names=True, oned_as='row');
                            else :
                                print "skiping as mat file exists :", mat_fname
                            
                    if 0 : # except :
                        print "Error processing ZIP :", zip_fname
                        traceback.print_stack()
            
                else :
                    # print "skipping : ", zip_fname
                    pass

            
def is_wififast_txt_file (fin) :
    # test that file is probably a wifi fast file.
    # wififast files consit of N lines with a single word (variable name)
    # the first non-variable line is a CSV line with N-2 values
    
    count = 0;
    for raw_ln in fin :
        count += 1;
        ln = raw_ln.strip()
        splt = ln.split()


        if len(splt) != 1 :
            splt = ln.split(',')
            # print splt            
            break;
        else :
            # print splt
            pass
            
    # print count, len(splt)


    
    if len(splt) == (count - 2) :
        rcd = True;
    else :
        rcd = False
    #print rcd
    return rcd
    
    
def walk_func(arg, dirname, names) :
    # print arg, dirname, names
    print
    print "enter directory  : ", dirname
    
    fname = os.path.join(dirname, arg)    
    flist = glob.glob(fname);
    # print flist
    process_list(flist)
    
    # print arg, flist
    # for name in names :
    if 0 : #for name in flist :
        base, ext = os.path.splitext(name)
        if ext.upper() == ".ZIP" or ext.upper() == ".TXT" :
            fname = os.path.join(dirname, name)
            print fname
            process_list(fname)
            # print name
            # print arg, name, fname
        else :
            print "skip : ", name



if __name__ == "__main__" :
    # opt = command_line_options(sys.argv);
    if not len(sys.argv[1:]) :
        head, fname = os.path.split(sys.argv[0])
        print """
        usage: %s  file1 [file2] ... [filen]
        
        where file represents a wifi_fast .txt file, and/or a .zip file that
        contains wifi_fast .txt files and/or a glob pattern that will expand
        to a .zip and/or .txt file.
        
        example: %s ..\wifilog\h22*.zip

        for each processed file, a matlab file will be created using the full
        path name of the .txt file using a .mat extension.

        In matlab, the processed files can be easily displayed as followed:

        d=dir('*.mat')
        pit( d(1).name)

        or
        pit ( recent_file('*.mat') )

        
        """ % (fname, fname)



    if 1 :
        path, pattern = os.path.split(sys.argv[1]);
        if not path  :
            path = "."        
        print path, pattern

        os.path.walk(path, walk_func, pattern)        
    else :
        for arg in sys.argv[1:] :
            flist = glob.glob(arg);
            process_list(flist)


