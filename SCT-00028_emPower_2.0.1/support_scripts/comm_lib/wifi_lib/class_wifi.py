import socket
import time
import string
import sys

import struct


LANTRON_PORT = 10001;

##########################################################################################
class class_wifi:
    
    def __init__(self) :
        
        self.DEV_SERIAL = 1;
        self.DEV_TCP = 2;
        self.DEV_UDP = 3;
        
        self.dev_type = None;

        self.timeout_cnt = 0;

        self.tcp_host = None;
        self.tcp_port = None;
        self.serial_port = None;
        self.baudrate = None;

        self.enable_bin_log = None        

    def __del__(self) :
        print "class_wifi.__del__"
        self.close()



        
    def connect_serial(self, port, baudrate=115200) :
        self.dev_type = self.DEV_SERIAL;
        self.serial_port = port
        self.baudrate = baudrate;
        os_port = port-1;
        self.sp = serial.Serial(os_port, baudrate, timeout=0.2, rtscts=0)


    def connect_tcp(self, HOST='', PORT=LANTRON_PORT, timeout = 1.0) :

        self.tcp_host = HOST;
        self.tcp_port = PORT;
        
        if (HOST) :
            self.tcp_addr = (HOST, PORT);

        print "Connect to IP %s PORT %s ..." % self.tcp_addr,

        try_cnt = 4;
        while try_cnt > 0 :
            try :
                self.sp.close();
            except :
                pass;

            try :
                self.sp = socket.socket(socket.AF_INET, socket.SOCK_STREAM);
                self.Timeout(timeout);
                self.sp.connect(self.tcp_addr);
                print "Success"
                try_cnt = 0;
                break;
            except :
                print "Retry",
                self.sp.close();
                time.sleep(1.0);
                try_cnt = try_cnt - 1;


        """
        str3 = time.strftime("%b%d %y %H:%M:%S  ", time.localtime());
        str1 = "OPEN: IP %s PORT %s  : packet_len %d" % (HOST, PORT , self.packet_len);
        if (self.error_fw) :
            self.error_fw.write(str3 + str1 + '\n');
        """

        return self.sp

    def reconnect(self) :
        if self.dev_type == self.DEV_SERIAL :
            rcd = self.connect_serial( self.serial_port, self.baudrate);
        else :
            rcd = self.connect_tcp(self.tcp_host, self.tcp_port);
        print "class_wifi.reconnect returns : ", rcd
        return rcd
    
    def Disconnect(self) :
        self.sp.close();                

    def close(self) :
        self.sp.close();
        
        if self.enable_bin_log :
            self.bin_log_fout.close()


    def start_bin_log(self, fname) :
        try :
            self.bin_log_fout = open(fname,'wb')
            self.enable_bin_log = True
        except :
            self.enable_bin_log = False
        

    def stop_bin_log(self) :
        self.enable_bin_log = False
        self.bin_log_fout.close()
        


    def Timeout(self, timeout) :
        if self.dev_type == self.DEV_SERIAL :
            self.sp.settimeout(timeout);

    def raw_read(self, cnt) :
        if self.dev_type == self.DEV_SERIAL :
            str = self.sp.read(cnt)
        else :
            str = self.sp.recv(cnt)

        if self.enable_bin_log :
            self.bin_log_fout.write(str)
        return str
    


    def send(self,ch) :
        if self.dev_type == self.DEV_SERIAL :
            cnt = self.sp.write(ch)
        else :
            cnt = self.sp.send(ch)
        return cnt

    def SendChar(self,ch) :
            self.send(ch)


    def SendList(self, list) :
        s = ''.join([chr(val) for val in list])
        cnt = self.send(s);
        return cnt


    def flush_soc(self) :
        try :
            ln = self.raw_read(1000);
            return 1
        except :
            print "exception - flush_soc"
            pass
        return 0


    def read(self, cnt) :
        # print "R",
        try :
            ln = self.raw_read(cnt);
        except :
            # print "T",
            self.timeout_cnt += 1;
            if (self.timeout_cnt == 1) :
                    print "DBR timeout ", 
            print self.timeout_cnt, 
            ln = ""
            if (self.timeout_cnt >= 9) :
                    self.reconnect();
        return ln


sys.path.append('..')
import class_process_v2_packet as mod_pp

if __name__ == "__main__" :
    HOST = '10.0.0.5';
    HOST = '192.168.192.4';    
    PORT = 10001
    
    wifi = class_wifi()
    wifi.connect_tcp(HOST, PORT);

    to = mod_pp.text_output();
    pp = mod_pp.process_v2_packet(40, wifi, to, None) ;
    

    for i in range(0,10) :
        ln = wifi.raw_read(512);
        print len(ln)

        
    for i in range(0,4000000) :        
        pk = pp.ReadAndProcessBuff();

    wifi.Disconnect()
