import socket;
import struct;
import time

DEF_UDP_HOST = '127.0.0.1';
DEF_UDP_DATA_PORT = 9999;

DEF_UDP_CONTROL_PORT = 9995;

DEF_COLLATE_CNT = 10;


##########################################################################################
# write data to this socket that is sent to ML for plotting.

l_pcnt8 = 0;
l_pcnt16 = 0;

from class_ErrorLogger import *

class SendToGraph :
    def __init__(self, host=DEF_UDP_HOST, port=DEF_UDP_DATA_PORT, collate_cnt = DEF_COLLATE_CNT)  :
        print "udp_matlab", host, port

        if type(host) == type([]) :  # caller sent list
            self.matlab_soc = []
            for h in host :
                print "SendToGraph : UDP to %s %s " % (h, port)
                soc = socket.socket(socket.AF_INET, socket.SOCK_DGRAM);
                soc.connect((h, port));
                self.matlab_soc.append(soc)
        else :
            self.matlab_soc = [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)];
            self.matlab_soc.connect((host, port));


        self.data = "";
        self.packet_cnt = 0;
        self.collate_cnt = collate_cnt;
        # OpenGraphicControl(host, 9995);
        self.l_pcnt8 = 0;
        self.l_pcnt16 = 0;
        self.err_log = ErrorLogger("error_udp_matlab.txt");
        print host
        print port
        self.STRUCT_UNPACK_STR = '>2b3h5f'
        self.debug_counter = 0;
        self.enable = False;
        self.test_open()
        self.enable_pcnt_diff = True;

    def test_open(self) :
        try : 
            for i in range(0,10) :
                self.matlab_soc[0].send("\x00" * 100)
                time.sleep(0.05);
            self.enable = True
        except :
            print
            print "Unable to send to plotter application"
            print
            self.enable = False
            

    def close(self) :
        for soc in self.matlab_soc :
            soc.close();
        

    def AddPacket(self, packet) :
        if self.enable == False :
            return
        self.data += ''.join(packet);
        self.packet_cnt += 1;
        if (self.packet_cnt == self.collate_cnt) :
            for soc in self.matlab_soc :
                try : 
                    soc.send(self.data)
                except :
                    pass
            
            self.packet_cnt = 0;
            self.data = ""
        

    def SetStructUnpackStr(self, str) :
        self.STRUCT_UNPACK_STR = str;
    
    
    def AddPacket_v03(self, unpacked_data, override_pos=None, override_data=None, quiet=False) :
        if self.enable == False :
            return         ""
        
        # print unpacked_data[0], unpacked_data[4], unpacked_data[9]
        self.debug_counter = self.debug_counter + 1;
        
        if (self.debug_counter > 500) :
            # print unpacked_data
            self.debug_counter = 0;


        e_str = ""

        if self.enable_pcnt_diff :
            (pcnt8,pcnt16) = (unpacked_data[0], unpacked_data[4])
            d8 = pcnt8 - self.l_pcnt8
            d16 = pcnt16 - self.l_pcnt16
            (self.l_pcnt8,self.l_pcnt16) = (pcnt8, pcnt16)

            if (d8 == -255) :
                d8 = 1;
            if (d16 == -65535) :
                d16 = 1;


            if quiet == False and ((d8 != 1) or (d16 != 1)) :
                str = "PACKET DIFFERENCE ERROR : pcnt8 %d:%d   pcnt16:%d:%d " % (pcnt8, d8, pcnt16, d16)
                self.err_log.log(str);
                print 
                print str
                print


        # matlab_data = unpacked_data[0:5] + unpacked_data[9:];
        # matlab_data = unpacked_data[0:7] + unpacked_data[9:];
        matlab_data = unpacked_data;

        if override_pos :
            for pos,val in zip(override_pos, override_data) :
                matlab_data[pos] = val

        
        f_str_lst = [struct.pack(">f", f) for f in matlab_data];


        self.data += ''.join(f_str_lst);


        self.packet_cnt += 1;
        if (self.packet_cnt == self.collate_cnt) :
            # print self.packet_cnt, self.collate_cnt
            for soc in self.matlab_soc :
                try :
                    soc.send(self.data)
                except :
                    pass
            
            self.packet_cnt = 0;
            self.data = ""

        return e_str;

    
    
    def AddPacket_v02(self, packet, override_pos=None, override_data=None, quiet=False) :
        if self.enable == False :
            return         ""
        unpacked_data = list(struct.unpack(self.STRUCT_UNPACK_STR,packet))

        # print unpacked_data[0], unpacked_data[4], unpacked_data[9]
        self.debug_counter = self.debug_counter + 1;
        
        if (self.debug_counter > 500) :
            # print unpacked_data
            self.debug_counter = 0;


        e_str = ""

        if self.enable_pcnt_diff :
            (pcnt8,pcnt16) = (unpacked_data[0], unpacked_data[4])
            d8 = pcnt8 - self.l_pcnt8
            d16 = pcnt16 - self.l_pcnt16
            (self.l_pcnt8,self.l_pcnt16) = (pcnt8, pcnt16)

            if (d8 == -255) :
                d8 = 1;
            if (d16 == -65535) :
                d16 = 1;


            if quiet == False and ((d8 != 1) or (d16 != 1)) :
                str = "PACKET DIFFERENCE ERROR : pcnt8 %d:%d   pcnt16:%d:%d " % (pcnt8, d8, pcnt16, d16)
                self.err_log.log(str);
                print 
                print str
                print


        # matlab_data = unpacked_data[0:5] + unpacked_data[9:];
        # matlab_data = unpacked_data[0:7] + unpacked_data[9:];
        matlab_data = unpacked_data;

        if override_pos :
            for pos,val in zip(override_pos, override_data) :
                matlab_data[pos] = val

        
        f_str_lst = [struct.pack(">f", f) for f in matlab_data];


        self.data += ''.join(f_str_lst);


        self.packet_cnt += 1;
        if (self.packet_cnt == self.collate_cnt) :
            # print self.packet_cnt, self.collate_cnt
            for soc in self.matlab_soc :
                try :
                    soc.send(self.data)
                except :
                    pass
            
            self.packet_cnt = 0;
            self.data = ""

        return e_str;

    def AddPacket_direct(self, data) :
        f_str_lst = [struct.pack(">f", f) for f in data];
        
        self.data += ''.join(f_str_lst);

        self.packet_cnt += 1;
        if (self.packet_cnt == self.collate_cnt) :
            # print self.packet_cnt, self.collate_cnt
            for soc in self.matlab_soc :
                try : 
                    soc.send(self.data)
                except :
                    pass
            
            self.packet_cnt = 0;
            self.data = ""

    
    def UpdateDataSize(self, vars, size) :
        print "remove this (UpdateDataSize)"
        time.sleep(5.0);
        pass
            
        # def UpdateGraphics(self, figure, rows, cols, titles, xlabels, ylabels) :



class MatlabGraphicControl :
    def __init__(self, host=DEF_UDP_HOST, port=DEF_UDP_CONTROL_PORT)  :
        self.graphic_soc = self.matlab_soc = socket.socket(socket.AF_INET, socket.SOCK_DGRAM);
        self.graphic_soc.connect((host, port));


        
        

if __name__ == "__main__":
    matlab_soc = UDP_Send_To_Matlab(DEF_UDP_HOST, DEF_UDP_DATAL_PORT);

