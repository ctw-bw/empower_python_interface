import os;



def return_source(path="", base="", extension="", file_num = 0, sample_period = 0.002)  :
    SOURCE = "clear\n"  
    SOURCE += "path = '%s';\n" % path;
    SOURCE += "default_base = '%s';\n" % base;
    SOURCE += "extension = '%s';\n" % extension;    
    SOURCE += "file_num = %d;\n" % file_num
    SOURCE += "sample_period= %f;\n" % sample_period;
    SOURCE += "prompt = 'Base file name (%s)';\n" % base;
    SOURCE += """
%% extension = '';
%% path='';


base = input(prompt,'s');

if isempty(base)
  base = default_base;
%%else 
%%  file_num = input('File number :','s');
end

if exist('file_num')
  match = sprintf(base, file_num);
else
  match = base;  
end


search_for = sprintf('%s%s%s', path, match,  extension);
disp(['searching for ' search_for]);

f = ls(search_for);
[n,l] = size(f);

if n == 1 
  disp('N == 1')
  pos = findstr(f,extension)
  base_fname = f;
  if pos > 0
    base_fname = f(1:pos-1);
  end
elseif n>1
  disp(sprintf('Found :'));
  valid_extension_count = 0;
  clear f_valid_ext
  for i = 1 : n
    [path, base_fname, ext] = fileparts(f(i,:));

    if  strcmp(strtrim(ext), '.txt') == 1
      valid_extension_count = valid_extension_count + 1 ;
      f_valid_ext{valid_extension_count} = f(i,:);
      disp(sprintf('%d : %s', valid_extension_count , f_valid_ext{valid_extension_count} ))

    end
  end
  strnum = input('Use which item ? ','s');
  ff = f_valid_ext{str2num(strnum)};
  [path, base_fname, ext] = fileparts( ff) ;
  clear strnum
else 
  disp('Not found')
end
  clear l n file_num ans search_for



  fname = sprintf('%s%s%s',path, strtrim(base_fname), extension)
  save_fname = sprintf('%s%s.mat', path, strtrim(base_fname));
  ss = importdata(fname);
  [num_points, num_columns] = size(ss.data);
  ss
  disp('import complete');

  
  data = struct();
  for i = 1 : num_columns
    f = cell2mat(ss.textdata(i));
    data.(f) = ss.data(:,i);
  end

  for i = num_columns + 1: length(ss.textdata)
    f = cell2mat(ss.textdata(i));

    data.(sprintf('field_%d', i)) = f
  end
  
  clear num_points num_columns

  %% parse the numerical text string
  s1 = round(data.text_int_value / 256.0); s2 = mod(data.text_int_value , 256);
  text_str = sprintf('%c', [s1';s2']);
  clear s1 s2

  tm = sample_period * (1:length(ss.data(:,1)));
  clear f i base match extension path fname base_fname 
  disp(['saving ' save_fname]);

  whos
  % save(save_fname);   % save everything
  save(save_fname, 'data', 'tm', 'sample_period', 'text_str', 'save_fname')
  
  set(0,'defaulttextinterpreter','none');  % disable latex
"""

        
    return SOURCE

if __name__ == "__main__" :
    s = return_source("", "ltf*","txt", 0);
    print s


