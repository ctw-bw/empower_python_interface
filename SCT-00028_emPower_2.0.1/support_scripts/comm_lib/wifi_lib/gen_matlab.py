
def my_print (str) :
    # print str
    pass

import matlab_source_wifiload

def gen_matlab(fname, txt_fname, mat_data_fname, fields_str) :

    raw_fields = fields_str.split(',');
    fields = [f.strip() for f in raw_fields];
    
    fout = open(fname,"w");
    
    
    str = "disp('importdata %s');" % txt_fname
    fout.write(str+"\n");            
    str = "ss = importdata('%s');" % txt_fname;
    fout.write(str+"\n");
    my_print(str)
    str = "disp('complete');"
    fout.write(str+"\n");        
    
    ndx = 1;
    for f in fields :
        str = "s.%s = ss.data(:,%d);" % (f,ndx)
        fout.write(str+"\n");
        str = "disp('%s %s');" % (ndx, f)
        fout.write(str+"\n");        
        my_print(str)
        ndx = ndx + 1;

    str = "clear ss"
    fout.write(str+"\n");
    my_print(str)
    
    str = "disp('saving %s');" % mat_data_fname
    fout.write(str+"\n");

    str = "fname = '%s';" % mat_data_fname
    fout.write(str+"\n");
    
    str = "whos" 
    fout.write(str+"\n");
    
    str = "save('%s','s')" % mat_data_fname
    fout.write(str+"\n");
    my_print(str)




def gen_matlab_2(fname, txt_fname, mat_data_fname, field_count, date_field) :

    
    fout = open(fname,"w");
    
    
    str = "disp('importdata %s');" % txt_fname
    fout.write(str+"\n");            
    str = "ss = importdata('%s');" % txt_fname;
    fout.write(str+"\n");
    my_print(str)
    str = "disp('complete');"
    fout.write(str+"\n");


    str = "session_date = ss.textdata(%d);" % date_field
    fout.write(str+"\n");

    str = "data = struct();";
    fout.write(str+"\n");

    str = "for i = 1 : %d" % field_count
    fout.write(str+"\n");

    str = """
    f = cell2mat(ss.textdata(i));
    data.(f) = ss.data(:,i);
    end
    """
    
    fout.write(str+"\n");    

    str = "clear ss i f fname"
    fout.write(str+"\n");
    
    
    str = "disp('saving %s');" % mat_data_fname
    fout.write(str+"\n");

    str = "fname = '%s';" % mat_data_fname
    fout.write(str+"\n");
    
    str = "whos" 
    fout.write(str+"\n");
    
    str = "save('%s','data')" % mat_data_fname
    fout.write(str+"\n");
    my_print(str)

def gen_matlab_3(fname, txt_fname, mat_data_fname, field_count, date_field) :
    print "generating matlab loader script ", fname
    str = matlab_source_wifiload.return_source("", txt_fname+"*", ".txt", 0);
    fid = open(fname,'w');
    fid.write(str);
    fid.close()
    
    
if __name__== "__main__" :
    """
    print "NAME"
    FORMAT_TEXT_STR = "time, qVelMech, qIq, state, ankle_torque, motor_rot, ank_vel_mot, ankle_angle_mot_deg, i_desired"
    
    raw_fields = FORMAT_TEXT_STR.split(',');
    
    fields = [f.strip() for f in raw_fields];
    print fields
    gen_matlab("test.m", "ascdata.txt", "mat_data_fname", FORMAT_TEXT_STR);
    """
    print matlab_source_wifiload.return_source("", "ltf_*","", 0);

