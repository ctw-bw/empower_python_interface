

class fancy_list_output :
    def __init__(self, fmt=[], scale=[], offset=[], label=[]) :
        self.set_struct_str();
        self.new_format(fmt, scale, offset, label);
        pass

    def set_struct_str(self, str = None) :
        self.struct_str = str;
    
    def new_format(self, fmt=[], scale=[], offset=[], label=[]) :
        self.format_list = [];
        self.scale_list = [];
        self.offset_list = [];
        self.label_list = [];
        for f,cnt in zip(fmt, range(0,1000)) :
            s = 1;
            o = 0;
            l = "INDEX %d" % cnt
            try :
                s = scale[cnt];
                o = offset[cnt];
                l = label[cnt];
            except :
                pass
            self.append(f, s, o, l);
        
    def append(self, fmt="%f", scale=1, offset=0, label=" ") :
        if fmt == '%d' :
            # test for non integer scale or offset
            if round(scale,4) != 1 or round(offset,4) != 0:
                fmt = '%6g'
            
        self.format_list.append(fmt);
        self.scale_list.append(scale);
        self.offset_list.append(offset);
        self.label_list.append(label);

        self.print_format_str = ','.join(self.format_list)


    def scale_data (self, data) :
        # really should do this with numpy to speed it up.
        out_list = []
        for v,cnt in zip(data, range(0,1000)) :
            nv = self.scale_list[cnt] * (v + self.offset_list[cnt]);
            out_list.append(nv);
        return out_list


    def make_string_from_data(self, output_seq) :
        if type(output_seq) == type( () ) :
            output_string = self.print_format_str % output_seq
        else :
            output_string = self.print_format_str % tuple(output_seq)

        return output_string

    def make_sting_from_packed_binary(self, bin_str) :
        data_list = struct.unpack(struct_str, pk)   # transform the binary data packet into a python list
        output_string = self.print_format_str % data_list
        return output_string


if __name__ == "__main__" :
    fmt = ["%d", "%f", "%d"]
    scale = [0.1, 0.001, 10]
    flo = fancy_list_output(fmt, scale);
    

    print flo.format_list
    print flo.scale_list
    print flo.offset_list
    print flo.label_list    

    print flo.print_format_str
    
    data = (2.1, 3.1, 4.1);
    print flo.make_string_from_data(data);


    print flo.make_string_from_data(    flo.scale_data(data));

    flo.append("%d", label="milliseconds");
    flo.append("%d", label="packet_number");
    flo.append("%d", label="text_int");




    print flo.format_list
    print flo.scale_list
    print flo.offset_list
    print flo.label_list    

    print flo.print_format_str




    data = [2.1, 3.1, 4.1, 11.1, 12.1, 13.1];
    print flo.make_string_from_data(data);
