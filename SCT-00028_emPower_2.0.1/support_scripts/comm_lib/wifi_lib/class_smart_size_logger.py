import time, os, math
import bz2

USE_THREADING = False
LOCAL_DEBUG = False

TEST_USING_SECS = False

if USE_THREADING :
    from threading import Thread
else :
    class Thread :
        pass
    
class nice_interval_generator(Thread) :

    def __init__(self, user_callback, files_per_hour = 6) :
        if USE_THREADING :
            Thread.__init__(self)
            
        self.terminate = False;        

        self.files_per_hour = files_per_hour;
        self.user_callback = user_callback;

        self.call_number = 1

        # get the preset time
        self.start_time = time.time();
        self.start_time_struct = time.localtime(self.start_time) 

        if not TEST_USING_SECS :
            mins = self.start_time_struct.tm_min;
        else :
            mins= self.start_time_struct.tm_sec;
        
        # compute the time of the first stop
        self.next_stop_minutes = self.compute_first_stop_minutes(mins);
        
        if USE_THREADING :
            self.start()

    def __del__(self) :
        print "__del__ called"
        self.terminate = True

    def test_minutes(self) :
        rcd = False
        t = time.localtime()

        if not TEST_USING_SECS :
            mins = t.tm_min;
        else :
            mins= t.tm_sec;

            # print mins, self.next_stop_minutes 
        if mins == self.next_stop_minutes :
            rcd = True
            self.call_number += 1                
            self.next_stop_minutes = self.compute_next_stop_minutes(self.next_stop_minutes)
            
            if LOCAL_DEBUG :
                print
                print "EVENT", t, t.tm_min, t.tm_sec, self.next_stop_minutes
                print

            if self.user_callback :
                self.user_callback(self.call_number)
                pass
        return rcd;
        
        
    
    def stop(self) :
        if USE_THREADING :
            print "stop timer"
            self.terminate = True
            self.join(1.0)
        
    def run(self) :
        if USE_THREADING :
            t = time.localtime()
            print "run started", t.tm_min, t.tm_sec, self.next_stop_minutes
            if self.user_callback : 
                self.user_callback(self.call_number)

            while not self.terminate :
                time.sleep(0.8)            
                t = time.localtime()
                mins = t.tm_min;
                # mins = t.tm_sec;

                if mins == self.next_stop_minutes :
                    self.call_number += 1                
                    self.next_stop_minutes = self.compute_next_stop_minutes(self.next_stop_minutes)

                    print "EVENT", t, t.tm_min, t.tm_sec, self.next_stop_minutes

                    if self.user_callback :
                        self.user_callback(self.call_number)
                        pass

    def compute_next_stop_minutes(self, mins) :
        
        mins = mins + self.minutes_interval;
        if mins >= 60 :
            mins -= 60;
        return mins
    
    def compute_first_stop_minutes(self, mins, DO_DEBUG=False) :
        """ compute a round number time to stop that is at least the interval in length """
        
        self.minutes_interval = round(60.0 / float(self.files_per_hour));        
        
        n = round(mins / float(self.minutes_interval));

        mdiff = mins - n*self.minutes_interval;
        if  mdiff < 0 :
            # rounded up, thus to residule + one interval
            first_mins = -mdiff + self.minutes_interval;
        else :
            # rounded down, thus just use the residual
            first_mins = self.minutes_interval - mdiff;

        first_stop = mins + first_mins;

        if first_mins < self.minutes_interval :
            first_mins += self.minutes_interval;
            first_stop += self.minutes_interval;

        if first_stop >= 60 :
            first_stop -= 60;

        if DO_DEBUG :
            print "compute first stop",mins, first_mins, first_stop
            
        return int(round(first_stop))
        # return int(round(first_secs))
            

    def test_stop(self) :
        for mins in range(0, 62) :
            fs = self.compute_first_stop_minutes(mins, True);
            # print mins, fs,ns
               


class period_filename_generator :

    def __init__(self, base_fname, callback1=None, callback2=None, callback3=None, callback4=None) :
        
        self.base_fname = base_fname;
        self.callback1 = callback1;
        self.callback2 = callback2;
        self.callback3 = callback3;
        self.callback4 = callback4;
        
        (self.filepath, self.filename) = os.path.split(self.base_fname);
        (self.shortname, self.extension) = os.path.splitext(self.filename)

        if self.extension and self.extension[-1] == '.' :  # strip the dot from the extension
            self.extension = self.extension[:-1]
        print "path=%s name=%s short=%s ext=%s " % (self.filepath, self.filename, self.shortname, self.extension)


    def open_next_file(self, number) :
        t = time.localtime()
        date_str = time.strftime("%y%m%d",t);
        time_str = time.strftime("%H%M%S",t);
        fname = "%s_%05d_%s_%s%s" % (self.shortname, number, date_str, time_str, self.extension)
        
        print fname
        if self.callback1 :
            self.callback1(fname);
            if self.callback2 :
                self.callback2(fname);
                if self.callback3 :
                    self.callback3(fname);
                    if self.callback4 :
                        self.callback4(fname);           
                

class logger :
    def __init__(self, fname_head, fname_foot, open_callback=None, close_callback=None, bzip2=False) :
        self.head = fname_head;
        self.foot = fname_foot;
        self.close_callback = close_callback;
        self.open_callback = open_callback;        
        self.fout = None
        self.bzip2 = bzip2;
        pass

    def __del__(self):
        if self.fout :
            if self.close_callback :
                self.close_callback(self.fout)            
            self.fout.close();        
        pass

    def reopen(self, var_fname) :

        fname = self.head + var_fname + self.foot                
        print "CALLED", fname
        
        self.new_fname = fname;
        self.reopen = True;
        """
        if self.fout :
            if self.close_callback :
                self.close_callback(self.fout)            
            self.fout.close();

        if self.bzip2 :
            self.fout = bz2.BZ2File(fname,"w");
        else :
            self.fout = open(fname, "w");

        if self.open_callback :
            self.open_callback(self.fout)
        """

    def append(self, data) :
        if self.reopen == True :
            print "appped w :",self.new_fname
            if self.fout :
                if self.close_callback :
                    self.close_callback(self.fout)            
                self.fout.close();            
            
            if self.bzip2 :
                self.fout = bz2.BZ2File(self.new_fname,"w");
            else :
                self.fout = open(self.new_fname, "w");

            if self.open_callback :
                self.open_callback(self.fout)
            self.reopen = False;

        self.fout.write(data)



class user_callback :
    def __init__(self, str=None) :
        self.str = str;
        pass
    def __call__(self, arg) :
        print "called ", self.str
        arg.write("called " + self.str + "\n");
    def test(self, arg) :
        print "called " + self.str + "\n";
        

if __name__ == "__main__" :

    if USE_THREADING :

        ucb1 = user_callback("open");
        ucb2 = user_callback("close");    

        log1= logger("tmp/ltf_log_bin", ".bin6", ucb1, ucb2);
        log2= logger("tmp/ltf_log_txt", ".txt.bz", None, None, True);
        pfn = period_filename_generator("", log1.reopen, log2.reopen)            
        nice_gen = nice_interval_generator(pfn.open_next_file, 20)

        # nice_gen.test_stop();


        try :
            while nice_gen.isAlive() :
                print "*", 
                nice_gen.join(1.0)
        except :
            nice_gen.stop();

        nice_gen.stop();
    else :
        ucb1 = user_callback("CB");
        nice_gen = nice_interval_generator(ucb1.test, 6)
        # nice_gen.test_stop()
        while 1 :
            nice_gen.test_minutes();
            time.sleep(0.8);
