#routintes to process wifi_fast data files.


import time
import zipfile

try:
  import scipy.io
except:
  print 'WARNING: YOU DO NOT HAVE SCIPY INSTALLED. MATLAB EXPORT FUNCTIONS WILL NOT WORK UNTIL YOU INSTALL SCIPY'
  

import collections

AS_DICT_OF_LIST = 1;
AS_LIST_OF_DICT = 2;

def load_wififast_data(fobj, FORMAT = AS_DICT_OF_LIST) :
    count = 0;
    # read the list of field which is terminated by a line that starts with "Date_" 
    field_list = [];
    # data_dict = collections.OrderedDict()
    data_dict = {}
    for raw_ln in fobj :
        key = raw_ln.strip()
        field_list.append(key)
        if key[0:5] == "Date_" :
            break;
        data_dict[key] = []

    # remainder of the file contains data 
    if FORMAT == AS_DICT_OF_LIST :
        # read in as list of dict
        for raw_ln in fobj :
            splt = raw_ln.split(',')
            count += 1;
            for key,val in zip(field_list, splt) :
                data_dict[key].append( float(val) )
                
        data = data_dict;

        
    elif FORMAT == AS_LIST_OF_DICT :
        data_list = []

        for raw_ln in fobj :
            # data_dict = collections.OrderedDict()  - using an order dict is MUCH slower
            data_dict = {}
            count += 1
            if not(count%1000 ) :
                print count/1000,
            splt = raw_ln.split(',')

            for key,val in zip(field_list, splt) :
                data_dict[key] = float(val)

            data_list.append(data_dict);

        data = data_list;

    data['_LENGTH'] = count
    return data


    

import sys


if __name__ == "__main__" :
    fzip = "../../wifilog/ankle_sn86_poletest_110722_1413.zip"
    fzip = "../../wifilog/junk_110722_1613.zip"
    fzip = "c:/ceb/0svn/h22v110/wifilog/junk_110722_1613.zip"
    
    fzip = sys.argv[1]
    zp = zipfile.ZipFile(fzip);

    
    zp = zipfile.ZipFile(fzip);

    dir = zp.namelist()
    print "DIR:",dir
    fin = zp.open( dir[1]);
    data = load_wififast_data(fin);
    fin.close
    
    scipy.io.savemat('test.mat', {'data':data}, oned_as='row');

    if 0: 
        fin = zp.open( dir[1]);
        data = load_wififast_data(fin, FORMAT=AS_DICT_OF_LIST);
        fin.close

        fin = zp.open( dir[1]);    
        data = load_wififast_data(fin, FORMAT=AS_LIST_OF_DICT);
        fin.close
        # note - saving a list of dict to matlab takes forever 
        # scipy.io.savemat('test.mat', {'data':data}, oned_as='row');

    

    
