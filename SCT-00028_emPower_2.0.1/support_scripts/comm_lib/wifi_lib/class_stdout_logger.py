import sys, traceback, time, os

import class_dict_to_columns

import StringIO

class log_stdout(StringIO.StringIO) :
    def __init__ (self, path, base_fname, TIMESTAMP_FILENAME = False, APPEND=True) :
        (filepath, filename) = os.path.split(base_fname);
        (shortname, extension) = os.path.splitext(filename)
        
        self.time_last_flush = time.time();
        
        self.full_fname = os.path.join(path, base_fname)
        print "open log file : ", self.full_fname
        try : 
            if (APPEND) :
                # http://docs.python.org/library/functions.html#open
                self.fout = open(self.full_fname, "a");  
            else :
                self.fout = open(self.full_fname, "w");
        except :
            print
            print "Unable to open log file : ", self.full_fname
            print
            self.fout = None;

        self.old_stdout = sys.stdout
        sys.stdout = self

    def __del__(self) :
        if (self.fout) :
            self.fout.close()
            
        # sys.stdout = sys.__stdout__


    def flush(self) :
        sys.__stdout__.flush()
        if (self.fout) :
            self.fout.flush()
        self.time_last_flush = time.time();

    def write(self, buff) :

        sys.__stdout__.write(buff)
        

        
        if (self.fout) :
            self.fout.write(buff);
            
        tm = time.time()
        if (tm -self.time_last_flush) > 2 :
            self.flush()


        
        if (0) :
            buff_list = buff.split('\n')

            # s = "LISTLEN=%d LEN=%d ORD=%d [[%s]] " % (len(buff_list), len(buff), ord (buff[-1]), buff_list )
            #sys.__stdout__.write(s)

            sys.__stdout__.write(buff_list[0])

            for str in buff_list[1:] :
                sys.__stdout__.write(str)
                sys.__stdout__.write('\r\n')            

            sys.__stdout__.flush()





class dict_logger :
    
    def __init__ (self, path, base_fname, TIMESTAMP_FILENAME = False, APPEND=True, FLUSH_INTERVAL=10, DEFAULT_COLUMNS=[], ONE_HEADER_MODE=True) :
        
        self.fout = None;
        
        (filepath, filename) = os.path.split(base_fname);
        (shortname, extension) = os.path.splitext(filename)
        
        self.time_last_flush = time.time();
        self.flush_interval = FLUSH_INTERVAL;
        
        
        if TIMESTAMP_FILENAME :
            # dstr = time.strftime("_%Y-%m-%d_%Hh%Mm%Ss", time.localtime())
            dstr = time.strftime("_%Y-%m-%d_%Hh%Mm", time.localtime())            
            fname = shortname + dstr + extension;
            self.full_fname = os.path.join(path, fname)
        else :
            self.full_fname = os.path.join(path, base_fname)
            
        print "dict_logger:open log file : ", self.full_fname

        try :
            fout = open(self.full_fname, "r");
            fout.close();            
            self.FILE_EXIST = True;
            WRITE_HEADER = not ONE_HEADER_MODE;
        except :
            self.FILE_EXIST = False;
            WRITE_HEADER = True;

        try :
            if (APPEND) :
                # http://docs.python.org/library/functions.html#open
                self.fout = open(self.full_fname, "a");
            else :
                self.fout = open(self.full_fname, "w");
        except :
            print
            print "Unable to open log file : ", self.full_fname
            print
            self.fout = None;


        self.dobj = class_dict_to_columns.dict_to_columns(DEFAULT_COLUMNS, WRITE_HEADER = WRITE_HEADER);

        return

        
    def __del__(self) :
        if (self.fout) :
            self.fout.close()
            

    def flush(self) :
        if (self.fout) :
            self.fout.flush()
        self.time_last_flush = time.time();


    def write(self, dict_obj) :
        # print "write w/", dict_obj
        self.dobj.add_data(dict_obj);
        str = self.dobj.get_log_str();

        if self.fout : 
            self.fout.write(str);
            self.fout.flush()
            tm = time.time()
            if (tm - self.time_last_flush) > self.flush_interval :
                self.flush()
        else :
            print "dict_logger:file not open"



if __name__ == "__main__" :

    """
    s = log_stdout("", "test.txt")
    for i in range (0,10) :
        print "a", i
    """

    dobj =  dict_logger("", "test_file.txt", True, FLUSH_INTERVAL=10.0);

    i = 0
    while 1 :
        i += 1;
        d = dict(a=time.time(), b=i, d=0, c=0);
        dobj.write(d);
        time.sleep(0.5);
