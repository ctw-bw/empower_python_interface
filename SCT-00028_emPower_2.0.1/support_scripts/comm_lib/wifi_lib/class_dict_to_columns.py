class dict_to_columns :
    
    def __init__ (self, DEFAULT_COLUMNS = [], NUMBER_HEADERS=True, WRITE_HEADER=True) :
        
        self.ordered_keys_list = DEFAULT_COLUMNS;
        self.keys_set = set(DEFAULT_COLUMNS);
        self.new_header = True
        self.header_str = ""
        self.data_str = ""
        self.kv_str = ""
        self.number_headers = NUMBER_HEADERS;
        self.WRITE_HEADER = WRITE_HEADER;
        
    def __del__(self) :
        pass

    def get_header(self) :
        return self.header_str;
    
    def get_data_str(self) :
        return self.data_str;

    def get_kv_str(self) :
        return self.kv_str;

    def get_log_str(self) :
        str = ""

        if self.new_header and self.WRITE_HEADER :
            str = "#" + self.header_str+'\n' + self.data_str+'\n';
        else :
            str = self.data_str+'\n';

        return str;
        
    def add_data(self, dict_obj) :
        
        self.new_header = False
        # start off by test for new keys in the object, if so append them to the key list
        ks = set(dict_obj.keys());
        # print "ks",ks, self.keys_set
        
        set_diff = ks.difference(self.keys_set);
        # print "set_diff", set_diff
        
        if len(set_diff) > 0 :  # are their new elements
            self.new_header = True
            for k in set_diff :
                # print "add ",k
                self.ordered_keys_list.append(k);
        self.keys_set = self.keys_set.union(set_diff);  # add the new elements into the permant set


        # if the first pass, write the header
        if self.new_header :
            if self.number_headers :
                slist = ["%s_%d" % (k,cnt) for (k,cnt) in zip(self.ordered_keys_list, range(1,100))];
            else :
                slist = ["%s" % k for k in self.ordered_keys_list];                
            str = ', '.join(slist);
            self.header_str = str;
            # print "# header : ", str

        # built a list of strings with the values
        slist = []
        kvlist = []        
        for k in self.ordered_keys_list :
            try :
                dd = dict_obj[k];
                if type(dd) == type(1.0) :
                    str = "%g" % dict_obj[k];
                    str2 = "%s=%g" % (k, dict_obj[k]);
                else :
                    str = "%s" % dict_obj[k];
                    str2 = "%s=%s" % (k, dict_obj[k]);

                
            except :
                str = ""
                str2 = ""
            slist.append(str);
            kvlist.append(str2);            
            
        # write the values as command seperated
        self.data_str = ', '.join(slist);
        self.kv_str  = ', '.join(kvlist);        

        return self.new_header

        




if __name__ == "__main__" :
    ##### TEST THE DICT TO COLUMNS CLASS
    dobj = dict_to_columns(['b','a','jj','xx']);

    dlist = [];
    d = dict(a=123, b=333, d=0, c=0);
    dlist.append(d);

    d = dict(a=123, b=333);
    dlist.append(d);

    d = dict(a=123, c=444);
    dlist.append(d);

    d = dict(a=123, c=444);
    dlist.append(d);
    
    d = dict(a=123, c=444, b=555);
    dlist.append(d);
    
    d = dict(c=666)
    dlist.append(d);
    
    d = dict(b=666,a=111)
    dlist.append(d);
    d = dict(b=666,d=111)
    dlist.append(d);    

    for d in dlist :
        rcd = dobj.add_data(d);
        print dobj.get_log_str();
        """
        if rcd :
            print "#header : ", dobj.header_str;
        print "#data :", dobj.data_str
        """
    
    
