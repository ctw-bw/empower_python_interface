# 10-20-09 : w 2.6.31 kernel - this no longer seems to be able to locate parents and children


# good example of writing a module with classes.
# http://svn.smedbergs.us/python-processes/trunk/killableprocess.py

# documentation on the /proc file system
# http://www.redhat.com/docs/manuals/linux/RHL-9-Manual/ref-guide/s1-proc-directories.html

import glob, re, os

class process_list() :
    def __init__ (self) :
        
        self.srch = re.compile ("(\d+)\s+([(].*[)])+\s+([RSDZTW])\s+(\d)+\s+(\d)+\s+(\d)+")
        self.read_plist()
        
        pass

    def list (self) :
        for pcs in self.pcs_list :
            print pcs[0:6]
        """
        for pcs in self.pcs_list :
            try :
                v = int(pcs[0])
                if v > 4000 : 
                    print pcs[0:6]
            except :
                pass
        """

        
    def get_process_list_file(self, file_name) :
        glob_fname = '/proc/*/%s' % file_name
        # print glob_fname
        re_fname = '/proc/[0-9]{1,5}/%s$' % file_name
        plist = []
        for f in glob.iglob(glob_fname) : 
            # print f
            r = re.findall(re_fname, f)
            if r :
                # we start with /proc/9999/some_file
                # first split returns ['/proc/9999', 'some_file']
                # we apply split again to get ['/proc','9999']
                # then convert that to an ind
                
                l1 = os.path.split(f)
                pid = int(os.path.split(l1[0])[1])
                # print pid
                
                fd = open(r[0],'r');
                str = fd.read(2048)
                arg_list = str.split('\x00');
                fd.close()
                pcs_fullpath = arg_list[0]
                pcs_name = os.path.split(pcs_fullpath)[1]
                pcs_path = os.path.split(pcs_fullpath)[0]             
                d = dict(pid=pid, data=str, name=pcs_name, path=pcs_path, full_path=pcs_fullpath, arg_list = arg_list[1:]);

                plist.append(d)
                # print d
        return plist
                


    def find_process(self, name) :
                
        pcs_list = self.get_process_list_file('cmdline')

        plist = []
        for pcs in pcs_list :
            if pcs['data'].find(name)>= 0 or pcs['name'] == name :
                plist.append(pcs)
                #print pcs['pid'], pcs['name']

        return plist
            
        
    def read_plist(self) :
        self.g_group_to_child = dict()
        self.group_to_child = dict()
        self.parent_to_child = dict()
        self.child_to_parent = dict()
        self.pid_to_name = dict()
        self.name_to_pid = dict();
        
        self.pcs_list = []
        for f in glob.iglob('/proc/*/stat') :
            # print f
            r = re.findall('/proc/[0-9]{1,5}/stat$', f)
            if r :
                fd = open(r[0],'r');
                ln = fd.read(1024);
                fd.close()
                stat_items = ln.split()
                


                
                # http://linux.die.net/man/5/proc
                # 0 process ID
                # 1 name
                # 2 stat
                # 3 PPid
                # 4 group parent?

                if (0) :
                    """
                    print
                    print stat_items[0:6]
                    print
                    print stat_items[3]
                    """
                    pid = int(stat_items[0])
                    ppid = int(stat_items[3])
                    gppid = int(stat_items[4])
                    ggppid = int(stat_items[5])
                else :
                    match_obj = self.srch.match(ln)
                    """
                    for i in xrange(1,7) :
                        print i, match_obj.group(i)
                    """
                    pid = int( match_obj.group(1) )
                    ppid = int( match_obj.group(4) )
                    gppid = int( match_obj.group(5) )
                    ggppid = int( match_obj.group(6) )

                self.pcs_list.append(stat_items)
                
                # print pid, ppid, gppid
                if self.parent_to_child.has_key(ppid) :
                    self.parent_to_child[ppid].append(pid)
                else :
                    self.parent_to_child[ppid] = [pid]

                if self.group_to_child.has_key(gppid) :
                    self.group_to_child[gppid].append(pid)
                else :
                    self.group_to_child[gppid] = [pid]                    


                if self.g_group_to_child.has_key(ggppid) :
                    self.g_group_to_child[ggppid].append(pid)
                else :
                    self.g_group_to_child[ggppid] = [pid]                    
                name = stat_items[1][1:-1];
                self.pid_to_name[pid] = name;
                if self.name_to_pid.has_key(name) :
                    self.name_to_pid[name].append(pid)
                else :
                    self.name_to_pid[name] = [pid]
                    
                if self.child_to_parent.has_key(pid) :
                    print "Error", pid
                else :
                    self.child_to_parent[pid] = ppid;



    def get_pid_by_name(self, name) :
        try :
            rcd = self.name_to_pid[name]
        except :
            rcd = None;
        return rcd;



        


    def get_process_stat(self, pid) :
        fname = "/proc/%d/stat" % pid
        fd = None
        try : 
            fd = open(fname,'r');
            ln = fd.read(1024);
        except :
            ln = None
        finally :
            if fd :
                fd.close()            
                
        return ln

    def get_process_cmdline(self, pid) :
        fname = "/proc/%d/cmdline" % pid
        fd = None
        try : 
            fd = open(fname,'r');
            ln = fd.read(2048);
        except :
            ln = None
        finally :
            if fd :
                fd.close()            
                
        return ln


    def get_process_status(self, pid) :
        fname = "/proc/%d/status" % pid
        fd = None
        try : 
            fd = open(fname,'r');
            ln = fd.read(2048);
        except :
            ln = None
        finally :
            if fd :
                fd.close()            
                
        return l

    """
    def get_child_list(self, ppid) :
        try :
            rcd = self.parent_to_child[ppid]
        except :
            rcd = None;
        return rcd

    def get_group(self, pid) :
        try :
            rcd = self.group_to_child[pid]
        except :
            rcd = None;
        return rcd
    
    def get_parent(self, pid) :
        try :
            rcd = self.child_to_parent[pid]
        except :
            rcd = None;
        return rcd
    """
        

if __name__ == "__main__" :
    pl = process_list();
    # pl.list()
    # pl.find_process('clock')
    # pl.get_process_list_file('cmdline')
    print pl.find_process('python')
    
