
import optparse, sys

class options :
    def __init__(self, args) :

        self.args = args

	usage = "usage: %prog [options]"
	parser=optparse.OptionParser();
	parser.add_option("-i", "--ip_address", dest="ip_address");
	parser.add_option("-P", "--port", dest="port", type="int");
	parser.add_option("-c", "--com_port", dest="com_port", type="int");
	parser.add_option("-b", "--baud_rate", dest="baud_rate", type="int", default=115200);
        
	parser.add_option("-n", "--num_tests", dest="number_of_tests", type="int", default=1);        
	parser.add_option("-f", "--filename", dest="filename", type="string", default="ankle_iochan.csv");

	# parser.disable_interspersed_args()
	(options, args) = parser.parse_args(sys.argv);
        

        self.values = parser.values
	self.parser = parser;
        # opt = parser.values
