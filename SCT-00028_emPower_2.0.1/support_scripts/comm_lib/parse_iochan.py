#!/usr/bin/env python



import sys
import python_paths as pyenv
import os
# this program will read an iochan.h file, capture the macro name and
# value, and colate the macros into the 3 controllers, and their type
# of action

DEF_FILE = "./mod_io_chan.h";
DEF_FILE = "./io_chan.h";
DEF_FILE = os.path.join(pyenv.FIRMWARE_BASE_PATH, pyenv.IOCHAN_RELATIVE_FNAME);


def is_int(str) :
    try :
        i = int(str);
        return 1;
    except :
        pass;
    
    return 0;
    

        
class parse_iochan :

    def __init__(self, filename = DEF_FILE) :
        self.list_AI = [];
        self.list_DI = [];
        self.list_AO = [];
        self.list_DO = [];
        self.d_chan_to_name = {};  # channel # is the key, name is the result
        self.d_name_to_chan = {}; 
        print "parse_iochan : ", filename
        fin = open(filename,"r");
        
        for ln in fin :
            parts = ln.strip().split();
            if parts and parts[0] == "#define" :
                # print parts
                str = parts[1];

                if str.find("IOCHAN_AI_",0) == 0 :
                    if ( len(parts)>=3 and is_int(parts[2])) :
                        self.d_chan_to_name[int(parts[2])] = parts[1];
                        self.d_name_to_chan[parts[1]] = int(parts[2]);
                        d = {'name':parts[1], 'channel':int(parts[2])}
                        self.list_AI.append(d);
                    

                if str.find("IOCHAN_DI_",0) == 0 :
                    if ( len(parts)>=3 and is_int(parts[2])) :
                        self.d_chan_to_name[int(parts[2])] = parts[1];
                        self.d_name_to_chan[parts[1]] = int(parts[2]);                        
                        d = {'name':parts[1], 'channel':int(parts[2])}
                        self.list_DI.append(d);

                if str.find("IOCHAN_AO_",0) == 0 :
                    if ( len(parts)>=3 and is_int(parts[2])) :
                        self.d_chan_to_name[int(parts[2])] = parts[1];
                        self.d_name_to_chan[parts[1]] = int(parts[2]);                                                
                        d = {'name':parts[1], 'channel':int(parts[2])}
                        self.list_AO.append(d);


                if str.find("IOCHAN_DO_",0) == 0 :
                    if ( len(parts)>=3 and is_int(parts[2])) :
                        self.d_chan_to_name[int(parts[2])] = parts[1];
                        self.d_name_to_chan[parts[1]] = int(parts[2]);                                                                        
                        d = {'name':parts[1], 'channel':int(parts[2])}
                        self.list_DO.append(d);
                    
        fin.close();

    
    def get_list(self, lst, min, max) :
        l=[];
        d={}
        for v in lst :
            c = v['channel'];
            if (c >= min and c <= max) :
                if not d.has_key(c) :
                    d[c] = 1;
                    l.append(c);
        return l
    
    def get_AI_list(self, min, max) :
        return self.get_list(self.list_AI, min, max);

    def get_DI_list(self, min, max) :
        return self.get_list(self.list_DI, min, max);        

    def get_DO_list(self, min, max) :
        return self.get_list(self.list_DO, min, max);


    

    def channel_to_name(self, chan) :
        t = type(chan);
        tl = type([1,2]);
        ti = type(1)
        if t == ti :
            name = self.d_chan_to_name.get(chan, "UKN");
        elif t == tl :
            name = [self.d_chan_to_name.get(c, "UKN") for c in chan];
        else :
            name = [];
            
        return name;
    
    def name_to_channel(self, name) :
        t = type(name);
        tl = type([1,2]);
        td = type({1:2});        
        ts = type('str')


        """
        print name
        print t, tl, td, ts

        for k in self.d_name_to_chan.iteritems() :
            print k,
        """

        
        if t == ts :
            chan = self.d_name_to_chan.get(name.strip(), "UKN");
        elif t == td :
            chan = [self.d_name_to_chan.get(n.strip()) for n in name.keys()];            
        elif t == tl :
            chan = [self.d_name_to_chan.get(n.strip(), "UKN") for n in name];
        else :
            chan = [];
        
        return chan;



    def output_results(self, d) :
        for k,value in d.iteritems() :
            name = self.channel_to_name(k);
            print "%5d  %-25.25s : " % (k, name),
            for v in value :
                print "%5d " % v,
            print

    def write_results_to_file(self, fname, dict) :

        fout = open(fname,"w");

        fout.write("channel, name, values\n");
        for k,value in dict.iteritems() :
            name = self.channel_to_name(k);
            str = "%5d, %-25.25s, " % (k, name)
            fout.write(str);
            for v in value :
                s = "%5d, " % v
                fout.write(s);


            fout.write('\n');
        fout.close();




    


if __name__=="__main__":
    pf = parse_iochan();
    for p in pf.list_AI :
        print p;

    for p in pf.list_DI :
        print p;

    for p in pf.list_AO :
        print p;

    for p in pf.list_DO :
        print p;

    print
    print
    print
    l = pf.channel_to_name(range(0,5));
    print l

    n  = pf.name_to_channel('IOCHAN_AI_LDSGSC');
    print n


            



