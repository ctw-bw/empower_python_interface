import time

import platform
import sys
import socket;

import time
import string;

                
if __name__ == "__main__":    
    
    if (len(sys.argv[1:])>0) :
        HOST = sys.argv[1];
    
    if (len(sys.argv[1:])>1) :
        a = sys.argv[2];
        PORT = int(a)
    
    print "IP : %s" % (HOST)
    print "PORT : %s" % (PORT)
    
    
    # sys = platform.system()
    
    import msvcrt;
    import traceback
    
    
    #self.line_buf.replace("\n\r","\n");
    #self.line_buf.replace("\r\n","\n");
    #self.line_buf.replace("\r","\n");
    #eol = self.line_buf.find('\n');
    
    
    try :
        # ss = SyncReadSerial(COM_PORT-1, BAUD_RATE);
        # sp = serial.Serial(port=COM_PORT-1, baudrate=BAUD_RATE, timeout=0.05, rtscts=0)
        sp = socket.socket(socket.AF_INET, socket.SOCK_STREAM);
        
        sp.settimeout(1.5);
        sp.connect((HOST,PORT));
        print "Connected to :", HOST, PORT
        sp.settimeout(0.5);
    except :
        print
        print 'Error opening ', HOST, PORT
        traceback.print_exc();
        sys.exit()
        
    
    
    
    d1 = ''.join( [chr(n) for n in range(0,10)])
    d2 = ''.join( [chr(n) for n in range(11,13)])
    d3 = ''.join( [chr(n) for n in range(14,32)])
    del_chars = d1+d2+d3;
    
    #for i in del_chars :
    # print ord(i),
    
    
    timeout_cnt = 0;
    tbl = ''.join([chr(ch) for ch in range(0,256)])
    
    
    while 1 :
    
        # ln = sp.read(128);
        try :
            ln = sp.recv(128);
            l = ln.translate(tbl, del_chars);
            sys.stdout.write(l);
            timeout_cnt = 0;
    
        except socket.timeout :
            if (timeout_cnt == 0 or timeout_cnt >25) :
                sys.stdout.write('[TIMEOUT] ');
                timeout_cnt = 0;
            timeout_cnt += 1
            time.sleep(0.1)
        except :
            print "EXIT"
            traceback.print_exc();
            break;
            
        if (msvcrt.kbhit()) :
            ch = msvcrt.getch();
            if ord(ch) == 27 :
                break;
            
            # sp.SendChar(ch)
            sp.send(ch)
    
    
        
    
    

