
import getopt

# -p COM PORT (<100) or IP address port(>100)
# -b baudrate
# -i ip address
# -g get <ai, di, do, do>
# -s special command number  <followed by list of arguments>
# -v version <m s i>
# -n repeat command

def get_value(str) :
    u = str.upper()
    if u[0:2] == "0X" :
        v = int(u,16);
    else :
        v = int(u)
    return v




