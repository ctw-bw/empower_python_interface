# binascii and zlib crc return the same value, zlib.adler32 is different
# be aware that python has a signed crc issue
# http://bugs.python.org/issue1202,  fix is to AND with 0xffffffff

import hashlib
class string_hash :
    def __init__(self, length) :
        self.hash_length_chars = length
    
    def hash(self, db_string) :
        m = hashlib.md5();
        m.update(db_string);
        digest_str = m.hexdigest()[-self.hash_length_chars:];
        # print "digest_str", digest_str
        return digest_str


