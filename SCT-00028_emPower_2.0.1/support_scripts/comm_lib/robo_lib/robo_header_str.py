import bz2

import re


import c_support

import python_paths_default  # test for user's python_paths.py and update paths
import python_paths as pyenv
from python_utils.ByteStream import ByteStream


class robo_header_str :
    """class to read and decode the header string"""
    def __init__(self, ctrl, target_obj, debug_level=0) :
        self.pf = ctrl.pfcmd;
        self.ctrl = ctrl;

        self.header_str = None;
        self.shared_object_address_source = None;
        self.ctype_dict = {}
        self.ctype_list = [];
        self.debug_level = debug_level;

        self.target_obj = target_obj;
        
        self.read_header_addr();
 

    def read_header_addr(self) :
        # determine the address in flash memory of the ENCODERED HEADER STRING
        result = self.target_obj.read_shared_addr();
        self.length = result['v0']
        self.flash_address = result['v1'] + (result['v2'] << 16) ;


    def read(self) :
        PROG_MEM = 2;  # 2-> FLASH MEMORY
        # mem_id = self.ctrl.get_memory_id(self.target_processor, "FLASH", False);

        mem_id = self.target_obj.get_flash_mem_id();
        if mem_id and len(mem_id) == 2 :
            PROG_MEM = mem_id[1];
        else :
            print "*" * 100
            print
            print "ERROR : robo_header_str:read:get_memory_id returns: ", mem_id
            print
            print "*" * 100
            return None

        result_list = self.ctrl.readwrite_mem('R', PROG_MEM, self.length, self.flash_address, QUIET=True);


        if result_list :
            image_list = [result['buff'] for result in result_list];
            bytestream = self.ctrl.result_to_bytestream(result_list);
            # print "RHS_READ_bytestream", bytestream

            self.image = bytestream;
            try :
                self.header_str = bz2.decompress(self.image);
            except :
                print "*" * 50
                print self.image
                print "length of image  received : ", len(self.image)
                print "requested length %d @ $%x: " % (self.length, self.flash_address)
                print "*" * 50
                print
                print "invalid compressed image received from source."
                print
                print "*" * 50
                raise

        return self.header_str


    def decode(self) :
        """ make a dictionary of the typedef data structures """
        if not self.header_str :
            self.read()


        splt = self.header_str.split('###')

        self.ctype_dict = {}
        self.ctype_list = [];
        d = {}

        self.shared_object_address_source = splt[0];

        for str in splt[1:] :
            ctd = c_support.class_c_typedef(None);
            tname = ctd.from_db_string(str);   # convert the DB string into a type

            if tname :
                self.ctype_dict[tname] = ctd;
                self.ctype_list.append(ctd);
                # print ctd

            splt2 = str.split('!!!');

            if (len(splt2) == 3):
                typedef_name = splt2[0];
                field_list = splt2[1].split('^^^');
                struct_str = splt2[2];
            elif (len(splt2) == 2):
                #Deal with single declared variables:
                typedef_name = splt2[0];
                field_list = splt2[0];
                struct_str = splt2[1];

            dd = {};
            dd['typedef_name'] = typedef_name
            dd['field_list'] = field_list
            dd['struct_str'] = struct_str
            d[typedef_name] = dd;

        return d

    def get_type_obj(self, type_name) :

        """ if info about type_name exists, then return the class_c_typedef object"""
        ret_obj = None;

        if type_name in self.ctype_dict :
            ret_obj = self.ctype_dict[type_name]

        return ret_obj



    def decode_shared_object_source(self, debug_level = None) :
        if not self.shared_object_address_source :
            self.decode()

        if debug_level == None :
            debug_level = self.debug_level


        soas = self.shared_object_address_source.split('\n');

        type_dict = {}
        glb_dict = {}
        line_list = []
        obj_list = []
        pos_count = 0;

        # macro_list = ['TO_VOID','FLASH_ADDRESS_M','RAM_ADDRESS_M','EEPROM_ADDRESS_M', 'RAM_EEPROM_ADDRESS_M']
        macro_list = ['SHARED_RAM_OBJ_M','SHARED_EE_OBJ_M', 'SHARED_FLASH_OBJ_M']

        for raw_ln in soas :
            ln = raw_ln.strip();
            if len(ln) == 0 :
                continue
            if ln[0:2] == '//' :
                continue

            if debug_level >= 3 :                print ln


            pos = ln.find('//')
            if pos < 0 :
                pos = len(ln);

            ln_search = ln[:pos];  # ignore section that start with comments
            result = re.findall('(\w+)', ln_search);

            if result :
                macro = result[0]
                if macro in macro_list :
                    # pos = macro_list.index(macro)
                    try :
                        type_name = result[1]
                        glb_name = result[2]
                    except :
                        print "error parsing line : ", ln_search
                        raise();

                    result.insert(0, pos_count);

                    line_list.append(result);
                    d = {'macro':macro, 'type_name':type_name, 'var_name':glb_name, 'var_id':pos_count}

                    type_dict[type_name] = d;
                    glb_dict[glb_name] = d;
                    obj_list.append(d);
                    pos_count += 1
                else :
                    if debug_level : print "line ignored : ", ln_search
            else :
                if debug_level : print "line ignored : ", ln_search



        self.shared_object_obj_list = obj_list;
        self.shared_object_line_list = line_list;
        self.shared_object_type_dict = type_dict;
        self.shared_object_glb_dict = glb_dict;

        return glb_dict;
        return type_dict


    def get_type_by_variable_name(self, var_name) :
        """ use the shared object to convert the variable name into a type """

        ret_obj = None;

        if var_name in  self.shared_object_glb_dict :
            type_name = self.shared_object_glb_dict[var_name]['type_name']

            ret_obj = self.get_type_obj(type_name)

        return ret_obj


    def get_var_id_by_variable_name(self, var_name) :
        """ use the shared object to convert the variable name into a type """

        ret_obj = None;

        if var_name in  self.shared_object_glb_dict :
            ret_obj = self.shared_object_glb_dict[var_name]['var_id']

        return ret_obj


    def get_SO_list(self) :
        return self.shared_object_obj_list


    








if __name__ == "__main__" :
          # test for user's python_paths.py and update paths
    import pfcmd
    pf = pfcmd.PFCmd(0,0);
    COM_PORT=17;
    pf.connect(COM_PORT, 115200);

    ctrl = pfcmd.controller(pf);




    rhs = robo_header_str(ctrl);
    str = rhs.read();

    d = rhs.decode()
    
    glb = rhs.decode_shared_object_source();
    if 0 :
        for k,v in glb.iteritems() :
            print k
    if 0 :
        for ln in rhs.shared_object_line_list :
            print ln



