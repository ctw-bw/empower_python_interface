"""
this module joins several other modules:

robo_header_str
key_value_file
robo_table_xfer
"""

if __name__ == "__main__" :
          # test for user's python_paths.py and update paths
    import pfcmd

import sys
import time
import struct
import datetime
import python_paths_default  # test for user's python_paths.py and update paths
import python_paths as pyenv
from python_utils.class_to_str import *
import robo_header_str


if __name__ == "__main__" :
          # test for user's python_paths.py and update paths
    import pfcmd

import comm_lib.crc8 as crc8
import binascii
import zlib
# import binascii


from comm_lib.robo_lib import robo_table_xfer
import c_support

import key_value_file

import pprint
pp = pprint.PrettyPrinter(indent=4);

import pyshared_addresses

class robo_table :
    def __init__(self, ctrl, target_device="SC") :
        self.ctrl = ctrl;
        self.debug = None
        self.clear_kvf();

        self.target_obj = pyshared_addresses.pyshared_addresses(ctrl, target_device);
        self.target_device = target_device;
        
        # read the header string from the robot, and decode 
        # self.rhs = robo_header_str.robo_header_str(ctrl, self.target_device);
        self.rhs = robo_header_str.robo_header_str(ctrl, self.target_obj);
        self.rhs.decode()
        self.rhs.decode_shared_object_source();


        
    def compute_crc_of_self_image(self, table_image, type="CRC8") :
        # print "calc_crc length : ", len(bytestream)
        # print "calc_crc  : ", ByteStream(bytestream)

        if type == "CRC8" :
            c8 = crc8.CRC8();
            crc= c8.upd_crc_string(table_image.mem_image[1:]);

        elif type == "CRC32" :            
            crc = binascii.crc32(table_image.mem_image[4:]) & 0xffffffff;
        else :
            print "compute_crc_of_self_image:unknown CRC type", type
            crc = None
        return crc
        
    def process_magic_fields(self, table_image) :
        PYTABLE_MAGIC_STR_NEW = "robo_hdr.".upper()
        PYTABLE_MAGIC_STR = "pytable_".upper()

        # print
        # print "PROCESS MAGIC FIELDS (2)"
        # print
        
        crc_magic_field = None;
        
        for field in table_image.field_name_list :
            uf = field.upper()
            found = 1

            if uf.startswith(PYTABLE_MAGIC_STR) :
                magic_field = uf[len(PYTABLE_MAGIC_STR) : ]
            elif uf.startswith(PYTABLE_MAGIC_STR_NEW) :
                magic_field = uf[len(PYTABLE_MAGIC_STR_NEW) : ]
            else :
                found = 0
                
            if found :

                if table_image.debug :
                    print "magic_field:",uf, magic_field
                    
                if magic_field == "CRC8" or magic_field == "CRC32" :
                    crc_magic_field = field;
                    crc_type = magic_field 
                    table_image.set_field_value(field,  0);
                    
                elif magic_field == "LENGTH" :
                    table_image.set_field_value(field,  table_image.calc_img_len);
    
                elif magic_field == "HASH32" or magic_field == "MD5HASH32" :
                    table_image.set_field_value(field,  table_image.type_obj.hash32_val);
    
                elif magic_field == "TIMESTAMP":
                    timestamp = make_timestamp()
                    table_image.set_field_value(field,  timestamp);
                elif magic_field == "DATE":
                    seconds_since_jan1_y2k = round((datetime.datetime.utcnow() - datetime.datetime.strptime("1 Jan 2000", "%d %b %Y")).total_seconds())
                    table_image.set_field_value(field,  seconds_since_jan1_y2k);
                elif magic_field == "RSV" :
                    pass
    
                else :
                    print
                    print "unknown magic field", uf, magic_field
                    print "sleeping 3 sec"
                    print
                    time.sleep(3.0);

        # print "process_magic_fields:pprint:::::"
        # pprint.pprint(self.value_dict)
        
        table_image.dict_to_image() # rebuild the self.mem_image bytestream
        
        if crc_magic_field :  # must compute after other magic fields are set
            crc = self.compute_crc_of_self_image(table_image, type = crc_type)
            # print "NEW CRC", crc
            # print "BEFORE",self.get_field_value(crc_magic_field);
            table_image.set_field_value(crc_magic_field,  crc);
            # print "AFTER",self.get_field_value(crc_magic_field);
        
        
    def write_table(self, var_name, named_dict, debug_level=None) :


        # print "var_name : ", var_name
        # print "named_dict : ", named_dict

        
        var_id = self.rhs.get_var_id_by_variable_name(var_name)
        if debug_level >= 1 : print "write_table : ", var_name, var_id
        
        table_xfer = robo_table_xfer.robo_table_xfer(self.ctrl, self.target_obj, var_id)      # XFER object for this variable ID
        
        if debug_level >= 2 : print "write_table, TABLE XFER: ", table_xfer

        tobj = self.rhs.get_type_by_variable_name(var_name)
        if debug_level >= 1 : print "write_table : tobj ", tobj

        if tobj :

            var_obj =  c_support.c_global(tobj, var_name);


            
            if debug_level >= 1 : print "write_table : var_obj ", var_obj
            
            if debug_level >= 1 :
                print "#"*100
                print "NAMED_DICT:", named_dict

            
            str = var_obj.dict_to_image(named_dict);  # convert named_dict to am inage *AND* copy INTO the calling object self image

            self.process_magic_fields(var_obj)  # update the calling OBJECT self image
            if debug_level >= 1 :            
                print "!"*100
                print "VAR_OBJ:", var_obj


            # print "NAMED_DICT:", named_dict
            # str = var_obj.dict_to_image(named_dict);               # CEB removed
            # line removed as the magic filed update operates on the var_obj where as supplying  named_object
            # caused var_obj internal image to be over written with the named_object image.
            # however, ONLY the fields that exist in the named_dict are overwritten - thus with robo_config
            # because the KVF doesn't contain "pytable_crc8" - the process_magic_field isn't overwritten!!!!

            # this is the new line : need to confirm that it don't effect robo_config
            # verified
            str = var_obj.dict_to_image();   # make an image using the calling opject self

            
            bytestream = robo_table_xfer.ByteStream(str)
            # print bytestream

            

            if debug_level >= 1 : 
                print
                print "** robo_table:write_table:bytestream  : ", len(bytestream)
                print "** robo_table:write_table:bytestream  : ", bytestream            
                print

            if table_xfer.requires_header() :
                tobj.insert_header(table_xfer);
                
            table_xfer.set_type_obj(tobj);
            
            result = table_xfer.write(bytestream);   # actually write the table


            if debug_level >= 1 : print "write_table : result ", result
            
        
    def read_table(self, var_name, debug_level=None) :

        var_id = self.rhs.get_var_id_by_variable_name(var_name)
        if debug_level >= 1 : print "read_table : ", var_name, var_id

        table_xfer = robo_table_xfer.robo_table_xfer(self.ctrl, self.target_obj, var_id)      # XFER object for this variable ID
        # print table_xfer.printable();
        
        if debug_level >= 2 : print "read_table, TABLE XFER: ", table_xfer

        tobj = self.rhs.get_type_by_variable_name(var_name)            


        glb_dict = None;
        if tobj :
            if table_xfer.requires_header() :
                tobj.insert_header(table_xfer);
                
            table_xfer.set_type_obj(tobj);
            if debug_level >= 2 :
                print "robo_table:read_table():type_obj:", tobj

            
            bytestream = table_xfer.read();   # read the memory image for this table
            if debug_level >= 1:
                print
                print "***robo_table:read_table:bytestream  : ", len(bytestream)
                print "***robo_table:read_table:bytestream  : ", bytestream            
                print
                
            if table_xfer.requires_header() :
                header_str = table_xfer.make_header_str (bytestream)
                if debug_level >= 1:                
                    print "robo_table:read_table:header_str : ", header_str
                valid = table_xfer.header_validate(bytestream);
                header_struct_str = table_xfer.get_struct_str();
            else :
                header_struct_str = None;

            if bytestream : 
                glb_obj = c_support.c_global(tobj, var_name, IMAGE=bytestream);    # create a C_global to locally hold the robot value

                
                if debug_level >= 3:
                    print "*" * 90
                    print "robo_table_glb_obj:", glb_obj
                    print "*" * 90
                glb_dict = glb_obj.get_dict();

                
                # print "robo_table_glb_dict:", glb_dict
                try: 
                    self.kvf.new_section(glb_dict)
                except: #Just add the updated values from the old section:
                    self.kvf.section_dict[glb_dict.get_name()] = glb_dict  # TODO - replace with a deepcopy
        
                    pass
                    # "Re-reading known section."
                    
            if self.debug :
                print "robo_table_glb_obj (using __str__ to print) :", glb_obj   # 

                


        return glb_dict


    def clear_kvf(self) :
        self.kvf = key_value_file.key_value_file();

    def get_kvf(self) :
        return self.kvf

    def show_avail(self, fout=sys.stdout) :
        """ display the shared object table """
        so_list = self.rhs.get_SO_list();
        fout.write( '\r\n');        
        fout.write( "%4s %-35s %-35s :: %s\r\n" % ('ID','(virtual)variable_name', 'C typedef name','table XFER properties'));
        fout.write( "-" * 120+'\r\n');
        for so in so_list :
            type_name = so['type_name']
            var_name = so['var_name']
            var_id = so['var_id']
            prop = robo_table_xfer.robo_table_properties(self.ctrl, var_id, self.target_obj);
            prop_str = prop.printable();
            # fout.write( "%4d %-35s %s\n" % (var_id, type_name, var_name) );
            fout.write( "%4d %-35s %-35s :: %s\r\n" % (var_id, var_name, type_name, prop_str) );

    
    def get_avail_tables(self):
        """ Returns the shared object table as a list of tables. """
        so_list = self.rhs.get_SO_list();
        keys =  ('ID','(virtual)variable_name', 'C typedef name','table XFER properties')
        for so in so_list :
            prop = robo_table_xfer.robo_table_properties(self.ctrl, so['var_id'], self.target_obj);
            so['details'] = prop.printable();
          
        return so_list

    def dump_table(self, fout=sys.stdout) :
        fout.write(self.rhs.header_str + '\n');
            
    def dump_decode_table(self, fout=sys.stdout) :
        """ display the shared object table """
        lst = self.rhs.header_str.split('###')
        for tobj in lst[1:] :
            segs = tobj.split('!!!');

            fout.write( "typedef struct {   // %s\n" % segs[2]);
            fields = segs[1].split('^^^');
            str = '\t' + ';\n\t'.join(fields)+';\n'
            fout.write( str);
            fout.write("} %s;\n\n" % segs[0]);




            
        

    def read_all(self, debug_level=0) :
        self.clear_kvf()
        
        so_list = self.rhs.get_SO_list();

        for so in so_list :
            var_name = so['var_name']
            print "#READING ", var_name

            glb_dict = self.read_table(var_name, debug_level = debug_level);

            
        return self.kvf

            



    def read_table_list(self, table_list=[]) :        
        pass


if __name__ == "__main__" :
          # test for user's python_paths.py and update paths
    import pfcmd
    
    pf = pfcmd.PFCmd(0,0);
    COM_PORT=41;
    pf.connect(COM_PORT, 115200);
    pf.quiet = True
    ctrl = pfcmd.controller(pf);
    
    operational = pf.test_operational() 
    rt = robo_table(ctrl)
    kvf = key_value_file.key_value_file();

    rt.show_avail()
    tbl = rt.read_table('EE_system_calb_param')
    print tbl
