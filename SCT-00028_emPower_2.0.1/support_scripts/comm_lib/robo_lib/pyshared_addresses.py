import python_paths_default  # test for user's python_paths.py and update paths
import python_paths as pyenv
from python_utils.class_to_str import *

class pyshared_addresses : 
    def __init__(self, ctrl, target = "SC") :
        #print "#" * 100
        #print "pyshared_addresses with ", target
        # print "#" * 100        
        self.ctrl = ctrl;
        self.pf = ctrl.pfcmd;        
        

        self.target_processor = target.upper();

        varid_map = {'SC':162, 'MC':1162, 'IMU':2162}
        self.varid_addr_cmd = varid_map[self.target_processor];

        object_map = {'SC':166, 'MC':1166, 'IMU':2166}
        self.shared_object_cmd = object_map[self.target_processor];
        
    def get_flash_mem_id (self) :
        mem_id = self.ctrl.get_memory_id(self.target_processor, "FLASH");
        return mem_id

    def get_mem_id (self, mem_type) :
        mem_id = self.ctrl.get_memory_id(self.target_processor, mem_type);
        try : 
            code = mem_id[1];
        except :
            print "pyshared_address:unknown mem_type", self.target_processor, mem_type, mem_id
            code = None
        return code
        
        
    def read_shared_addr(self) :
        # determine the address in flash memory of the ENCODERED HEADER STRING
        result = self.pf.do_command('special',[self.shared_object_cmd]);
        return result


    def read_calb_table_properties(self,var_id=0) :
        #print "#" * 100
        #print "pyshared_addresses: read_calb_table_properties", var_id
        # print "#" * 100
        
        result = self.ctrl.pfcmd.do_command('special', [self.varid_addr_cmd, var_id])
        return result;



    def __str__(self) :
        return class_to_str(self)
