import os


import struct
import python_paths_default
import python_paths as pyenv

DB_STRING_SEP = '!' * 3
DB_STRING_FIELD_SEP = '^' * 3

from python_utils.class_to_str import *
import time
import string
import table_string_hash
import types
import python_utils.named_ordered_dict as named_ordered_dict
import binascii
import re

from python_utils.ByteStream import ByteStream

    

def struct_str_to_list(str) :
    alignment = "@=<>!"
    mark = None
    lst = [];
    mode = ""
    for pos in range(0, len(str)) :
        ch = str[pos];
        if ch in alignment :
            mark = None
            mode = ch;
            # print "mode",mode
                
        elif ch in string.digits :
            if mark == None :
                # print "mark:", pos
                mark = pos;
            
        elif ch in string.ascii_letters :
            if mark :
                fmt = str[mark:pos+1]
                lst.append(mode + fmt)
                # print fmt
                mark = None;
            else :
                lst.append(mode + ch)
    return lst
            
            
def make_timestamp(now=None) :
    """ call with now=time.localtime() """
    if now == None :
        now = time.localtime();
        
    keys = ['year','month','day','hour','min','sec']
    d = {}
    for k,v in zip(keys, now) :
        d[k] = v;
        
    serial = (d['year'] - 2009) * 86400*12*31 +\
        d['month'] * 31 * 86400 +\
        d['day'] * 86400 +\
        d['hour'] * 3600 +\
        d['min'] * 60 +\
        d['sec'];
    return serial;



class class_c_typedef :
    # object for holding relevent information for a datatype in C (eg typedef)
    def __init__(self, type_name, name_list=[], type_list=[], struct_list=None, header_obj=None) :
        self.hash32_str = None;
        self.hash32_val = None;
        
        self.hash_length_chars = 8
        
        self.field_name_list = []
        self.struct_list = []
        self.field_type_list = []                
        
        self.set_type_name(type_name);
        self.set_field_names(name_list);                
        self.set_field_types(type_list)
        self.set_struct_list(struct_list);
        self.header_obj = header_obj;
        self.debug = None;
        
    def __str__(self) :
        return class_to_str(self)

    if 0 :
        def insert_header(self,header_obj) :
            if self.header_obj == None :
                self.header_obj = header_obj

                name_list = header_obj.get_field_name_list();
                name_list.reverse()
                for name in name_list :
                    self.field_name_list.insert(0, name)
                self.set_field_names();

                header_struct_str = header_obj.get_struct_str()
                self.set_struct_list(header_struct_str + self.struct_list);

                type_list = header_obj.get_field_type_list();        
    
    def set_type_name(self, name) :
        # set the (C) name of the data structure
        if name : 
            self.type_name = name.strip();
        else :
            self.type_name = name;

    def get_type_name(self) :
        return self.type_name

    def set_field_names(self, name_list=None) :
        # specifiy the members in the data structure
        if name_list :
            self.field_name_list = name_list

        
        self.field_name_to_pos_dict = {}
        self.field_pos_to_name_dict = {}
        # implmenting an ordered dictionary
        for pos, name in zip( xrange(0,9999), self.field_name_list) :
            self.field_name_to_pos_dict[name] = pos;
            self.field_pos_to_name_dict[pos] = name            


    

        
    def get_field_position(self, field_name) :
        # return the list index  based on the field_name
        return             self.field_name_to_pos_dict[field_name]

    
    def set_struct_list(self, struct_str_or_list=None) :
        # provide a list of 'struct chars' that specify the data type in the typedef
        # print "set_struct_list : ", self.type_name, struct_list

        if type(struct_str_or_list) == types.StringType :  # need to convert to a list
            struct_list = struct_str_to_list(struct_str_or_list)
        else :
            struct_list = struct_str_or_list;
            # print "set_struct_list:", struct_list
            
        if struct_list : 
            self.struct_list = struct_list;
            
        self.field_name_to_offset = {}
        offset = 0;
        if self.struct_list :
            # print "setting offsets:",self.field_name_list
            for struct_fmt, name in zip(self.struct_list, self.field_name_list) :
                self.field_name_to_offset[name] = offset;
                # print "IMG LEN", struct.calcsize('<'+struct_fmt), struct.calcsize(struct_fmt)
                sz = struct.calcsize('<'+struct_fmt);
                offset += sz;
                # print "set_struct_list:",struct_fmt, sz, offset
                # print name, offset
                
        self.object_length_bytes = offset;

    def set_field_types(self, type_list=None) :
        if type_list : 
            self.field_type_list = type_list
            

    
    
    def from_db_string(self, string) :
        # convert the database string back to the python data structure
        td_list = string.split(DB_STRING_SEP);
        if td_list and len(td_list) == 3 :
            self.set_type_name( td_list[0] );
            
            
            # field_name_list = split(DB_STRING_FIELD_SEP, td_list[1] )
            field_name_list = td_list[1].split(DB_STRING_FIELD_SEP)
            new_field_name_list = []

            
            for field in field_name_list :    # this for loop goes over the field names and remove the ARRAY porition of the field name
                ln = field
                obj = re.match('([\w\d\.]+)', ln)
                if obj :
                    key = obj.group(1)
                    
                    ln = ln[ obj.end() :];
                    new_field_name_list.append(key)
                else :
                    if (len(field) > 0):
                        print "#c_support.from_db_string : bad field name: (%s)" % field
                    continue
                arry = 1;
                
                obj = re.match('\s*[[]\s*(\d+)\s*[]]', ln)         # look for " [ /d ] " for an array
                if obj :
                    arry = int(obj.group(1))
                    ln = ln[ obj.end() :];
                    if self.debug : 
                        print "#c_support.from_db_string:ARRY MATCH : ", obj.group(0), obj.group(1), obj.end()

                    
            
            # self.set_field_names( field_name_list);
            self.set_field_names( new_field_name_list);

            struct_list = [ch for ch in td_list[2]];
            self.set_struct_list( td_list[2] );
            
        self.hash32_str = self.string_to_hash(string)
        self.hash32_val = int(self.hash32_str, 16) & 0xffffffff
        
        return self.get_type_name()

    def string_to_hash(self, db_string) :
        tsh = table_string_hash.string_hash(8);
        return tsh.hash(db_string);















class c_global :
    
    def __init__(self, type_obj, name, IMAGE=None) :
        
        # class_c_typedef.__init__(self, type_name);
        
        self.name = name;
        
        self.value_dict = named_ordered_dict.NamedOrderedDict(name);
        self.struct_dict = named_ordered_dict.NamedOrderedDict(name);

        
        
        for field_name in type_obj.field_name_list :
            self.value_dict[field_name] = 'NA';
            
        for field_name,struct_str in zip(type_obj.field_name_list, type_obj.struct_list) :
            self.struct_dict[field_name] = struct_str;
            
        
        
        
        self.type_obj = type_obj;
        self.struct_list = type_obj.struct_list;
        
        self.field_name_list = type_obj.field_name_list;
        self.field_type_list = type_obj.field_type_list;

        self.calc_img_len = self.type_obj.object_length_bytes
        
        # print "IMG LEN", struct.calcsize('<' + self.struct_str), struct.calcsize(self.struct_str);
        # print "struct str ", self.struct_str
        # print "c_global IMAGE::: ", IMAGE
        self.debug = None;

        if IMAGE and len(IMAGE) != self.calc_img_len :
            print "*" * 80
            print
            print "class c_global: the supplied image length (%d) does not match the expected length (%d)" % (len(IMAGE), self.calc_img_len)
            print "can't convert the image to a structure.  Expect values to be NA"
            print
            print "sleeping for 10 sec"
            print
            time.sleep(10.0)

        if IMAGE and len(IMAGE) == self.calc_img_len :
            self.mem_image = IMAGE
            self.image_to_list();
        else :
            self.mem_image = chr(0) * self.calc_img_len


            

    def __str__(self) :
        return class_to_str(self)
    
    def get_name(self) :
        return self.name

    def get_dict(self) :
        # return a named ordered "value" dictionary with name=c_global_name, and key:value are the fields
        return self.value_dict

   
    
                   
    def image_to_list(self, image = None) :
        if not image  :
            image = self.mem_image
            
        # print "image_to_list:",image

        # struct_str = '='+self.struct_str
        # obj_list = struct.unpack(struct_str, image)
        
        pos = 0;
        obj_list = []
        for struct_fmt in self.struct_list :
            #print struct_fmt
            #print 'IMG LEN', struct.calcsize('<'+struct_fmt), struct.calcsize(struct_fmt)
            sz = struct.calcsize('<'+struct_fmt);
            #print pos, sz, len(image[pos: pos+sz]);
            struct_val = struct.unpack('<'+struct_fmt,   image[pos: pos+sz]);
            val = list(struct_val)
            
            obj_list.append(  val)
            pos += sz;
            
        # populate the value dict
        for key,val in zip(self.value_dict.keys(), obj_list) :
            self.value_dict[key] = val;
            
        return list(obj_list)


    
    def dict_to_image(self, val_dict=None) :
        """ convert the supplied ordered value dictionary into an image """
        if self.debug : 
            print "&&&&&&&&&&&&&&& from dict_to_image:", val_dict
            print "&&&&&&&&&&&&&&& from dict_to_image:", self.value_dict

        if val_dict : 
            for field_name in val_dict :
                value = val_dict[field_name]
                if type(value) == type("") :
                    print "bad type in val_dict: ", field_name, value
                    value = [0]
                    
                if field_name in self.value_dict :
                    self.value_dict[field_name] = value
                else :
                    print "attempt to add invalid field to value dict:", field_name, value

        lst = []                    
        for field_name, value in self.value_dict.iteritems() :
            if type(value) == type("") :
                print "dict_to_image:bad value type : ", field_name, value
                value = [0]
                self.value_dict[field_name] = value
            
            lst = lst + value
            if field_name not in self.value_dict :
                print "dict_to_image: adding unknown field (%s) to self.value_dict " %(field_name)

            # print value

        struct_fmt = '='+''.join(self.struct_list)
        lst.insert(0, struct_fmt);

        if self.debug : 
            print "dict_to_image:", lst
        
        self.mem_image = apply(struct.pack, lst)


        
        # print lst
        # print len(image), struct_fmt
        # print ByteStream(image)

        return self.mem_image;



    
    def set_field_value(self, field_name, value) :
        # set the value of a specific field in the object's image
        
        if field_name not in self.value_dict :
            print "dict_to_image: adding unknown field (%s) to self.value_dict " %(field_name)

        ###############
        # the follow code is used to verify that the data object will fit.
        pos = self.type_obj.get_field_position(field_name);        
        struct_fmt = self.struct_list[pos]
        #print '#'*75
        sz = struct.calcsize('<'+struct_fmt);
            
        try :
            if type(value) == types.ListType :
                list_value = list(value)
            else :
                list_value = [value]            

            args = list(list_value)
            args.insert(0, struct_fmt);
            bin = apply(struct.pack, args);
        except :
            print "value (%s) doesn't fit in this field (%s) " % (value, field_name)
            print pos, struct_fmt
            raise;
        #
        ###############

        self.value_dict[field_name] = list_value
        hex = "$%2.2X" % value
        if self.debug : 
            print "set field %30.30s with %10.10s, %10.10s " % (field_name, value, hex)








