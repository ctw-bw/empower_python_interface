#Turns the robo_lib folder into a module that can be imported at the top level.

import os, sys


is_frozen = bool( getattr(sys, 'frozen', None) )
# Get path variable
if not is_frozen:
    # not frozen: in regular python interpreter
    approot = os.path.abspath(os.path.dirname(__file__))
else: 
   # py2exe package:
   approot = os.path.abspath(os.path.dirname(sys.executable))


for module in os.listdir(approot):
    if module == '__init__.py' or module[-3:] != '.py':
        continue
    __import__(module[:-3], locals(), globals())
del module
