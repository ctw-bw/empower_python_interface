
import python_paths_default  # test for user's python_paths.py and update paths
import python_paths as pyenv

import re, types
from python_utils.class_to_str import *
import optparse
import struct, sys, os
import named_ordered_dict

import copy
    
class key_value_file :

    def __init__(self,fname=None, debug_level = 0) :
        self.debug_level = debug_level;
        self.section_list = []
        
        self.clear(fname)

        re_sep = '[,;:= ]+'
        re_pat = '(\w+)\s*%s\s*(.*)' % re_sep
        self.re_obj_1 = re.compile(re_pat);
        #
        #  test case
        #  re.findall('\s*(\w+)\s*([[][ 0-9]*[]])?\s*([;,:= ])?\s*([.\d]+)','abc_123 [ 19] = 15.4;')
        #
        re_pat = '\s*(\w+)\s*([[][ 0-9]*[]])?\s*([;,:= ])?\s*([.\d]+)'
        self.re_obj_2 = re.compile(re_pat);
        
        
        self.re_sec_obj = re.compile('^[[](.+)[]]');
        if fname :
            self.read_file(fname);

    def __str__(self) :
        return class_to_str(self)

    def clear(self, name) :
        self.section_dict = named_ordered_dict.NamedOrderedDict(name);        

    def read(self, fin, debug_level=None) :
        
        if debug_level == None :
            debug_level = self.debug_level
            
        ln_cnt = 0;
        for raw_ln in fin :
            ln_cnt += 1;            
            ln = raw_ln.strip();
            if not ln :
                continue
            if ln[0] == '#' :
                continue
            
            sec = re.findall(self.re_sec_obj, ln)
            if sec :
                section_name = sec[0];                

                if section_name in self.section_dict : 
                    if (debug_level >= 1) : print "duplicate section", section_name
                    section = self.section_dict[section_name];
                else :
                    if (debug_level >= 2) : print "adding new section", section_name                   
                    # create a new section
                    section = self.new_section_name(section_name);
                    # self.section_dict[section_name] = named_ordered_dict.NamedOrderedDict(section_name);
                    # section = self.section_dict[section_name];                    

                    # print "NEW SECTION : ", section
                continue

            if 0 :
                splt = re.findall(self.re_obj_1, ln)
                if not splt :
                    print "ignoring line %d (%s)" % (ln_cnt, ln)
                    continue

                key = splt[0][0];
                val = splt[0][1];
            if 1 :

                obj = re.match('([\w\d.]+)', ln)
                if obj :
                    key = obj.group(1)
                    ln = ln[ obj.end() :];
                    # print "key ", key, obj.end(), ln
                else :
                    print "skip",ln
                    continue
                    
                arry = 1;
                obj = re.match('\s*[[]\s*(\d+)\s*[]]', ln)         # look for " [ /d ] " for an array
                if obj :
                    arry = int(obj.group(1))
                    ln = ln[ obj.end() :];
                    # print "arry ", arry, obj.end(), ln


                obj = re.match('\s*([=:;, ])', ln) 
                if obj :
                    sep = obj.group(1)
                    ln = ln[ obj.end() :];                    
                    # print "sep", sep, obj.end(), ln

                val_list = []
                while 1 :
                    # the \d in the follow RE matchs any digit  (not the letter 'd'
                    # match $xX for hex
                    obj = re.match('\s*[,;:]?\s*([-\.\deE+\$xX]+)', ln)
                    if obj :
                        # figure the type of the number
                        str = obj.group(1).upper();
                        try : 
                            if str.find('.')>=0 or str.find('E')>=0 :
                                val = float(str);
                            elif str[0] == "$" :
                                val = int(str[1:], 16);
                            elif str[0:2] == "0X" :
                                val = int(str[2:], 16);
                            else :
                                val = int(str, 10);
                        except :
                            val = 0;

                        # print "value : ", val, str


                        val_list.append(val)
                        ln = ln[ obj.end() :];                                        
                        # print "val", val, obj.end()
                    else :
                        break

                
                if debug_level :
                    print key, arry, sep, val_list
                
                if arry != len(val_list) :
                    print arry, len(val_list)
                    print "key_value_file:read:number of elements do not match. The line from the file was:"
                    print '"%s"' % raw_ln.strip()


            if key in section :
                if (debug_level >= 1) : print "duplicate key (%s) on line %d (%s)" % (key, ln_cnt, ln)

            section[key] = val_list
            if (debug_level >= 3) : print "adding",key,val_list
            
            if (debug_level >= 4) :
                print "-" * 70
                for k,v in self.section_dict.iteritems() :
                    print "key=(%s)" %k,v

            # self.write(sys.stdout)

        return self.section_dict

    
    

    def new_section_name(self, section_name, key_list=None) :
        
        if section_name in self.section_dict :
            raise ValueError('new_section : section (%s) already exists.' % section_name);

        self.section_dict[section_name] = named_ordered_dict.NamedOrderedDict(section_name)
        section = self.section_dict[section_name];
        
        return section
        

    def new_section(self, NO_dict) :
        # add a section object into the KVF object
        section_name = NO_dict.get_name();
        # print "new_section : dict : ",NO_dict
        
        if section_name in self.section_dict :
            raise ValueError('new_section : section (%s) already exists.' % section_name);
            
        # add the section

        self.section_dict[section_name] = NO_dict  # TODO - replace with a deepcopy
        # print "new section return : ", self.section_dict[section_name]
        
        return self.section_dict[section_name] 

        
    def merge(self, kvf_obj, debug_level = None) :
        # merge the provided kvf_obj into the present KVF
        # kvf_obj must be a named_ordered_dict of named_ordered_dicts
        # print "MERGE:", kvf_obj

        if debug_level == None :
            debug_level = self.debug_level        
        
        for section_name, NO_dict in kvf_obj.section_dict.iteritems() :
            
            
            if debug_level >= 1 : print "merge section : ", section_name
            
            if section_name in self.section_dict :
                # both KVFs have the same section name, merge the keys
                print "section exists : ", section_name
                self.section_dict[section_name].update(NO_dict);
            else :
                # print "merge dict :", NO_dict
                new_dict = self.new_section(NO_dict);  
                # print "NEW_DICT :", new_dict

    def compare(self, kvf_obj, section_name = None, debug_level = None,compare_fh = None) :
        # compare the self KVF with the provided one
        # print
        # print "*" * 100
        # print
        if debug_level == None :
            debug_level = self.debug_level        
        # print kvf_obj
        # print kvf_obj.keys()


        if section_name == None : 
            section_name = kvf_obj.get_name()
        
        # print self.section_dict
        # print self.section_dict.keys()
        if section_name not in self.section_dict :
            print "KVF file does not contain section : %s" % section_name
            return None;
        
        sec = self.section_dict[section_name];
        self_set = set(sec.keys());
        kvf_set = set(kvf_obj.keys());

        # print kvf_set-self_set   # fields in kvf_set but not in self_set
        # print self_set-kvf_set   # fields in
        test_set= self_set & kvf_set   # fields in both
        # print test_set
        errorCount = 0
        for field_name, self_val in sec.iteritems():
            # if debug_level >= 1 : print "field : ", field_name, val
            if field_name in kvf_obj :
                kvf_val = kvf_obj[field_name];
                print field_name, type(self_val[0]), type(kvf_val[0])            
                
                if debug_level >= 5 : print "field : ", field_name, self_val, kvf_val
                if type(kvf_val[0]) is int and type(self_val[0]) is int :
                    # INT to INT Compare
                    # print 'INT/INT'
                    if kvf_val != self_val :
                        print "CALIBRATION PARAM MISMATCH (int/int): %s" % (field_name)
                        print '\tANKLE:', kvf_val
                        print '\tFILE: ', self_val
                        # , "Types are (self,kvf):", type(self_val[0]), type(kvf_val[0])
                        print
                        errorCount += 1

                        # TODO write to compare file
                        if compare_fh :
                            compare_fh.write("CALIBRATION PARAM MISMATCH (int/int): %s" % (field_name))
                            compare_fh.write('\n')
                            compare_fh.write('\tANKLE:')
                            compare_fh.write(','.join(['%d' % elm for elm in kvf_val]))
                            compare_fh.write('\n')
                            compare_fh.write('\tFILE:')
                            compare_fh.write(','.join(['%d' % elm for elm in self_val]))
                            compare_fh.write('\n')

                    else :
                        # print "value perfectmatch:", field_name, self_val, kvf_val
                        # , "Types are (self,kvf):", type(self_val[0]), type(kvf_val[0])
                        pass

                elif type(kvf_val[0]) is int or type(self_val[0]) is int :
                    # INT to FLOAT Compare (doesn't matter which is int and which is float)
                    for i in range(0,len(kvf_val)) :
                        kvf_i32  = int(kvf_val[i])
                        self_i32 = int(self_val[i])
                        # print 'INT/FLOAT'
                        if kvf_i32 != self_i32 :
                            print "CALIBRATION PARAM MISMATCH (int/float): %s" % (field_name)
                            print '\tANKLE:', kvf_i32
                            print '\tFILE: ', self_i32
                            print
                            errorCount += 1

                            # TODO write to compare file
                            if compare_fh :
                                compare_fh.write("CALIBRATION PARAM MISMATCH (int/float): %s" % (field_name))
                                compare_fh.write('\n')
                                compare_fh.write('\tANKLE: %d' % kvf_i32)
                                compare_fh.write('\n')
                                compare_fh.write('\tFILE:  %d' % self_i32)
                                compare_fh.write('\n')

                        else :
                            # print "value perfectmatch:", field_name, self_i32, kvf_i32
                            # , "Types are (self,kvf):", type(self_val[0]), type(kvf_val[0])
                            pass

                else :
                    # FLOAT to FLOAT Compare (force precision to be 32bits for both)
                    fmt = '>f'
                    for i in range(0,len(kvf_val)) :
                        kvf_v64  = kvf_val[i]
                        self_v64 = self_val[i]

                        # HACK to convert a 64 bit float to 32 bit resolution, note that the data
                        # is still in 64 bit format, but it's truncated within to be equivalent to
                        # a 32 bit float
                        kvf_v32  = struct.unpack(fmt,struct.pack(fmt,kvf_v64))
                        self_v32 = struct.unpack(fmt,struct.pack(fmt,self_v64))

                        # print 'FLOAT/FLOAT'
                        if kvf_v32 != self_v32 :
                            print "CALIBRATION PARAMETER MISMATCH (float/float): %s" % (field_name)
                            print '\tANKLE:', kvf_v32
                            print '\tFILE: ', self_v32
                            print
                            errorCount += 1

                            # TODO write to compare file
                            if compare_fh :
                                compare_fh.write("CALIBRATION PARAM MISMATCH (float/float): %s" % (field_name))
                                compare_fh.write('\n')
                                compare_fh.write('\tANKLE: %f' % kvf_v32)
                                compare_fh.write('\n')
                                compare_fh.write('\tFILE:  %f' % self_v32)
                                compare_fh.write('\n')

                        else :
                            # print "value perfectmatch:", field_name, self_v32, kvf_v32
                            # , "Types are (self,kvf):", type(self_val[0]), type(kvf_val[0])
                            pass

        return errorCount
                

        # value_mismatch
        # key_mismatch



    
    def get_section(self, section_name) :
        if section_name in self.section_dict :
            return self.section_dict[section_name];

    def get_section_list(self) :
        return self.section_dict.keys()

        
    def write(self, fout, file_header=None, sec_header_func=None, PPRINT=False) :
        # write all the sections to a file
        # fout = open(file, 'w');

        # print "write:", self.__dict__
        
        if file_header :
            fout.write(file_header);
            
        for section_name, val in self.section_dict.iteritems() :
            # print "SECTION : ", section_name
            if sec_header_func :
                str = sec_header_func(section_name)
                fout.write(str);

            fout.write('#\n#\n');
            fout.write(  '[%s]\n' % section_name);

            for key, val in val.iteritems() :

                a = len(val)
                if type(val) == type([]) :
                    str_list = [];
                    for v in val :
                        if type(v) == type(1L) :
                            str = "$%X" % v
                        else :
                            str = "%s" % v
                        str_list.append(str);
                    val = ', '.join(str_list);
                                        
                            
                            
                                     
                # print "KEY : ", key

                if a > 1:
                    key_x = "%s[%d]" % (key,a)
                else :
                    key_x = "%s" % key

                if PPRINT : 
                    fout.write( '%45s : %s\n' % (key_x, val))
                else:
                    fout.write( '%s : %s\n' % (key_x, val))

            fout.write( '\n');

            # fout.close();





######################
class c_struct :
    pass

   

def key_value_file_options(args) :

        usage = "usage: %prog [options]"
        parser = optparse.OptionParser();

        parser.add_option("-p", "--path", dest="filename_path", type="str", default = "", help = "path to append to file names");
        parser.add_option("-f", "--filename", action="append", dest="filename", help = "KVF files to read, repeat option to read a list of files.");
        parser.add_option("-w", "--write_filename", dest="write_filename", type="str", help = "name of the KVF file to write.")

        parser.add_option("-d", "--display", dest="display", action="store_true");
        
        # parser.disable_interspersed_args()
        (options, args) = parser.parse_args(args);

        # make a struct from the options dictionary
        opt = c_struct();
        for k,v in options.__dict__.iteritems() :
            opt.__dict__[k] = v

        return opt





            

if __name__ == "__main__" :

    opt = key_value_file_options(sys.argv) 
        
    kvf_master =  key_value_file(debug_level=1)
    for fname in opt.filename :
        file_name = os.path.join(opt.filename_path, fname)
        kvf_temp =  key_value_file(debug_level=0)
        
        try :
            fin = open(file_name);
            print "*****      reading file : ", file_name            
        except :
            print "***** can not open file : ", file_name
            fin = None
            
        if fin : 
            kvf_temp.read(fin);
            fin.close()
        
        # kvf_temp.write(sys.stdout, "file_header\n", sec_header_function);
        
        kvf_master.merge(kvf_temp);



    
    if opt.write_filename :

        write_fname = os.path.join(opt.filename_path, opt.write_filename)
        try :
            fout = open(write_fname,'w')
            print "**** writing file : ", write_fname            
        except :
            print "**** can not write file : ", write_fname;
            fout = None;
            
        if fout : 
            kvf_master.write(fout);
            fout.close()

    if opt.display :
        kvf_master.write(sys.stdout);
        pass
    

