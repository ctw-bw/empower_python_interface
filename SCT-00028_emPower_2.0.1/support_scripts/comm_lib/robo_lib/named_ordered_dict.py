

import sys, copy


if sys.version_info < (2,6) :
    print "PYTHON VERSION :", sys.version_info, "from (named_ordered_dict)"    
    from _abcoll import *
    import _abcoll
    raise "must use python 2.6 or greater"

if sys.version_info < (2,7) :
    from _abcoll import *
    import _abcoll    


    #from _collections import deque, defaultdict
    #from operator import itemgetter as _itemgetter, eq as _eq
    #from keyword import iskeyword as _iskeyword
    #import sys as _sys
    #import heapq as _heapq
    from itertools import repeat as _repeat, chain as _chain, starmap as _starmap, \
                          ifilter as _ifilter, imap as _imap


    try:
        from thread import get_ident
    except ImportError:
        from dummy_thread import get_ident


    def _recursive_repr(user_function):
        'Decorator to make a repr function return "..." for a recursive call'
        repr_running = set()

        def wrapper(self):
            key = id(self), get_ident()
            if key in repr_running:
                return '...'
            repr_running.add(key)
            try:
                result = user_function(self)
            finally:
                repr_running.discard(key)
            return result

        # Can't use functools.wraps() here because of bootstrap issues
        wrapper.__module__ = getattr(user_function, '__module__')
        wrapper.__doc__ = getattr(user_function, '__doc__')
        wrapper.__name__ = getattr(user_function, '__name__')
        return wrapper





    ################################################################################
    ### OrderedDict
    ################################################################################

    class OrderedDict(dict, MutableMapping):
        'Dictionary that remembers insertion order'
        # An inherited dict maps keys to values.
        # The inherited dict provides __getitem__, __len__, __contains__, and get.
        # The remaining methods are order-aware.
        # Big-O running times for all methods are the same as for regular dictionaries.

        # The internal self.__map dictionary maps keys to links in a doubly linked list.
        # The circular doubly linked list starts and ends with a sentinel element.
        # The sentinel element never gets deleted (this simplifies the algorithm).
        # Each link is stored as a list of length three:  [PREV, NEXT, KEY].

        def __init__(self, *args, **kwds):
            '''Initialize an ordered dictionary.  Signature is the same as for
            regular dictionaries, but keyword arguments are not recommended
            because their insertion order is arbitrary.

            '''
            if len(args) > 1:
                raise TypeError('expected at most 1 arguments, got %d' % len(args))

            try:
                self.__root
            except AttributeError:
                self.__root = root = [None, None, None]     # sentinel node
                PREV = 0
                NEXT = 1
                root[PREV] = root[NEXT] = root
                self.__map = {}
            self.update(*args, **kwds)

        def __setitem__(self, key, value, PREV=0, NEXT=1, dict_setitem=dict.__setitem__):
            'od.__setitem__(i, y) <==> od[i]=y'
            # Setting a new item creates a new link which goes at the end of the linked
            # list, and the inherited dictionary is updated with the new key/value pair.
            if key not in self:
                root = self.__root
                last = root[PREV]
                last[NEXT] = root[PREV] = self.__map[key] = [last, root, key]
            dict_setitem(self, key, value)

        def __delitem__(self, key, PREV=0, NEXT=1, dict_delitem=dict.__delitem__):
            'od.__delitem__(y) <==> del od[y]'
            # Deleting an existing item uses self.__map to find the link which is
            # then removed by updating the links in the predecessor and successor nodes.
            dict_delitem(self, key)
            link = self.__map.pop(key)
            link_prev = link[PREV]
            link_next = link[NEXT]
            link_prev[NEXT] = link_next
            link_next[PREV] = link_prev

        def __iter__(self, NEXT=1, KEY=2):
            'od.__iter__() <==> iter(od)'
            # Traverse the linked list in order.
            root = self.__root
            curr = root[NEXT]
            while curr is not root:
                yield curr[KEY]
                curr = curr[NEXT]

        def __reversed__(self, PREV=0, KEY=2):
            'od.__reversed__() <==> reversed(od)'
            # Traverse the linked list in reverse order.
            root = self.__root
            curr = root[PREV]
            while curr is not root:
                yield curr[KEY]
                curr = curr[PREV]

        def __reduce__(self):
            'Return state information for pickling'
            items = [[k, self[k]] for k in self]
            tmp = self.__map, self.__root
            del self.__map, self.__root
            inst_dict = vars(self).copy()
            self.__map, self.__root = tmp
            if inst_dict:
                return (self.__class__, (items,), inst_dict)
            return self.__class__, (items,)

        def clear(self):
            'od.clear() -> None.  Remove all items from od.'
            try:
                for node in self.__map.itervalues():
                    del node[:]
                self.__root[:] = [self.__root, self.__root, None]
                self.__map.clear()
            except AttributeError:
                pass
            dict.clear(self)

        setdefault = MutableMapping.setdefault
        update = MutableMapping.update
        pop = MutableMapping.pop
        keys = MutableMapping.keys
        values = MutableMapping.values
        items = MutableMapping.items
        iterkeys = MutableMapping.iterkeys
        itervalues = MutableMapping.itervalues
        iteritems = MutableMapping.iteritems
        __ne__ = MutableMapping.__ne__

        
       
        @_recursive_repr
        def __repr__(self):
            'od.__repr__() <==> repr(od)'
            if not self:
                return '%s()' % (self.__class__.__name__,)
            return '%s(%r)' % (self.__class__.__name__, self.items())

        def copy(self):
            'od.copy() -> a shallow copy of od'
            print "OD_copy : ", self.__class__
            return self.__class__(self)

        

        def __eq__(self, other):
            '''od.__eq__(y) <==> od==y.  Comparison to another OD is order-sensitive
            while comparison to a regular mapping is order-insensitive.

            '''
            if isinstance(other, OrderedDict):
                return len(self)==len(other) and \
                       all(_imap(_eq, self.iteritems(), other.iteritems()))
            return dict.__eq__(self, other)

        def __str__(self) :
            str = "\t** OrderedDict with keys:\n"

            max_len_k = 0;
            for k in self.keys() :
                if len(k) > max_len_k : max_len_k = len(k)
            fmtstr = "\t%%-%ds : %%s\n" % max_len_k
            #        print fmtstr

            for k,v in self.iteritems() :
                str += fmtstr % (k,v)
            return str
else :
    from collections import OrderedDict

class NamedOrderedDict(OrderedDict) :
    def __init__ (self, name) :
        self.name = name
        # OrderedDict.__init__(self)
        super(NamedOrderedDict, self).__init__(self)

    def get_name(self) :
        return self.name

    def __str__(self) :
        str = "NOD : Name=%s\n" % self.name
        str += OrderedDict.__str__(self)
        return str

    """
    def __copy__(self) :
        print "NOD : __copy__"
        return NamedOrderedDict( self.name)
    def __deepcopy__(self, memo) :
        print "NOD __deepcopy__(%s)" % str(memo)
        return NamedOrderedDict( copy.deepcopy(self.name, memo))
    """

##############    ##############    ##############    ##############    ##############    
# dict helper functions
      
##############    ##############    ##############    ##############    ##############            
if __name__ == "__main__" :

    # nd = OrderedDict()
    nd = NamedOrderedDict('test')    


    nd['ex']=1;
    nd['xb']=2;
    nd['fc']=3;
    nd['dd']=4;
    nd['a']=5;
    
    for n in nd.iteritems() :
        print n

    print nd.keys()

    # example for a KVF
    od = OrderedDict();
    for sec in range(1,5) :
        str = "sec%d" % sec;
        nd = NamedOrderedDict(str);
        od[str] = nd;

        for key in range(1,5) :
            str = "key%d" % key
            val = key*10;
            nd[str] = val;

    print od

        

    
        
