


"""
perform robo table transfers operations

This is a communcation module that basically knows the communcation commands that can be used to transfer tables, get tables address, and to read the hash table.
"""


import robo_header_str

import python_paths_default  # test for user's python_paths.py and update paths
import python_paths as pyenv
from python_utils.class_to_str import *
from python_utils.ByteStream import ByteStream


import struct
import binascii
import comm_lib.crc8 as crc8
import comm_lib.pfcmd as pfcmd


USE_CRC32 = False

class c_struct :
    def __init__(self, **kwargs) :
        for k,v in kwargs.iteritems() :
            self.__dict__[k] = v;

    def __str__(self) :
        return class_to_str(self);


        
        
        




def calc_crc(bytestream) :
    # print "calc_crc length : ", len(bytestream)
    # print "calc_crc  : ", ByteStream(bytestream)
    
    if USE_CRC32 :
        crc = binascii.crc32(bytestream) & 0xffffffff    # http://bugs.python.org/issue1202,  fix is to AND with 0xffffffff
    else :
        c8 = crc8.CRC8();
        crc= c8.crc_string(bytestream);   # NOTE THIS IS THE WRONG CRC TO USE WITH ANKLE
        crc= c8.upd_crc_string(bytestream);   
    # print "calc_crc value : ", crc
    return crc;
    


class eeprom_table :
    """
    convert a table bytestream, into one for the EEPROM
    An EEPROM table has a 12 byte header that other tables lack
    UINT32_T crc;           // NOTE the CRC is of all bytes in the table LESS the CRC
    UINT16_T rsv;
    UINT16_T length;        // length is the length of the data section (not including header)
    UINT32_T magic;         // magic is the md5 hash of the source code the typedef used to create the data section
    """

    def __init__(self, type_obj=None) :

        self.header_struct_str = "=" + self.get_struct_str()
        self.header_size = struct.calcsize(self.header_struct_str);
        self.header_dict = {}
        
        self.type_obj = type_obj

    def set_type_obj(self, type_obj) :
        self.type_obj = type_obj

    def get_field_name_list (self) :
        return ['crc','rsv','length','magic'];
    def get_field_type_list (self) :
        return ['UINT32_T', 'UINT16_T', 'UINT16_T', 'UINT32_T']

    def get_field_struct_list(self) :
        return ['L','H','H','L']

    def set_header_values(self, crc, length, rsv, magic) :
        self.header_dict['crc'] = crc;
        self.header_dict['length'] = length;
        self.header_dict['rsv'] = rsv;
        self.header_dict['magic'] = magic;            
        
    def header_validate(self, bytestream) :
        
        crc = calc_crc(bytestream[4:])

        bs_values = struct.unpack(self.header_struct_str, bytestream[0:self.header_size] );

        if 0 :
            print 
            print "eeprom_table:validate:bs_values", ByteStream(bytestream[4:]);
            print len(bytestream[4:])
            print "eeprom_table:validate:bs_values", bs_values, crc;
        return (crc == bs_values[0]);

        
    def make_header_str (self, bytestream) :
        # return a header bytestream
        if not self.type_obj :
            return None, "class eeprom_table : need to specifiy the type obj";
        
        # wonder if there could be a sign issue?  results of test with python 2.6.6 32 bit
        # long("-0x10000000",16) & 0xffffffff == long("0xf0000000", 16)

        # print  self.type_obj.hash32
        hash32 = long(self.type_obj.hash32, 16)   # convert base 16 hash32 string into a long
        
        if (hash32 < 0) :
            hash32 = long(self.type_obj.hash32, 16) & 0xffffffff
        
        # print self.type_obj.hash32, hash32;
        temp_str = struct.pack(self.header_struct_str, 0x88776655, 0, len(bytestream), hash32);

        crc_str = temp_str[4:] + bytestream;  # skip the first 4 bytes of the composit string
        crc = calc_crc(crc_str)

        self.set_header_values(crc, len(bytestream), 0, hash32);
        head_str = struct.pack(self.header_struct_str, crc, 0, len(bytestream), hash32);

        # print ' '.join( ["%2.2x" % ord(ch) for ch in head_str])
        return ByteStream(head_str)


    def get_struct_str(self) :
        return ''.join (self.get_field_struct_list())


class robo_buff : 
    def __init__(self, ctrl, mem_type=None,  address=None, length=0) :
        self.ctrl = ctrl;
        self.mem_type = mem_type;
        self.address = address;
        self.length = length;
        self.image = None;

    def __str__(self) :
        return class_to_str(self);

    def set_image(self, image) :
        self.image = list(image);

    def get_image(self) :
        if self.image :
            str = ''.join(self.image);
        else :
            str = None;
        return str;

    def write(self, image=None) :
        if image == None:
            image = ''.join(self.image);
        #print "robo_table_xfer.c:robo_buff:write : ", image

        # print "robo_buff:write"
        result_list = self.ctrl.readwrite_mem('W', self.mem_type, self.length, self.address, WRITE_BUFF = image, QUIET=True);
        # print "robo_table_xfer.c:robo_buff:write:", result_list
        
        
    def read(self) :
        
        # print "robo_table_xfer.c:robo_buff:read calling readwrite_mem", self.mem_type, self.length, self.address
        result_list = self.ctrl.readwrite_mem('R', self.mem_type, self.length, self.address, QUIET=True);

            
        if result_list :
            # print ['%2.2x ' % ord(ch) for ch in result_list[0]['buff']]
            bytestream = self.ctrl.result_to_bytestream(result_list)
            
            image_list = [result['buff'] for result in result_list];
            image = ''.join(image_list)
            self.image = list(image);
            bytestream.print_addr = True;
            # print "robo_buff::::read:xx:\n", len(bytestream), bytestream
            self.image = bytestream
        return self.image;
        #return image[: self.length];

    def copy_from(self, src) :
        # copy the image from the provided object
        # used to transfer a memory image to a different address or memory type
        self.image = list(src.image)

        



class robo_table_properties(eeprom_table) :
    # class to read the table properties from the ankle (uses command 162)
    def __init__(self, ctrl, var_id, target_obj) :
        self.ctrl = ctrl;
        
        self.target_obj = target_obj;
        if var_id == None :
            return

        result = self.target_obj.read_calb_table_properties(var_id);    # uses special command 162        
        print "robo_table_properties: ", result
        self.record_hash = result['v1'] & 0xffffffff;
        self.mem_type_code = result['v2'];
        self.rec_size = result['v3'];
        self.address = result['v4'];
        self.header_size = 0;
        

        MEM_TYPE_CODE_ROBO_TABLE_XFER_ENUM_CODE_MAP = {1:'RAM', 2:'EEPROM', 3: 'FLASH'};
        try :
            mem_type_str = MEM_TYPE_CODE_ROBO_TABLE_XFER_ENUM_CODE_MAP  [self.mem_type_code]
        except :
            print "Key error - unknown memory type.  Command 162 returned:"
            print result
            raise
        
        self.mem_type = self.target_obj.get_mem_id(mem_type_str);

        self.needs_header = self.requires_header();
        if self.needs_header :
            eeprom_table.__init__(self);


        self.xfer_buff = robo_buff(self.ctrl, self.mem_type, address=self.address, length=self.rec_size + self.header_size);

        self.image = None;

    def requires_header(self) :
        #str = self.mem_type_str.upper()
        # return str.find("EE")>=0
        # for now just put the headers in the C data structure
        return False
    
    def printable(self) :
        str = "ADDR:0x%4.4X   TYPE:%6.6s   LENGTH:%4d   HASH 0x%8.8x" % (self.address, self.mem_type, self.rec_size, self.record_hash);
        return str

    def __str__(self) :
        return class_to_str(self)
        

    
class robo_table_xfer(robo_table_properties) :
    # read and write the DS hash table
    def __init__(self, ctrl, target_obj, table_id = None) :
        self.pf = ctrl.pfcmd;

        self.ctrl = ctrl;
        

        self.table_id = table_id;

        robo_table_properties.__init__(self, self.ctrl, self.table_id, target_obj);
        # self.read_properties();


    def __str__(self) :
        return class_to_str(self)

    #def read_properties(self) :
    #    self.table = robo_table_properties(self.ctrl, self.table_id);


    def write(self, image) :
        return self.xfer_buff.write(image)


    
    def read(self) :
        """ return a string or bytestream"""
        self.image = self.xfer_buff.read()

        # print "robo_table_xfer:read:", len(self.image) ,''.join(['%x ' % ord(ch) for ch in self.image])
        
        return self.image
        # return ByteStream(self.image)

    def set_image(self, image) :
        self.image = image;

    #def write_eeprom(self) :
    #    self.eeprom_buff.write(self.image)

    




if __name__ == "__main__" :
    pf = pfcmd.PFCmd(0,0);
    COM_PORT=32;
    pf.connect(COM_PORT, 115200);

    ctrl = pfcmd.controller(pf);

    
    table_xfer = robo_table_xfer(ctrl)
    print table_xfer
    img = table_xfer.read_eeprom();
    print ["%x " % ord(ch) for ch in img]
    # table_xfer.write_eeprom();
    

    
    

    if 0 :
        robo_xfer.read_table_properties(1);
        robo_xfer.read_table_properties(2);        
        result = pf.command(164,1);
        result.display_header();
        result.display(FORMAT="%-8X " );



    
