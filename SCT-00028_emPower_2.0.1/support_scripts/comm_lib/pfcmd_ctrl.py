#!/usr/bin/env python
SVN_SUB_REV_STR = "$Revision: 9633 $"

import sys, traceback, os, time
import struct;
import collections

import python_paths_default  # test for user's python_paths.py and update paths
import python_paths as pyenv
from python_utils.ByteStream import ByteStream




class controller :
    """ this class is indented to support commands that reside on the state controller """
    """ origional it was just a higher level interface to pfcmd """
    
    #Constants used as BiOM Interface:
  
    
    
    def __init__ (self, pfcmd) :
        self.pfcmd = pfcmd;
        self.debug_level = 0;
        pass;


    def read_analog_in(self, channels) :
        list = []
        for chan in channels :
            if (self.debug_level > 2) :
                print "chan",chan,
            result = None
            result = self.pfcmd.do_command('get_an_in', [chan]);
            # print "result ", chan, result
            if (result == None) :
                print "No result";
            list.append(result);
            time.sleep(0.03);
        return list;

    def read_digital_in(self,channels) :
        list = []
        for chan in channels :
            if (self.debug_level > 2) :
                print "chan",chan,
            result = None
            result = self.pfcmd.do_command('get_dig_in', [chan]);
            if (result == None) :
                print "No result";
            list.append(result);
            time.sleep(0.03);
        return list;
        pass

    def read_digital_out(self, channels) :
        list = []
        for chan in channels :
            if (self.debug_level > 2) :
                print "chan",chan,
            result = None
            result = self.pfcmd.do_command('get_dig_out', [chan]);
            if (result == None) :
                print "No result";
            list.append(result);
            time.sleep(0.03);
        return list;
        pass

    def write_digital_out(self, channel, value):
        result = self.pfcmd.do_command('set_dig_out', [channel, value]);
        return result;
        pass


    def read_analog_out(self, channels) :
        list = []
        for chan in channels :
            if (self.debug_level > 2) :
                print "chan",chan,
            result = None
            result = self.pfcmd.do_command('get_an_out', [chan]);
            if (result == None) :
                print "No result";
            list.append(result);
            time.sleep(0.03);
        return list;
        pass
    
    
    def write_analog_out(self, channel, value):
        result = self.pfcmd.do_command('set_an_out', [channel, value]);
        return result
        pass

    def special(self, command, values=[], secs=0.1, suffix="") :
        values.insert(0,command);
        # result = self.pfcmd.do_command('special', values);
        result = self.pfcmd.do_command('special'+suffix, values);
        time.sleep(secs);
        return result;

    

    def set_ankle_time(self, USE_UTC=False) :  # use special command 100 to set the SC clock
        print "called pfcmd_ctrl.set_ankle_time()"
        
        if USE_UTC :
            tm = time.gmtime()
        else :
            tm = time.localtime()

        lst = [v for v in tm[0:6]];
        vals = lst[:3]+[0]+lst[3:]
        vals[0] = vals[0] - 2000;
        result = self.special(100, vals);
        # print "set_time",self.print_result_values(result)
        pass
    """
    data->param[0] = time.year;
    data->param[1] = time.month;
    data->param[2] = time.day;
    data->param[3] = time.weekday;
    data->param[4] = time.hour;
    data->param[5] = time.min;
    data->param[6] = time.sec;
    """

    def get_ankle_time(self) : # use special command 101
        name_list=['year','month','day','weekday','hour','min','sec']
        result = self.special(101,[0]);
        d = {}
        field_list = result['rsp_fields'][1:]
        for k,name in zip(field_list, name_list) :
            # print k,name, result[k]
            d[name] = result[k]


        if d['sec'] & 128 :
            d['sec'] &= 0x7f
            d['initialize'] = True;
            pre_str = "(get ankle tine) clock is UNINITIALIZED"
        else :
            pre_str = "(get ankle time) clock is INITIALIZED"

        str = "%s : (SI)%d-%d-%d : %2.2d:%2.2d:%2.2d : weekday (%d)" % (pre_str, d['year'], d['month'], d['day'], d['hour'], d['min'], d['sec'], d['weekday']);

        d['str'] = str;

        return d


    

    
    





   

    
   
    # HOSTCMD_IMU_MFG_EEPROM_MEM_TYPE      13
    def get_memory_str_from_enum(self, integer_type_value):
        mem_dict= {       1:  'HOSTCMD_SC_RAM_MEM_TYPE',
                          2:   'HOSTCMD_SC_FLASH_MEM_TYPE',
                          3:   'HOSTCMD_SC_MFG_EEPROM_MEM_TYPE',
                          4:   'HOSTCMD_SC_USR_EEPROM_MEM_TYPE',
                          5:   'HOSTCMD_MC_RAM_MEM_TYPE',
                          6:   'HOSTCMD_MC_FLASH_MEM_TYPE',
                          7:   'HOSTCMD_MC_PROGRAM_MEM_TYPE',
                          8:   'HOSTCMD_SC_PROGRAM_MEM_TYPE',

                          11:   'HOSTCMD_IMU_RAM_MEM_TYPE',
                          12:   'HOSTCMD_IMU_FLASH_MEM_TYPE',
                          14:   'HOSTCMD_IMU_USR_EEPROM_MEM_TYPE',
                          16:   'HOSTCMD_IMU_PROGRAM_MEM_TYPE',
                             };
        return mem_dict[integer_type_value]
    
    def get_memory_id(self, target=None, mem_type=None, QUIET=True) :
        mem_dict= {         'HOSTCMD_SC_RAM_MEM_TYPE' :               1,
                             'HOSTCMD_SC_FLASH_MEM_TYPE' :             2,
                             'HOSTCMD_SC_MFG_EEPROM_MEM_TYPE' :        3,
                             'HOSTCMD_SC_USR_EEPROM_MEM_TYPE' :        4,
                             'HOSTCMD_MC_RAM_MEM_TYPE' :               5,
                             'HOSTCMD_MC_FLASH_MEM_TYPE' :             6,
                             'HOSTCMD_MC_PROGRAM_MEM_TYPE' :           7,

                             'HOSTCMD_SC_PROGRAM_MEM_TYPE' :           8,

                             'HOSTCMD_IMU_RAM_MEM_TYPE' :              11,
                             'HOSTCMD_IMU_FLASH_MEM_TYPE' :            12,
                             'HOSTCMD_IMU_USR_EEPROM_MEM_TYPE' :       14,
                             'HOSTCMD_IMU_PROGRAM_MEM_TYPE' :            16,
                             };

        if 0 :
            print "pfcmd_ctrl.py:get_memory_id:", target, mem_type

        if target == None or mem_type == None :
            return mem_dict

        target = "_%s_" % target.upper();
        mem_type = "_%s_" % mem_type.upper();
        for name in mem_dict.keys() :
            if target in name and mem_type in name :
                if not QUIET :
                    print "get_memory_id FOUND: ", name, mem_dict[name]
                return name, mem_dict[name]

        return None


    def print_readwrite_mem_types(self) :
        mem_dict = self.get_memory_id(None);
        for w in sorted(mem_dict, key=mem_dict.get) : print "%2.2d  : %s" % (mem_dict[w], w)



    def readwrite_mem(self, direction, mem_type, total_length, base_address, WRITE_BUFF=None, QUIET=True) :
        """
        read write memory on SC, MC or IMU

        def readwrite_mem(self, direction, mem_type, length, base_address, WRITE_BUFF=None) :
        direction == 'W' --> write to ankle, 'R' -> read
        mem_type == 'RAM' or 'EEPROM'
        length == number of bytes to read/write
        base_address == address inside ankle target memory
        WRITE_BUFF == 'char' like packed binary data
        """

        if not QUIET :
            print "readwritemem (%s %s %d, $%x)" % (direction, mem_type, total_length, base_address)

        # direction
        MEM_TYPE_DICT = {'RAM':1,'FLASH':2,'EEPROM':4,'MFG':3};

        IO_BLOCK_SIZE = 128;

        type = None;
        try :
            u = mem_type.upper();
            type = MEM_TYPE_DICT[u];
        except :
            pass

        if (type == None) :
            try :
                type = int(mem_type);
                if type >= 10 :
                        # QUIET=False
                        IO_BLOCK_SIZE = 32;
            except :
                return {'ERROR':'unknown memory type'};
                print sys.exc_info()


        # print "pfcmd.py, readwrite_mem: LENGTH",total_length, IO_BLOCK_SIZE

        # print "readwritemem (%s %s (%s), %d, $%x)" % (direction, mem_type, type, length, base_address)
        dir = direction.upper();

        result_list = [];
        cnt  = 0;
        offset = 0;


        for buf_ndx in range(0, total_length, IO_BLOCK_SIZE) :

            read_length = min(total_length - buf_ndx, IO_BLOCK_SIZE)
            # print "read_length ", read_length


            address = base_address + offset

            if dir == "W":
                wbuff = WRITE_BUFF[buf_ndx:] + (chr(0) * IO_BLOCK_SIZE); #Fills in any additional packet slots with zeroes. 
                write_length = read_length;
                if (not QUIET):
                    print "WRITE_iter:%d   offset:$%x  len:%d   address:$%x " % (cnt, offset, write_length, address)

                for i in range(0,2) :
                       result = self.pfcmd.do_command('write_mem', [type, write_length, address, wbuff[0:IO_BLOCK_SIZE]]);

                       if not QUIET :
                              print "pfcmd_ctrl:readwrite_mem:WRITE:result", result
                       if result :
                              break;
                       print "try # ", i
            else :   # memread
                if (not QUIET):
                      print "READ:iter:%d   offset:$%x  len:%d   address:$%x " % (cnt, offset, read_length, address)
                
                result = self.pfcmd.do_command('read_mem', [type, read_length, address]);
                    
                if result and len(result) :
                         # img = result['buff']
                         # print ' '.join( ["%2.2x" % ord(ch) for ch in img])
                         # print result.keys(), result['len']
                         result_list.append(result)

                else :
                         print "NO RESULT"
                    
            cnt = cnt + 1

            offset += read_length

        return result_list;





    def result_to_bytestream(self, result_list) :
        
        str_list = []
        for result in result_list :
            str = result['buff']
            str_list.append(str[: result['len'] ])

        str = ''.join(str_list)
        return ByteStream(str)


    
    
    def get_state_echo(self, QUIET = False, secs = .1):
        result = self.pfcmd.do_command('state_echo', ["12345"]);
        time.sleep(secs);
        if not QUIET:
            print "ECHO result (should be 12345):", result
        return result;
    
    def get_motor_echo(self, QUIET = False, secs = .1):
        result = self.pfcmd.do_command('motor_echo', ["12345"]);
        time.sleep(secs);
        if not QUIET:
            print "ECHO result (should be 12345):", result
        return result;
    
    def get_imu_echo(self, QUIET = False, secs = .1):
        result = self.pfcmd.do_command('imu_echo', ["12345"]);
        time.sleep(secs);
        if not QUIET:
            print "ECHO result (should be 12345):", result
        return result;
    
    
    
    def get_version(self, device, QUIET = False) :
        """ obsolete
        issue the version command to the SC and/or MC
        """
        rcd = -1;
        msg_str_list = []

        version_cmd_tbl = {0:'state_version', 1:'motor_version', 2:'imu_version'};
        command = version_cmd_tbl[device];
        result = self.pfcmd.do_command(command, []);

        if QUIET != True :
            print
            print
        for k,v in result.iteritems() :
            if k == "rsp_string" :
                continue
            try :
                # try to print as a string, if not then just print the key and value
                s = self.pfcmd.strip_nonprintable(result[k]);
                str = "%12s  %s" % (k, s)
                msg_str_list.append(str)
                if QUIET != True :
                    print str
            except :
                # this exception isn't really an error - don't output debugging
                # print sys.exc_info()
                # traceback.print_exc()
                str = "%12s  %s" % (k, v)
                msg_str_list.append(str)
                if QUIET != True :
                    print str
                pass

        if (0 and result) :
            other = self.pfcmd.strip_nonprintable(result['other']);
            print "match", other
        if QUIET != True :
            print
            print

        return result, msg_str_list


    



