import python_paths_default  # test for user's python_paths.py and update paths
import python_paths as pyenv

import sys, os, string, time, socket, struct

#import class_process_v2_packet
import threading, Queue, sys, serial

#Local modules used
import comm_lib.crc8 as crc8
from wifi_lib import class_read_varlist, class_fancy_list_output
        

LANTRON_PORT = 10001;

##########################################################################################
class datasource:
    SERIAL = 1;
    TCP = 2;
    UDP = 3;
    FILE = 4    

    def __init__(self, connection_type = None, host = None, port = LANTRON_PORT, file_in = None, file_out = None, file_log = None, baudrate=1250000) :
        
        
        self.enable_bin_log = False
        
        self.timeout_cnt = 0;
        
#         self.soc = None
#         self.tcp_host = None;
#         self.tcp_port = None;
#         self.udp_host = None;
#         self.udp_port = None;
#         self.serial_port = None;
#         self.baudrate = None;
#         self.file_in = None
#         self.file_out = None
            
        if (file_log):
            self.enable_bin_log = True
            self.enable_bin_log_fout = open(file_log, 'wb')        

        self.dev_type = connection_type;
        
        if self.dev_type == self.SERIAL:
            self.connect_serial(port, baudrate)
        elif self.dev_type == self.TCP:
            self.connect_tcp(HOST=host, PORT=LANTRON_PORT)
        elif self.dev_type == self.UDP:
            self.connect_udp(host, port)
        elif self.dev_type == self.FILE:
            self.connect_file(file_in, file_out)
        else:
            raise Exception("ERROR: CONNECTION TYPE "+str(self.dev_type)+" NOT RECOGNIZED")    
            
                    
    def __del__(self) :
        #print "universal_dataport.datasource.__del__"
        #self.close()
        pass

    def connect_udp(self, HOST, PORT) :
        self.udp_host = HOST
        self.udp_port = PORT

        self.soc = socket.socket(socket.AF_INET, socket.SOCK_DGRAM);
        self.soc.bind((self.udp_host, self.udp_port));    
        
        
    def connect_file(self, file_in, file_out):    
        self.file_in = open(file_in, 'rb')
        self.file_out = open(file_out, 'ab')
        return self.file_in
        
    def connect_serial(self, port, baudrate=115200) :
        self.dev_type = self.SERIAL;
        self.serial_port = port
        self.baudrate = baudrate;
        if os.name == 'nt': #windows serial port
            os_port = port-1;
        else: #linux address
            import comm_lib.serial_port_helper
            sph = comm_lib.serial_port_helper.serial_port_helper()
            mpd_command_portNum = int(sph.get_port_to_use())
            port = mpd_command_portNum + 1
            #Note: assumes linux setup hardcoded telemetry to Port mpdcommand + 1.        
            os_port = sph.get_os_serial_port_name(port);
            
        self.sp = serial.Serial(os_port, baudrate, timeout=0.2, rtscts=0)
        return self.sp

    def connect_tcp(self, HOST='', PORT=LANTRON_PORT, timeout = 1.0) :

        self.tcp_host = HOST;
        self.tcp_port = PORT;
        
        if (HOST) :
            self.tcp_addr = (HOST, PORT);

        #print "Connect to IP %s PORT %s ..." % self.tcp_addr,

        try_cnt = 4;
        while try_cnt > 0 :
            try :
                self.soc.close();
            except :
                pass;

            try :
                self.soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM);
                self.Timeout(timeout);
                self.soc.connect(self.tcp_addr);
                #print "Success"
                try_cnt = 0;
                break;
            except :
                #print "Retry",
                self.soc.close();
                time.sleep(1.0);
                try_cnt = try_cnt - 1;


       
        return self.soc

    def reconnect(self) :
        if self.dev_type == self.SERIAL :
            rcd = self.connect_serial( self.serial_port, self.baudrate);
        elif self.dev_type == self.TCP :
            rcd = self.connect_tcp(self.tcp_host, self.tcp_port);
        elif self.dev_type == self.UDP:
            rcd = self.connect_udp(self.udp_host, self.udp_port);
        elif self.dev_type == self.FILE:
            rcd = self.connect_file(self.file_in, self.file_out)    
            
        #print "class_wifi.reconnect returns : ", rcd
        return rcd
    
    def disconnect(self) :
        if self.dev_type in [self.TCP,self.UDP]:
            self.soc.close()
        elif self.dev_type == self.FILE:
            try:
                self.file_in.close()
                self.file_out.close()    
            except Exception as e:
                print e
        elif self.dev_type == self.SERIAL:
            self.sp.close()
        
    def close(self) :
        if self.dev_type in [self.TCP,self.UDP]:
            self.soc.close()
        elif self.dev_type == self.FILE:
            try:
                self.file_in.flush()
                self.file_in.close()
                self.file_out.flush()
                self.file_out.close()    
            except Exception as e:
                print e
        elif self.dev_type == self.SERIAL:
            self.sp.close()
        
        
        if self.enable_bin_log :
            self.bin_log_fout.close()


    def start_bin_log(self, fname= None) :
        if (fname):
            try :
                self.bin_log_fout = open(fname,'wb')
                self.enable_bin_log = True
            except :
                self.enable_bin_log = False
        elif self.bin_log_fout != None:
            self.enable_bin_log = True
        else :
            self.enable_bin_log = False
          

    def stop_bin_log(self) :
        self.enable_bin_log = False
        self.bin_log_fout.close()
        


    def set_timeout(self, timeout) :
        if self.dev_type == self.SERIAL :
            self.sp.settimeout(timeout);
        else:
            raise Exception("Can't set timeout on connection that is not of type datasource.SERIAL.")
            
    def raw_read(self, cnt = None, timeout = sys.maxint) :
       string = ''
       start = time.clock() 
       if self.dev_type == self.TCP:
            if cnt == None:
                chunksize = 4096
                chunk = "neverused" #entry into loop condition
                while chunk != '': #get all data available
                    chunk = self.soc.recv(chunksize)
                    string = string + chunk    
            else:
                string = self.soc.recv(cnt)
                while len(string) < cnt and time.clock() - start < timeout:
                    chunk = self.soc.recv(cnt - len(string))
                    string = string + chunk    
       elif self.dev_type == self.UDP:
            if cnt == None:
                chunksize = 4096
                chunk = "neverused" #entry into loop condition
                while chunk != '' and len(string) < (4096*20): #get all data available, but stop if we get too much at a time.
                    chunk, addr = self.soc.recvfrom(chunksize)
                    string = string + chunk    
                
            else:
                string, addr = self.soc.recvfrom(cnt)
                while len(string) < cnt and time.clock() - start < timeout:
                    chunk, addr = self.soc.recvfrom(cnt - len(string))
                    string = string + chunk          
            
       elif self.dev_type == self.FILE:
            if cnt == None:
                chunksize = 1
                string = self.file_in.read(chunksize)
            else:
                string = self.file_in.read(cnt)
            
       elif self.dev_type == self.SERIAL:
            if cnt == None:
                string= self.sp.read(self.sp.inWaiting())
            else:
                string= self.sp.read(min(cnt, self.sp.inWaiting()))
                while len(string) < cnt and time.clock() - start < timeout:
                    string= string + self.sp.read(min(cnt - len(string), self.sp.inWaiting()))
        
        
       if self.enable_bin_log :
            self.bin_log_fout.write(string)
       return string
    


    def send(self,ch) :
        if self.dev_type in [self.TCP,self.UDP]:
            cnt = self.soc.send(ch)
        elif self.dev_type == self.FILE:
            cnt = self.file_out.write(ch)
        elif self.dev_type == self.SERIAL:
            cnt = self.sp.write(ch)
        
        return cnt

    def write(self, ch):
        return self.send(ch)

    def SendList(self, list) :
        s = ''.join([chr(val) for val in list])
        cnt = self.send(s);
        return cnt


    def flush(self) :
       if self.dev_type in [self.TCP,self.UDP]:
          try :
            ln = self.raw_read(1000);
            return 1
          except :
            #print "exception - flush_soc"
            pass
            return 0
       elif self.dev_type == self.FILE:
            #Goto end of file:
            self.file_in.seek(0, 2)
            return 1
       elif self.dev_type == self.SERIAL:
            self.sp.read(self.sp.inWaiting())  
            return 1
    
 
    def __del__(self):
        self.disconnect()    

    def read(self, cnt = None) :
        # #print "R",
        try :
            ln = self.raw_read(cnt);
        except Exception as e:
            print "Datasource Read Error", e
            # #print "T",
            self.timeout_cnt += 1;
            if (self.timeout_cnt == 1) :
                    print "DBR timeout ", 
            #print self.timeout_cnt, 
            ln = ""
            if (self.timeout_cnt >= 9) :
                    self.reconnect();
        return ln
 
class datasource_pkt(dict):
    """An open-ended format that extends a python dictionary designed to be a compact representation of many possible packet types. 
    It extends the python dict object to include convenience methods specific for parsing packets. """
    def __init__(self, field_list = None): 
        if field_list != None:
            for f in field_list:
                self[f] = None
    
    """ Returns the label that this packet uses as a timestamp. Returns 'None' if this packet type isn't ."""
    def getTimestampLabel():
        return None
     
    
     
class datasource_parser:
    """A skeleton class that lists the method a packet parser needs to have.""" 
    def eat_and_parse(self, buffer):
        """ A skeleton parse method that provides the API needed to implement a parser. Raises Exception if actually called.
        Inputs:
            buffer: a byte buffer that packets can be created from.
        Output: 
            pkts : a list of dataport_pkt objects
            leftovers: a byte buffer that contains any remaining bytes in the buffer that were not processed  
        """
        raise Exception("Error. You need to implement the eat_and_parse method for parser "+str(self.__class__.__name__))
            
class vlist_parser(datasource_parser):    
    SYNC_CHAR_1 = '!';    # 0x21
    SYNC_CHAR_2_V2 = chr(0x42); # 0x42
    SYNC_CHAR_3 = '#';    # 0x23
    SYNC_STR = SYNC_CHAR_1 + SYNC_CHAR_2_V2;
    
    """Parser that generates packets using format information pulled from a vlist."""
    def __init__(self, vlist_fname, err_quiet = True):
        self.vlist_obj = class_read_varlist.read_varlist(vlist_fname)
        self.struct_str = self.vlist_obj.make_struct_string()
        self.packet_len = self.vlist_obj.get_length() + 5 + 4 #Add header and sync length
        self.header_len = 6
        self.offset = 4 #Offset is 2 char sync and ???
        self.err_quiet = err_quiet
        self.pk_cnt = 0
        self.output_formatter = class_fancy_list_output.fancy_list_output()
                
        for v in self.vlist_obj.get_varlist():
            #str = "%s, %s, %s" % (v['varname']+"_raw", 0.0, 1.0)
            self.output_formatter.append(v['format'], v['scale'], v['offset'], v['varname']);
            ##print "%4s  %10.5g  %5.5g   %-30s" % (v['format'], v['scale'], v['offset'], v['varname'])
           
            
        self.output_formatter.append("%d", label="milliseconds");
        self.output_formatter.append("%d", label="packet_number");
        self.output_formatter.append("%d", label="text_int");


    def eat_and_parse(self, buffer):
        """Implementation of datasource_parser.eat_and_parse method that parses packets using vlists. Simplified code from class_read_v2_packet.ProcessBuff from """ 
        if (len(buffer) < self.packet_len+self.header_len + self.offset) :
            return [], buffer;
        packets = []
        ndx = buffer.find(vlist_parser.SYNC_STR, 0);
        
        while ndx != -1 and ndx + self.packet_len < len(buffer) :
            header = struct.unpack(">HBBH", buffer[ndx:ndx+self.header_len]);
            # #print ["%x" % h for h in header]
            # 0 : sync header
            # 1 : packet ID
            # 2 : data section length
            # 3 : composite word w/stream_id:3, stream:len:3, mon_var_id:10
             
            stream_id = header[3]  & 7
            stream_len = (header[3] >> 3) & 7
            
            mon_var_id = (header[3]>>6) & 0x3ff;
            mon_var_length = 0;
            mon_var_offset = self.header_len + stream_len;
   
            data_offset = ndx + mon_var_offset + mon_var_length;
            text_offset = ndx + self.header_len    
                        
            crc = crc8.calc_upd_crc(buffer[ndx : ndx +  self.packet_len -1])
            
            eop_char = buffer[ndx + self.packet_len+ -1];
            valid_packet = (ord(eop_char) == crc) #Our CRC Matches the end of the packet, we have a valid packet.
            
            if valid_packet: 
                self.pk_cnt = self.pk_cnt + 1;

                txt = buffer[text_offset : mon_var_offset];
                
                
                if len(txt) == 2 :
                    text_int_value = struct.unpack(">h", txt)[0];
                else : 
                    text_int_value = 0;
                 
                
                pk = buffer[data_offset : ndx + self.packet_len-1];
                data_list = self.vlist_obj.vlist_unpack(pk)   # transform the binary data packet into a python list  (or sequence???)
               
                
                tm = int(time.clock() * 1000)
                output_list = data_list + (tm, self.pk_cnt, text_int_value)     # append to the list the time in mS, the packet count, and a 16 bit INT that represents 0 to 2 ASCII chars
            
                scaled_data = self.output_formatter.scale_data(output_list)
                output_string = self.output_formatter.make_string_from_data(scaled_data) # make a nicer output string. new class with scale,offset 3-9-2010
                
                label_indices = self.vlist_obj.get_varlist_map()
                packet = datasource_pkt()
                for l in label_indices.keys(): #label all the data in the packet:
                    data_index = label_indices[l]
                    packet[l] = scaled_data[data_index]
                
                ##print packet    
                packets.append(packet)
                buffer = buffer[ndx + self.packet_len :];
            else :

                str1 = "CRC : %2x %2x " % (ord(eop_char), crc)
                val_list = [('%2x' % ord(val) ) for val in buffer[ndx: ndx + self.packet_len]];
                str2 = ' '.join(val_list);
                if self.err_quiet == False :
                    print str1, str2
                str3 = time.strftime("%b%d %y %H:%M:%S  ", time.localtime());
                str4 = " PC:%3x BL:%3x" % (self.pk_cnt, len(buffer));
                buffer = buffer[2:];
           
            ndx = buffer.find(vlist_parser.SYNC_STR, 0);
            
 
        return packets, buffer #We can't process anything in this buffer. For now, return it.
        #TODO: Decide if we should flush a buffer we can't use or ignore it so that a different packet processor could try.
       

       
        

class datasource_handler(threading.Thread):
    
    """ A class designed to consolidate many possible input streams such as TCP, UDP, Bluetooth and serial port packets,
    parse them, timestamp them, and provide them asynchronously to a requester. Any provided keyboard input stream must implement the 'read' FileIO method. """

    def __init__(self, data_source, parser, keyboard_input = sys.stdin) :
        threading.Thread.__init__(self)
        self.daemon = True        #Kill this thread if someone closes the program that started it. 
        self.terminate = threading.Event()
        self.time_last_cycle = time.time()
        self.received_packets = Queue.Queue()
        self.last_received = None #Keep a copy of the latest element put into the queue so we can peek at it if we want
        self.time_last_received = None        
        self.datasource = data_source
        self.buffer = ''
        self.parser = parser
        self.got = 0
        self.keyboard_input = keyboard_input
        self.set_self_terminate_timeout(-1) #default to never closing if we go 10 seconds without data
        self.last_access = time.time()
        self.timestamp_offset_seconds = 0; # It's possible that a data stream has a clock that is different from the local computer. This is where you store the offset.
        self.timestamp_label_key = None #If packets are timestamped, the data handler looks for those values and adds the local time offset so that when the requester gets data, all value timestamps are synced
        self.rebroadcaster = None
     
    """ Allow all packets we recieve from any port to get rebroadcast over internal UDP so we can share data between processes.""" 
    def setup_rebroadcasting(self, host, port):
        self.rebroadcaster = rebroadcaster(host, port)

    def set_timestamp_offset(self, offset):
        self.timestamp_offset_seconds = offset
    
    def add_timestamp_key(self, label):
        self.timestamp_label_key = label
       
    def __del__ (self) :
        self.datasource.disconnect()        
        if not self.terminate.isSet():
            self.terminate.set()
        ##print "datasource_handler.__del__"


    def set_self_terminate_timeout(self, seconds) :
        """
        set the automatic termination (timeout) of the thread
        if larger than zero, then packets must be read at least every 'seconds' or
        the thread will self terminate. Setting the timeout to a value smaller than zero disables
        automatic termination.
        """
        
        self.self_terminate_seconds = seconds;

    def runAsProcess(self):
        self.run()
        
    def run(self) :
        """ Handles data forever until stop() method is called.
        This method must be called before any attempt to receive packets.
        Handles data by using reading in bytes from the datasource,
        converting bytes to packets, and putting packets in the queue.
        Other classes should access packets by calling get_next_available()
        Thread can be set to automatically termiante if get_next_available() is not called.
        """
        while not self.terminate.isSet():
 
            self.time_last_cycle = time.time()
            #Check keyboard for input:
            #typed_in = ''
            #try:
            #    typed_in = typed_in + ''.join(map(self.keyboard_input.read, [1]))
            #finally:
            #    
            #    #print 'intercepted:', typed_in
            #    self.datasource.send(typed_in)
            
            just_read = self.datasource.read()
            self.buffer = ''.join([self.buffer, just_read])
            if (self.rebroadcaster):
                self.rebroadcaster.rebroadcast(just_read)
                
            new_packets, self.buffer = self.parser.eat_and_parse(self.buffer)
            [self.received_packets.put(p) for p in new_packets];
            if len(new_packets) > 0:
            #    print "Got %d new packets" % len(new_packets)
                self.last_received = new_packets[-1] #Keep a copy of the last element received
                self.time_last_received = time.time()
            
            self.got = self.got + len(new_packets)
            
            # test for user thread dead - and possibly terminate self
            interval_since_last_access = time.time() - self.last_access;
            
            if self.self_terminate_seconds > 0 and interval_since_last_access > self.self_terminate_seconds :
                self.terminate.set()

                
            time.sleep(0)  # slow the thread to not hog the processor
        print "#"*100
        print "Datasource terminated!" 
        print "#"*100
        self.datasource.disconnect() #disconnect once we're done             
        if (self.rebroadcaster != None):
            self.rebroadcaster.udp_socket.close()      
        
    def is_terminated(self) :
        """return the termination status"""
        return self.terminate.isSet()
        
    def stop(self) :
        """ Stops listening for packets and ends thread. """
        self.terminate.set()
        
            
    def get_next_available(self, timeout = None):

        """ A method that returns the next available dataport_pkt object.
        If optional parameter timeout is non-zero, method will block until the
        next packet is available or timeout.
        """
        
        self.last_access = time.time()
        
        if timeout:
            return self.received_packets.get(True, timeout)
        else: 
            return self.received_packets.get_nowait()

    def available(self) :
        """ return True if packet buffer is not empty        """
        
        self.last_access = time.time()
        return not self.received_packets.empty()
    
    def get_count(self) :
        """ return the number of packets in the queue """
        self.last_access = time.time()
        return self.received_packets.qsize()

    def peek_last_received(self):
        """ Peeks at the last packet received (i.e. TAIL of Queue.) Does not remove it from the queue."""
        self.last_access = time.time()
        return self.last_received

    def flush(self):
        self.get_all(); 

    def get_all(self) :
        """return a list of all packets in the queue """
        packet_list = []
        while 1:
            try:
                last_packet = self.get_next_available()
                packet_list.append(last_packet)
            except:
                break

        return packet_list

class rebroadcaster():
    """ Simple UDP Broadcaster designed to allow a separate process reading data continuously to communicate data to other processes for writing or graphing. """

    def __init__(self, host, port):
        print "udp:", host, port
        self.udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM);
        self.host = host
        self.port = port
        self.packet_cnt = 0;
        
    def rebroadcast(self, bytes):
        self.udp_socket.sendto(bytes, (self.host, self.port))
    
    
    def __del__(self):
        try:
            self.udp_socket.close()        
        except:
            pass #already closed.

if __name__ == "__main__":                         
    
    #swifi_source = datasource(connection_type = datasource.TCP, host = '192.168.192.' + sys.argv[1])
        
#     test_in = open('test_in', 'wb')
#     test_in.write("the quick brown fox jumped over the lazy dog.")
#     test_in.close()
#     file_source = datasource(connection_type = datasource.FILE, file_in = 'test_in.txt', file_out = 'test_out.txt')
#     b = datasource_handler(file_source)
#     b.run()
    
    
    
    serial_source = datasource(connection_type = datasource.SERIAL, baudrate = 1250000, port = int(sys.argv[1]))
    my_parser = vlist_parser("vlist/calibrate.vlist",err_quiet = False)
    a = datasource_handler(serial_source, my_parser)
    a.set_self_terminate_timeout(-1)    
    a.start()

    
    #file_source = datasource(connection_type = datasource.FILE, file_in = sys.argv[1], file_out = sys.argv[1]+'packet_parsed.txt')
    #my_parser = vlist_parser("../vlist/dataport3h.vlist",err_quiet = False)
    #a = datasource_handler(file_source, my_parser)
    #a.set_self_terminate_timeout(5)
    #a.start()
    
    parsed = 0
    #    a = datasource_handler(serial_source, my_parser)
    #    a.set_self_terminate_timeout(-1)
    #    a.start()
    first = True
    import python_utils.keyboard
    keybd_obj =python_utils.keyboard.keyboard()
    while 1:
        hits = 0
        misses = 0
        while 1:
            try:
                x = a.get_next_available()
                parsed = parsed + 1
                if (first):
                    print x.keys()
                    #file_source.write("\n".join(x.keys() +['milliseconds', 'packet_number' ])) #output matlab packet format- add estimated milliseconds,packet_number,text_int_value
                    #file_source.write("\n")
                    first = False;
                    print x.values()
               
                for k in x.keys():
                    print k, x[k]
                
                to_write= ','.join([str(v) for v in x.values()]) +','+ ','.join([str(parsed*2), str(parsed), '0']) +"\n"  #when recovering from a file, estimate 1 packet every 2 ms.
                
                file_source.write(to_write) #fill file_out with a wifi_fast matlab output packet. 
                
                
                if (hits == 0): 
                    #print "Missed:", misses
                    misses = 0
                hits= hits + 1
            except Exception as e:
                #print "Exception:", e.__class__
                #print 
                
                if (misses == 0): 
                    #print "Hits:", hits
                    hits = 0
                misses = misses + 1 
                
                
            (raw_ch, esc, fun, ch) = keybd_obj.getch_packed();
            if raw_ch == '\x1b' or ch == '\x1b' :
                #print 'Exiting.'
                a.stop()
                sys.exit(-1)
                break;
            else :
                pass
                ##print ch, raw_ch
