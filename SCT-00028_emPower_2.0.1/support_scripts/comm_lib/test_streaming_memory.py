
import python_paths_default  # test for user's python_paths.py and update paths
import python_paths as pyenv

BAUD_RATE = 115200;
HEADER_LEN = 12
PACKET_LEN = 128 
HEX_DUMP_WIDTH = 16

length = 0x1000
start_addr = 0x0000
mem_type = 4 # flash eeprom

import time
import serial
import sys

import string;

if __name__ == "__main__" :

    if (len(sys.argv[1:])>0) :
        a = sys.argv[1];
        COM_PORT = int(a)
    
    if (len(sys.argv[1:])>1) :
        a = sys.argv[2];
        BAUD_RATE = int(a)

    if (len(sys.argv[1:])>2) :
        a = sys.argv[3];
        start_addr = int(a)
        
    if (len(sys.argv[1:])>3) :
        a = sys.argv[4];
        length = int(a)
        
    if (len(sys.argv[1:])>4) :
        a = sys.argv[5];
        mem_type = int(a)

    print "COM PORT     : %s" % (COM_PORT)
    print "BAUD_RATE    : %s" % (BAUD_RATE)
    print "Start Address: %s" % (start_addr)
    print "Length       : %s" % (length)
    print "Mem-Type     : %s" % (mem_type)

    import msvcrt;

    from python_utils.ByteStream import ByteStream

    import pfcmd

    pf = pfcmd.PFCmd();
    pf.connect(COM_PORT, BAUD_RATE, 0.1);

    result = pf.do_command('special',[507, mem_type, start_addr, start_addr + length, 5]);
    print result
    ln = ''
    entire_stream = ln
    z = 0
    timeout_count = 0
    sp = pf.comm_device
    
    ''' Process incoming stream.
        Exit on 5 consecutive packet timeouts.'''
    
    while timeout_count < 5 :  

        ln = ln + sp.read(PACKET_LEN + HEADER_LEN);
        ind = ln.find('\xdd\xe0')
        if len(ln) >= (PACKET_LEN + HEADER_LEN + ind):
            
            ''' Debugging Info '''
            #seq_last = ln[ind+5:ind+6]
            #print "%s" % seq_last
            #print z
            #z = z + 1
            #print ByteStream(ln[ind:ind + HEADER_LEN]);
            #print ByteStream(ln[ind+HEADER_LEN:ind + HEADER_LEN + PACKET_LEN]);

            entire_stream += ln[ind+HEADER_LEN:ind+HEADER_LEN + PACKET_LEN]
            ln = ln[ind + PACKET_LEN + HEADER_LEN:]
            timeout_count = 0
        else :
            timeout_count += 1
        
        ''' Check for keyboard input. Exit on <ESC> character.'''
        if (msvcrt.kbhit()) :
            ch = msvcrt.getch();
            sp.write(ch)
            if 0 and ch == '`' :
                for i in range(64, 64+27) :
                    sp.write( 100*('%c' % i))
                sp.write( '\n');
            if ord(ch) == 27 :
                sp.close()
                break;

    ''' Hex dump of the stream received, with addresses '''
    str = entire_stream[0:length]
    str_out = []
    
    for ndx in range(0, len(str), HEX_DUMP_WIDTH) :
    
        str_out.append ( "$%4.4X : " % (start_addr+ndx) )

        lst = ["%2.2x" % ord(ch) for ch in str[ndx: ndx+HEX_DUMP_WIDTH] ]
        str_out.append(  ' '.join(lst) )

        str_out.append('\r\n')

    print ''.join(str_out)


