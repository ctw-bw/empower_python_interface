#!/usr/bin/env python


SVN_SUB_REV_STR = "$Revision: 6501 $"


def output_results(d) :
    for k,value in d.iteritems() :
        name = pio.channel_to_name(k);
        print "%5d  %-25.25s : " % (k, name),
        for v in value :
            print "%5d " % v,
        print

def write_results_to_file(fname, dict) :
    
    fout = open(fname,"w");
    
    fout.write("channel, name, values\n");
    for k,value in dict.iteritems() :
        name = pio.channel_to_name(k);
        str = "%5d, %-25.25s, " % (k, name)
        fout.write(str);
        for v in value :
            s = "%5d, " % v
            fout.write(s);
            

        fout.write('\n');
    fout.close();

def append_results(results, dct) :
    for r in results :
        if r : 
            if dct.has_key(r['channel']) :
                dct[r['channel']].append(r['value']);
            else :
                dct[r['channel']] = [ r['value'] ];
    
    return dct;
    







def check_for_esc() :
    import python_utils.keyboard
    keybd =python_utils.keyboard.keyboard()
    (raw_ch, esc, fun, ch) = keybd.getch_packed();
    if (raw_ch == '\x1b') :
        print "!!!!!!!!!!!  RAW_CH == ESC.  CALLING BREAK"
        sys.exit()

if __name__ == "__main__":     
    
    import sys
    import pfcmd
    import serial
    import time;
    
    import parse_iochan;
    pio = parse_iochan.parse_iochan();
    
    import iochan_capture_options
    parser = iochan_capture_options.options(sys.argv)
    opt = parser.values
    
    
    if opt.com_port == None :
        print "specifiy a comm port"
        sys.exit(-1)

    
    sp = serial.Serial(opt.com_port-1, baudrate=opt.baud_rate, timeout=0.05, rtscts=0);
    pf = pfcmd.PFCmd(0,sp);
    
    ctrl = pfcmd.controller(pf)
    
    
    
    
    dict_AI = {}
    dict_DI = {}
    dict_DO = {}       
    for i in range(0, opt.number_of_tests) :
        print
        print
        
        check_for_esc();
        c_list = pio.get_AI_list(0,1999);
        result_AI = ctrl.read_analog_in(c_list);
        append_results(result_AI, dict_AI)
        output_results(dict_AI);
    
        if (1) :
            check_for_esc();        
            c_list = pio.get_DI_list(0,1999);
            result_DI = ctrl.read_digital_in(c_list);
            append_results(result_DI, dict_DI)
            output_results(dict_DI);        
    
            check_for_esc();        
            c_list = pio.get_DO_list(0,1999);
            result_DO = ctrl.read_digital_out(c_list);
            append_results(result_DO, dict_DO)        
            output_results(dict_DO);
    
    
    # build a dict with all the results - in preperation to writing to a file
    merge_dict = {};
    for k,v in dict_AI.iteritems() :
        merge_dict[k] = v;
    for k,v in dict_DI.iteritems() :
        merge_dict[k] = v;
    for k,v in dict_DO.iteritems() :
        merge_dict[k] = v;    
    
    
    write_results_to_file(opt.filename, merge_dict);
    
    
    
    """    
    for r in result_AI :
        name = pio.channel_to_name(r['channel']);
        print "%5d  %-25.25s %5d " % (r['channel'], name, r['value'])
    
    
    for r in result_DI :
        name = pio.channel_to_name(r['channel']);
        print "%5d  %-25.25s %5d " % (r['channel'], name, r['value'])
    
    
    for r in result_DO :
        name = pio.channel_to_name(r['channel']);
        print "%5d  %-25.25s %5d " % (r['channel'], name, r['value'])
    
    
    """
