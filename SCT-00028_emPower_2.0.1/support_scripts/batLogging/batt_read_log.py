import sys, os, time, calendar, optparse, math
import struct, argparse
import collections;
from collections import OrderedDict;

import python_paths_default     # test for user's python_paths.py and update paths
import python_paths as pyenv
import serial

import comm_lib.pfcmd as pfcmd


import csv;

MEM_TYPE_EVENT_LOG = 4
MEM_TYPE_CUMULATIVE_STATS_LOG = 6

trans_tbl = ' '*32 + ''.join(  ['%c' % chr(ndx) for ndx in range(32, 256)] );

######################
class c_struct :
    pass

#helper for options so we can send cmd line arguments as hex or binary if we want. 
def int_any_fmt(x):
   return int(x, 0)    

        
class command_options :
    
    def __init__(self, args) :
        
        self.args = args
        
# allow special command data to be saved to find with command line redirection.
        try:
            arg_end = args.index(">")
            args = args[:arg_end];
            print i
        except:
            pass

        parser = argparse.ArgumentParser();
        parser.add_argument("-B", dest="device_string", type=str, help="MAC or Linux device name");
        parser.add_argument("-P", "--port", dest="port", type=int_any_fmt, default=10002, help="port number for TCP connections.");
        parser.add_argument("-p","-c", "--com_port", dest="com_port", type=int_any_fmt, help="COM PORT number for serial port connection");
        parser.add_argument("--LIN", dest="LIN_mode", action="store_true", default=False, help="Use with LIN mode comm: ignores the local echo on the RX line of whatever command was just sent out.")
        parser.add_argument("-b", "--baud_rate", dest="baudrate", type=int_any_fmt, default=115200, help="baurdrate when using serial port (default is 115200)");
        parser.add_argument("-t", "--timeout", dest="timeout", default=5, help="serial or TCP communcations timeout (sec)");
        parser.add_argument("--hex_output_file", dest="hex_output_file", type = str, help="Store resulting hex output in this binary data file");
        parser.add_argument("-f", "--file_name", dest="log_file_name", help="log the results to a file.");
        parser.add_argument("-R", "--read_address", dest="read_address", type=str, help = "read memory from address (int or hex)");
        
        #parser.add_argument("-W", "--write_address", dest="write_address", type=str, help = "write data to address (int or hex)");
        parser.add_argument("-L", "--read_length", dest="read_length", type=str, default = "0x100", help = "length of memory buffer to read");
        
        parser.add_argument("-M", "--memory_type", dest="memory_type", type=int, default=None, help="numeric code to specify the memory type and platform for read and write. Use 4 to get EVENT LOGS and 6 to get CUMULATIVE STATISTICS logs.")
        
       
        
        # parser.disable_interspersed_args()
        args = parser.parse_args()
        
        # make a struct from the options dictionary
        opt = c_struct();
        for k,v in args.__dict__.iteritems() :
            opt.__dict__[k] = v
            #print k,v
            self.opt = opt;
        
        return  

CUMUL_STATS_BAT_MONITOR_LABELS = [ "CHARGE_TIME_EXCEEDED_SEC_LOG","CHARGE_OVERSTUFF_MILLIAMP_HOURS_LOG","CHARGE_TEMP_HIGH_CENTIDEG_LOG","CHARGE_TEMP_LOW_CENTIDEG_LOG","AFE_I2C_COMM_CHECK_LOG","CURRENT_OVERLOAD_CENTI_A_LOG","EEPROM_I2C_COMM_CHECK_LOG","INSTANT_CELL_HIGH_MV_LOG","FILTERED_CELL_LOW_MV_LOG","INSTANT_PACK_LOW_MV_LOG","PACK_VS_SYSOUT_CONSISTENCY_MV_LOG","INPUT_CHARGE_OVERCURRENT_CENTI_A_LOG","TEMP_OUT_OF_RANGE_CENTI_DEG_LOG","TEMP2_OUT_OF_RANGE_CENTI_DEG_LOG","AFE_3V3_PRIMARY_REF_MV_LOG","ANALOG_REF_EXTERNAL_3V0_MV_LOG","ANALOG_REF_INTERNAL_3V3_MV_LOG","CURRENT_ZERO_OFFSET_CENTI_A_LOG","INVALID_MFG_TABLE_LOG"];
        
CUMUL_STATS_LOG_FMT = "<HL 19H2L 11H 11L 5H 2h2L 8H2B";
"""
       {
        uint16_t record_number; //2 Order record was written (timestamp may not be trustworthy for order since it will be reset by external sources fairly often.)
        uint32_t timestamp;     //6     
        
        uint16_t battery_monitor_triggered[NUM_BATT_MONITOR_COUNTERS]; //44
        uint32_t step_count;                //48 TODO: How to get this in incremental quantities from ankle? "Steps since last comm?"
        uint32_t charger_terminals_live ;    //52     number of times the battery was inserted into the charger and we power on
        
        uint16_t host_insertion;     //54   
        uint16_t cell_lv_shutdowns[NUM_CELLS];        //64 Which cell is the lowest at low voltage shutdown. (NORMAL OPERATION- NOT ERROR)
        uint16_t cell_hv_shutdowns[NUM_CELLS];        //74 cell over-voltage shutdown (Can happen during charging)
        
        uint32_t ankle_terminals_live;                     //78 number of times the battery was inserted into the ankle and we power on.
        uint32_t hist[LOG_NUM_HIST_BINS];    //118 battery histogram (steps at peak current)
        
        uint16_t AFE_CRC_errors;              //120 number of times the AFE reference got corrupted
        uint16_t ankle_comm_errors;          //122 
        uint16_t charger_comm_errors;          //124 
        uint16_t EEPROM_comm_errors;                //126 formerly ignoreErrors
        uint16_t ADC_timeout_errors;    //128 Record when our ADC times out instead of giving us a value. Unlikely unless we get stuck in too many interrupts.
        
        
        int16_t max_temp;                   //130 highest recorded temperature (initialize well below ambient)
        int16_t min_temp;                     //132 lowest recorded temperature (initialize well above ambient)
        uint32_t diag_monitor_triggered;        //136 copy of batt_mon test failed bitfield from last execution
        uint32_t diag_monitor_testrun;          //140 copy of batt_mon test executed bitfield from last execution
      
        uint16_t brown_outs;                 //144
        uint16_t ext_resets;
        uint16_t wdt_resets;                //148
        uint16_t trap_oscillator;                   
        uint16_t write_reason;              //152
        uint16_t trap_address;              //154     
        uint16_t trap_math;    //156
        uint16_t trap_stack;   //158
        
        uint8_t padding;
        uint8_t crc8;                    //160 Checksum (CRC)
"""    
def parse_cumul_stats_log(bytestream, vers='2.0.0'):
    global CUMUL_STATS_LOG_FMT;
    global CUMUL_STATS_BAT_MONITOR_LABELS;
    lf = collections.OrderedDict();
      
    items = struct.unpack(CUMUL_STATS_LOG_FMT, bytestream);
    if (items[0] == 0xFFFF):
        return None #0xffff index means not a valid record. 
    lf['record_index'] = items[0];
    lf['timestamp'] = timestamp2str(items[1]);
    for i in range(len(CUMUL_STATS_BAT_MONITOR_LABELS)):
        lf[CUMUL_STATS_BAT_MONITOR_LABELS[i]] = items[2+i];
    lf['step_count'] = items[21]
    lf['charger_terminals_live'] = items[22]
    
    lf['host_insertion'] = items[23]
    for i in range(5):
        lf["cell_lv_shutdowns"+str(i)] = items[24+i]
        lf["cell_hv_shutdowns"+str(i)] = items[29+i]
    lf['ankle_terminals_live'] = items[34]
    for i in range(10):
        lf['peak_current_histogram bin'+str(i)] = items[35+i]
    
    lf['AFE_CRC_errors'] = items[45]
    lf['ankle_comm_errors'] = items[46]
    lf['charger_comm_errors'] = items[47]
    lf['EEPROM_comm_errors'] = items[48]
    lf['ADC_timeout_errors'] = items[49]
    lf['max_temp seen'] = items[50]
    lf['min_temp seen'] = items[51]
    lf['diag_monitor_triggered'] = bin(items[52])
    lf['diag_monitor_testrun'] = bin(items[53])
    lf['brown_outs'] = items[54]
    lf['ext_resets'] = items[55]
    lf['wdt_resets'] = items[56]
    lf['trap_oscillator'] = items[57]
    lf['write_reason'] = items[58]
    lf['trap_address']  = items[59]               
    lf['trap_math']     = items[60] 
    lf['trap_stack']    = items[61]
    #Note: last items are flopped because memory is read/written in 2byte words, so bytewise data is flipped.
    lf['padding'] = items[62]
    lf['crc8'] = items[63]
    return lf;

def timestamp2str(tstamp):
    '''
    days = (uint32_t)(time->year) * (365);
    for(i = 0; i < time->month && i < 12; i++)
      days += (uint32_t)(_month_len[i]);  // Count up the months
    
    days += (uint32_t)(time->year >> 2); // Adjust for leap years past (year 0 = 2000)
    if((time->month > 1) && ((time->year & 0x03) == 0))
      days += 1;  // Adjust for this year if it is a leap year
    
    secs = days * ((uint32_t)3600 * 24);
    secs += (uint32_t)time->hour * (uint32_t)3600;
    secs += (uint32_t)time->min * 60;
    secs += (uint32_t)time->sec;
    '''
    
    secs = tstamp + calendar.timegm(time.strptime('2000-01-01 00:00:00', "%Y-%m-%d %H:%M:%S"));
    
    return time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime(secs));


def get_battery_state(state):
    states=["BS_MANUFACTURING","BS_PERMANENT_FAULT","BS_WAKEUP_FROM_SLEEP","BS_WAIT_FOR_HARDWARE","BS_WAIT_FOR_VALID_HOST_SIGNAL","BS_SLEEP","BS_CYCLIC_STORAGE_SOC_CHECK","BS_AWAKE_TERMINALS_LIVE","BS_BRAINDEATH"]
    try:
        return states[state - 10]; #offset of 10 to avoid battery 1.5 confusion. 
    except:
        return state
     
def get_failure_bits(bitfield):
    fields = ["DT_CHARGE_TIME_EXCEEDED_SEC", "DT_CHARGE_OVERSTUFF_MILLIAMP_HOURS", "DT_CHARGE_TEMP_HIGH_CENTIDEG", "DT_CHARGE_TEMP_LOW_CENTIDEG", "DT_AFE_I2C_COMM_CHECK","DT_CURRENT_OVERLOAD_CENTI_A", "DT_EEPROM_I2C_COMM_CHECK", "DT_INSTANT_CELL_HIGH_MV", "DT_FILTERED_CELL_LOW_MV","DT_INSTANT_PACK_LOW_MV","DT_PACK_VS_CELL_CONSISTENCY_MV", "DT_PACK_VS_SYSOUT_CONSISTENCY_MV", "DT_PERMANENT_CELL_IMBALANCE_MV", "DT_PERMANENT_CELL_LOW_MV", "DT_INPUT_CHARGE_OVERCURRENT_CENTI_A", "DT_TEMP_OUT_OF_RANGE_CENTI_DEG", "DT_TEMP2_OUT_OF_RANGE_CENTI_DEG", "", "","", "", "", "", "", "DT_AFE_3V3_PRIMARY_REF_MV","DT_AFE_OPEN_CELL_DETECTION","DT_ANALOG_REF_EXTERNAL_3V0_MV","DT_ANALOG_REF_INTERNAL_3V3_MV","DT_CURRENT_ZERO_OFFSET_CENTI_A","DT_INVALID_MFG_TABLE","DT_PREVIOUS_PERMANENT_LOCKOUT", ""]
    bits = []
    for i in range(32):
        if (bitfield & 2**i):
            bits.append(fields[i])
    return bits
 
        
        
def get_event_bits(bitfield):
    event_desc = ["EVENT_BATTERY_RESET",  "EVENT_ANKLE_TERMINALS_ON","EVENT_CHARGER_TERMINALS_ON", "EVENT_TERMINALS_OFF","EVENT_CHARGE_START","EVENT_CHARGE_FULL","EVENT_UNUSED", "EVENT_TEMP_FAULT","EVENT_PERMANENT_FAULT","EVENT_HOST_INSERTION ","EVENT_HOST_REMOVAL", "EVENT_STORAGE_SOC_CHECK", "EVENT_BATTERY_DEPLETED",   "EVENT_POST_FAIL","EVENT_BALANCING_RECALCULATE", "EVENT_EXTERNAL_TRIGGER"];

    events = []
    for i in range(16):
        if (bitfield & 2**i):
            events.append(event_desc[i])
    return events

def get_statemachine_bits(bitfield):
    #print bitfield
    state_desc =[  "capacity_check_button_level_debounced ", "capacity_check_lowtohi_flag ", "external_shutdown_request_flag ", "battery_capacity_too_low ", "biom_comm_confirmed ", "charger_comm_confirmed ", "external_sleep_request_flag ", "afe_failure ", "host_level_debounced ", "host_level_change_flag ", "emergency_fet_disabled ", "timeout_begun ", "permanent_fault_detected ", "temp_fault_detected ", "storage_check_wakeup ", "hw_reset_completed "];
    state = [bin(bitfield)]
    for i in range(16):
        if (bitfield & 2**i):
            state.append(state_desc[i])
    return state

def get_reset_bits(bitfield):
    #print bitfield
    reset_desc =[  "POWER_ON_RESET", "BROWN_OUT_RESET",  "WATCHDOG_RESET", "EXTERNAL_RESET", "TRAP_RESET", "SOFTWARE_RESET", "CONFIG_MISMATCH_RESET",         "ILLEGAL_OP_CODE_RESET", "UNKNOWN_RESET",  "SLEEP_DUE_TO_CHARGER_COMM_LOSS", "SLEEP_DUE_TO_EXTERNAL_CMD"];
    reset = []
    for i in range(16):
        if (bitfield & 2**i):
            reset.append(reset_desc[i])
    return reset

    
    
EVENT_LOG_FMT = "<HLLH 8hH 2h5H2h H2Lh2H";
""" Log format (see ../firmware/logging.h)
       typedef struct {
            uint16_t record_number;  //2
            uint32_t timestamp;       // 6
            uint32_t uptime;          // 10
            uint16_t event;           // 12
    } EVENT_HDR_T;
    
    typedef struct
         
        {                             // # of bytes
            int16_t packV;             // 14
            int16_t cellV[NUM_CELLS];  // 24
            int16_t current;           // 26
            int16_t temp1;             // 28
            uint16_t mAh;              // 30
            
            int16_t bsm_state;         // 32
            int16_t milliamp_hours_charging; // 34
            uint16_t balance_time_left[NUM_CELLS]; // 44
            int16_t seconds_charging;    // 46
            int16_t battery_monitor_value; // 48  //Latest Value that caused test to fail.
            
            uint16_t battery_monitor_last_failed; // 50  //Latest Value that caused test to fail.
            uint32_t battery_monitor_failed;// 54  //All tests that have failed
            uint32_t battery_monitor_run;   // 58  //All tests that have been run
            uint16_t temp2;                //60
            uint16_t statemachine_flags;   //62
            uint16_t reset_flags;               //64 
            
        } eventRecord_t; //Fault event records get different data
"""
def parse_event_rec(bytestream, addr):
    global EVENT_LOG_FMT
    rec = collections.OrderedDict();
    items = struct.unpack(EVENT_LOG_FMT, bytestream);
    
    rec['rec'] = addr;
    if (items[0] == 0xFFFF):
        return None #0xffff index means not a valid record. 
    rec['record_index'] = items[0];
    rec['timestamp'] = timestamp2str(items[1]);
    rec['uptime (72Hz ticks)'] = items[2];
    rec['events'] = get_event_bits(items[3]);
    rec['pack V'] = items[4] * 0.001;
    for i in range(5):
        rec['cell%d V' % (i+1)] = items[5+i] * 0.001;
    rec['current A'] = items[10] * 0.01;
    rec['temp1'] = items[11] * 0.01;
    rec['temp2'] = items[25] * 0.01;
    rec['mAh in battery'] = items[12]
    rec['battery_state'] = get_battery_state(items[13])
    rec['mAh charging'] = items[14]
    for i in range(5):
        rec['balance_time_left_%d' % (i+1)] = items[15+i];
    rec['seconds_charging'] = items[20]
    rec['battery_monitor_value'] = items[21]
    rec['battery_monitor_diag_triggered'] = ['{:032b}'.format(items[23])] + get_failure_bits(items[23])
    if (items[23] != 0):
        rec['battery_monitor_last_failed'] = get_failure_bits(2**(items[22]))
    else:
        rec['battery_monitor_last_failed'] = items[22]
    
    rec['battery_monitor_run           '] = '{:032b}'.format(items[24])
    
    rec['statemachine_flags'] = get_statemachine_bits(items[26])
    rec['log_processor_reset_and_sleep_flags'] = get_reset_bits(items[27])
    
    return rec;

def bytes_into_cumulative_stats_files(string_in):
    global CUMUL_STATS_LOG_FMT;
    i = 0
    records = []
    print len(string_in)
    while(i+struct.calcsize(CUMUL_STATS_LOG_FMT) <= len(string_in)):
        r = parse_cumul_stats_log(string_in[i:i+struct.calcsize(CUMUL_STATS_LOG_FMT)])
        if (r != None):
            records.append(r)
        else:
            break;
        i += struct.calcsize(CUMUL_STATS_LOG_FMT);
        print "Created cumulative stats log."
    return records
    
def bytes_into_records(string_in, starting_addr):
    i = 0
    records = []
    print len(string_in), struct.calcsize(EVENT_LOG_FMT)
    while(i+struct.calcsize(EVENT_LOG_FMT) <= len(string_in)):
        r = parse_event_rec(string_in[i:i+struct.calcsize(EVENT_LOG_FMT)], starting_addr+i)
        if (r != None):
            records.append(r)
        else:
            break;
        i += struct.calcsize(EVENT_LOG_FMT);
        print "Created record."
    return records
    
def loadFile(fp):
    reader = csv.reader(fp);
    
    fdata = OrderedDict();
    cols = OrderedDict();
    
    # Convert CSV into dictionary of record types
    for line in reader:
        rowname = line[0];
        cols[rowname] = line[1:];
    
    try:
        numFiles = len(cols['fileNo']);
    except:
        numFiles = 0;
    
    # Build dictionary of log files   
    for i in range(numFiles):
        fn = cols['fileNo'][i];
        fdata[fn] = OrderedDict();
        for k in cols:
            fdata[fn][k] = cols[k][i];
    
    return fdata;

        
if __name__ == "__main__" :
   
    parser = command_options(sys.argv)
    opt = parser.opt;
    
    def get_value(str) :
        u = str.upper()
        if u[0:2] == "0X" :
            v = int(u,16);
        else :
            v = int(u)
        return v
    
    ctrl = pfcmd.controller(None)
    
    
    pf = pfcmd.PFCmd();
    if opt.device_string :
        dev = os.path.join("/dev", opt.device_string);
        print dev
        pf.connect(dev, opt.baudrate);
        pf.set_serial_timeout(opt.timeout);
    elif opt.com_port :
        print "SERIAL"
        pf.connect(opt.com_port, opt.baudrate);
        pf.set_serial_timeout(opt.timeout);
    elif os.name != 'nt':
          print 'SERIAL LINUX'
          # Discover the USB Serial Ports to use
          sph = serial_port_helper.serial_port_helper()
          # Note: detects PSER movement from 0,1 to 2,3 (Linux)
          portNumStr = sph.get_port_to_use()
          if portNumStr == None :
            print('USB Serial Port NOT FOUND... Exiting')
            exit(-1)
          else :
            portNum = int(portNumStr)
            MPD_COMMAND_SERIAL_PORT = portNum
          
          MPD_COMMAND_COM_PORT = sph.get_os_serial_port_name(MPD_COMMAND_SERIAL_PORT);
          print("Using MPD COMMAND SERIAL COM:" + str(MPD_COMMAND_COM_PORT))
          port = MPD_COMMAND_COM_PORT
          pf.connect(port, opt.baudrate);
          pf.set_serial_timeout(opt.timeout);
    else:
        print "you must specify an IP address or COMPORT."
        sys.exit()
        
        
    
    
    ctrl = pfcmd.controller(pf)
    
    if opt.LIN_mode:
        pf.ignore_rx_of_local_echo = True;
    

    if opt.read_address or (opt.memory_type == MEM_TYPE_CUMULATIVE_STATS_LOG):
        addr = ""
        total_read_length = 0
        if opt.memory_type == MEM_TYPE_CUMULATIVE_STATS_LOG: #Hard coded cumulative stats log in on-chip EEPROM.
            addr = 66 #Offset from mfg. config in code. 
            total_read_length = 2*struct.calcsize(CUMUL_STATS_LOG_FMT)
        else:
            addr = get_value(opt.read_address);
            total_read_length  = get_value(opt.read_length);
        
        
        hex_output = None
        if (opt.hex_output_file):
            hex_output = open(opt.hex_output_file, 'wb');
        
        
        block_length = 128;
        
        last_addr = 0;
        trans_str = ""
        whole_bytestream = ''
        for buf_ndx in range(0, total_read_length, block_length) :
            read_length = min(total_read_length-buf_ndx, block_length)
            
            
            try :
                result = ctrl.readwrite_mem('R', opt.memory_type, read_length, addr, QUIET=True)
            except :
                print "read error"
    
                
            if result and len(result) :
                if (hex_output != None):
                    hex_output.write(result[0]['buff'])
    
                if "ERROR" in result:
                    print
                    print "do_command:error:", result
                    print                
                elif "ecode" in result[0]: 
                    print "ecode", result[0]
                    buff_len = 0
                     
                else :
                    buff = result[0]['buff']
                    buff_len = len(buff)
                    
                    bytestream = ctrl.result_to_bytestream(result);
                    bytestream.print_addr = True;
                    bytestream.base_addr = addr;
                    
                    if opt.memory_type == MEM_TYPE_EVENT_LOG:
                        records = bytes_into_records(result[0]['buff'], addr)
                        for r in records:
                            print "Record:"
                            for key in r.keys():
                                print key, r[key] 
                            print ""
                        print addr
                        print bytestream
                    elif opt.memory_type == MEM_TYPE_CUMULATIVE_STATS_LOG:
                        whole_bytestream += result[0]['buff'] #Stats log doesn't line up with packet size, so process it at the end.
                        print bytestream
                        
                    else:
                        print bytestream

            addr += read_length

        if opt.memory_type == MEM_TYPE_CUMULATIVE_STATS_LOG:
            stats = bytes_into_cumulative_stats_files(whole_bytestream)
            for r in stats:
                print "Cumulative Statistics:"
                for key in r.keys():
                    print key, r[key] 
                print ""        
        
        if (hex_output != None):
            hex_output.close();    
    
    opt.log_fout = None
    if opt.log_file_name :
        opt.log_fout = open(opt.log_file_name, 'w');    
    
    if opt.log_fout :
        opt.log_fout.close()
        