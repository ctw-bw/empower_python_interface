'''
Created on Aug 15, 2014

@author: ekurz
'''
import logging
import sys
import os

class logger_class() :
    """logger class"""
    def __init__(self,logFileName) :

        # Create a Logger, File Handler, and Formatter
        self.log    = logging.getLogger('mfg')
        self.logFh  = logging.FileHandler(logFileName)
        self.logFmt = logging.Formatter('%(levelname)-10s %(asctime)s %(message)s')
        self.logName = logFileName
        # Connect the Created Objects
        self.logFh.setFormatter(self.logFmt)
        self.log.addHandler(self.logFh)

        # Set the log level to grab everything
        self.log.setLevel(logging.DEBUG)

    def write(self,record) :
        # Write to console and the log
        sys.__stdout__.write(record)
        self.log.info(record)

    def logInfo(self,record) :
        # Write to log only using info level
        self.log.info(record)

    
    
    def setDir(self,folder):
        self.logFh2 = logging.FileHandler(os.path.join(folder,self.logName))
        self.logFh2.setFormatter(self.logFmt)
        self.log.addHandler(self.logFh2)
        self.log.removeHandler(self.logFh)
        self.logFh.flush()
        self.logFh.close()
        os.remove(self.logName)
    def close(self):
        self.log.removeHandler(self.logFh2)
        self.logFh2.flush()
        self.logFh2.close()