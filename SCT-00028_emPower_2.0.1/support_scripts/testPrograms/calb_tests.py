#!/usr/bin/env python
'''
Created on Jul 21, 2014

@author: ekurz
'''
from __future__ import print_function
import python_utils

SVN_SUB_REV_STR  = "$Revision: 16978 $"
SVN_SUB_URL_STR  = "$URL: http://svn.iwalk.com/svn/svn_engineering/PowerFoot/tags/MFG/MFG_v2X/MFG_v2X_BootloadScriptv_v1.0.1b1/support_scripts/testPrograms/calb_tests.py $"
SVN_SUB_DATE_STR = "$Date: 2017-01-30 11:33:33 -0500 (Mon, 30 Jan 2017) $"

import python_paths_default  # test for user's python_paths.py and update paths
import python_paths as pyenv
import pfcmd
import ankle_commands 
import ConfigParser
import ordereddict
import time
import numpy
import loggHandeler
import os, sys, optparse

import wx
import random
import criterea
import scipy.stats as stats
import grapher_matplotlib
import subprocess
import calb_spring_analysis

class inputError(Exception):
    pass

class unknownError(Exception):
    pass

class struct :
    pass

def doSubprocess2(execStr, logger, soFn, seFn, retries=10, delay=1.0, simulation=False) :
    """wrapper around python's subprocess() with optional retries (and delay)"""
    ret = -1
    if simulation :
        print('', file = logger)
        print('PRETEND Executing: ', execStr, file = logger)
        return 0
    else :
        # print()
        # print('Executing: ', execStr)
        for retry in range(retries) :
            print('retry %d'%retry, file = logger)
            # ret code is wrong, known Python bug
            execStr2 = '(%s | tee %s) 2> %s' % (execStr,soFn,seFn)
            ret = subprocess.call(execStr2, shell=True)
            logSubprocessResults(logger,soFn,seFn)
            if not ret :
                break
            time.sleep(delay)
            continue

        return ret

def logSubprocessResults(loggerInstance, soFn, seFn) :
    """Log the results from a subproccess call to a logger"""
    try :
        for line in open(soFn,'r') :
            loggerInstance.logInfo(line)
        for line in open(seFn,'r') :
            loggerInstance.logError(line)
    except :
        print('Logging Exception', file = loggerInstance)
        
class calb_test_options :
    
    
    def __init__(self, args) :
        self.args = args

        usage = "usage: %prog [options]"
        parser = optparse.OptionParser();
        parser.add_option("-p", "--port", dest="port", type="int", default="9", help="Communication port connected to BiOM");
        parser.add_option ('-f', '--template_ini_file',  dest="template_ini_file", default = os.path.abspath('template_ankle_tables.ini'),\
                            help="Template ini file to use to format output results. Defaults to 'template_ankle_tables.ini'.")
        parser.add_option("--motor_encoder_cpt", type="int", default="4096", help="Motor encoder counts per motor revolution. Defaults to 4096")
        parser.add_option("--joint_encoder_cpt", type="int", default="8192", help="Ankle Joint encoder counts per revolution. Defaults to 8192")
        parser.add_option("-w","--workflow", type='str', default='full', help='determine witch tests are to be run: full, calb, spring')
        parser.add_option("-m","--move",action='store_false',dest='move',default=True,help='do not write anything to the UUT')
        parser.add_option("-b","--baudrate", type="int", default="115200", help="Baud rate for ankle commands. Defaults to 115200.")
        parser.add_option("--comm_timeout_milliseconds", type="int", default="500", help="Timeout used before timing out on a command.")
        parser.add_option('--anyfw',action='store_true',default=False,help='allow any fw to be used')
   
        # parser.disable_interspersed_args()
        (options, args) = parser.parse_args(args);
        self.options = options;
        self.args = args;
        
        # make a struct from the options dictionary
        opt = struct();
        for k,v in options.__dict__.iteritems() :
            opt.__dict__[k] = v
            # print k,v
            self.opt = opt;
        return


CALB_MOTOR_MOVE_VOLTAGE = 1000   
FORCEFIELD_OFFSET_INCREMENTAL_ENC_COUNTS = 6000 
ANKLE_ANGLE_ENCODER_COUNTS_TOLERANCE = 4
    
class BiOMCalbScripts(object):
    '''
    calibration tools for getting ankle calibration parameters without a fixture
    dose not get k2 or k3 values
    '''
    def __init__(self, ankleCon, ankleDataCon, calbFile, logFile=None, br= 115200):
        '''
        set up object
        '''
        self.baudrate = str(br)
        self.ankleCon  = ankleCon
        #self.incRes = 2048 #for original custom motor
#         self.incRes = 4096 #for off the shelf motors
#         self.AAEres = 8192.0
        self.ankleDataCon = ankleDataCon #data stream from the ankle -- dataport object?
        if isinstance(calbFile, ConfigParser.ConfigParser):
            self.calbFile = calbFile # ankle calibration file
        elif isinstance(calbFile, str): #Load a config parser from specified calbFile:
            self.calbFile = ConfigParser.ConfigParser(dict_type=ordereddict.OrderedDict)
            self.calbFile.read(calbFile)
        
        
        else: 
            raise inputError('you must pass a ConfigParser.ConfigParser object or a valid ankle INI file into the calbFile value')
        
        if isinstance(logFile, loggHandeler.logger_class):
            self.logFile = logFile #log file for calibration process
        elif logFile == None:
            self.logFile = None
        else:
            raise inputError('you must pass a loggHandeler.logger_class object into the logFile value')
        
        self.incRes = 2.0 ** int(self.calbFile.get("actuator_cfg_shared", "motor_enc_bits"))
        self.AAEres = 2.0 ** int(self.calbFile.get("AAE_cfg_shared","joint_encoder_bits"))
        self.phased = False
        self.moveTime=0.01
        self.relaxTime=1#0.75
        self.app = wx.App(False)
        self.retryes = 10
        
        self.analysis = calb_spring_analysis.springAnalysis(self.logFile,cfg = self.calbFile)
        
        self.calbFile.set('mfg_table', 'calb_tests.SVN_SUB_REV_STR', SVN_SUB_REV_STR)
        self.calbFile.set('mfg_table', 'calb_tests.SVN_SUB_URL_STR', SVN_SUB_URL_STR)
        self.calbFile.set('mfg_table', 'calb_tests.SVN_SUB_DATE_STR', SVN_SUB_DATE_STR)
        
    def checkConnection(self):
        '''
        confirms that we can get a response from the UUT
        '''
        try:
            res = self.special(ankle_commands.s_test_command())
            return True
        except unknownError:
            return False
        
    def disableTables(self):
        '''
        set the ankle to lock the calibration tables.
        '''
        self.special(ankle_commands.s_micro_write_protection_cal_on())
        
    def enableTable(self):
        '''
        set the ankle to unlock all the tables. 
        '''
        self.special(ankle_commands.s_micro_write_protection_off())
        
    def reseetTables(self):
        print('reset tables', file = self.logFile)
        time.sleep(0.1)
        v = self.rc.reset_RAM_to_defaults(1)
#         print(v, file=self.logFile)
        if v == []:raise unknownError('no response from UUT; resetting table: 1')
        time.sleep(0.1)
        v=self.rc.reset_RAM_to_defaults(2)
#         print(v, file=self.logFile)
        if v == []:raise unknownError('no response from UUT; resetting table: 2')
        time.sleep(0.1)
        v=self.rc.reset_RAM_to_defaults(9)
#         print(v, file=self.logFile)
        if v == []:raise unknownError('no response from UUT; resetting table: 9')
        time.sleep(0.1)
        v=self.rc.reset_RAM_to_defaults(10)
#         print(v, file=self.logFile)
        if v == []:raise unknownError('no response from UUT; resetting table: 10')
        time.sleep(0.1)
        v=self.rc.reset_RAM_to_defaults(15)
#         print(v, file=self.logFile)
        if v == []:raise unknownError('no response from UUT; resetting table: 15')
        time.sleep(0.1)
        v = self.special(ankle_commands.s_clear_k3_table())
        if v == []:raise unknownError('no response from UUT; resetting table: 15')
        print('end reset', file = self.logFile)
        return True #this program will create an error if it fails
        
    def reseetk3Table(self):
        print('reset k3 adaption table',file=self.logFile)
        v = self.special(ankle_commands.s_clear_k3_table())
        if v == []:raise unknownError('no response from UUT; resetting table: 15')
        return True
        
    def checkFW(self):
        '''
        check UUT FW
        '''
        toRet = True
        fw = self.ankleCon.do_command('state_version', [])
        self.calbFile.set('mfg_table','fw_rev_and_CRC',fw.get('fw_rev_and_CRC'))
        self.calbFile.set('mfg_table','fw_build_date',fw.get('fw_build_date'))
        self.calbFile.set('mfg_table','fw_svn_revision',fw.get('svn_revision'))
        expectedFW = criterea.FW()
        if expectedFW[0] not in fw.get('fw_rev_and_CRC'):
            toRet = False
            print('expected',file = self.logFile)
            print(expectedFW[0],file = self.logFile)
            print('got',file = self.logFile)
            print(fw.get('fw_rev_and_CRC'),file = self.logFile)
        if expectedFW[1] not in fw.get('fw_build_date'):
            toRet = False
            print('expected',file = self.logFile)
            print(expectedFW[1],file = self.logFile)
            print('got',file = self.logFile)
            print(fw.get('fw_build_date'),file = self.logFile)
        if expectedFW[2] not in str(fw.get('svn_revision')):
            toRet = False
            print('expected',file = self.logFile)
            print(expectedFW[2],file = self.logFile)
            print('got',file = self.logFile)
            print(fw.get('svn_revision'),file = self.logFile)
        return toRet
    
    def ckeckLog(self):
        '''
        check that the log files have been created 
        '''
        toRet = True
        logs = self.special(ankle_commands.s_logs_check())
#         print(logs)
#         print(logs.get('v1'))
#         print(type(logs.get('v1')))
        print('numlogs: %d'%(logs.get(ankle_commands.num_logs())))
        if logs.get(ankle_commands.num_logs()) != criterea.logNumFiles():
            self.special(ankle_commands.s_logs_setup())
            time.sleep(23)
            logs = self.special(ankle_commands.s_logs_check())
        if logs.get(ankle_commands.num_logs()) != criterea.logNumFiles():
            toRet = False
        return toRet
        
    def saveTables(self):
        to =self.ankleCon.comm_device.timeout
        self.ankleCon.set_serial_timeout(2)
        time.sleep(0.1)
        print('save tables', file = self.logFile)
        v = self.rc.save_RAM_to_flash(1)
#         print(v,file=self.logFile)
        if v == []:raise unknownError('no response from UUT; saving table: 1')
        time.sleep(0.1)
        v = self.rc.save_RAM_to_flash(2)
#         print(v,file=self.logFile)
        if v == []:raise unknownError('no response from UUT; saving table: 2')
        time.sleep(0.1)
        v=self.rc.save_RAM_to_flash(9)
#         print(v,file=self.logFile)
        if v == []:raise unknownError('no response from UUT; saving table: 9')
        time.sleep(0.1)
        v=self.rc.save_RAM_to_flash(10)
#         print(v,file=self.logFile)
        if v == []:raise unknownError('no response from UUT; saving table: 10')
        time.sleep(0.1)
        v=self.rc.save_RAM_to_flash(15)
#         print(v,file=self.logFile)
        if v == []:raise unknownError('no response from UUT; saving table: 15')
        print('end save tables ', file = self.logFile)
        self.ankleCon.set_serial_timeout(to)
        return True #this program will create an error if it fails
        
    def timeSet (self):
        toRet = True
        self.special(ankle_commands.s_set_time())
        tiUUT = self.special(ankle_commands.s_get_time())
        tiComp = time.gmtime()
        print(tiComp)
        UUTTimeString = '20%02d%02d%02d_%02d%02d%02dGMT'%(\
                        tiUUT.get(ankle_commands.time_year()),tiUUT.get(ankle_commands.time_month()),tiUUT.get(ankle_commands.time_day()),\
                        tiUUT.get(ankle_commands.time_hour()),tiUUT.get(ankle_commands.time_min()),tiUUT.get(ankle_commands.time_sec()))
        UUTTime = time.strptime(UUTTimeString, '%Y%m%d_%H%M%S%Z')
        print(UUTTime)
        UUTTime = time.mktime(UUTTime)
        
        print(UUTTime)
        tiComp = time.mktime(tiComp)
        print(tiComp)
        timeDif = UUTTime-tiComp
        print(timeDif)
        if timeDif > criterea.timeDif():
            toRet = False
        return toRet
         
        
    def setRunMotor(self):
        ret = self.special( ankle_commands.s_runlevel_get())
        #print(ret,file=self.logFile)
        while ret.get('v0') !=10:
            self.special( ankle_commands.s_runlevel_set_motor())
            ret = self.special( ankle_commands.s_runlevel_get())
#             print(ret,file=self.logFile)
            
    def setRunAnkle(self):
        ret = self.special( ankle_commands.s_runlevel_get())
#         print(ret,file=self.logFile)
        while ret.get('v0') !=20:
            self.special( ankle_commands.s_runlevel_set_ankle())
            ret = self.special( ankle_commands.s_runlevel_get())
#             print(ret,file=self.logFile)
    
    def setWalkAnkle(self):
        ret = self.special( ankle_commands.s_runlevel_get())
#         print(ret,file=self.logFile)
        while ret.get('v0') !=70:
            self.special( ankle_commands.s_runlevel_set_walk())
            ret = self.special( ankle_commands.s_runlevel_get())
#             print(ret,file=self.logFile)
    def setRoboConfig(self,rc):
        self.rc = rc
    
    def setDir(self,folder):
        self.calDir = folder
    def special(self,command,tryLim=10):
        toRet = self.ankleCon.do_command(ankle_commands.s_command_type(), command)
        tryes= 0
        while type(toRet)!= python_utils.printable_dict.printable_dict and tryes < tryLim:#type(toRet) ==None:
            print('#'*25,file=self.logFile)
            print('command failed retry: %d'%tryes,file=self.logFile)
            print('#'*25,file=self.logFile)
#             print('type')
#             print(type(toRet))
            self.logFile.logInfo("WARNING: HAD TO RETRY FOR:"+" ".join(str(s) for s in command))
            toRet = False
            self.ankleCon.flush()
            toRet = self.ankleCon.do_command(ankle_commands.s_command_type(), command)
            tryes+=1
            
        if toRet == [] or type(toRet) != python_utils.printable_dict.printable_dict:
            self.logFile.logInfo("ERROR: NEVER GOT RSP FROM:"+" ".join(str(s) for s in command))
            raise unknownError('no response from UUT; command: '+str(command))
#         else:
#             toRet = toRet2
        
        self.logFile.logInfo(toRet)
        return toRet
    
    def specialFloat(self,command):
        toRet = self.ankleCon.do_command_w_struct_str(ankle_commands.s_command_type(), command,\
                                                       ankle_commands.float_get_format(), ankle_commands.float_get_labels())
        if toRet ==[]:
            self.logFile.logInfo("WARNING: HAD TO RETRY FOR:"+" ".join(str(s) for s in command))
            toRet = False
            self.ankleCon.flush()
            toRet2 = self.ankleCon.do_command_w_struct_str(ankle_commands.s_command_type(), command, \
                                                           ankle_commands.float_get_format(), ankle_commands.float_get_labels())
            if toRet2 == [] or toRet2 == None:
                self.logFile.logInfo("ERROR: NEVER GOT RSP FROM:"+" ".join(str(s) for s in command))
                raise unknownError('no response from UUT; command: '+str(command))
            else:
                toRet = toRet2
        self.logFile.logInfo(toRet)
        return toRet
        
    def getUUTSN(self,roboConfigurator):
        '''
        get the SN from the ankle
        '''
        result = roboConfigurator.read_tables(['sys_key_shared'], debug_level=0)
        return result.section_dict['sys_key_shared']['ankle_serial_number'][0]
        
    def motorPhaseMiddle(self):
        '''
        phase the motor in middle of range of motion
        '''
        t1 = self.motorPhase()
        t2 = self.moveToEnd(True)
        t3 = self.moveDistanceINCCounts(20000,1000)
#         self.special(ankle_commands.s_motor_voltage(CALB_MOTOR_MOVE_VOLTAGE,0))
#         time.sleep(0.5)
#         self.special(ankle_commands.s_motor_stop())
#         time.sleep(0.1)
        t4 = self.motorPhase()
        return t1 and t2 and t3 and t4
    
    def moveDistanceINCCounts(self,target, tol,voltage=250):
        '''
        move the motor based upon incremental
        positive encoder values move ___
        '''
        encoders = self.special(ankle_commands.s_get_encoders())
        INC = encoders.get(ankle_commands.ENC_INCREMENTAL())
        tLow = INC + target - tol
        tHigh = INC + target + tol 
        target = INC +target
        moveCount = 0
        loop = 0
        inRange = 0
        print('moving distance ')
        while moveCount <5 and inRange < 3:
            loop +=1
            INCold = INC
            INC = self.special(ankle_commands.s_get_encoders()).get(ankle_commands.ENC_INCREMENTAL())
            if abs(INC - INCold)<3:
                moveCount +=1
            else: moveCount =0
            if tLow < INC < tHigh:
                inRange += 1
            if tLow+(tol/2) < INC < tHigh-(tol/2):
                volt = 0
            elif INC > target:
                volt = -voltage
                inRange = 0
            else:
                volt = voltage
                inRange=0
            self.special(ankle_commands.s_motor_voltage(volt, 0))
            time.sleep(0.1)
            print( '%d    %d    %d    %d    %d'%(loop, moveCount,inRange,INC,target),file=self.logFile)
        self.special(ankle_commands.s_motor_stop())
        if inRange >0:
            print('move succeeded ')
            return True
        print('unable to move to correct position',file = self.logFile)
        return False
            
    
    def motorPhase(self):
        '''
        phase the motor
        '''
        print('phase motor',file=self.logFile)
        toRet = True
        print('setting system idle.', file=self.logFile)
        idleSet = self.special(ankle_commands.s_set_system_idle())
        if idleSet ==None: toRet=False
#         print(idleSet, file=self.logFile)
        time.sleep(0.5)
        
        print('phase motor',file=self.logFile)
        #command the motor to phase
        #may have different process depending on motor type
        phase1 = self.special(ankle_commands.s_motor_phase(2000))
        if phase1 ==None: toRet=False
#         print( phase1, file=self.logFile)
        time.sleep(0.5)
#         phase2 = self.special(ankle_commands.s_motor_phase(5000))
# #         print( phase2, file=self.logFile)
#         time.sleep(0.1)
        #new motor
            #move motor to index
        #current motor
        encoders =  self.special(ankle_commands.s_get_encoders())
        if encoders ==None: toRet=False
#         print( encoders, file=self.logFile)
        #set motor to not moving
        stopMot = self.special(ankle_commands.s_motor_stop())
        if stopMot ==None: toRet=False
#         print(stopMot,file=self.logFile)
                
        mi = encoders.get(ankle_commands.ENC_ABSOLUTE())
        
#         print( mi,file=self.logFile)
        self.calbFile.set('calb_table_shared','encoder.motor_abs_encoder_comm_ref',str(mi))
        
        
        #save motor phase data to the calibration file
        #recored that the motor has been phased. 
        self.phased = toRet
        self.calbFile.set('mfg_table', 'motor_phase', toRet)
        return toRet
    
    def MotorOffsets(self,retry=0):
        '''
        find the motor offsets
        '''
        toRet = True
        print('Motor Offsets',file=self.logFile)
        offsetStopMotor = self.special(ankle_commands.s_motor_stop())
        if offsetStopMotor.get('v9')<0: toRet = False
#         print(offsetStopMotor,file=self.logFile)
        
        offsetStartCalb = self.special(ankle_commands.s_motor_current_offsets_start_calb())
        if offsetStartCalb.get('v9')<0: toRet = False
#         print(offsetStartCalb,file=self.logFile)
        time.sleep(0.25)
        offsetUseCalb = self.special(ankle_commands.s_motor_current_offsets_read_calb())
#         print(offsetUseCalb,file=self.logFile)
#         print(offsetUseCalb.get('v9'),file=self.logFile)
        loop = 0
        while offsetUseCalb.get('v9')<0 and loop < 120:
            offsetUseCalb = self.special(ankle_commands.s_motor_current_offsets_read_calb())
#             print(offsetUseCalb,file=self.logFile)
            time.sleep(0.25)
            loop +=1
        if loop == 100:
            offsetStartCalb = self.special(ankle_commands.s_motor_current_offsets_start_calb())
            if offsetStartCalb.get('v9')<0: toRet = False
            loop = 0
            time.sleep(0.25)
            offsetUseCalb = self.special(ankle_commands.s_motor_current_offsets_read_calb())
            while offsetUseCalb.get('v9')<0 and loop < 120:
                offsetUseCalb = self.special(ankle_commands.s_motor_current_offsets_read_calb())
    #             print(offsetUseCalb,file=self.logFile)
                time.sleep(0.25)
                loop +=1
        if loop == 100:
            print('error in getting motor offsets', file = self.logFile)
            toRet = False
            raise unknownError('error in getting motor offsets')
        
        
        #Make sure that values are being used:
        result = self.rc.read_tables(['current_calb_shared'], debug_level=0)
        phase_u_counts = result.section_dict['current_calb_shared']['phase_u_counts'][0]
        phase_v_counts = result.section_dict['current_calb_shared']['phase_v_counts'][0]
        battery_counts_adc = result.section_dict['current_calb_shared']['battery_counts_adc'][0]
        
        if phase_u_counts < criterea.phaseUcounts()[0]:
            toRet = False
            print('phase_u_counts is low',file=self.logFile)
            print('expected max: %f got: %f'%(criterea.phaseUcounts()[0],phase_u_counts),file=self.logFile)
            self.addError('phase_u_counts is low')
        if phase_u_counts > criterea.phaseUcounts()[1]:
            toRet = False
            print('phase_u_counts is high',file=self.logFile)
            print('expected max: %f got: %f'%(criterea.phaseUcounts()[1],phase_u_counts),file=self.logFile)
            self.addError('phase_u_counts is high')
            
        if phase_v_counts < criterea.phaseVcounts()[0]:
            toRet = False
            print('phase_v_counts is low',file=self.logFile)
            print('expected max: %f got: %f'%(criterea.phaseVcounts()[0],phase_v_counts),file=self.logFile)
            self.addError('phase_v_counts is low')
        if phase_v_counts > criterea.phaseVcounts()[1]:
            toRet = False
            print('phase_v_counts is high',file=self.logFile)
            print('expected max: %f got: %f'%(criterea.phaseVcounts()[1],phase_v_counts),file=self.logFile)
            self.addError('phase_v_counts is high')
            
        if battery_counts_adc < criterea.batteryCountsADC()[0]:
            toRet = False
            print('battery_counts_adc is low',file=self.logFile)
            print('expected max: %f got: %f'%(criterea.batteryCountsADC()[0],battery_counts_adc),file=self.logFile)
            self.addError('battery_counts_adc is low')
        if battery_counts_adc > criterea.batteryCountsADC()[1]:
            toRet = False
            print('battery_counts_adc is high',file=self.logFile)
            print('expected max: %f got: %f'%(criterea.batteryCountsADC()[1],battery_counts_adc),file=self.logFile)
            self.addError('battery_counts_adc is high')
        
        ###################################
        #add section checking calibrated sensors
        ###################################
        
        #get currents
        currents = self.specialFloat(ankle_commands.sf_get_currents())
        
        if currents.get(ankle_commands.current_batt())<criterea.batteryAmp()[0]:
            toRet = False
            print('battery current low',file=self.logFile)
            print('expected max: %f got: %f'%(criterea.batteryAmp()[0],currents.get(ankle_commands.current_batt())),file=self.logFile)
            self.addError('battery current low')
        if currents.get(ankle_commands.current_batt())>criterea.batteryAmp()[1]:
            toRet = False
            print('battery current high',file=self.logFile)
            print('expected max: %f got: %f'%(criterea.batteryAmp()[1],currents.get(ankle_commands.current_batt())),file=self.logFile)
            self.addError('battery current high')
        
        if currents.get(ankle_commands.current_U())<criterea.phaseUamp()[0]:
            toRet = False
            print('phase U low',file=self.logFile)
            print('expected max: %f got: %f'%(criterea.phaseUamp()[0],currents.get(ankle_commands.current_U())),file=self.logFile)
            self.addError('phase U low')
        if currents.get(ankle_commands.current_U())>criterea.phaseUamp()[1]:
            toRet = False
            print('phase U high',file=self.logFile)
            print('expected max: %f got: %f'%(criterea.phaseUamp()[1],currents.get(ankle_commands.current_U())),file=self.logFile)
            self.addError('phase U high')
        
        if currents.get(ankle_commands.current_V())<criterea.phaseVamp()[0]:
            toRet = False
            print('phase V low',file=self.logFile)
            print('expected max: %f got: %f'%(criterea.phaseVamp()[0],currents.get(ankle_commands.current_V())),file=self.logFile)
            self.addError('phase V low')
        if currents.get(ankle_commands.current_V())>criterea.phaseVamp()[1]:
            toRet = False
            print('phase U high',file=self.logFile)
            print('expected max: %f got: %f'%(criterea.phaseVamp()[1],currents.get(ankle_commands.current_V())),file=self.logFile)
            self.addError('phase V high')
        
        self.calbFile.set('current_calb_shared','phase_u_counts',str(phase_u_counts))
        self.calbFile.set('current_calb_shared','phase_v_counts',str(phase_v_counts))
        self.calbFile.set('current_calb_shared','battery_counts_adc',str(battery_counts_adc))
        self.calbFile.set('mfg_table', 'motor_offsets', toRet)
        print('*'*25)
        print(toRet)
        print(retry)
        print(retry < 2)
        print('*'*25)
        if (toRet == False) and (retry < 2):
            print('#'*75,file=self.logFile)
            print('#'*75,file=self.logFile)
            print('retrying motor offset',file=self.logFile)
            print('#'*75,file=self.logFile)
            print('#'*75,file=self.logFile)
            toRet = self.MotorOffsets(retry+1)
        return toRet
        
    def addError (self,errorS):
        '''
        adding an error to log
        '''
        print(errorS,self.logFile,file=self.logFile)
        if self.calbFile.has_option('mfg_table', 'errors'):
            errorS = self.calbFile.get('mfg_table', 'errors')+', '+errorS
        self.calbFile.set('mfg_table', 'errors',errorS)
        
    def zeroTorqueFind(self):
        '''
        find the zero torque relation between the encoders
        same movement method could be used to determine the current offsets. 
        '''
        
        if not self.phased: self.motorPhase()
        cycles = 0
        print('zero Torque Find',file=self.logFile)
        toRet = True
        self.moveToEnd(True)
        AAEsample = self.special(ankle_commands.s_get_encoders()).get(ankle_commands.ENC_ANKLE_ANGLE())
        AAEold = AAEsample
        
        Enc = []
        
        volt = 2000
        while cycles < 2:#5:
            if volt>0:
                print( 'cycle count %d'% cycles,file=self.logFile)
            else:
                cycles +=1
            count = 0 
            print('count,    AAE,    Inc,   Abs', file=self.logFile)
            while count <5:
                
                AAEold = AAEsample
                self.special(ankle_commands.s_motor_voltage(volt,0))
                ti = self.moveTime/2 +random.random() * (self.moveTime/2)
                time.sleep(ti)
                self.special(ankle_commands.s_motor_stop())
                time.sleep(self.relaxTime/2)
                self.special(ankle_commands.s_motor_current_amp(0, 0))
                time.sleep(self.relaxTime)
                encoders = self.special(ankle_commands.s_get_encoders())
                AAEsample = encoders.get(ankle_commands.ENC_ANKLE_ANGLE())
                
                Enc.append([encoders.get(ankle_commands.ENC_ANKLE_ANGLE()) ,encoders.get(ankle_commands.ENC_INCREMENTAL()),encoders.get(ankle_commands.ENC_ABSOLUTE())])
                
                print( '%5d, %6d, %6d, %5d'%(count, encoders.get(ankle_commands.ENC_ANKLE_ANGLE()) ,encoders.get(ankle_commands.ENC_INCREMENTAL()),\
                                         encoders.get(ankle_commands.ENC_ABSOLUTE())),file=self.logFile)
                if abs(AAEsample-AAEold)<2: count +=1
                else: count =0
            print('',file=self.logFile)
            print( 'reverse',file=self.logFile)
            volt = -volt
        
        self.moveToEnd(False)
        
        EncSort = sorted(Enc,key=lambda Enc:Enc[0])
        data = []
        for element in EncSort:
            if element[0] > (int(self.calbFile.get('calb_table_shared','encoder.aae_sensor_at_true_north'))) and \
                    element[0]< (int(self.calbFile.get('calb_table_shared','encoder.aae_retraction_hardstop'))):
                data.append(element)
        AAEHS = float(self.calbFile.get('calb_table_shared','encoder.aae_sensor_at_true_north'))
        
        incHS = float(self.calbFile.get('calb_table_shared','encoder.inc_forward_hardstop'))#+400
        
        
    #########################################
    #calculation of absolute encoder at true north using 
    #absolute vs inc relation and the more data found in this test
    #########################################
    
        absData = []
        absDataVector = []
        for elm in data:
            absData.append([elm[1],elm[2]])
            absDataVector.append(elm[2])
        absSort = sorted(absData,key=lambda absData:absData[0])
        
#         absTNs = []
        dif = 50000
        absCorected = 0
        for elm in absSort:
#             if abs(elm[0]-(incHS%4096))<=30:
#                 absTNs.append(elm[1])
            if abs(elm[0]-(incHS%4096)) <= abs(dif):
                dif = (elm[0]-(incHS%4096))
                absCorected = (elm[1]-dif)%4096
        self.calbFile.set('calb_table_shared', 'encoder.motor_abs_encoder_at_true_north', absCorected)
    ####################################################
    #end of abs encoder at true north
    ##################################################
        
        aaeData = []
        incData = []
        AAE = []
        ABS = []
        INC = []
        INCMod = []
        for elem in data:
            aaeData.append((elem[0]-AAEHS)/self.AAEres)
            incData.append((elem[1]-incHS)/self.incRes)
            AAE.append(elem[0])
            INC.append(elem[1])
            ABS.append(elem[2])
            INCMod.append(elem[1]%4096)
        vals = numpy.polyfit(aaeData, incData, 2)
        quad=numpy.poly1d(vals)
        valsLin = numpy.polyfit(aaeData,incData,1)
        lin = numpy.poly1d(valsLin)
        
        #grapher.thetabeta(aaeData,incData,lin(aaeData),quad(aaeData),self.calDir, self.app)
        dataSets = [];
        dataSets.append([aaeData,incData, 'Data', 'blue'])
        dataSets.append([aaeData,lin(aaeData), 'linear', 'red','line'])
        dataSets.append([aaeData,quad(aaeData), '2nd order', 'green','line'])
        grapher_matplotlib.graph(dataSets,"theta-beta", "normalized AAE", "normalized INC ENC (rotations)", os.path.join(self.calDir ,'theta-beta.png'))
   
        #grapher.thetaEncsVsBetaEncs(data,self.calDir,self.app)
        dataSets = [];
        dataSets.append([AAE,INC, 'INC', 'blue'])
        dataSets.append([AAE,ABS, 'ABS', 'green'])
        grapher_matplotlib.graph(dataSets,'beta encoders vs theta encoder','AAE','motor encoders', os.path.join(self.calDir ,'encs.png'))
   
        #grapher.INCvsABS(data,self.calDir, self.app)
        dataSets = []
        dataSets.append([INCMod,ABS, 'INC', 'blue'])
        grapher_matplotlib.graph(dataSets,'absolute vs incremental','incremental','absolute', os.path.join(self.calDir ,'motor.png'))
   

        self.calbFile.set('calb_table_shared','encoder.theta_beta_poly','%f, %f, %f'%(vals[0],vals[1],vals[2]))
        self.calbFile.set('mfg_table', 'zero_torque', toRet)
        
        ###########################
        #add coralation check?
        ############################
        
        return toRet
    
    def zeroTorqueCheck(self):
        '''
        check the zero torque relation between the encoders
        '''
#add startup criteria 
        print('zero Torque check',file=self.logFile)
        toRet=True
        print('setting system idle.', file=self.logFile)
        idleSet = self.special(ankle_commands.s_set_system_idle())
#         print(idleSet, file=self.logFile)
        time.sleep(0.5)
        
        self.moveToEnd(False)
        AAEsample = self.special(ankle_commands.s_get_encoders()).get(ankle_commands.ENC_ANKLE_ANGLE())
        AAEold = AAEsample
        
        torques = []
        startups = [] 
        startupsDif = [] 

        t = []
        t2=[]
        cycles = 0
        AAE=[]
        
        volt = -1500
        while cycles < 3:
            if volt>0:
                print( 'cycle count %d'% cycles,file=self.logFile)
            else:
                cycles +=1
            count = 0 
            print('count,  AAE, SS torque, startup dif, num out', file=self.logFile)
            soft_low,soft_high,soft_number = criterea.startup_soft()
            out=0
            while count <5:
                
                AAEold = AAEsample
                self.special(ankle_commands.s_motor_voltage(volt,0))
                ti = self.moveTime/2 +random.random() * (self.moveTime/2)
                time.sleep(ti)
                self.special(ankle_commands.s_motor_stop())
                time.sleep(self.relaxTime/2)
                self.special(ankle_commands.s_motor_current_amp(0, 0))
                time.sleep(self.relaxTime)
                torque = self.specialFloat(ankle_commands.sf_torque_get())
                startup = self.special(ankle_commands.s_startup_run())
                encs = self.special(ankle_commands.s_get_encoders())
                AAEsample=encs.get(ankle_commands.ENC_ANKLE_ANGLE())
                torques.append([encs.get(ankle_commands.ENC_ANKLE_ANGLE()) ,torque.get(ankle_commands.TORQUE_SERIES_TORQUE_NM()), \
                                encs.get(ankle_commands.ENC_INCREMENTAL())])
                t.append(torque.get(ankle_commands.TORQUE_SERIES_TORQUE_NM()))
#                 print(encs.get(ankle_commands.ENC_ANKLE_ANGLE()) > int(self.calbFile.get('calb_table_shared','encoder.aae_sensor_at_true_north'))+10)
                if encs.get(ankle_commands.ENC_ANKLE_ANGLE()) > int(self.calbFile.get('calb_table_shared','encoder.aae_sensor_at_true_north'))+10:
                    #maby add criterea for the other end
                    if startup != [] and startup != None:
                        startups.append([startup.get(ankle_commands.startup_AAE()),startup.get(ankle_commands.startup_ABS()),\
                                            startup.get(ankle_commands.startup_dif())/1000.0,encs.get(ankle_commands.ENC_INCREMENTAL()),volt])
                
                    t2.append(torque.get(ankle_commands.TORQUE_SERIES_TORQUE_NM()))
                    AAE.append(torque.get(ankle_commands.TORQUE_AAE_FLOAT_RADIANS()))
                    if startup != [] and startup != None:
                        startupsDif.append(startup.get(ankle_commands.startup_dif())/1000.0)
                        if soft_low > startup.get(ankle_commands.startup_dif())/1000.0:
                            print('out low',file=self.logFile)
                            out+=1
                        if startup.get(ankle_commands.startup_dif())/1000.0 > soft_high:
                            print('out high',file=self.logFile)
                            out+=1
                print( '%5d, %4d, %9f, %11f, %7d'%(count,encs.get(ankle_commands.ENC_ANKLE_ANGLE()) ,torque.get(ankle_commands.TORQUE_SERIES_TORQUE_NM())\
                                             ,startup.get(ankle_commands.startup_dif())/1000.0, out),file=self.logFile)
                if abs(AAEsample-AAEold)<2: count +=1
                else: count =0
            print('',file=self.logFile)
            print( 'reverse',file=self.logFile)
            volt = -volt
        
        rangeOfMotoin = max(AAE)-min(AAE)
        maxT = max(t2)
        minT = min(t2)
        #should there be more than one failure?
        maxDif = max(startupsDif)
        minDif = min(startupsDif)
        
        print(rangeOfMotoin,file=self.logFile)
        print(rangeOfMotoin < criterea.startupAAE(),file = self.logFile)
        print(maxT > criterea.maxTorque(),file = self.logFile)
        print(minT < criterea.minTorque(),file = self.logFile)
        
        if out > soft_number:
            self.calbFile.set('mfg_table', 'result', 'Fail')
            self.calbFile.set('mfg_table', 'error', 'startup model soft')
        if rangeOfMotoin < criterea.startupAAE():
            self.calbFile.set('mfg_table', 'result', 'Fail')
            self.calbFile.set('mfg_table', 'error', 'motion')
            toRet=False
        if maxT > criterea.maxTorque():
            self.calbFile.set('mfg_table', 'result', 'Fail')
            self.calbFile.set('mfg_table', 'error', 'torque high')
            toRet=False
        if minT < criterea.minTorque():
            self.calbFile.set('mfg_table', 'result', 'Fail')
            self.calbFile.set('mfg_table', 'error', 'torque negative')
            toRet = False
        
        if maxDif > criterea.startup()[1]:
            self.calbFile.set('mfg_table', 'result', 'Fail')
            self.calbFile.set('mfg_table', 'error', 'startup model')
            toRet = False
        if minDif < criterea.startup()[0]:
            self.calbFile.set('mfg_table', 'result', 'Fail')
            self.calbFile.set('mfg_table', 'error', 'startup model')
            toRet = False
        
        self.calbFile.set('mfg_table', 'zero_torque_check', toRet)
        
        #AAE,ABS,dif,INCREMENTAL,volt
        dataSets = [];
        AAE = []
        DIF = []
        AAEP = []
        DIFP = []
        AAEN = []
        DIFN = []
        for elm in startups:
            AAE.append(elm[0])
            DIF.append(elm[2])
            if elm[4] >0:
                AAEP.append(elm[0])
                DIFP.append(elm[2])
            else:
                AAEN.append(elm[0])
                DIFN.append(elm[2])
        dataSets.append([AAE,DIF, 'Data', 'blue'])
        
        grapher_matplotlib.graph(dataSets,'startup error vs. AAE position','AAE','startup error rotations', os.path.join(self.calDir ,'startup.png'))
        dataSets = []
        dataSets.append([AAEP,DIFP,'positive','blue'])
        dataSets.append([AAEN,DIFN,'negative','green'])
        grapher_matplotlib.graph(dataSets,'startup error vs. AAE position','AAE','startup error rotations', os.path.join(self.calDir ,'startup_2.png'))
        
        #AAE,TORQUE_SERIES,INCREMENTAL
        index = []
        AAE = []
        ZTorque = []
        i=0
        for elm in torques:
            index.append(i)
            i +=1
            AAE.append(elm[0])
            ZTorque.append(elm[1])
            
        dataSets = []
        dataSets.append([AAE,ZTorque, 'torque','blue'])
        grapher_matplotlib.graph(dataSets,'torque vs. AAE position','AAE','estimated torque', os.path.join(self.calDir,'torque.png'))
        dataSets = []
        dataSets.append([index,AAE,'AAE','blue'])
        grapher_matplotlib.graph(dataSets,'AAE position vs time','sample number','AAE',os.path.join(self.calDir,'torque_index.png'))
        return toRet
        
    
    def moveToEnd(self, frunt = True):
        if not frunt:
            volt = CALB_MOTOR_MOVE_VOLTAGE
        else:
            volt = -1*CALB_MOTOR_MOVE_VOLTAGE
        print('move to end',file=self.logFile)
        AAEencoder = self.special(ankle_commands.s_get_encoders()).get(ankle_commands.ENC_ANKLE_ANGLE())
        AAEold = AAEencoder
        self.special(ankle_commands.s_motor_voltage(volt,0))
        count = 0
        loop = 0
        while count <10:
            loop +=1
            AAEold = AAEencoder
            AAEencoder = self.special(ankle_commands.s_get_encoders()).get(ankle_commands.ENC_ANKLE_ANGLE())
            if abs(AAEencoder - AAEold)<3:
                count +=1
            else: count =0
            time.sleep(0.1)
            print( '\r%d    %d    %d    %d    %d'%(loop, count,(abs(AAEencoder - AAEold)),AAEencoder,AAEold),end='',file=self.logFile)
        print('',file=self.logFile)
        print('moved to end',file=self.logFile)
        self.special(ankle_commands.s_motor_stop())
        return True
        
    def trueNorthFind(self):
        '''
        find the true north position of the ankle
        '''
        if not self.phased: self.motorPhase()
        toRet = True
        print('true North Find',file=self.logFile)
        
        self.moveToEnd(True)
        #####################################
        #this finds the aproxemet location of the end of travel
        #due to friction the actuator does not fully relax meaning that there is still a load on the spring
        ###################################
        loop =0
        AAE = []
        inc = []
        mile = []
        while loop <2:
#             print('\rdata cycle %d '%loop,end='',file=self.logFile)
            loop +=1
            self.special(ankle_commands.s_motor_voltage(-1*CALB_MOTOR_MOVE_VOLTAGE,0))
            time.sleep(self.moveTime)
            self.special(ankle_commands.s_motor_current_amp(0, 0))#.s_motor_open_leads())#(ankle_commands.s_motor_voltage(0,0))
            time.sleep(self.relaxTime*3)
            encoders = self.special(ankle_commands.s_get_encoders())
            time.sleep(0.01)
            AAE.append(encoders.get(ankle_commands.ENC_ANKLE_ANGLE()))
            mile.append(encoders.get(ankle_commands.ENC_ABSOLUTE()))
            inc.append(encoders.get(ankle_commands.ENC_INCREMENTAL()))
            print('%d, %d, %d, %d'%(loop, encoders.get(ankle_commands.ENC_ANKLE_ANGLE()),encoders.get(ankle_commands.ENC_INCREMENTAL()),\
                                    encoders.get(ankle_commands.ENC_ABSOLUTE())),file=self.logFile)
        
        print('',file=self.logFile)
        aaeHSPre = sum(AAE)/len(AAE)
        incHSPre = sum(inc)/len(inc)
        
        
        self.calbFile.set('mfg_table', 'encoder.aae_sensor_at_true_north_pre',str(aaeHSPre))
        self.calbFile.set('mfg_table', 'encoder.inc_forward_hardstop_pre',str(incHSPre))
        
        
        ####################################################
        #this runs the ankle into the hardstop while taking data then finds the hardstop using the estemite preveously created. 
        loop =0
        data = []
        AAE = []
        inc = []
        mile = []
        while loop <10:
            print('\rdata cycle %d '%loop,end='',file=self.logFile)
            #move away from hardstop
            self.special(ankle_commands.s_motor_voltage(CALB_MOTOR_MOVE_VOLTAGE,0))
            t = time.time()
            while time.time() <= t+0.5:
                da = self.special(ankle_commands.s_get_encoders())
                data.append(da)
                self.logFile.logInfo(da)
#                 time.sleep(0.01)
            #move into hardstop
            self.special(ankle_commands.s_motor_voltage(-CALB_MOTOR_MOVE_VOLTAGE,0))
            t = time.time()
            while time.time() <= t+2:
                da = self.special(ankle_commands.s_get_encoders())
                data.append(da)
                self.logFile.logInfo(da)
#                 time.sleep(0.01)
            
            #relax motor
            self.special(ankle_commands.s_motor_current_amp(0, 0))#s_motor_open_leads())#(ankle_commands.s_motor_voltage(0,0))
            while time.time() <= t+0.5:
                da = self.special(ankle_commands.s_get_encoders())
                data.append(da)
                self.logFile.logInfo(da)
#                 time.sleep(0.01)
            loop +=1
        for d in data:
            AAE.append(d.get(ankle_commands.ENC_ANKLE_ANGLE()))
            mile.append(d.get(ankle_commands.ENC_ABSOLUTE()))
            inc.append(d.get(ankle_commands.ENC_INCREMENTAL()))
        
        if min(AAE)==max(AAE):raise unknownError("AAE giving constant value")
        if min(mile)==max(mile):raise unknownError("motor absolute giving constant value")
        if min(inc)==max(inc):raise unknownError("motor incremental giving constant value")
        
        AAE1=[]
        inc1=[]
        AAE2=[]
        inc2=[]
        
        AAET = AAE[0]
        INCavT = []
        AAEav = []
        INCAV = []
        for i in range(0,len(AAE)):
            if AAE[i] == AAET:
                INCavT.append(inc[i])
            else:
                AAEav.append(AAET)
                INCAV.append(sum(INCavT)/len(INCavT))
                AAET = AAE[i]
                INCavT = [inc[i]]
        
        g = zip(AAEav,INCAV)
        sorted(g,key=lambda g:g[0])
#         print('*'*100)
#         print(g)
#         print(AAE)
        ae = []
        nc = []
        for element in g:
            ae.append(element[0])
            nc.append(element[1])
        AAE = ae#g[0]#AAEav
        inc = nc#g[1]#INCAV
        
#         deriv = []
#         AAED = []
        AAET = None
        up = incHSPre +2000
        low =incHSPre -2000
        
#         print('inc %d, %d'%(up,low))
        for i in range(0,len(AAE)):
            if AAE[i]> aaeHSPre + 10:
                AAE1.append(AAE[i])
                inc1.append(inc[i])
            elif inc[i]<up and inc[i] >low:
                AAE2.append(AAE[i])
                inc2.append(inc[i])
        
        slope1, inter1, _, _, _ = stats.linregress(AAE1,inc1)
        
##########################################################
        #find true north using distance from line
#         print('#'*100,file=self.logFile)
        
        #make linear equation for free movment data
        freeMove = numpy.poly1d([slope1,inter1])
        #find the min and max ankle angle
        maxAAE = max(AAE)
        minAAE = min(AAE)
        #starting from the max ankle angle find the average and comair to equation
        AAEDif = []
        dDif = []
        AAETNvals = []
        for c in range(maxAAE,minAAE,-1):
            data = []
            indices = [i for i, x in enumerate(AAE) if x == c]
            if len(indices)>=1:
                for i in indices: data.append(inc[i])
                avVal = sum(data)/float(len(data))
                linVal = freeMove(c)
                dif = linVal - avVal  
                AAEDif.append(c)
                dDif.append(dif)
                if dif > 350: #this is to use data that is away from the hardstop guess so that we know that the data is in the free movement of the UUT
                    AAETNvals.append(c)
                else:
                    AAETNvals = []
                
#                 print('%d, %f, %f, %f'%(c,dif,avVal,linVal),file=self.logFile)
        dDif2=[]
        for a in range(0,len( dDif)):
            if AAEDif[a]> aaeHSPre + 10:# and AAEDif[a] <aaeHSPre +310:
                dDif2.append(dDif[a])
        av = numpy.average(dDif2)
        std = numpy.std(dDif2)
        maxDif = max(dDif2)
        AAETN = max(AAETNvals)
        AAETNVals2 = []
        AAETNVals3 = []
        dDif2 = []
        mult = 6.0
#         print('#'*100)
#         print('av: %d'%av)
#         print('std: %d'%std)
# #         print('av+std: %d'%(av+std*3))
#         print('freeMaxDif: %d'%maxDif)
#         print('freeMaxDif + %dstd: %d'%(mult,maxDif + mult*std))#*3.0/4.0))
# #         print('mindif: %d'%minDif)
#         print('#'*100)
        for c in range(0,len(AAEDif)):
            if dDif[c]>(maxDif + mult*std):#*3.0/4.0):
                dDif2.append(dDif[c])
                AAETNVals2.append(AAEDif[c])
            else:
                AAETNVals3.append(AAEDif[c])
#         AAETN2 = max(AAETNVals2)
        AAETN2 = min(AAETNVals3)#-1
        
        
        dataSets = [];
        
        dataSets.append([AAE,inc, 'All Data', 'green'])
        dataSets.append([AAE,freeMove(AAE), 'Free Movement', 'green','line'])
        dataSets.append([AAEDif,dDif,'dif < freeMaxDif + %0.1fstd'%mult,'orange'])
        dataSets.append([AAETNVals2,dDif2,'dif > freeMaxDif + %0.1fstd'%mult,'blue'])
#         dataSets.append([AAEDif2,dDif2,'dif from linear','yellow'])
#         dataSets.append([[AAETN],[freeMove(AAETN)],'true north','red'])
        dataSets.append([[AAETN2],[freeMove(AAETN2)],'true north ','red'])
        dataSets.append([[aaeHSPre],[incHSPre], 'True North First Apprx.', 'yellow'])
        grapher_matplotlib.graph(dataSets,'Hardstop Estimation','Ankle Encoder Counts','Incremental Encoder', os.path.join(self.calDir ,'hs-1.png'),4)
        
        self.calbFile.set('calb_table_shared', 'encoder.aae_sensor_at_true_north',str(int(AAETN2)))
        self.calbFile.set('calb_table_shared', 'encoder.inc_forward_hardstop',str(int(freeMove(AAETN2))))
        
        self.calbFile.set('mfg_table', 'encoder.aae_sensor_at_true_north',str(int(AAETN)))
        self.calbFile.set('mfg_table', 'encoder.inc_forward_hardstop',str(int(freeMove(AAETN))))
        
        AAER =int(self.calbFile.get('calb_table_shared', 'encoder.aae_retraction_hardstop'))\
                    -int(self.calbFile.get('calb_table_shared', 'encoder.aae_sensor_at_true_north'))
        INCR = int(self.calbFile.get('calb_table_shared', 'encoder.inc_retraction_hardstop'))\
                    -int(self.calbFile.get('calb_table_shared', 'encoder.inc_forward_hardstop'))
        print('AAER: %d'%AAER,file=self.logFile)
        print('INCR: %d'%INCR,file=self.logFile)
        if AAER < criterea.AAERange()[0]:
            toRet = False
            self.addError('AAE range too low')
        if AAER > criterea.AAERange()[1]:
            toRet = False
            self.addError('AAE range too high')
        if INCR < criterea.INCRange()[0]:
            toRet = False
            self.addError('INC range too low')
        if INCR > criterea.INCRange()[1]:
            toRet = False
            self.addError('INC range too high')
            
        self.calbFile.set('mfg_table', 'true_north', toRet)
        print('true North Find end',file=self.logFile)
        return toRet
    
    
    def hardstopFind(self):
        '''
        find ballscrew hardstop location
        '''
        if not self.phased: self.motorPhase()
        
        print('Hardstop Find',file=self.logFile)
        toRet = self.moveToEnd(False)
        
        AAE = []
        mile = []
        inc = []
        loop = 0
        print('cycle, AAE, mile, incremental',file=self.logFile)
        while loop <10:
            
            loop +=1
            self.special(ankle_commands.s_motor_voltage(CALB_MOTOR_MOVE_VOLTAGE,0))
            time.sleep(self.moveTime)
            self.special(ankle_commands.s_motor_stop())
            time.sleep(self.relaxTime*2)
            encoders  = self.special(ankle_commands.s_get_encoders())
            AAE.append(encoders.get(ankle_commands.ENC_ANKLE_ANGLE()))
            mile.append(encoders.get(ankle_commands.ENC_ABSOLUTE()))
            inc.append(encoders.get(ankle_commands.ENC_INCREMENTAL()))
            
            print('\r%d, %d, %d, %d'%(loop, encoders.get(ankle_commands.ENC_ANKLE_ANGLE()),encoders.get(ankle_commands.ENC_INCREMENTAL()),\
                                    encoders.get(ankle_commands.ENC_ABSOLUTE())),end='',file=self.logFile)
        print(file = self.logFile)
        aaeHS = sum(AAE)/loop
        incHS = sum(inc)/loop
        self.calbFile.set('calb_table_shared', 'encoder.aae_retraction_hardstop',str(aaeHS))
        self.calbFile.set('calb_table_shared', 'encoder.inc_retraction_hardstop',str(incHS))
        self.calbFile.set('calb_table_shared','ff_k3.hardlimit',str(int(incHS)))
        self.calbFile.set('calb_table_shared','ff_k3.activation',str(int(incHS-FORCEFIELD_OFFSET_INCREMENTAL_ENC_COUNTS)))
        
        self.calbFile.set('mfg_table', 'hard_stop', toRet)
        
        return toRet
    
    def moveAFrame(self):
        '''move aframe to test position and restart ankle.  '''
        tmpSeFn = 'stderr_log.txt'
        tmpSoFn = 'stdout_log.txt'
        outPut = os.path.join(self.calDir,time.strftime('moveLog_%y%m%d_%H%M%S.txt'))
        dataGood = False
        loop = 0
        while(not dataGood and loop < self.retryes):
            loop +=1
            '''
            #subprocees is used because of a fault in the coply control software that quits pyhton on failure 
            '''
            doSubprocess2('python2.7 -u ./move_ankle.py -b %s -f %s'%(self.baudrate, outPut), self.logFile, tmpSoFn, tmpSeFn)
            if os.path.exists(outPut):
                f = open(outPut)
                for line in f:
                    if 'moved correctly' in line:
                        dataGood = True 
        return dataGood 
    
    def moveAFrame2(self):
        '''move aframe to test position and restart ankle.  '''
        tmpSeFn = 'stderr_log.txt'
        tmpSoFn = 'stdout_log.txt'
        outPut = os.path.join(self.calDir,time.strftime('moveLog_%y%m%d_%H%M%S.txt'))
        dataGood = False
        loop = 0
        while(not dataGood and loop < self.retryes):
            loop +=1
            '''
            #subprocees is used because of a fault in the coply control software that quits pyhton on failure 
            '''
            doSubprocess2('python2.7 -u ./move_ankle2.py -b %s -f %s'%(self.baudrate, outPut), self.logFile, tmpSoFn, tmpSeFn)
            if os.path.exists(outPut):
                f = open(outPut)
                for line in f:
                    if 'moved correctly' in line:
                        dataGood = True 
        return dataGood
    
    def findk2(self):
        """"use system call to collect data then check for the existance of the data"""
        tmpSeFn = 'stderr_log.txt'
        tmpSoFn = 'stdout_log.txt'
        ssFile = os.path.join(self.calDir,time.strftime('sspring_log_%y%m%d_%H%M%S.txt'))
        loop = 0
        dataGood = False
        while (not dataGood and loop < self.retryes):
            loop += 1
            '''
            #subprocees is used because of a fault in the coply control software that quits pyhton on failure 
            '''
            doSubprocess2('python2.7 -u ./collect_ss_data.py -b %s -f %s'%(self.baudrate, ssFile), self.logFile, tmpSoFn, tmpSeFn)
            
            #check data if data is not good delete file
            if os.path.exists(ssFile):
                f = open(ssFile)
                for line in f:
                    if 'data collection complete' in line:
                        dataGood = True 
        #run analysis on data
        self.analysis.importData(ssFile,self.calDir)
        toRet = self.analysis.calk2()
        self.calbFile.set('mfg_table', 'spring_k2', toRet)
        return dataGood and toRet
        
    def findk3(self):
        """"use system call to collect data then check for the existance of the data"""
        tmpSeFn = 'stderr_log.txt'
        tmpSoFn = 'stdout_log.txt'
        hsFile = os.path.join(self.calDir ,time.strftime('hstop_log_%y%m%d_%H%M%S.txt'))
        loop = 0
        dataGood = False
        while (not dataGood and loop < self.retryes):
            loop += 1
            '''
            #subprocees is used because of a fault in the coply control software that quits pyhton on failure 
            '''
            doSubprocess2('python2.7 -u ./collect_hs_data.py -b %s -f %s'%(self.baudrate, hsFile), self.logFile, tmpSoFn, tmpSeFn)
            #check data if data is not good delete file 
            if os.path.exists(hsFile):
                f = open(hsFile)
                for line in f:
                    if 'data collection complete' in line:
                        dataGood = True 
        #run analysis on data
        self.analysis.importData(hsFile,self.calDir)
        toRet = self.analysis.calk3()
        self.calbFile.set('calb_table_shared','springs.k3_Nm_p_rad',self.analysis.k3_val)
        self.calbFile.set('mfg_table', 'spring_k3', toRet)
        return dataGood and toRet
    
    def checkBatteries(self):
        """check the state of charge of the coin cell batteries"""
        toRet = True
        volts = self.specialFloat(ankle_commands.sf_get_voltages())
        clockV = volts.get(ankle_commands.ClockCoinCell())
        shortedLeadsC = volts.get(ankle_commands.Shorted_Leads_Coin_Cell())
        if clockV < criterea.clockCoineCellMin():
            print( "clock coin cell was %f but needed to be above %f"%(clockV,criterea.clockCoineCellMin()),file =self.logFile)
            toRet = False
        if shortedLeadsC < criterea.shortedLeadsCoinCellMin():
            print( "shorted leads coin cell was %f but needed to be above %f"%(shortedLeadsC,criterea.shortedLeadsCoinCellMin()),file =self.logFile)
            toRet = False
        return toRet
    
if __name__ == '__main__':
    opt_parser = calb_test_options(sys.argv);
    opt = opt_parser.opt;

    
    ankleCon = pfcmd.PFCmd(0,0)
    
    test = BiOMCalbScripts( ankleCon, None, opt.template_ini_file, None)
    ankleCon.connect(opt.port)
    
    test.zeroTorqueCheck()
    #set runlevel
    #restart
    
