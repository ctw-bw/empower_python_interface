#!/usr/bin/python

import wx, os
import logging
import wx.lib.plot as plot

class PlotCanvasExample(plot.PlotCanvas):
    def __init__(self, parent, id, desiredSize):
        ''' Initialization routine for the this panel.'''
        plot.PlotCanvas.__init__(self, parent, id, style=wx.BORDER_NONE, size=desiredSize)
        self.data = [(1,2), (2,3), (3,5), (4,6), (5,8), (6,8), (10,10)]
        line = plot.PolyLine(self.data, legend='', colour='pink', width=2)
        self.gc = plot.PlotGraphics([], 'Line Graph', 'X Axis', 'Y Axis')
        
    

class MyFrame(wx.Frame):
    """"""

    def __init__(self, appToExitWhenDone):
        """Constructor"""
        wx.Frame.__init__(self, parent=None, title="Add / Remove Buttons")
        self.Bind(wx.EVT_WINDOW_CREATE, self.saveFigureOnceCreated)
        self.Show()
    
        self.panel = PlotCanvasExample(self, 0, wx.Size(300,250))
        self.panel.Draw(self.panel.gc, xAxis=(0,15), yAxis=(0,15))
        self.appToExitWhenDone = appToExitWhenDone
    
    def saveFigureOnceCreated(self, *evt):
        self.panel.SaveFile("testplot.jpg")
        self.appToExitWhenDone.Exit()
 
#----------------------------------------------------------------------
if __name__ == "__main__":
    app = wx.App(False)
    frame = MyFrame(app)

    app.MainLoop()
    
