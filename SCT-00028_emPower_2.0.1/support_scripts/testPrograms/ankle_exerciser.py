"""
Exercises an ALREADY CALIBRATED ankle back and forth in multiple modes and records wifi_fast.py results. 
"""

import sys, optparse, struct
import python_paths_default  # test for user's python_paths.py and update paths
import python_paths as pyenv
import comm_lib.pfcmd as pfcmd
import ankle_commands 
import time

quiet = False
def output(things):
    if not quiet:
        print things

class CONTROL_MODES:
    CURRENT = 1
    #VOLTAGE = 2
    
class ENDPOINT_MODES:    
    ANKLE_ANGLE = 1
    BALLNUT_POSITION = 2
    
class exerciser_options :
    def __init__(self, args) :
        self.args = args

        usage = "usage: %prog [options]"
        parser = optparse.OptionParser();
        parser.add_option("-p", "--command_port", dest="command_port", type="int", default="9", help="Command port connected to BiOM");
        parser.add_option("--start", dest="start", type="int", default="30", help="Starting encoder value for BiOM exerciser");
        parser.add_option("--end", dest="end", type="int", default="400", help="Ending encoder value for BiOM exerciser");
        parser.add_option("--encoder_choice", dest="encoder_choice", type="int", default=ENDPOINT_MODES.ANKLE_ANGLE, help="Control back and forth using Ankle encoder value (1) or Ball Nut Position (2). Defaults to Ankle Encoder.");
        parser.add_option("--control_type", dest="control_mode", type="int", default=CONTROL_MODES.CURRENT, help="Control back and forth with Current (1) or Voltage (2). Defaults to CURRENT.");
        
        parser.add_option("-n", "--number_of_cycles", dest="cycles", type="int", default="10", help="Number of cycles to run. Defaults to 10.");
        
        
        # parser.disable_interspersed_args()
        (options, args) = parser.parse_args(args);
        self.options = options;
        self.args = args;
        
        # make a struct from the options dictionary
        class empty:
            pass
        opt = empty()
        for k,v in options.__dict__.iteritems() :
            opt.__dict__[k] = v
            self.opt = opt;
        return

   
def prep_for_test(ankle_comm):
        output (ankleCon.do_command(ankle_commands.s_command_type(), ankle_commands.s_set_system_idle()))
        time.sleep(0.5)
        
        #command the motor to phase
        phase1 = ankleCon.do_command(ankle_commands.s_command_type(), ankle_commands.s_motor_phase(2000))
        time.sleep(0.5)
        phase2 = ankleCon.do_command(ankle_commands.s_command_type(),ankle_commands.s_motor_phase(5000))
        time.sleep(0.1)
        
        #set motor to not moving
        output ( ankleCon.do_command(ankle_commands.s_command_type(),ankle_commands.s_motor_stop()))
        
"""
case SPECCMD_PERIPH_MOTOR_ENCODER_READ:     // 5002
            /*!  @sp_cmd_send{ @copybrief SPECCMD_PERIPH_MOTOR_ENCODER_READ, 5002,,,,,,,,,,}
                 @sp_cmd_rsp{5002, QEI initialized flag, motor position encoder counts, QEI counter register, QEI input A, QEI input B, QEI index input, QEI enable, index pulse 'edge' count, MILE encoder,ball-screw position in encoder counts}
              */
            motor_EncoderDebugRead(data->param);
            break;

        case SPECCMD_PERIPH_AAE_READ:               // 5003
            /*!  @sp_cmd_send{ @copybrief SPECCMD_PERIPH_AAE_READ, 5003,,,,,,,,,,}
                 @sp_cmd_rsp{5003, AAE initialized flag, raw AAE in counts, AAE counts, ankle angle milli-degrees, maximum invalid count, present AAE error state,,,,}
"""

def back_and_forth(ankle_comm, control_mode, endpoint_mode, startpoint, endpoint,  input_volts=2.0, cycles = 1):
    cmd = [5003]
    key = 'v2'
    if (endpoint_mode is ENDPOINT_MODES.BALLNUT_POSITION):
        cmd = [5002]
        key = 'v9'
        
    if control_mode is CONTROL_MODES.CURRENT:
        pos  =  ankleCon.do_command(ankle_commands.s_command_type(), cmd).get(key)
        posLast = pos
        sign = 1;
        
        posStuck = 0;
        output(ankleCon.do_command(ankle_commands.s_command_type(),ankle_commands.s_motor_current_amp(1.0)))
        mini_cycles = 5
        
        for input_amps in range(-50, 0, 1):
          n_switch = 1;
          while(n_switch < 2*mini_cycles):
            try: 
                pos = ankleCon.do_command(ankle_commands.s_command_type(), cmd).get(key)
                if pos == posLast:
                    posStuck += 1
                else: 
                    posStuck = 0;
                
                #if (pos < between1 or pos > between2)
                if (posStuck > 5): #stuck or out of range. 
                    n_switch +=1 
                    sign = sign * -1
                    output(ankleCon.do_command(ankle_commands.s_command_type(),ankle_commands.s_motor_current_amp(sign*input_amps/10.0)))
                
                posLast = pos
                time.sleep(.05)
            except Exception as e:
                print e
                #set motor to not moving
                output(ankleCon.do_command(ankle_commands.s_command_type(),ankle_commands.s_motor_stop()))
                break;
        
        
        #set motor to not moving
        output(ankleCon.do_command(ankle_commands.s_command_type(),ankle_commands.s_motor_stop()))
        

if __name__ == '__main__':
    DATAPORT_SERIAL_BAUD_RATE = 1250000
    MPD_COMMAND_BAUD_RATE = 115200
    opt = exerciser_options(sys.argv).opt;
    
    ankleCon = pfcmd.PFCmd(0,0)
    ankleCon.connect(opt.command_port)
    prep_for_test(ankleCon);
    back_and_forth(ankleCon, opt.control_mode, opt.encoder_choice, opt.start, opt.end, cycles = opt.cycles);