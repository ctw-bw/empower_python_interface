'''
Created on Jul 21, 2014

@author: ekurz

this will contain all commands used 
'''

import time

def s_command_type(): return 'special'

def s_test_command(): return [9000]

'''see notes document for description of run levels'''
def s_runlevel_set_virgin (): return  [9001, 0]
def s_runlevel_set_motor (): return  [9001, 10]
def s_runlevel_set_ankle (): return  [9001, 20]
def s_runlevel_set_walk (): return  [9001, 70]
def s_runlevel_get (): return  [9002]
def s_set_system_idle (): return  [9003]


''' current calibration commands '''
def s_motor_current_offsets_start_calb(): return  [9004]
def s_motor_current_offsets_read_calb (): return  [9005]#
#def s_motor_current_offsets_use_calb (): return  [9006]

'''Read sensors'''
def s_get_encoders (): return  [9009]
def ENC_INCREMENTAL (): return 'v0'
def ENC_ABSOLUTE  (): return 'v1'
def ENC_ANKLE_ANGLE (): return 'v2'



'''startup'''
def s_startup_run():return [9010]
def startup_AAE (): return'v0'
def startup_ABS (): return'v1'
def startup_INC (): return'v2'
def startup_dif (): return'v3'

'''motor commands'''
def s_motor_stop (): return  [9100]
def s_motor_phase(qvd):return [9102, qvd]
def s_motor_voltage(qVq, qVd): return [9106, -1*qVq, qVd]
def s_motor_current_amp(iQ_amps, iD_amps):return [9103, -1*iQ_amps*1000, iD_amps*1000]#
def s_motor_current_milliamp(iQ_milliamps, iD_milliamps):return [9103, -1*iQ_milliamps, iD_milliamps]#
def s_motor_open_leads (): return  [9110]
def s_motor_PID_start (unit,target): 
    '''
    unit = 1 for radians, 2 for degrees
    target = angle in milli-radians for milli-degrees
    0 is at true north, positive is into hardstop 
    '''
    return [8050, unit, target]
def s_motor_PID_stop(): return [8052]

'''torque commands'''
def sf_torque_get(): return  [9200]
def TORQUE_AAE_FLOAT_RADIANS (): return  'v0'
TORQUE_AAE_FLOAT_RADIANS_IND = 1
def TORQUE_HARDSTOP_TORQUE_RATE_NMPERSEC (): return  'v1'
def TORQUE_HARDSTOP_TORQUE_NM (): return  'v2'
def TORQUE_SERIES_TORQUE_RATE_NMPERSEC (): return  'v4'
def TORQUE_SERIES_TORQUE_NM (): return  'v3'
def TORQUE_TOTAL_ANKLE_TORQUE_NM (): return  'v5'
def TORQUE_SERIES_DEFLECTION_RAD (): return 'v6'


'''memory commands'''
#TODO: These are really part of robo config.
def s_mem_flash_to_ram (): return  [7010]
def s_mem_ram_to_flash(tableID): return [7011, tableID, 1]
def s_mem_ram_to_defaults(tableID): return [7012, tableID, 1]


'''IMU commands'''
def s_get_IMU (): return [9210]
def IMU_acelX (): return 'v0'
def IMU_acelY (): return 'v1'
def IMU_acelZ (): return 'v2'
def IMU_gyroX (): return 'v3'
def IMU_gyroY (): return 'v4'
def IMU_gyroZ (): return 'v5'

'''float commands'''
def float_get_format (): return  'ffffffffff'
def float_get_labels (): return  ['v0','v1','v2','v3','v4','v5','v6','v7','v8','v9']

'''Time commands'''
def s_set_time(): 
    s = time.gmtime()
    return [9007,s.tm_year-2000, s.tm_mon, s.tm_mday, s.tm_wday, s.tm_hour, s.tm_min, s.tm_sec]
def s_get_time(): return [9008]
def time_year():return 'v0'
def time_month():return 'v1'
def time_day():return 'v2'
def time_hour():return 'v4'
def time_min():return 'v5'
def time_sec():return 'v6'
def s_timers_get():return [4005]
def timers_upTime():return 'v1'

'''getSensors'''
def sf_get_currents():return [9221]
def current_batt(): return 'v0'
def current_U(): return 'v1'
def current_V(): return 'v2'

def sf_get_voltages():return [9220]
def Supply_Input_Voltage(): return 'v0' 
def Supply9v(): return'v1'
def Supply5v(): return'v2'
def Supply6_1(): return'v3'
def ClockCoinCell(): return'v4'
def Shorted_Leads_Coin_Cell(): return'v5'

def sf_get_temp():return[9222]
def Bridge_Temperature():return 'v0'
def Motor_Case_Temperature():return'v1'
#def Microprocessor_Temperature():return'v2' removed in ankle FW 2.1.0b3

'''microprocessor reset'''
def s_micro_reset():return [4551,0,77]

'''write protection'''
#def s_micro_write_protection_table_on():return[5205,1]# this locks walking tables
def s_micro_write_protection_cal_on():return[5205,0]
def s_micro_write_protection_off():return[5205,-1]

'''clear table'''
def s_clear_k3_table():return[5206, 0xFD2000]

'''log commands'''
def s_logs_check():return[7650, 50]
def num_logs():return 'v1'
def s_logs_setup():return[7610]
