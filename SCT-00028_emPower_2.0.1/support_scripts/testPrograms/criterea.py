'''
Created on Dec 7, 2015

@author: ekurz
'''

# class torqueCheck():
'''criterea for torque check'''
def minTorque(): return -4 
def maxTorque(): return 4 

def startup(): return[-0.15,0.15] #min and max for the offset in rotations at startup between expected and actual postion.
def startup_soft():return[-0.1,0.1,3] #soft min and max with the number that can be over the line.  
def startupAAE():return 0.35#min range for torque test

'''FW'''
def FW():return ['2.0.3 c:71C0C934\x00\x00\x00\x00','Oct 19 16 @ 16:25:19','16813'] #expected firmware rev

'''motor offsets'''
def phaseUcounts(): return [-110,110]#min,max for phase_u_counts in current_calb
def phaseVcounts(): return [-110,110]#min,max for phase_v_counts in current_calb
def batteryCountsADC(): return [-279,279]#min,max for battery_counts_adc in current_calb

def phaseUamp():return [-0.025, 0.025] #min,max for phaseU after calibration with no motor current (A)
def phaseVamp():return [-0.025, 0.025] #min,max for phaseU after calibration with no motor current (A)
def batteryAmp():return [-0.025, 0.025]  #min,max for phaseU after calibration with no motor current (A)

'''range of motion'''
def AAERange():return[400,600] #min,max rage for AAE true north-hardstop
def INCRange():return[70000,100000]#currently setup for both 66 and 72 #min,max rage for INC true north-hardstop

'''springs'''
def k2PullRange():return [300,900] #min,max value for k2pull
def k2PushRange():return [350,950] #min,max value for k2push
def k2CycleDif():return 200 #needs to be lower #max diff between cycles  
def k2pushForce():return -20.0 #30 for final? #required force to calculate the push value
def k2pullforce():return 70.0 #90 for final? #required force to calculate the pull value
def k2cycles():return 8 #minimum number of good data cycles

def k3Range():return[6500,100000] #min,max value for k3
def k3CycleDif():return 600 #max diff between cycles
def k3Force():return 80.0 #95 for final? #force required to perform calculation 
def k3cycles():return 8 # minimum number of good data cycles
def k3forceRange():return 40 #used to ensure that there is enough range of data to make a good calculation. 

'''battery voltage'''
def clockCoineCellMin():return 2.4 # minimum voltage for the clock coin cell
def shortedLeadsCoinCellMin():return 2.4 #minimum voltage for the shorted leads coin cell

'''clock'''
def timeDif():return 2 #max time diference alowed when setting clock. clock only responds in whole seconds and there may be time delays. 

'''log critera'''
def logNumFiles():return 4