#!/usr/bin/env python
'''
Created on Aug 26, 2014

@author: ekurz
'''
from __future__ import print_function
import os, sys, thread
import python_paths_default
import python_paths

SVN_SUB_REV_STR  = "$Revision: 16851 $"
SVN_SUB_URL_STR  = "$URL: http://svn.iwalk.com/svn/svn_engineering/PowerFoot/tags/MFG/MFG_v2X/MFG_v2X_BootloadScriptv_v1.0.1b1/support_scripts/testPrograms/calb_springs.py $"
SVN_SUB_DATE_STR = "$Date: 2016-11-18 12:58:29 -0500 (Fri, 18 Nov 2016) $"

import pfcmd
import ConfigParser
import loggHandeler
import ordereddict
import subprocess

if os.name != 'nt':
#     import CopleyWrapper
    import LtfTrigGeomUtil
    import DataBorgLTF

# import read_from_ankle
# import read_ankle

import ankle_commands
import time
import serial_port_helper
# import copley_sig_test
# import class_calibrate_log
# import k2_support

# import grapher_matplotlib

import numpy as np
# import scipy.stats as stats
import calb_tests
# import csv
# import matplotlib.pyplot as plt

from calb_spring_analysis import springAnalysis

TORQUE_OFFSET_FROM_HARDSTOP = 20; #Amount of torque we need to be IN to the hardstop to ensure that we 
#  get a clean slope and the regression isn't affected by k3 engagement contact squishiness. 
LTF_TORQUE_LEVER_ARM_METERS = 0.131096#    0.159040#0.1346# #Torque lever arm on LTF in meters

K2_OPEN_ANKLE_ANGLE_START_POSITION_RAD = 2.0*(2*np.pi)/360 #Calculating K2 needs an ankle that is 2 degrees off its hardstop so we're only measuring spring torque.  

K3_TORQUE_START_DATA_NM = -10
K3_TORQUE_END_DATA_NM = 120

####################################################################################################
#TANGO SPECIFIC HARDWARE PARAMETERS, ported from MATLAB torque model at http://svn.iwalkhq.iwalk.com/svn/svn_engineering/PowerFoot/trunk/ControlSystem_v2x/matlab_simulations/DSPbetaCalculationGenerateTable.m
# Newest (1.11.2016) mechanical design geometry model:
#ANKLE_ANGLE_ENCODER_COUNTS_PER_TURN = 8192 #Tango hardware returns AAE in encoder counts, we need the translation value. 

#                         % 26/1000 comes from the measurement of this
#                         % compression using BiOM 343- it may be more or
#                         % less on other BiOMs. 
TANGO_TQ_MODEL_SPRING_OFFSET = 26/1000.0 * 2.54 /100.0; # The plate/foot structure, when torqued down, affect the 'c' in our model- spring and sorbethane compress the spring from the CAD model # geometry 
TANGO_TQ_MODEL_A= 0.0406100; #meters
TANGO_TQ_MODEL_B= 0.10455; #meters mm
TANGO_TQ_MODEL_C= 0.10092 - TANGO_TQ_MODEL_SPRING_OFFSET;  #meters,  neutral spring position (actually varies +-.5 mm amount depending on whether we're up against hardstop)
TANGO_THETA0 = np.arccos((TANGO_TQ_MODEL_A**2+TANGO_TQ_MODEL_B**2-TANGO_TQ_MODEL_C**2)/(2*TANGO_TQ_MODEL_A*TANGO_TQ_MODEL_B)); #radians
# TANGO_PULLEY_TEETH = 72;


k2_cycles=13
dwell_time=7.0
pull_time_to_move=0.25
pull_max_amps=22.0#18. #3/8 actuator
#pull_max_amps=12. #18/72 actuator 
push_time_to_move=0.25
push_max_amps=-10.0#7.0 #3/8 actuator
#push_max_amps=-4.67  #18/72 actuator
initial_amps=0.0

############### END OF TANGO HARDWARE CONSTANTS ###################################################   


#Python port of same code model that Tango ankle uses to calculate torque. 
# def DSPbetaCalculation(MotorAngleRadsArray):
#   
#     aTimesb2 = TANGO_TQ_MODEL_A * TANGO_TQ_MODEL_B * 2;
#     aSqdPlusbSqd = TANGO_TQ_MODEL_A * TANGO_TQ_MODEL_A + TANGO_TQ_MODEL_B * TANGO_TQ_MODEL_B;
#     K_pulley = 18.0/TANGO_PULLEY_TEETH;         # motor rotation ratio; motor=18 teeth
#     K_screw =  3e-3/2.0/np.pi;  #m/ per rad ;  ball screw travel distance per screw revolution
#     theta_c = TANGO_THETA0; # rad; arccos((constant.a^2+constant.b^2-c0^2)/(2*constant.a*constant.b)); // considering the theta initial position
#     c = np.sqrt(aSqdPlusbSqd - (aTimesb2)*np.cos(theta_c));   # calculate c while theta was not at neutral position
#     deltaC = K_pulley * K_screw * MotorAngleRadsArray;
# 
#     #beta angle if you use a model that assumes spring pivots around ankle angle
#     betaAngleArray = np.arccos((aSqdPlusbSqd - (c+deltaC)*(c+deltaC))/(aTimesb2)) - theta_c;   # radians
#     return betaAngleArray



class inputError(Exception):
    pass


class moveTest(DataBorgLTF.LTFTestFramework):
    '''
    Simple test to move BiOM back and forth and find the endpoints where we should gather data. 
    most of the comm plumbing guts are in ltf/LTFTestFramework.py, this subclass defines the K2 
    test specific copley profile. (holding the AFrame in place right off the hardstop) and the ankle motion while we do so.
    '''

    def holdAnkleOffHardstop(self, goalAnkleAngleRad =  K2_OPEN_ANKLE_ANGLE_START_POSITION_RAD*-1.0):
#         goalAnkleAngleRad = K2_OPEN_ANKLE_ANGLE_START_POSITION_RAD*-1.0 
        ankleRad = (self.biomCommand([ankle_commands.sf_torque_get()], struct_str = ankle_commands.float_get_format())[0])[ankle_commands.TORQUE_AAE_FLOAT_RADIANS_IND][1]
        direction = 0
    
        print("Ankle Position (Rad):" +str(ankleRad) + " Goal: "+str(goalAnkleAngleRad) + " Direction:"+str(direction), end = '')
        while abs(ankleRad - goalAnkleAngleRad) > np.pi*2.0/(2**int(self.cfg.get('AAE_cfg_shared','joint_encoder_bits'))):#ANKLE_ANGLE_ENCODER_COUNTS_PER_TURN: #TODO: see if tolerance of +-2 AAE counts is acceptable. 
            if (ankleRad < goalAnkleAngleRad):
                direction = 1 #Too close to hardstop- open ankle.  
            else: 
                direction = -1
                
            dist = max(5, abs(ankleRad - goalAnkleAngleRad)*2) #Moving Copley about X counts moves ankle about .1*X counts.     
            self.motor.moveRelativeUsingProfile(direction*dist, velocityRPM = 200, blocking = False)
            time.sleep(.2)        
            ankleRad = (self.biomCommand([ankle_commands.sf_torque_get()], struct_str = ankle_commands.float_get_format())[0])[ankle_commands.TORQUE_AAE_FLOAT_RADIANS_IND][1]
            print('\r                                                                               ',end='')
            print("\rAnkle Position (Rad):" +str(ankleRad) + " Goal: "+str(goalAnkleAngleRad) + " Direction:"+str(direction),end='')
        print()
        self.copleyPosAtOpenAnkle = self.motor.readMotorStatusTimestamped()['position']
        self.motor.moveRelativeUsingProfile(0, velocityRPM = 1000, blocking = False) #Add velocity to increase hold of motor to stay in this position.         
    
    def moveToStart(self):
        '''
        move to a position where the ankle will startup without a problem. 
        '''
        maxStartError = 150
        direction = 0
        loop = 0 
        startError =self.biomConnection.do_command(ankle_commands.s_command_type(), ankle_commands.s_startup_run()).get(ankle_commands.startup_dif())
#         startError = (self.biomCommand([ankle_commands.s_startup_run()])[0])[ankle_commands.startup_dif()][1]
        print('startup error: '+str(startError)+' max: '+ str(maxStartError)+ " Direction:"+str(direction), end = '')
        while abs(startError) >maxStartError and loop !=300:
            loop += 1
            if startError <0:
                direction = -1
            else:
                direction = 1
            dist = max(abs(startError/50),5)
            self.motor.moveRelativeUsingProfile(direction*dist, velocityRPM = 85, blocking = False)
            time.sleep(0.2)
            startError = startError =self.biomConnection.do_command(ankle_commands.s_command_type(), ankle_commands.s_startup_run()).get(ankle_commands.startup_dif())
            #(self.biomCommand([ankle_commands.s_startup_run()])[0])[ankle_commands.startup_dif][1]
            print('\r')
            print('startup error: '+str(startError)+' max: '+ str(maxStartError)+ " Direction:"+str(direction), end = '')
        print()
    
    def performTest(self):
        self.holdAnkleOffHardstop(K2_OPEN_ANKLE_ANGLE_START_POSITION_RAD*-0.5)
        self.holdAnkleOffHardstop(K2_OPEN_ANKLE_ANGLE_START_POSITION_RAD*-1.0) 
        self.biomCommand([ankle_commands.s_motor_voltage(0, 0)])
        self.moveToStart()

            
        self.biomCommand([ankle_commands.s_motor_stop()])
        
    def performTest2(self):
        self.holdAnkleOffHardstop(K2_OPEN_ANKLE_ANGLE_START_POSITION_RAD*-0.5)
        self.holdAnkleOffHardstop(K2_OPEN_ANKLE_ANGLE_START_POSITION_RAD*-1.0) 
        self.biomCommand([ankle_commands.s_motor_voltage(0, 0)])
#         self.moveToStart()
# 
#             
#         self.biomCommand([ankle_commands.s_motor_stop()])

    def dataReadThread(self):
        self.stillLogging = True;
        oldData = self.biomTelemetryHandler.peek_last_received()
        while (self.stillLogging):

            d = dict() 
            d["Copley Encoder Pos"] = self.motor.readMotorStatusTimestamped()['position']
                
            d2 = self.loadCell.getLatestDict()
            tqNM = LtfTrigGeomUtil.ConvertLoadCellForceTANGOJointTorque(d2["FzNewtons"], d2["FyNewtons"])
            d["BiOM Joint Torque (Nm) according to LoadCell"] = tqNM
            for k in d2.keys():
                d[k] = d2[k]       

            data = self.biomTelemetryHandler.peek_last_received();
            if data != None:               
                #Just get whatever data is coming out of the vlist
                for k in data.keys():
                    d[k] = data[k] 
                self.biomTelemetryHandler.flush() #Just flush data after we've read latest because we only want the latest sample matched to our other data.                

                d["endReadTimeMilliSec"] =  DataBorgLTF.millis() #Put timestamp in our dictionary
                d["cycleCount"] = self.testCycleCount #K3 cal uses which count we're on. 
                #if oldData != data and (time.time()-self.biomTelemetryHandler.time_last_received)<0.5:
                    #add time check?
                self.logData("ALL_DATA", d) #Only log data when we have a new telemetry packet.
                oldData = data
            time.sleep(0.001)     
        self.stillLogging = False
        if hasattr(self, "logfile"): self.logfile.close()

class calbK2Test(DataBorgLTF.LTFTestFramework):
    '''
    Simple test to move BiOM back and forth and find the endpoints where we should gather data. 
    most of the comm plumbing guts are in ltf/LTFTestFramework.py, this subclass defines the K2 
    test specific copley profile. (holding the AFrame in place right off the hardstop) and the ankle motion while we do so.
    '''

    def holdAnkleOffHardstop(self):
        goalAnkleAngleRad = K2_OPEN_ANKLE_ANGLE_START_POSITION_RAD*-1.0 
        ankleRad = (self.biomCommand([ankle_commands.sf_torque_get()], struct_str = ankle_commands.float_get_format())[0])[ankle_commands.TORQUE_AAE_FLOAT_RADIANS_IND][1]
        direction = 0
    
        print("Ankle Position (Rad):" +str(ankleRad) + " Goal: "+str(goalAnkleAngleRad) + " Direction:"+str(direction), end = '')
        while abs(ankleRad - goalAnkleAngleRad) > 2*np.pi*2.0/(2**int(self.cfg.get('AAE_cfg_shared','joint_encoder_bits'))):#ANKLE_ANGLE_ENCODER_COUNTS_PER_TURN: #TODO: see if tolerance of +-2 AAE counts is acceptable. 
            if (ankleRad < goalAnkleAngleRad):
                direction = 1 #Too close to hardstop- open ankle.  
            else: 
                direction = -1
                
            dist = max(5, abs(ankleRad - goalAnkleAngleRad)) #Moving Copley about X counts moves ankle about .1*X counts.     
            self.motor.moveRelativeUsingProfile(direction*dist, velocityRPM = 85, blocking = False)
            time.sleep(.2)        
            ankleRad = (self.biomCommand([ankle_commands.sf_torque_get()], struct_str = ankle_commands.float_get_format())[0])[ankle_commands.TORQUE_AAE_FLOAT_RADIANS_IND][1]
            print('\r                                                                               ',end='')
            print("\rAnkle Position (Rad):" +str(ankleRad) + " Goal: "+str(goalAnkleAngleRad) + " Direction:"+str(direction),end='')
        print()
        self.copleyPosAtOpenAnkle = self.motor.readMotorStatusTimestamped()['position']
        self.motor.moveRelativeUsingProfile(0, velocityRPM = 1000, blocking = False) #Add velocity to increase hold of motor to stay in this position. 


    def ramp(self,rt,start,stop):
        '''
        ramp from start current (milliamps) to stop current (milliamps) in time rt
        '''
        #print 'time %f sC %f eC %f'%(rt,start,stop)
        cDif = stop-start
        st = time.time()
        endT = st+rt
        ct = st
        while ct<endT:
            dif = (ct-st)*1.0/rt
            c = start + cDif*dif
            self.biomCommand([ankle_commands.s_motor_current_amp(c,0)])
            ct=time.time()
            #print 'current %f'%c
            #time.sleep(rt/10)
        self.biomCommand([ankle_commands.s_motor_current_amp(stop,0)])
            
    
    def performTest(self):
        self.holdAnkleOffHardstop(); #Copley will hold this position, all other work done by ankle. 
        self.testCycleCount = 0
        ################################################
        #amp values need to be uped so that more force is applied for the calibration. 
        ################################################
        
        
        # Lets wait for the queue to get some data
        time.sleep(dwell_time)
        for c in range(0,k2_cycles):
#             print('k2 cycle %d of %d'%(c,cycles),file = self.logfile)
            self.testCycleCount = c            
            #set ankle motor to not move
            self.biomCommand([ankle_commands.s_motor_current_amp(int(initial_amps),0)])
            #ramp to pull current
            self.ramp(pull_time_to_move, int(initial_amps), int(pull_max_amps))
            #ramp down to 0
            self.ramp(pull_time_to_move, int(pull_max_amps), int(initial_amps))
            time.sleep(dwell_time/8.0)
            #ramp to push current
            self.ramp(push_time_to_move, int(initial_amps), int(push_max_amps))
            #ramp to 0
            self.ramp(push_time_to_move, int(push_max_amps), int(initial_amps))
            print("\rSpring Pull Cycle:" +str(c) + " of "+str(k2_cycles),end='')
            self.biomCommand([ankle_commands.s_motor_current_amp(0,int(initial_amps))])
            #dwell time
            time.sleep(dwell_time)
        print()
            
        self.biomCommand([ankle_commands.s_motor_stop()])
        

    def dataReadThread(self):
        self.stillLogging = True;
        oldData = self.biomTelemetryHandler.peek_last_received()
        while (self.stillLogging):

            d = dict() 
            d["Copley Encoder Pos"] = self.motor.readMotorStatusTimestamped()['position']
                
            d2 = self.loadCell.getLatestDict()
            tqNM = LtfTrigGeomUtil.ConvertLoadCellForceTANGOJointTorque(d2["FzNewtons"], d2["FyNewtons"])
            d["BiOM Joint Torque (Nm) according to LoadCell"] = tqNM
            for k in d2.keys():
                d[k] = d2[k]       

            data = self.biomTelemetryHandler.peek_last_received();
            if data != None:               
                #Just get whatever data is coming out of the vlist
                for k in data.keys():
                    d[k] = data[k] 
                self.biomTelemetryHandler.flush() #Just flush data after we've read latest because we only want the latest sample matched to our other data.                

                d["endReadTimeMilliSec"] =  DataBorgLTF.millis() #Put timestamp in our dictionary
                d["cycleCount"] = self.testCycleCount #K3 cal uses which count we're on. 
                #if oldData != data and (time.time()-self.biomTelemetryHandler.time_last_received)<0.5:
                    #add time check?
                self.logData("ALL_DATA", d) #Only log data when we have a new telemetry packet.
                oldData = data
            time.sleep(0.001)     
        self.stillLogging = False
        if hasattr(self, "logfile"): self.logfile.close()



class calbK3Test(DataBorgLTF.LTFTestFramework):
    ''' Simple test to move BiOM back and forth and find the endpoints where we should gather data. 
    Most of the comm plumbing guts are in ltf/LTFTestFramework.py, this subclass defines the K3 test specific motor profile. 
    '''
    
    def findCopleyPosAtTq(self, goal_Nm):
        loads = self.loadCell.getLatestDict()
        tqNm = LtfTrigGeomUtil.ConvertLoadCellForceTANGOJointTorque(loads["FzNewtons"], loads["FyNewtons"])
        #print("Copley Position (Counts):" +str(motorPos)+" Ankle Calculated Torque:"+str(tqNm))
        while abs(goal_Nm - tqNm) > 3: #TODO: See if 2Nm tolerance is precise enough (or possibly too precise), eric saw ankle try to target location add bounce between positions,expanded to 3Nm
            if (tqNm >= goal_Nm):
                direction = -1 #Too much torque- servo to let up slightly. 
            else: 
                direction = 1
            self.motor.moveRelativeUsingProfile(direction*400, velocityRPM = 35, blocking = False)
            time.sleep(.3)        
            loads = self.loadCell.getLatestDict()
            tqNm = LtfTrigGeomUtil.ConvertLoadCellForceTANGOJointTorque(loads["FzNewtons"], loads["FyNewtons"])
            #print("Copley Position (Counts):" +str(motorPos)+" Ankle Calculated Torque:"+str(tqNm))
        return self.motor.readMotorStatusTimestamped()['position']
        
    def findCopleyPosOffHardstop(self):
        goalAnkleAngleRad = K2_OPEN_ANKLE_ANGLE_START_POSITION_RAD*-1.0 
        ankleRad = (self.biomCommand([ankle_commands.sf_torque_get()], struct_str = ankle_commands.float_get_format())[0])[ankle_commands.TORQUE_AAE_FLOAT_RADIANS_IND][1]

        print("Ankle Position (Rad):" +str(ankleRad) + " Goal: "+str(goalAnkleAngleRad),end='')
        while abs(ankleRad - goalAnkleAngleRad) > 2*np.pi*2.0/(2**int(self.cfg.get('AAE_cfg_shared','joint_encoder_bits'))):#ANKLE_ANGLE_ENCODER_COUNTS_PER_TURN: #TODO: see if tolerance of +-2 AAE counts is acceptable. 
            if (ankleRad < goalAnkleAngleRad):
                direction = 1 #Too close to hardstop- open ankle.  
            else: 
                direction = -1
                
            dist = max(5, abs(ankleRad - goalAnkleAngleRad)) #Moving Copley about X counts moves ankle about .1*X counts.     
            self.motor.moveRelativeUsingProfile(direction*dist, velocityRPM = 85, blocking = False)
            time.sleep(.2)        
            ankleRad = (self.biomCommand([ankle_commands.sf_torque_get()], struct_str = ankle_commands.float_get_format())[0])[ankle_commands.TORQUE_AAE_FLOAT_RADIANS_IND][1]
            print('\r                                                                         ',end='')
            print("\rAnkle Position (Rad):" +str(ankleRad) + " Goal: "+str(goalAnkleAngleRad),end='')
        print()
        return self.motor.readMotorStatusTimestamped()['position']
        
    
    def performTest(self):
        copleyAtStart = self.motor.readMotorStatusTimestamped()['position']
        copleyAtEndTorque = self.findCopleyPosAtTq(K3_TORQUE_END_DATA_NM)
        self.motor.moveAbsoluteUsingProfile(copleyAtStart, velocityRPM = 20, blocking = True, accelRadPSec2 = 5, decelRadPSec2=5, threshold = 1)
        copleyAtStartTorque = self.findCopleyPosOffHardstop()#self.findCopleyPosAtTq(K3_TORQUE_START_DATA_NM)
        passes = 15
        dwellTime = 2
        self.testCycleCount = 0
        try:
            # Rev C 85 RPM ~= 84.92 ~= 100 / 1.1776 
            self.motor.moveToZero()
            time.sleep(dwellTime) 
            for i in range(0, passes):
                print('k3 cycle %d of %d'%(i,passes))#,file = self.logfile)
                      
                self.findCopleyPosAtTq(K3_TORQUE_END_DATA_NM)
#                self.motor.moveAbsoluteUsingProfile(copleyAtEndTorque, velocityRPM = 20, blocking = True, accelRadPSec2 = 5, decelRadPSec2=5, threshold = 1)
                time.sleep(dwellTime)    
                self.motor.moveAbsoluteUsingProfile(copleyAtStartTorque, velocityRPM = 50, blocking = True, accelRadPSec2 = 5, decelRadPSec2=5, threshold = 1)
                self.testCycleCount += 1
            self.motor.moveToZero()
        except Exception as e:
            print(e, file = self.logfile)
            self.motor.stop()        

    def dataReadThread(self):
        self.stillLogging = True;
        oldData = self.biomTelemetryHandler.peek_last_received()
        while (self.stillLogging):

            d = dict() 
            d["Copley Encoder Pos"] = self.motor.readMotorStatusTimestamped()['position']
                
            d2 = self.loadCell.getLatestDict()
            tqNM = LtfTrigGeomUtil.ConvertLoadCellForceTANGOJointTorque(d2["FzNewtons"], d2["FyNewtons"])
            d["BiOM Joint Torque (Nm) according to LoadCell"] = tqNM
            for k in d2.keys():
                d[k] = d2[k]       

            data = self.biomTelemetryHandler.peek_last_received();
            if data != None:               
                #Just get whatever data is coming out of the vlist
                for k in data.keys():
                    d[k] = data[k] 
                self.biomTelemetryHandler.flush() #Just flush data after we've read latest because we only want the latest sample matched to our other data.                

                d["endReadTimeMilliSec"] =  DataBorgLTF.millis() #Put timestamp in our dictionary
                d["cycleCount"] = self.testCycleCount #K3 cal uses which count we're on. 
                if data != oldData:
                    self.logData("ALL_DATA", d) #Only log data when we have a new telemetry packet.
                oldData = data
            time.sleep(0.001)
        self.stillLogging = False
        if hasattr(self, "logfile"): self.logfile.close()
    
        

class springClass(object):
    '''
    calibration tools for k2 and k3 using old drivers
    this assumes that ankle has a phased motor. 
    '''


    def __init__(self, ankleCon, ankleTelemetryPort, calbFile, fileDir, logHandler):
        '''
        Constructor for object used to find k2 and k3 values. 
        '''
        if isinstance(ankleCon,pfcmd.PFCmd):
            self.ankleCon = ankleCon #connection to the ankle controler
        else:
            raise inputError('you must pass a pfcmd.PFCmd object into ankleCon')
        
        self.ankleTelemetryPort = ankleTelemetryPort #data stream from the ankle -- dataport object?
        self.vlist = './vlist/calibrate.vlist'
        
        if isinstance(calbFile, ConfigParser.ConfigParser):
            self.calbFile = calbFile # ankle calibration file
        else:
            raise inputError('you must pass a ConfigParser.ConfigParser object into the calbFile value')
        
        if isinstance(logHandler, loggHandeler.logger_class):
            self.logger = logHandler #log file for calibration process
        else:
            raise inputError('you must pass a loggHandeler.logger_class object into the logFile value')
        
#         if not os.path.exists(fileDir):
#             try:
#                 os.mkdir(fileDir)
#             except:
#                 raise inputError('you must pass a file name into the fileDir value')
        self.fileDir = fileDir
        
#         self.analysis = springAnalysis(self.logger,cfg = self.calbFile)
        self.startPosition = 0#-600

        
        self.calbFile.set('mfg_table', 'calb_springs.SVN_SUB_REV_STR', SVN_SUB_REV_STR)
        self.calbFile.set('mfg_table', 'calb_springs.SVN_SUB_URL_STR', SVN_SUB_URL_STR)
        self.calbFile.set('mfg_table', 'calb_springs.SVN_SUB_DATE_STR', SVN_SUB_DATE_STR)
        
    def __exit__(self):
        subprocess.call('stty echo cooked', shell=True);
        
    
    def special(self,command):
        toRet = self.ankleCon.do_command(ankle_commands.s_command_type(), command)
        if toRet ==[]:
            self.logger.logInfo("WARNING: HAD TO RETRY FOR:"+" ".join(str(s) for s in command))
            toRet = False
            self.ankleCon.flush()
            toRet2 = self.ankleCon.do_command(ankle_commands.s_command_type(), command)
            if toRet2 == [] or toRet2 == None:
                self.logger.logInfo("ERROR: NEVER GOT RSP FROM:"+" ".join(str(s) for s in command))
                raise calb_tests.unknownError('no response from UUT; command: '+str(command))
            else:
                toRet = toRet2
        self.logger.logInfo(toRet)
        return toRet


    def findk3(self):
        '''
        find k3 value
        requires k2 values to have been calculated. 
        '''
             
        hs = self.fileDir#os.path.join(self.fileDir ,time.strftime('hstop_log_%y%m%d_%H%M%S.txt'))
#         data_source = []
        
        #set ankle to do nothing. 
        self.special(ankle_commands.s_set_system_idle())
        self.special(ankle_commands.s_motor_current_amp(0, 0))#s_motor_open_leads())
        
       
        
        myTest = calbK3Test(biomCommandComport = None, biomTelemetryComport = self.ankleTelemetryPort, biomTelemetryVlist = self.vlist, connectToCopley = True,                 connectToLoadCell = True, logFileNameOverride = hs, cfg = self.calbFile)
        myTest.biomConnection = self.ankleCon
        myTest.testCycleCount = 0    
        myTest.findCopleyPosOffHardstop()    
        thread.start_new_thread(myTest.dataReadThread, ())    
        time.sleep(1)
        myTest.performTest()
        myTest.destroyFramework() #disconnect from our created connections. 
        
#         self.analysis.importData(hs,self.fileDir)
#         self.analysis.calk3()
#         self.calbFile.set('calb_table_shared','springs.k3_Nm_p_rad',self.analysis.k3_val)
        #self.calbFile.set("calb_table_shared","spring.k3_Nm_p_rad",self.analysis.k3_val)
        #self.calbFile.set("calb_table_shared","spring.k3_engagement_angle_mil_deg",self.analysis.k3_intersept_val)
    
    def findk2(self):
        '''
        move ankle off hardstop, have ankle displace spring pull and push to measure force displacement. Used to find k2 spring values
        '''
        ss = self.fileDir#os.path.join(self.fileDir,time.strftime('sspring_log_%y%m%d_%H%M%S.txt'))
#         data_source = []
        
        #set ankle to do nothing. 
        self.special(ankle_commands.s_set_system_idle())
        self.special(ankle_commands.s_motor_current_amp(0, 0))#s_motor_open_leads())
        
        myTest = calbK2Test(biomCommandComport = None, biomTelemetryComport = self.ankleTelemetryPort, biomTelemetryVlist = self.vlist, connectToCopley = True, connectToLoadCell = True, logFileNameOverride = ss, cfg = self.calbFile)
        myTest.biomConnection = self.ankleCon #We already have an active ankle connection, so set it here instead of initializing a comport from scratch.
        myTest.testCycleCount = 0        
        myTest.holdAnkleOffHardstop()
        thread.start_new_thread(myTest.dataReadThread, ())    
        time.sleep(1)
        myTest.performTest()
        myTest.destroyFramework() #disconnect from our created connections. 
        
#         self.analysis.importData(ss,self.fileDir)
#         self.analysis.calk2()
#         self.calbFile.set('calb_table_shared','springs.k2_pull_Nm_p_rad',self.analysis.k2pull_val)
#         self.calbFile.set('calb_table_shared','springs.k2_push_Nm_p_rad',self.analysis.k2push_val)
        #self.calbFile.set("calb_table_shared","spring.k2_pull_Nm_p_rad",self.analysis.k2pull_val)
        #self.calbFile.set("calb_table_shared","spring.k2_push_Nm_p_rad",self.analysis.k2push_val)

    def move(self):
        '''move to startup location and restart the UUT'''
        
        #set ankle to do nothing. 
        self.special(ankle_commands.s_set_system_idle())
        self.special(ankle_commands.s_motor_current_amp(0, 0))#s_motor_open_leads())
        
        myTest = moveTest(biomCommandComport = None, biomTelemetryComport = self.ankleTelemetryPort, biomTelemetryVlist = self.vlist, connectToCopley = True, connectToLoadCell = True, logFileNameOverride = None, cfg = self.calbFile)
        myTest.biomConnection = self.ankleCon #We already have an active ankle connection, so set it here instead of initializing a comport from scratch.
        myTest.testCycleCount = 0        
#         myTest.holdAnkleOffHardstop()
        thread.start_new_thread(myTest.dataReadThread, ())    
     
        myTest.performTest()
        myTest.destroyFramework() #disconnect from our created connections. 
    
    def move2(self):
        '''move to startup location and restart the UUT'''
        
        #set ankle to do nothing. 
        self.special(ankle_commands.s_set_system_idle())
        self.special(ankle_commands.s_motor_current_amp(0, 0))#s_motor_open_leads())
        
        myTest = moveTest(biomCommandComport = None, biomTelemetryComport = self.ankleTelemetryPort, biomTelemetryVlist = self.vlist, connectToCopley = True, connectToLoadCell = True, logFileNameOverride = None, cfg = self.calbFile)
        myTest.biomConnection = self.ankleCon #We already have an active ankle connection, so set it here instead of initializing a comport from scratch.
        myTest.testCycleCount = 0        
#         myTest.holdAnkleOffHardstop()
        thread.start_new_thread(myTest.dataReadThread, ())    
     
        myTest.performTest2()
        myTest.destroyFramework() #disconnect from our created connections. 

def run_calb_springs():   
#     DATAPORT_SERIAL_BAUD_RATE = 1250000
    MPD_COMMAND_BAUD_RATE = 115200
    # Discover the USB Serial Ports to use
    sph = serial_port_helper.serial_port_helper()
    # Note: detects PSER movement from 0,1 to 2,3 (Linux)
    portNumStr = sph.get_port_to_use()
    if portNumStr == None :
        print( 'USB Serial Port NOT FOUND... Exiting')
        exit(-1)
    else :
        portNum = int(portNumStr)
        MPD_COMMAND_SERIAL_PORT = portNum
#         DATAPORT_SERIAL_PORT    = portNum + 1

    MPD_COMMAND_COM_PORT = sph.get_os_serial_port_name(MPD_COMMAND_SERIAL_PORT);
    print( "Using MPD COMMAND SERIAL COM:", MPD_COMMAND_COM_PORT)
    # Unused
    # DATAPORT_OS_COM_PORT = sph.get_os_serial_port_name(DATAPORT_SERIAL_PORT);
    # print "Using DATA SERIAL COM:"       , DATAPORT_OS_COM_PORT
    pf = pfcmd.PFCmd()
    pf.connect(MPD_COMMAND_COM_PORT, MPD_COMMAND_BAUD_RATE);
    cfg = ConfigParser.ConfigParser(dict_type=ordereddict.OrderedDict)
    cfg.read(os.path.join('testPrograms','template_ankle_tables.ini'))
        
    log = loggHandeler.logger_class(time.strftime('calb_spring_unittest_log_%y%m%d_%H%M%S.txt'))
    
    
    spring = springClass(pf, 1, cfg, ".", log )

    k2 = spring.findk2()
    k3 = spring.findk3()
    print(k2)
    print(k3)
    
def unit_test_calb_from_saved_data():
    analysis = springAnalysis()
    analysis.k2push = 571
    analysis.importData('hstop_log_160209_203412.txt', '.')
    analysis.calk3()

def unit_test_calbk2_from_saved_data():
    analysis = springAnalysis()
    analysis.importData('1699004/1699004_calibration_160218_110412/sspring_log_160218_110431.txt', '1699004/1699004_calibration_160218_110412/')
    analysis.calk2()

 

def unit_test_calbk3_from_saved_data():
    analysis = springAnalysis()
    
    analysis.importData('1699004/1699004_calibration_160219_155904/hstop_log_160219_160150.txt', '1699004/1699004_calibration_160219_155904/')
    analysis.calk2()
    
if __name__ == '__main__' :
    #run_calb_springs()  
    #unit_test_calb_from_saved_data();
    #unit_test_calbk2_from_saved_data()
    unit_test_calbk3_from_saved_data()

