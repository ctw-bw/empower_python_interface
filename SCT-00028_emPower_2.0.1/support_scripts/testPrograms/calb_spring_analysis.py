#!/usr/bin/env python
'''
Created on Feb 19, 2016

@author: ekurz
'''
from __future__ import print_function
import numpy as np
import scipy.stats as stats
import grapher_matplotlib
import os
# import matplotlib.pyplot as plt
import datetime
import criterea

ANKLE_ANGLE_ENCODER_COUNTS_PER_TURN = 8192
TORQUE_OFFSET_FROM_HARDSTOP = 20
TANGO_TQ_MODEL_SPRING_OFFSET = 26/1000.0 * 2.54 /100.0; # The plate/foot structure, when torqued down, affect the 'c' in our model- spring and sorbethane compress the spring from the CAD model # geometry 
TANGO_TQ_MODEL_A= 0.0406100; #meters
TANGO_TQ_MODEL_B= 0.10455; #meters mm
TANGO_TQ_MODEL_C= 0.10092 - TANGO_TQ_MODEL_SPRING_OFFSET;  #meters,  neutral spring position (actually varies +-.5 mm amount depending on whether we're up against hardstop)
TANGO_THETA0 = np.arccos((TANGO_TQ_MODEL_A**2+TANGO_TQ_MODEL_B**2-TANGO_TQ_MODEL_C**2)/(2*TANGO_TQ_MODEL_A*TANGO_TQ_MODEL_B)); #radians
# TANGO_PULLEY_TEETH = 72;

def workflowSetTS(cfgObj) :
    """set the datetime of the last update to the configuration"""
    d = datetime.datetime.now()
    cfgObj.set('mfg_table', 'last_update', d.strftime('%Y%m%d_%H%M%S'))

def commitConfigFileLocally(cfg,fn,logger) :
    """commit the current configuration to local file, including timestamp"""
    workflowSetTS(cfg)

    with open(fn,'w') as fh :
        cfg.write(fh)
        fh.flush()

def DSPbetaCalculation(MotorAngleRadsArray,cfg):
  
    aTimesb2 = TANGO_TQ_MODEL_A * TANGO_TQ_MODEL_B * 2;
    aSqdPlusbSqd = TANGO_TQ_MODEL_A * TANGO_TQ_MODEL_A + TANGO_TQ_MODEL_B * TANGO_TQ_MODEL_B;
    K_pulley = 18.0/int(cfg.get('actuator_cfg_shared','pulley_teeth'))#TANGO_PULLEY_TEETH;         # motor rotation ratio; motor=18 teeth
    K_screw =  3e-3/2.0/np.pi;  #m/ per rad ;  ball screw travel distance per screw revolution
    theta_c = TANGO_THETA0; # rad; arccos((constant.a^2+constant.b^2-c0^2)/(2*constant.a*constant.b)); // considering the theta initial position
    c = np.sqrt(aSqdPlusbSqd - (aTimesb2)*np.cos(theta_c));   # calculate c while theta was not at neutral position
    deltaC = K_pulley * K_screw * MotorAngleRadsArray;

    #beta angle if you use a model that assumes spring pivots around ankle angle
    betaAngleArray = np.arccos((aSqdPlusbSqd - (c+deltaC)*(c+deltaC))/(aTimesb2)) - theta_c;   # radians
    return betaAngleArray


class springAnalysis(object):
    def __init__(self, logger=None, tango = True,cfg = None):
        self.k3_calculated = 'not run'
        self.k2_calculated = 'not run'
        self.k2_error = None
        self.k3_error = None
        self.logger = logger
        self.tango = tango
        self.cfg = cfg
        

        
    def importData(self,fileName,FileDir):
        '''
        import data from file
        '''
        self.fileDir= FileDir
        if 'hstop' in fileName:
            test = 'HS'
        elif 'sspring' in fileName:
            test = 'SS'
        else:
            print( 'error', file=self.logger)
            return
#os.path.join(self.fileDir,fileName)
        data = np.genfromtxt (fileName, delimiter=",", dtype=float, names=True)
#         data = data[~np.isnan(data).any(axis=1)]
        self.cycleCount = data['cycleCount']
        AnkleAngle = data['calibratedAnkleAngleEncoderCounts']               
        AnkleAngleRads = AnkleAngle*np.pi*2.0/(2**float(self.cfg.get('AAE_cfg_shared','joint_encoder_bits')))#ANKLE_ANGLE_ENCODER_COUNTS_PER_TURN
        Fy = data['FyNewtons']
        Fz = data['FzNewtons']
        mot_rotation = data['mot_rotation']
        self.mot_current = data['qIq']
        self.mot_loc = data['MC_mot_revs']
        self.sensedJointTorque = data['BiOM_Joint_Torque_Nm_according_to_LoadCell']        
        self.sensedJointTorqueOffset = self.sensedJointTorque - np.median(self.sensedJointTorque[0:30])
        
        if test is 'SS':
            # assuming no force at start. used for k2
            self.FyScailed =  Fy - Fy[0]
            self.FzScailed =  Fz - Fz[0]
        if test is 'HS':
            #assuming no force for first 300 samples. used for k3
            self.FyScailedK3 = Fy - np.median(Fy[0:300])
            self.FzScailedK3 = Fz - np.median(Fz[0:300])
            
        #converted reference frames for encoders
        self.externalAnkleAngleRads = AnkleAngleRads*-1 
        self.externalAnkleAngleDegrees = -1 * AnkleAngleRads *360.0/(2*np.pi)
        self.externalMotorEncoderRads = -1.0* mot_rotation*2.0* np.pi
        betaAngleRads = DSPbetaCalculation(self.externalMotorEncoderRads,self.cfg)    
        thetaMbeta = self.externalAnkleAngleRads - betaAngleRads
               


        #TODO: Unclear if zeroed out torque setting is actually correct for calculating K3...
        #for i in range(0,len(self.Fy)):
        #    self.driveTorque.append(LTF_TORQUE_LEVER_ARM_METERS*(math.sqrt(self.FzScailed[i]**2.0+self.FyScailed[i]**2.0))*math.copysign(1,self.FzScailed[i]))
        #    self.externalAnkleAngleScailed.append((self.externalAnkleAngle[i]-self.externalAnkleAngle[0])/180.0*math.pi)
        #    self.driveTorqueK3.append(LTF_TORQUE_LEVER_ARM_METERS*(math.sqrt(self.FyScailedK3[i]**2+self.FzScailedK3[i]**2))*math.copysign(1,self.FzScailedK3[i]))
        if test is 'HS':        
            seriesTorque = []  
            for i in range(0, len(thetaMbeta)):
                if thetaMbeta[i] > 0:
                    seriesTorque.append(np.average(self.k2pull_0)*thetaMbeta[i])
                else:
                    seriesTorque.append(np.average(self.k2push)*thetaMbeta[i])
            self.torqueK3 = self.sensedJointTorqueOffset - seriesTorque
        
        elif test is 'SS':        
            self.driveTorqueK2 = self.sensedJointTorqueOffset
            self.thetaMbetaK2 = thetaMbeta


    def calk2(self):
        '''calculates the k2 push and pull values'''
        toRet = True
        self.k2_calculated = 'running'
        self.k2_error = None
#         graphs = []
        self.k2push = []
        self.k2pull_0 = []
        self.k2pull_25 = []
        self.k2pull_50 = []
        self.k2pull_75 = []
        self.rpull_0 = []
        self.rpull_25 = []
        self.rpull_50 = []
        self.rpull_75 = []
        self.rpush = []
        
        thMBpull_0_AllCycles = []
        linData_AllCycles = []
        linDataR_AllCycles = []

        thMBpush_AllCycles = []
        linDataPush_AllCycles = []
        linDataRPush_AllCycles = []
        #print(self.cycleCount)
        #setup shows as cycle 0
        for cycle in range(int(np.min(self.cycleCount)+1),int(np.max(self.cycleCount)+1)):
        
            thMBCycle = []
            driveTorqueCycle = []
            thMBpull_0 = []
            thMBpull_25 = []
            thMBpull_50 = []
            thMBpull_75 = []
            thMBpush = []
            dTpull_0 = []
            dTpull_25 = []
            dTpull_50 = []
            dTpull_75 = []
            dTpush = []
            
            driveTorqueCycle = self.driveTorqueK2[self.cycleCount == cycle]
            
            #compair min an max to criterea 
            maxForce = max(driveTorqueCycle)
            minForce = min(driveTorqueCycle)
            if criterea.k2pushForce()<minForce:
                #toRet = False
                print('k2 push force',file=self.logger)
                print(maxForce,file=self.logger)
                continue
            if criterea.k2pullforce()>maxForce:
                #toRet = False
                print(criterea.k2pullforce())
                print('k2 pull force',file=self.logger)
                print(maxForce,file=self.logger)
                continue
            
            thMBCycle = self.thetaMbetaK2[self.cycleCount == cycle]
#             print(thMBCycle)

            minTB = min(thMBCycle)#np.min(0, min(thMBCycle)) #Push value must be negative
            if (minTB >= 0):
                print("Cycle count:"+ str(cycle)+": WARNING! No push values this cycle! Skipping!",file=self.logger)
                continue

            pushCutoff = minTB/5.0
            pushMax = minTB *3/4
            maxTB = max(thMBCycle)
            pullCutoff_0 = 0# maxTB/4
            pullCutoff_25 = maxTB/4
            pullCutoff_50 = maxTB/2
            pullCutoff_75 = maxTB*3/4
            thMBpull_0 = thMBCycle[thMBCycle > pullCutoff_0]
            dTpull_0   = driveTorqueCycle[thMBCycle > pullCutoff_0]
            thMBpull_25 = thMBCycle[thMBCycle > pullCutoff_25]
            dTpull_25   = driveTorqueCycle[thMBCycle > pullCutoff_25]
            thMBpull_50 = thMBCycle[thMBCycle > pullCutoff_50]
            dTpull_50   = driveTorqueCycle[thMBCycle > pullCutoff_50]
            thMBpull_75 = thMBCycle[thMBCycle > pullCutoff_75]
            dTpull_75   = driveTorqueCycle[thMBCycle > pullCutoff_75]

#             print(str(minTB)+" "+str(pushCutoff)+" "+str(pushMax))
            thMBpush = thMBCycle[(thMBCycle < pushCutoff) & (thMBCycle > pushMax)]
            dTpush   = driveTorqueCycle[(thMBCycle < pushCutoff) & (thMBCycle > pushMax)]
                    
            slope, inter, r_value, _, _ = stats.linregress(thMBpull_0,dTpull_0)
            self.k2pull_0.append(slope)
            self.rpull_0.append(r_value)
            gr = np.poly1d([slope,inter])
            
            thMBpull_0_AllCycles.append(thMBpull_0)
            linData_AllCycles.append(gr(thMBpull_0))
            linDataR_AllCycles.append(dTpull_0)
 
            if len(thMBpull_25)>30:
                slope, inter, r_value, _, _ = stats.linregress(thMBpull_25,dTpull_25)
                self.k2pull_25.append(slope)
                self.rpull_25.append(r_value)
            if len(thMBpull_50)>30:
                slope, inter, r_value, _, _ = stats.linregress(thMBpull_50,dTpull_50)
                self.k2pull_50.append(slope)
                self.rpull_50.append(r_value)
            if len(thMBpull_75)>30:
                slope, inter, r_value, _, _ = stats.linregress(thMBpull_75,dTpull_75)
                self.k2pull_75.append(slope)
                self.rpull_75.append(r_value)
            
            slope, inter, r_value, _, _ = stats.linregress(thMBpush,dTpush)
            #####################
            # add lin fit to graphs with polyline 
            grk2 = np.poly1d([slope, inter])
            
            thMBpush_AllCycles.append(thMBpush)         
            linDataPush_AllCycles.append(grk2(thMBpush))
            linDataRPush_AllCycles.append(dTpush)
        

            #graphs.append(PolyLine(linDataK2, legend='linear', colour='red', width=3))
            self.k2push.append(slope)
            self.rpush.append(r_value)
###########################################        
#current torque equation
#todo replace with motor torqe model from ben
###########################################
        kt = 0.0136 #Nm/A 
        gear = 0.866 #gear ratio 
#         actuatorRatio = 
        a = 40.6#mm
        b = 104.55#mm
        c = 100.92 -np.array(self.mot_loc)*18.0/66.0*3.0#mm this is a function of motor location
        gometricRatio = c/(a*b*(1-((a**2+b**2-c**2)**2)/(4*a**2*b**2))**(0.5))
        gearRatio = -1/(gometricRatio*(3.0 *(18.0/66.0) /(3.14*2)))
        cteData = kt*gear*gearRatio*np.array(self.mot_current)
#         cte = np.poly1d([kt*gear*gometricRatio, 0])
#         cteData = cte(self.mot_current)
        
        
        dataSets1 = []
        dataSets1.append([self.driveTorqueK2,self.mot_current, 'Cycle Data', 'green'])
        dataSets1.append([cteData,self.mot_current,'ideal','blue','line'])
        
        grapher_matplotlib.graph(dataSets1,'k2 friction','Torque(Nm)','motor current (A)',  os.path.join(self.fileDir ,'k2_friction.png'))
        
        dataSets = [];
        dataSets.append([self.thetaMbetaK2,self.driveTorqueK2, 'Cycle Data', 'green'])
        for i in range(0,len(thMBpull_0_AllCycles)):
            dataSets.append([thMBpull_0_AllCycles[i], linData_AllCycles[i] , '', 'red','line'])
        for i in range(0,len(thMBpush_AllCycles)):
            dataSets.append([thMBpush_AllCycles[i], linDataPush_AllCycles[i] , '', 'red','line'])
        grapher_matplotlib.graph(dataSets,'k2 springs','theta-beta(rad)','Torque(Nm)',  os.path.join(self.fileDir ,'k2.png'))
        
        if len(self.k2pull_0) <criterea.k2cycles():
            toRet = False
            print('k2 number of cycles: %d'%len(self.k2pull_0),file = self.logger)
        else:
            k2pullR = max(self.k2pull_0)-min(self.k2pull_0)
            if k2pullR > criterea.k2CycleDif():
                toRet = False
                print('k2 pull diff', file=self.logger)
                print(k2pullR,file=self.logger)
            k2pushR = max(self.k2push)-min(self.k2push)
            if k2pushR > criterea.k2CycleDif():
                toRet = False
                print('k2 push diff',file=self.logger)
                print(k2pushR,file=self.logger)
            
            self.k2pull_val = np.average(self.k2pull_0)
            self.k2push_val = np.average(self.k2push)
            
            if self.k2pull_val < criterea.k2PullRange()[0]:
                toRet = False
                print('k2 pull val low',file=self.logger)
                print(self.k2pull_val,file = self.logger)
            if self.k2pull_val > criterea.k2PullRange()[1]:
                toRet = False
                print('k2 pull val high',file=self.logger)
                print(self.k2pull_val,file = self.logger)
            
            if self.k2push_val < criterea.k2PushRange()[0]:
                toRet = False
                print('k2 push val low',file=self.logger)
                print(self.k2push_val,file = self.logger)
            if self.k2push_val > criterea.k2PushRange()[1]:
                toRet = False
                print('k2 push val high',file=self.logger)
                print(self.k2push_val,file = self.logger)
            #if self.k2pull_val 
            
            st = ''
            for v in self.k2pull_0: st = st+'%f, '%v
            self.cfg.set('mfg_table','springs.k2_pull_Nm_p_rad_cycle',st)
            st = ''
            for v in self.k2push: st = st+'%f, '%v
            self.cfg.set('mfg_table','springs.k2_push_Nm_p_rad_cycle',st)
            
            print( 'pull_0', file=self.logger)
            print( self.k2pull_0, file=self.logger)
            print( np.average(self.k2pull_0), file=self.logger)
            print( self.rpull_0, file=self.logger)
            print( self.k2pull_val, file=self.logger)
            self.cfg.set('calb_table_shared','springs.k2_pull_Nm_p_rad',self.k2pull_val)
            
            print( 'push', file=self.logger)
            print( self.k2push, file=self.logger)
            print( np.average(self.k2push), file=self.logger)
            print( self.rpush, file=self.logger)
            print( self.k2push_val, file=self.logger)
            self.cfg.set('calb_table_shared','springs.k2_push_Nm_p_rad',self.k2push_val)
        
        return toRet
            
    def calk3(self):
        '''calculates the k3 values '''
        toRet = True
        self.k3_calculated = 'running'
        self.k3_error = None
        
        self.k3 = []
        self.k3Intersept = []
        self.k3_rval = []
  
        aar_UsedInK3Slope = []
        torque_UsedInK3Slope = []
        grk3_UsedInK3Slope = []
        #removing data that is from setup. that shows up as cycle 0
        for cycle in range(int(np.min(self.cycleCount)+1),int(np.max(self.cycleCount))):
            print('.',end='',file=self.logger)
            driveTorqueCycle = self.torqueK3[self.cycleCount == cycle]
            AnkleAngleCycle = self.externalAnkleAngleRads[self.cycleCount == cycle]
            
            springK3dtCycle = []
            springK3AACycle = []
            
            
            maxTo = max(driveTorqueCycle)
            start = driveTorqueCycle[0] + TORQUE_OFFSET_FROM_HARDSTOP #Need to be a certain amount into HS so we ignore nonlinear k3 engagement region. 
            maxReached = False
            startNow = False

            #Slice a linear chunk of data from initial hardstop entry to max torque hit. 
            for j in range(0,len(driveTorqueCycle)):
                if driveTorqueCycle[j] == maxTo: maxReached = True
                if driveTorqueCycle[j] >= start: startNow = True
                if startNow and not maxReached:
                    springK3dtCycle.append(driveTorqueCycle[j])
                    springK3AACycle.append(AnkleAngleCycle[j])
            if len(springK3dtCycle)>4:
                forceR = max(springK3dtCycle)-min(springK3dtCycle)
                if forceR>criterea.k3forceRange():
                    slope, intersept, r_value, _, _ = stats.linregress(springK3AACycle, springK3dtCycle)
                    grk3 = np.poly1d([slope, intersept])
                    #add intersept point
                    springK3dtCycle.append(0)
                    springK3AACycle.append((-intersept)/slope)
                    
                    torque_UsedInK3Slope.append(springK3dtCycle)
                    aar_UsedInK3Slope.append(springK3AACycle)
                    grk3_UsedInK3Slope.append(grk3(springK3AACycle))
                    
                    self.k3.append(slope)
                    self.k3_rval.append(r_value)
                    #######################################
                    #  add lin slope to graph for each cycle
                    self.k3Intersept.append(-intersept/slope)
        print('',file=self.logger)
        
        dataSets = [];
        dataSets.append([self.externalAnkleAngleRads,self.torqueK3, 'Cycle Data', 'yellow'])
        AAE_vals = []
        torque_vals = []
        for i in range(0,len(torque_UsedInK3Slope)):
            dataSets.append([aar_UsedInK3Slope[i], torque_UsedInK3Slope[i], '', 'blue'])
            dataSets.append([aar_UsedInK3Slope[i], grk3_UsedInK3Slope[i] , '', 'red','line'])
            for elm in aar_UsedInK3Slope[i]: AAE_vals.append(elm)
            for elm in torque_UsedInK3Slope[i]:torque_vals.append(elm)
        if AAE_vals > 10:
            slope, intersept, r_value, _, _ = stats.linregress(AAE_vals, torque_vals)
            grk3 = np.poly1d([slope, intersept])
            dataSets.append([AAE_vals,grk3(AAE_vals),'','green','line'])
        grapher_matplotlib.graph(dataSets,'k3 spring','Ankle angle(rad)','Torque(Nm)',  os.path.join(self.fileDir ,'k3.png'))
        
        
#         self.cfg.set('mfg_table','springs.k3_Nm_p_rad_all_data',slope)
#         self.cfg.set('mfg_table','springs.k3_engagement_angle_mil_deg_all_data',(-intersept/slope))
#         self.cfg.set('mfg_table','springs.k3_rVall_all_data',r_value)
        
        st = ''
        for v in self.k3: st = st+'%f, '%v
        self.cfg.set('mfg_table','springs.k3_Nm_p_rad_cycle',st)
        st = ''
        for v in self.k3Intersept: st = st+'%f, '%v
        self.cfg.set('mfg_table','springs.k3_engagement_angle_mil_deg_cycle',st)
        self.k3_val = np.average(self.k3)
        self.k3_intersept_val = np.average(self.k3Intersept)
        self.k3_intersept_val_mil_deg = self.k3_intersept_val*360.0/(2*np.pi)*1000
        
#########################
#add criterea section
########################
        if len(self.k3)<criterea.k3cycles():
            toRet = False
            print('k3 cycles: %d'%len(self.k3),file=self.logger)
        else:
            k3R = max(self.k3)-min(self.k3)
            if k3R >criterea.k3CycleDif():
                toRet = False
                print('k3 rage',file=self.logger)
                print(k3R,file=self.logger)
                
            if criterea.k3Range()[0]>self.k3_val:
                toRet = False
                print('k3 low',file = self.logger)
                print(self.k3_val,file = self.logger)
            if criterea.k3Range()[1]<self.k3_val:
                toRet = False
                print('k3 high',file = self.logger)
                print(self.k3_val,file=self.logger)
    #         k2pushR = max(self.k2push)-min(self.k2push)
    #         if k2pushR > criterea.k2CycleDif():
    #             toRet = False
            
            print( 'k3', file=self.logger)
            print( self.k3, file=self.logger)
            print( self.k3_val, file=self.logger)
            self.cfg.set('calb_table_shared','springs.k3_Nm_p_rad',self.k3_val)
            print( 'k3 intercept', file=self.logger)
            print( self.k3Intersept, file=self.logger)
            print( self.k3_intersept_val, file=self.logger)
            self.cfg.set('calb_table_shared','springs.k3_engagement_angle_mil_deg',self.k3_intersept_val_mil_deg)
        
        return toRet
        
def unit_test_calbk3_from_saved_data(calFile):
    cfg = cp.ConfigParser(dict_type=ordereddict.OrderedDict)
    cfg.optionxform=str
    cfg.read(calFile)
    analysis = springAnalysis(cfg=cfg)
    
    analysis.k2push = float(cfg.get('calb_table_shared', 'springs.k2_push_Nm_p_rad'))
    analysis.k2pull_0 = float(cfg.get('calb_table_shared', 'springs.k2_pull_Nm_p_rad'))
    analysis.importData('..\\1699004\\1699004_calibration_160628_165137_full\\hstop_log_160628_170736.txt', '..\\1699004\\1699004_calibration_160628_165137_full\\')
    analysis.calk3()
    commitConfigFileLocally(cfg, calFile, None)
    
def unit_test_calbk2_from_saved_data(calFile):
    cfg = cp.ConfigParser(dict_type=ordereddict.OrderedDict)
    cfg.optionxform=str
    cfg.read(calFile)
    print(calFile)
    print(os.path.exists(calFile))
    print(cfg.get('AAE_cfg_shared', 'joint_encoder_bits'))
    analysis = springAnalysis(cfg=cfg)
    
    analysis.importData('../1699111/1699111_calibration_160705_150103_full/sspring_log_160705_151243.txt', '../1699111/1699111_calibration_160705_150103_full')
    analysis.calk2()
    commitConfigFileLocally(cfg, calFile, None)

if __name__ == '__main__' :
    import python_paths_default
    import python_paths
    import ordereddict
    import ConfigParser as cp
    calFile = 'C:\\svn\\PowerFoot\\trunk\\MFG_v2X\\support_scripts\\1699111\\1699111_calibration_160705_150103_full\\1699111_calbstop_tables_160705_150103.txt'
    #run_calb_springs()  
    #unit_test_calb_from_saved_data();
    unit_test_calbk2_from_saved_data(calFile)
#     unit_test_calbk3_from_saved_data(calFile)
