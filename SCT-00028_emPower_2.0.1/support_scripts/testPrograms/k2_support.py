'''
Created on Oct 31, 2014

@author: ekurz
'''

import python_paths_default
import python_paths

import threading
import ankle_commands
import time
import calb_tests

class k2_support(threading.Thread):
    '''
    classdocs
    '''


    def __init__(self, pf, cycles, dwell_time, settle_time, pull_time_to_move, pull_max_amps, \
                 push_time_to_move, push_max_amps, initial_amps,logit):
        '''
        Constructor
        '''
        self.pf = pf
        self.cycles=cycles
        self.dwell_time=dwell_time
        self.settle_time=settle_time
        self.pull_time_to_move=pull_time_to_move
        self.pull_max_amps=pull_max_amps
        self.push_time_to_move=push_time_to_move
        self.push_max_amps=push_max_amps
        self.initial_amps=initial_amps
        self.logit = logit
        threading.Thread.__init__(self)
        
    def special(self,command):
        toRet = self.pf.do_command(ankle_commands.s_command_type(), command)
        if toRet ==[]:
            self.logFile.logInfo("WARNING: HAD TO RETRY FOR:"+" ".join(str(s) for s in command))
            toRet = False
            self.pf.flush()
            toRet2 = self.pf.do_command(ankle_commands.s_command_type(), command)
            if toRet2 == [] or toRet2 == None:
                self.logFile.logInfo("ERROR: NEVER GOT RSP FROM:"+" ".join(str(s) for s in command))
                raise calb_tests.unknownError('no response from UUT; command: '+str(command))
            else:
                toRet = toRet2
        self.logFile.logInfo(toRet)
        return toRet
    
    def ramp(self,rt,start,stop):
        '''
        ramp from start current (milliamps) to stop current (milliamps) in time rt
        '''
        print 'time %f sC %f eC %f'%(rt,start,stop)
        cDif = stop-start
        st = time.time()
        endT = st+rt
        ct = st
        
        
        while ct<endT:
            dif = (ct-st)*1.0/rt
            c = start + cDif*dif
            self.special(ankle_commands.s_motor_current_amp(c,0))
            ct=time.time()
            print 'current %f'%c
            time.sleep(rt/10)
        self.special(ankle_commands.s_motor_current_amp(stop,0))
            
    def run(self):
        '''
        make the ankle push and pull
        '''
        for c in range(0,self.cycles):
            self.logit.set_cycle_count(c+1)
            print c
            #set ankle motor to not move
            self.special(ankle_commands.s_motor_current_amp(int(self.initial_amps),0))
            #ramp to pull current
            self.ramp(self.pull_time_to_move, int(self.initial_amps), int(self.pull_max_amps))
            #ramp down to 0
            self.ramp(self.pull_time_to_move, int(self.pull_max_amps), int(self.initial_amps))
            time.sleep(self.dwell_time/8.0)
            #ramp to push current
            self.ramp(self.push_time_to_move, int(self.initial_amps), int(self.push_max_amps))
            #ramp to 0
            self.ramp(self.push_time_to_move, int(self.push_max_amps), int(self.initial_amps))
            
            self.special(ankle_commands.s_motor_current_amp(0,int(self.initial_amps)))
            #dwell time
            time.sleep(self.dwell_time)
            
        self.special(ankle_commands.s_motor_stop())
        
        
        
        
        
        
        
if __name__== '__main__':
    import pfcmd
    import ordereddict
    import ConfigParser as cp
    import calb_tests
    MPD_COMMAND_COM_PORT = 9
    pf = pfcmd.PFCmd(0, 0)
    pf.connect(MPD_COMMAND_COM_PORT, 115200)
    cfg = cp.ConfigParser(dict_type=ordereddict.OrderedDict)
    cfg.read('template_ankle_tables.ini')
    calbT = calb_tests.BiOMCalbScripts(pf,None,cfg,None)
    
    runLevel = pf.do_command(ankle_commands.s_command_type(), ankle_commands.s_runlevel_get())
    print runLevel
    
    cycles=1
    dwell_time=7.0
    settle_time=1.0
    pull_time_to_move=0.5
    pull_max_amps=-1#8.
    push_time_to_move=1.0
    push_max_amps=1#7.0
    initial_amps=0.0
    
    calbT.motorPhase()
    
    ankle = k2_support(pf, cycles, dwell_time, settle_time, \
                        pull_time_to_move, pull_max_amps, \
                        push_time_to_move, push_max_amps, initial_amps)
    ankle.start()
