'''
Created on Feb 10, 2016

@author: ekurz
'''
import matplotlib.pyplot as plt
def graph(dataSets,title, xlabel,ylabel,fName, location =2):
    for elm in dataSets:
#         print('test')
#         print(elm[2])
        if len(elm)==4:
            plt.scatter(elm[0], elm[1], s=25, c=elm[3],label=elm[2],marker='+')
        else:
            plt.plot(elm[0],elm[1],c=elm[3],label = elm[2])
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.legend(loc=location)
    plt.grid(True)
    plt.savefig(fName)
    plt.clf() #clear the figure, otherwise next time we call this function, we'll get the old data. 


if __name__=='__main__':
    from numpy.random import rand
    dataSets = []
    for color in ['red', 'green', 'blue']:
        n = 50
        x, y = rand(2, n)
        dataSets.append([x,y,color,color])
    graph(dataSets,'title','x label','y label', 'test.png')
    dataSets = []
    for color in ['red', 'green', 'blue','orange']:
        n = 50
        x, y = rand(2, n)
        dataSets.append([x,y,color,color])
    graph(dataSets,'title','x label','y label', 'test2.png')
