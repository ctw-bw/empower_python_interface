
import types
global indent;
indent = 0;
def class_to_str(obj) :
    class_name = obj.__class__
    global indent;        
    str_list = []
    indstr = ' '*indent;                    
    for k,v in obj.__dict__.iteritems() :
        if k != 'class_name' :
            if type(v) == types.InstanceType :
                indent += 4;
                str_list.append("%s%-20.20s *** %s\n" % (indstr,k, v))
            else :

                str_list.append("%s%-20.20s %s\n" % (indstr,k, v))
            
    str=  ''.join(str_list);
    indent -= 4
    
    return '\n\n%s***START %s\n%s%s***END %s\n' % (indstr, class_name, str, indstr, class_name)
