import os

"""
modification of Danny Yoo code found : http://code.activestate.com/recipes/134892/

"""


if os.name == 'nt' :
    import msvcrt, time
    class py_getch :
        def __init__(self, TIMEOUT=None):
            self.timeout = TIMEOUT
        def __call__(self):
            if self.TIMEOUT :
                time.sleep(self.timeout);
            return msvcrt.getch()

        def getch(self) :
            self.__call__();




if os.name == 'posix' :
    import sys, termios, time

    class py_getch() :
        def __init__(self, BLOCK=False, ECHO=False, TIMEOUT=None):

            # constants that termios should define.
            termios.LFLAG = 3   # http://docs.python.org/library/termios.html see tcgetattr
            termios.C_CC = 6

            self.fd = sys.stdin.fileno()

            self.timeout = TIMEOUT;
            self.old_settings = termios.tcgetattr(self.fd)  # save to restore the TTY later

            # tty.setcbreak(self.fd)  # this turns off bit 3 (val of 8) in LFLAG
            # tty.setraw(self.fd))   # this disables ctrl-C and keyboard interrupt processing

            new_settings = termios.tcgetattr(self.fd)
            c_cc = new_settings[termios.C_CC]

            if BLOCK :
                c_cc[termios.VMIN] = 1;    # number of chars needed before return, 0 -> non-blocking
            else :
                c_cc[termios.VMIN] = 0

            if ECHO :
                new_settings[termios.LFLAG] |= termios.ECHO
            else :
                new_settings[termios.LFLAG] &= ~termios.ECHO

            new_settings[termios.LFLAG] &= ~termios.ICANON   # same as setcbreak

            c_cc[termios.VTIME] = 0;   # each unit appears to be ~0.1 sec -> total wait time with char

            termios.tcsetattr(self.fd, termios.TCSANOW, new_settings)


        def __del__(self) :
            """  this is the distructor called, lacks exception processing """
            print "__del__ (py_getch) called"
            termios.tcsetattr(self.fd, termios.TCSADRAIN, self.old_settings)


        def __call__(self):
            """ the __call__ function is called when no method specified with the instance """
            if self.timeout :
                time.sleep(self.timeout)
            ch = sys.stdin.read(1)
            return ch
        
        def getch(self) :
            self.__call__();
            
        def close(self) :
            self.__del__();



if __name__ == "__main__" :
    getch = py_getch(BLOCK=False, ECHO=False, TIMEOUT=0.01)

    i = 0;
    while i < 200 :
        ch = getch()      # same as getch.__call__()
        if ch :
            print i, ch, len(ch),  [ord(c) for c in ch]
            i += 1
            
        


        

