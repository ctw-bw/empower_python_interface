

class printable_dict(dict) :
    # extends dict, by allowing the specifications of printable fields
    def __new__(cls, obj) :
        # print "new:", obj
        ob = super(printable_dict, cls).__new__(cls, obj)
        return ob

    
    def __init__ (self, obj, printable_list = [], print_horz = True) :
        self.printable_list = printable_list;
        self.print_horz = print_horz;
        
        # print "_init_:",obj
        super(printable_dict, self).__init__(self)

    def set_printable(self, field) :
        if field not in self.printable_list :
            self.printable_list.append(field)
        return self.printable_list
    
    def clear(self) :
        """ clear the printable fields list"""
        self.printable_list = []
        
    def __str__(self) :
        str = ""
        # print "printable", self.printable_list
        if len(self.printable_list) == 0 :
            str = "printable_dict: no printable fields: (%s)   " % (' '.join(self.keys()))
        else :
            if self.print_horz :
                for field in self.printable_list :
                    if field in self :
                        str += "%10s " % (self[field])
                    else :
                        str += "%10s " % ('UKN(%s)' % field)
                # str += "\r\n";  # removed 8-29-11 as string should not have NL appended.
                
            else : 
                for field in self.printable_list :
                    if field in self :
                        str += "%-20.20s : %s\r\n" % (field, self[field])
                    else :
                        str +=  "unknown field (%s)\r\n" % field
        return str


if __name__ == "__main__" :
    d = {'a' : 1, 'b' : 2}
    s = printable_dict(d);
    s.set_printable('b')
    print "s:", s
    print "d:", d
