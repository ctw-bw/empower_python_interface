

class address_helper :
    def __init__ (self, lval=190, rval=240, base="192.168.192.") :
        self.lower_value = 190;
        self.upper_value = rval;
        self.base_ip = base;

    def get_address(self, str) :
        try :
            vl = int(str);
            if vl >=self.lower_value and vl <= self.upper_value :
                ip_address = self.base_ip + str;
        except :
            ip_address = str;

        return ip_address
