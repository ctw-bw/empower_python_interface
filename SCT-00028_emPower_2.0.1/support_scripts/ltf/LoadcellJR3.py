""" A port of the JR3 PCI drivers to Python using the libray pycomedi to make it easier to get at this data in Python more directly. No C or C++ compilation required, 
 libraries are auto installed by script. #See http://svn.iwalk.com/svn/svn_engineering/PowerFoot/branches/abolger/LTF/usr_local_lib_x64_fedora/pycomedi-0.7-0554204/demo for demo code that was used as example code to setup this library. """

try:
    import comedi
    import pycomedi
    import serial
except:
    print "Comedi python library not installed. This requires that this Linux box have the comedilib-0.10.1 installed."
    print "You need to download this folder:  http://svn.iwalk.com/svn/svn_engineering/PowerFoot/trunk/MFG/MFG_v2X/documentation/"
    print "Run 'fedora_12_environment_setup.sh' to install necessary libraries. "

from pycomedi.device import Device
from pycomedi.channel import AnalogChannel, DigitalChannel
from pycomedi.constant import SUBDEVICE_TYPE, AREF, UNIT, IO_DIRECTION, INSN
from pycomedi.chanspec import ChanSpec
import numpy, socket, struct, time


import python_paths_default, python_paths
import comm_lib.universal_dataport as universal_dataport



JR3COM_MAX_DATA_RANGE=16384  #Max resolution of force data- note that we don't use this, we actually query each channel for it's own resolution and use the provided Converter object from the pycomedi library. 
JR3COM_NUM_DATA_CHANNEL=56 # Highest channel that returns force or moment data (0 indexed, so 56 total channels from the JR3)
JR3COM_FORCE_DATA_ROW_SIZE=8 #JR3 has 56 channels, so we read 8 variables * 7 channels per variable. Each channel represents a different Filter of the same variable value.

# Data conversion constants
JR3COM_LBS_TO_N=4.4482                        #/*Lbs -> Newtons */
JR3COM_DKGF_TO_N=(JR3COM_LBS_TO_N * 2.205 / 10)      #/*Deci_KGforce -> Newtons */ ### THIS IS THE DEFAULT RESULT RETURNED FROM THE JR3!
JR3COM_KLBS_TO_N=(1000 * JR3COM_LBS_TO_N)            #/*Kilo_Lbs -> Newtons */
JR3COM_IN_LBS_TO_NM=(JR3COM_LBS_TO_N * 2.54 / 100)   #/*in_Lbs -> Nm */
JR3COM_DN_M_TO_NM=(1 / 10)                    #/*Deci Nm - > Nm */
JR3COM_KGF_CM_TO_NM=(JR3COM_LBS_TO_N * 2.205 / 100)  #/*KGforce_cm -> Nm */
JR3COM_KIN_LBS_TO_NM=(1000 * JR3COM_IN_LBS_TO_NM)    #/*Kilo_in_Lbs -> Nm */  ### THIS IS THE DEFAULT RESULT RETURNED FROM THE JR3!
SIScaleFactors = [JR3COM_KLBS_TO_N,JR3COM_KLBS_TO_N,JR3COM_KLBS_TO_N,JR3COM_KIN_LBS_TO_NM,JR3COM_KIN_LBS_TO_NM,JR3COM_KIN_LBS_TO_NM,1,1]

""" Translates a single data point to the correct SI Unit. Can use with enum JR3_ScaleToSIUnits or with a raw index from 0 to 8. 
        Example:   scaleToSIUnit(JR3_ScaleToSIUnits.Fx, 0.334234) """
def scaleToSIUnit(sampleIndex, sampleValue):    
        return sampleValue * SIScaleFactors[sampleIndex]

""" Convenience method that take in a standard 7 sample x 8 variable data reading matrix and scales all 56 values to SI Units: Newtons for Force, Newton-meters for Torque """
def rescaleSampleMatrixToSIUnits(matrix):
        return [numpy.multiply(SIScaleFactors, sample) for sample in matrix]

""" Convenience method that take in a standard 1 row sample x 8 variable data reading matrix and scales all 8 values to SI Units: Newtons for Force, Newton-meters for Torque """
def rescaleSampleArrayToSIUnits(dataArray):
        return numpy.multiply(SIScaleFactors, dataArray)

""" Takes our standard 56 channel sample and returns 7 samples of 8 points each in an array """
def createSampleMatrixFromSampleArray(dataArray):
        return numpy.array(dataArray).reshape(7, 8)



""" Enum class used to label row of 8 data points read from the JR3 in order. """
class JR3_ScaleToSIUnits:
    Fx, Fy, Fz,Mx, My, Mz, V1, V2 = range(JR3COM_FORCE_DATA_ROW_SIZE) #Python version of an ENUM 


class LoadCellJR3:
    """ Opens a connection to the JR3 and zeros the inputs at start. """ 
    def __init__(self, deviceName = '/dev/comedi0'):
        self.openJR3Connection(deviceName)
        self.tareOffsetMatrixSI = self.tareJR3() #All readings after this point can be offset by our offset matrix. A different offset is used for each filter level.
        self.createTimestampedRowRequestInstructionSet() #Default init of the command set that is used to read samples of data
        
        # get as CLOSE as we can to the actual offset time between 2 devices:
        start = time.clock()
        inter = self.getTimestamp()
        end = time.clock() 
        self.loadCellToUnixSecsToAdd = inter - (start + end)/2.0       
        

    """ Dynamic value designed to remove offset values from the JR3 so that all remaining readings are 0. Offsets are stored in SI Units of Newton and Newton-meters"""
    tareOffsetMatrixSI = ([0]*JR3COM_FORCE_DATA_ROW_SIZE)*(JR3COM_NUM_DATA_CHANNEL/JR3COM_FORCE_DATA_ROW_SIZE) #Creates an 8x7 matrix of zeros for offset values    

    
    """ Filter level of data to be read out. Filter level ranges from 0 (unfiltered) to 6 (max filtered)"""
    filterLevel = 0

    """ Opens a connection to the JR3. Returns a list of channels, each of which can be queried for data. """
    def openJR3Connection(self, deviceName = '/dev/comedi0'):
        self.device = Device(deviceName)
        self.device.open()
        self.subdevice = self.device.subdevice(0)  #JR3 only has 1 subdevce
        #Generate a list of channels you wish to control.
        #Analog Configure the channels.
        self.data_channels = [self.subdevice.channel(i, factory=AnalogChannel) for i in range(0,JR3COM_NUM_DATA_CHANNEL)]
        for chan in self.data_channels:
            chan.range = chan.get_range(index=0)
        
    """ Returns array of ALL channel values scaled by max available value (NOT an SI Unit). 
        Unit returned depends on which axis: either Deci_KG for force or Kilo-IN_LBS for torque"""
    def readScaledValues(self, nsec_delay=1e3):
        values = [c.data_read_delayed(nano_sec=nsec_delay) for c in self.data_channels]
        for i in range(0, len(values)):    
            values[i] = self.data_channels[i].get_converter().to_physical(values[i])
            #values[i] = (values[i] -JR3COM_MAX_DATA_RANGE)*1.0/JR3COM_MAX_DATA_RANGE*channels[i].range.max  ##Don't need to do this by hand because information is recorded within the channel. 
        return values
    
    

    """ Samples present readings on the JR3 and creates an offset matrix that will tare all future readings. Returns an array of offsets for all 8 channels in SI Units""" 
    def tareJR3(self, samples=1000):
        sumArray = [0]*JR3COM_NUM_DATA_CHANNEL
        samplesAdded = 0        
        for i in range(0,samples):
            samplesAdded = samplesAdded + 1            
            sumArray = numpy.add(sumArray, self.readScaledValues()) 
        return rescaleSampleMatrixToSIUnits(createSampleMatrixFromSampleArray(numpy.divide(sumArray, samplesAdded)))
        
    """ Returns JR3 Timestamp in units of seconds """
    def getTimestamp(self):
        insn = self.device.insn()
        insn.insn = INSN.gtod #Use get time of day instruction.
        insn.data = [0, 0]  # space where the time value will be stored
        num_returned = self.device.do_insn(insn)
        return insn.data[0] + insn.data[1]/1.0e6
        

    """ Reads ALL 56 channels of JR3 load cell 1x and returns 7 data samples scaled to SI Units and offset by initial load cell zeroing."""    
    def getOneReadingSI(self):
        dataMatrix = rescaleSampleMatrixToSIUnits(createSampleMatrixFromSampleArray(self.readScaledValues()))
        dataMatrix = numpy.subtract(dataMatrix, self.tareOffsetMatrixSI)     
        return dataMatrix 
    
    """Unit test that sends a single instruction """
    def read1Point(self):
        insn = self.device.insn()
        insn.insn = INSN.read #Use get time of day instruction.
        insn.data = [0]  # space where the time value will be stored
        insn.chanspec = ChanSpec(chan=1)
        num_returned = self.device.do_insn(insn)
        return insn.data[0]
        
    
    """Changes the filter level of data read out by all functions that only return 1 row of data at a time. 
           FilterLevel may be any value between 0 and 6. Out of range values will raise an exception.
           This affects getOneTimestampedRow() and getOneTimestampedSIRow(). 
       It does not affect any function that returns the whole sample matrix (because the matrix contains all the filter levels anyway)."""
    def changeFilterLevel(self, filterLevel):
        self.filterLevel = filterLevel
        self.createTimestampedRowRequestInstructionSet() #Re-init command set that is used to read samples of data to read from our new filter level channels
    

    """ Creates an instruction list we can re-use every time we want to ask for data from the Load Cell. """
    def createTimestampedRowRequestInstructionSet(self):    
        blankInstrList = []
        for i in range(0, 2+ JR3COM_FORCE_DATA_ROW_SIZE): # We need X instructions: 1 start timestamp, 1*N instruction to get data sample from each channel, and 1 end timestamp
            blankInstrList.append(self.subdevice.insn())

        blankInstrList[0].insn = INSN.gtod #Use get time of day instruction.
        blankInstrList[0].data = [0, 0]  # space where the time value will be stored
        blankInstrList[-1].insn = INSN.gtod #Use get time of day instruction.
        blankInstrList[-1].data = [0, 0]  # space where the time value will be stored

        #Reads go in the middle:
        for i in range(0, JR3COM_FORCE_DATA_ROW_SIZE):
            blankInstrList[i+1].insn = INSN.read #Read a  sample.
            blankInstrList[i+1].data = [0]  # space where sample value will be stored
            blankInstrList[i+1].chanspec = ChanSpec(chan=i + self.filterLevel*JR3COM_FORCE_DATA_ROW_SIZE, range=self.data_channels[i].range, aref=self.data_channels[i].aref) #Just read all the channels in order
        
        self.timestampedDataInstructionSet = blankInstrList        

    """ Uses fast instruction list API to get a series of timestamped data sample. Returns a tuple object of a start time, a data matrix, and an end time"""
    def getOneTimestampedRow(self):
        
        # Perform instructions        
        num_returned = self.device.do_insnlist(self.timestampedDataInstructionSet)
        
        # Copy data OUT of list to return in a matrix, since it will get overwritten if it stays in our template instruction set:
        startTime = self.timestampedDataInstructionSet[0].data[0] + self.timestampedDataInstructionSet[0].data[1]/1.0e3
        endTime = self.timestampedDataInstructionSet[-1].data[0] + self.timestampedDataInstructionSet[-1].data[1]/1.0e3
        rawData = [x.data[0] for x in self.timestampedDataInstructionSet[1:-1]] #Data is returned as a tuple with ([data], labeledDataType) info. 
        return (startTime, rawData, endTime)

    """ Uses fast instruction list API to get a series of timestamped data sample. Returns a tuple object of:
    1) start time in seconds
    2) a data row labeled in Newtons or Newton-meters 
    3) end time in seconds."""
    def getOneTimestampedRowSIUnits(self, useZeroOffset= True):
        tup = self.getOneTimestampedRow()        
        scaledData = [self.data_channels[i + self.filterLevel*JR3COM_FORCE_DATA_ROW_SIZE].get_converter().to_physical(tup[1][i]) for i in range(0, JR3COM_FORCE_DATA_ROW_SIZE)]
        if useZeroOffset:        
            return (tup[0], numpy.subtract(rescaleSampleArrayToSIUnits(scaledData), self.tareOffsetMatrixSI[self.filterLevel]) , tup[2])
        else:    
            return (tup[0], rescaleSampleArrayToSIUnits(scaledData) , tup[2])
     
    def getLatestDict(self, useZeroOffset= True):
        latestLoadCell =  self.getOneTimestampedRowSIUnits(useZeroOffset= False)[1]        
        d = dict(zip(["FxNewtons", "FyNewtons", "FzNewtons" ,"Mx_Nm", "My_Nm", "Mz_Nm", 'v1', 'v2'], latestLoadCell))
        return d
              
   
    
    def __del__(self):
        self.device.close()





class LoadCellJR3UDPBroadcaster:
    """ Simple UDP Broadcaster designed to allow a separate process reading data continuously to communicate data to other processes for writing or graphing. """

    def __init__(self, host, port, alreadyRunningLoadCell = None):
        print "udp:", host, port
        self.udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM);
        self.host = host
        self.port = port
        self.packet_cnt = 0;
        self.gracefulStopRequest = False

        if alreadyRunningLoadCell != None:
            self.loadCell = alreadyRunningLoadCell
            
        else:   
            self.loadCell = LoadCellJR3();
            self.loadCell.changeFilterLevel(2)


    def broadcast(self, loadCellPackets):
        bytes = ""        
        for lcp in loadCellPackets: 
            bytes += lcp.pack(self.packet_cnt) 
            self.packet_cnt+=1    
        #print "LoadCellJR3UDPBroadcaster sent", len(bytes), "bytes"    
        self.udp_socket.sendto(bytes, (self.host, self.port))
    
    
    
    def continuouslyBroadcast(self, alreadyRunningLoadCell = None):
     
        
        while not self.gracefulStopRequest:    
            now = time.clock()
            queuedUp = []  
            while(time.clock() - now < .2):
                #Broadcast 10x per second spurts for better UDP performance:
                data = self.loadCell.getOneTimestampedRowSIUnits()
                pkt = LoadCellJR3Packet(valueList = [data[0]] + [x for x in data[1]] +  [data[2] , self.loadCell.loadCellToUnixSecsToAdd*1000])        
                queuedUp.append(pkt)           
            self.broadcast(queuedUp)
        self.udp_socket.close()        
        return

    def __del__(self):
        self.gracefulStopRequest = True

class LoadCellJR3Packet(universal_dataport.datasource_pkt):
    """ A data format designed to broadcast our load cell readings and time so that other processes can read and plot the data coming out. """
    
    def __init__(self, fieldList = ["StartReadMilSec", "FxNewtons", "FyNewtons", "FzNewtons" ,"Mx_Nm", "My_Nm", "Mz_Nm", "V1", "V2", "EndReadMilSec", "unixTimeOffsetMilli"], valueList = [None]*11): 
        if fieldList != None:
            i =0 
            self.fieldList = fieldList        
            for f in self.fieldList:
                self[f] = valueList[i]
                i+=1        
    
    @staticmethod  
    def getTimestampLabel():
        return "EndReadMilSec"  #Use the latest reading of the loadCell time as a timestamp for this packet. 
      
    def pack(self, packetIndex=0):
        values = [LoadCellJR3DataParser.SYNC_CHARS[0], LoadCellJR3DataParser.SYNC_CHARS[1], packetIndex] + [self[f] for f in self.fieldList]
        return struct.pack(">cc"+(len(values)-2)*"d", *values)

    
class LoadCellJR3DataParser(universal_dataport.datasource_parser):
    """A data parser implemented to follow the universal_dataport.py API so that other processes can parse load cell data """ 
    
    SYNC_CHARS = "!!"
    packet_len = 2 + 12*8 #2 sync char + Data points returned from JR3 are all signed 64bit, and there are ll points on each reading.
    last_valid = -1

    
    """Implementation of datasource_parser.eat_and_parse method that parses LoadCellJR3Packet packet format. """ 
    def eat_and_parse(self, buffer):
        packets = []
        ndx = buffer.find(LoadCellJR3DataParser.SYNC_CHARS, 0);
        if ndx == -1 or (len(buffer) < self.packet_len) :
            return [], buffer;
        while ndx != -1 and ndx + self.packet_len < len(buffer) :
            pkt = struct.unpack(">cc12d", buffer[ndx:ndx+self.packet_len]);
            if self.last_valid == -1:
                self.last_valid = pkt[2]
            else:    
                #Packets are produced in order, warn if we are dropping packets:
                if self.last_valid - pkt[2] > 1: #TODO Add rollover handling
                    print "DROPPED LOAD CELL DATA PACKET."
                self.last_valid = pkt[2]    
            
            p = LoadCellJR3Packet(valueList=pkt[3:]) #remove sync chars and packet index
            packets.append(p) 
            
            buffer = buffer[ndx+self.packet_len:]           
            temp_ndx = buffer.find(LoadCellJR3DataParser.SYNC_CHARS, 0);
            if temp_ndx != -1:
                ndx = temp_ndx #continue parsing
            else:
                break
        return packets, buffer


def read1PointDemo():
    loadCell = LoadCellJR3();
    while True:
        print loadCell.read1Point()


def timestampUsageDemo():
    loadCell = LoadCellJR3();
    while True:
        print "Get Timestamp Only example usage:" , loadCell.getTimestamp()

def tareZeroingDemo():
      loadCell = LoadCellJR3(); #Tared autmatically at startup
      print "View offset matrix example usage: (Newtons and Newton-meters):"
      initialOffsets = loadCell.tareOffsetMatrixSI
      for row in initialOffsets:    
          print ["%02.4f" % d for d in row]

def streamingDataDemo():
      loadCell = LoadCellJR3();
      while True:
        print "Get streaming, un-timestamped data example usage:"        
        data = loadCell.getOneReadingSI() 
        for row in data:    
            print ["%02.4f" % d for d in row]
        
def timestampedDataDemo():
     loadCell = LoadCellJR3();
     print "Get raw data sandwiched between start and end timestamps. Returned in a tuple:"
     sandwichedData =  loadCell.getOneTimestampedRow()         
     print "Get timestamped reading example:", "Start:", sandwichedData[0], "End:", sandwichedData[2], "Elapsed (millisecs):", sandwichedData[2] - sandwichedData[0] 
     print sandwichedData[1]

     print "Get timestamped reading in Newtons and Newton-meters example usage:"""
     moreSandwichedData =  loadCell.getOneTimestampedRowSIUnits()        
     
     print "Get timestamped reading example:", "Start:", moreSandwichedData[0], "End:", moreSandwichedData[2], "Elapsed (millisecs):", moreSandwichedData[2] - moreSandwichedData[0] 
     print moreSandwichedData[1]

     print "Get unix time offset example usage:"
     print "Local computer Unix time is approximately:", loadCell.loadCellToUnixSecsToAdd + loadCell.getTimestamp() 


def filterLevelDemo():
    loadCell = LoadCellJR3();
    while True:    
        print "Changing filter level example usage:"
        
        print "Filter Level 0 (No filtering):"
        loadCell.changeFilterLevel(0)
        for i in range(0,5):
            row =  loadCell.getOneTimestampedRowSIUnits()[1]        
            print ["%02.4f" % d for d in row]
        loadCell.changeFilterLevel(2)
            
        print "Filter Level 2:"
        for i in range(0,5):
            row =  loadCell.getOneTimestampedRowSIUnits()[1]        
            print ["%02.4f" % d for d in row]
        loadCell.changeFilterLevel(4)
        
        print "Filter Level 4:"
        for i in range(0,5):
            row =  loadCell.getOneTimestampedRowSIUnits()[1]        
            print ["%02.4f" % d for d in row]
        
        loadCell.changeFilterLevel(6)
        print "Filter Level 6 (Max filtering):"
        for i in range(0,5):
            row =  loadCell.getOneTimestampedRowSIUnits()[1]        
            print ["%02.4f" % d for d in row]
        



""" Example usage of most useful library functions. """
if __name__ == "__main__":
    #read1PointDemo()
    #timestampUsageDemo()
    #tareZeroingDemo()
    streamingDataDemo()   
    #timestampedDataDemo()   
    #filterLevelDemo()   


        
        
       

        

        




