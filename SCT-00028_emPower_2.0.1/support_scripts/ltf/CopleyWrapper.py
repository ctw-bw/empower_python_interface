#!/usr/bin/env python
""" Python class wrapper for  Copley Motion Library and LTFPy.so to make it easy to use for iWalk Life Test Fixture experiments. """
import struct, time, sys, socket, thread, os

import python_paths_default
import python_paths
import numpy
#Compiled .so object. See http://svn.iwalk.com/svn/svn_engineering/PowerFoot/branches/abolger/LTF/usr_local_lib_x64_fedora/Copley/CML/examples/server/src/ServerPython.cpp for CopleyWrapperCpp interface. 

import CopleyWrapperCpp 

# BiOM trunk support scripts library: http://svn.iwalk.com/svn/svn_engineering/PowerFoot/trunk/H22_release_A/support_scripts
import comm_lib.universal_dataport as universal_dataport




DEBUG_LEVEL = 0

def debug(printString):
    global DEBUG_LEVEL
    if DEBUG_LEVEL > 2:
        print printString

def verbose(printString):
    global DEBUG_LEVEL
    if DEBUG_LEVEL > 1:
        print printString


COPLEY_ENCODER_COUNTS_P_REV = 800*8.0/3 #Copley motor has 800 counts per rev * gear ration of 8/3

""" Interface to C++ library to control the Copley motor. 
"""
class MotorController():
    def __init__(self, copley_node_id = 1, max_sample_queue = 1024, sample_rate_hz = 5000, manualZeroing = False, amplifierIndex = 0):
        tmp_ecat_conn = CopleyWrapperCpp.MoveEcat("eth1", copley_node_id)
        tmp_ecat_conn.InitAmp()
        tmp_amplifier = tmp_ecat_conn.getAmp(amplifierIndex)
        
        self.zeroOffset = 0
        #TODO: set this up automatically somehow?
        
        tmp_amplifier.setPositionMotor(0)
        print "Raw motor position is:", tmp_amplifier.getPositionMotor()
        tmp_ecat_conn.close() #This frees up the motor to move freely.
        if manualZeroing:
            raw_input("Manually zero the ball screw with the BiOM. Press any key to continue.")
    
        self.ecat_conn = CopleyWrapperCpp.MoveEcat("eth1", copley_node_id)
        self.ecat_conn.InitAmp()
        self.amplifier = self.ecat_conn.getAmp(amplifierIndex)
        self.amplifierIndex = amplifierIndex
        

        self.params = CopleyWrapperCpp.MotorOperationParams.create() #See MotorOperationParams.hpp for list of all available parameters  
        self.params.setParam("initial_position", self.zeroOffset)
        self.params.setParam("final_position", self.zeroOffset)
        self.params.setParam("accel", 10.0)
        self.params.setParam("decel", 10.0)
        self.params.setParam("rpm", 0)
        self.params.setParam("passes", 1)
        self.params.setParam("mode", "p2p")

        self.ecat_conn.setParams(self.params)
        
        print "Raw motor position is:", self.readRawMotorPosition()
        self.setZeroMotorPosition() #Declare this new position to be 0
        

        
        #Record a system timestamp offset delay so that we can make sure when we send timestamped data out using python that the values can be synced to other data. 
        start = time.clock()
        inter = self.params.getSystemTimestampUSecs()/1000.0/1000.0
        end = time.clock() 
        self.posixTimeToUnixSecsToAdd = inter - (start + end)/2.0  
        print self.posixTimeToUnixSecsToAdd
     
    
    def readMotorStatusTimestamped(self, rawValues = False):
        status = self.ecat_conn.getLatestMotorStatus()
        d = dict([(k, getattr(status,k)) for k in dir(CopleyWrapperCpp.MotorStatus) if not k.startswith("__")])
        if rawValues:
            return d
        else:
            d['position'] = d['position'] - self.zeroOffset
            return d         

    def setZeroMotorPosition(self):
        self.zeroOffset = self.readRawMotorPosition()
 
    def moveToZero(self, blockWhileMoving = True):
        self.moveAbsoluteUsingProfile(0,blocking = blockWhileMoving)
    
    def setMotorRPM(self, rpm = 0):
        self.params.setParam("rpm", rpm)
        self.ecat_conn.setParams(self.params)
        

    def isMoving(self):
        return self.ecat_conn.isMoving()    
    
    def isStopped(self):
        return self.ecat_conn.isStopped()
 
    
    def start(self, initialRPM = 0):
        self.params.setParam("rpm", initialRPM)
        self.ecat_conn.setParams(self.params)
        
        self.ecat_conn.start()    
        self.ecat_conn.enable() #enable amplifier.     
        result = self.moveAbsoluteRawPosition(self.readRawMotorPosition(), blocking = True, threshold = 1) #TODO: THIS IS A HACK. It servoes to "where we are" to zero out any old saved trajectory information.     
        debug(result)

    def stop(self):
        tmp = sys.stderr #ecat always reports an error message on close so we'll just log it. 
        sys.stderr = open('err_log.txt', 'w')        
        self.ecat_conn.disable() #disable amplifier
        self.ecat_conn.stop()
        self.ecat_conn.close()
        
    def destroy(self):
        del(self)    

    def readRawMotorPosition(self):
        return self.amplifier.getPositionMotor()
    
    def readMotorPosition(self):
        return self.amplifier.getPositionMotor() - self.zeroOffset
            

    def moveAbsoluteUsingProfile(self, goalPosition, velocityRPM = 55, blocking = True, accelRadPSec2 = 10, decelRadPSec2=10, threshold = 1):
        """Move the Copley motor to goalPosition. Moves at speed velocityRPM and acceleration/deceleration values accelRadPSec2 and decelRadPSec2. If 'blocking' is set to "True", function waits until Motor is within 'threshold' counts of the goal position to return. Otherwise function returns immediately. """
        resultCode = self.amplifier.moveUsingProfile(accelRadPSec2, decelRadPSec2, velocityRPM, self.zeroOffset + goalPosition, False) #double accelRadPSec2, double decelRadPSec2, uunit velocityRPM, uunit goalPosition, bool relative
        
        if not blocking: return resultCode
        else:
            startPosition = self.amplifier.getPositionMotor()        
            while ( abs(self.readMotorPosition() - goalPosition) > threshold):
                verbose(["Present:", self.readMotorPosition(),"Goal:", goalPosition])
                verbose(["Velocity:",self.readMotorStatusTimestamped()['velocity']])             
                #TODO put timeout logic here.                 
                #continue
                time.sleep(.0005) #Sleep for .5 milliseconds is mostly to keep thread from hogging more processing power than it needs to since we can't read motor position anywhere near 2KHz            

    
    def moveRelativeUsingProfile(self, goalDistance, velocityRPM = 55, blocking = True, accelRadPSec2 = 10, decelRadPSec2=10, threshold = 1):
        """Move the Copley motor to goalPosition. Moves at speed velocityRPM and acceleration/deceleration values accelRadPSec2 and decelRadPSec2. If 'blocking' is set to "True", function waits until Motor is within 'threshold' counts of the goal position to return. Otherwise function returns immediately. """
        goalPosition = self.readMotorPosition() + goalDistance
        resultCode = self.amplifier.moveUsingProfile(accelRadPSec2, decelRadPSec2, velocityRPM, goalDistance, True) #double accelRadPSec2, double decelRadPSec2, uunit velocityRPM, uunit goalPosition, bool relative
        
        if not blocking: return resultCode
        else:
            startPosition = self.amplifier.getPositionMotor()        
            while ( abs(self.readMotorPosition() - goalPosition) > threshold):
                verbose(["Present:", self.readMotorPosition(),"Goal:", goalPosition])
                verbose(["Velocity:",self.readMotorStatusTimestamped()['velocity']])             
                #TODO put timeout logic here.                 
                #continue
                time.sleep(.0005) #Sleep for .5 milliseconds is mostly to keep thread from hogging more processing power than it needs to since we can't read motor position anywhere near 2KHz    



    def moveRelativeRawPosition(self, goalDistance, blocking = True, threshold = 1):
        """Move the Copley motor a certain distance in raw motor units (no scaling or zero offset applied). Moves to initial pos + goalDistance. Moves at whatever speed and acceleration values have already been programmed into the Copley profile. If 'blocking' is set to "True", function waits until Motor is within 'threshold' counts of the goal position to return. Otherwise function returns immediately. """

        startPosition = self.amplifier.getPositionMotor()        
        resultCode = self.amplifier.movePosition(goalDistance, True)
        if not blocking: return resultCode
        else:
            while ( abs(self.readRawMotorPosition() - (startPosition + goalDistance)) > threshold):
                verbose(["Present:", self.readRawMotorPosition(),"Goal:",startPosition + goalDistance])
                verbose(["Velocity:",self.readMotorStatusTimestamped()['velocity']])             
                #TODO put timeout logic here.                 
                time.sleep(.0005) #Sleep for .5 milliseconds is mostly to keep thread from hogging more processing power than it needs to since we can't read motor position anywhere near 2KHz        
            

    def moveAbsoluteRawPosition(self, goalPosition, blocking = True, threshold = 1):
        """Move the Copley motor to a certain position in raw motor units (no scaling or zero offset applied). Moves to goalPosition. Moves at whatever speed and acceleration values have already been programmed into the Copley profile. If 'blocking' is set to "True", function waits until Motor is within 'threshold' counts of the goal position to return. Otherwise function returns immediately. """
        resultCode = self.amplifier.movePosition(goalPosition, False)
        if not blocking: return resultCode
        else:
            while ( abs(self.readRawMotorPosition() - goalPosition) > threshold):
                time.sleep(.0005) #Sleep for .5 milliseconds is mostly to keep thread from hogging more processing power than it needs to since we can't read motor position anywhere near 2KHz                            
            

    def runSawtoothProfile(self, speed = 85, goalStart = 0, goalFinish = -500, passes = 1, accel = 10, decel=10, dwellTime = 0):
        """Convenience method that will run the motor back and forth between encoderStart and encoderFinish for "passes" number of times."""
        self.moveAbsoluteUsingProfile(goalStart, velocityRPM = speed, blocking = True, accelRadPSec2 = accel, decelRadPSec2=decel, threshold = 1)
        for i in range(0, passes):
            time.sleep(dwellTime)       
            self.moveAbsoluteUsingProfile(goalFinish, velocityRPM = speed, blocking = True, accelRadPSec2 = accel, decelRadPSec2=decel, threshold = 1)
            time.sleep(dwellTime)    
            self.moveAbsoluteUsingProfile(goalStart, velocityRPM = speed, blocking = True, accelRadPSec2 = accel, decelRadPSec2=decel, threshold = 1)
            
    def runSinusoidalProfile(self, amplitudeEncoderCounts = 200, frequencyHz = 1, passes = 1):
        raise Exception("Sinusoid not implemented!")        
        #pass
            

    def __del__(self):
        try: #Make sure everything that wasn't already explicitly closed gets closed.
            self.ecat_conn.close()        
            pass        
        except:
            pass 
            



class CopleyMotorUDPBroadcaster:
    """ Simple UDP Broadcaster designed to allow a separate process reading data continuously to communicate data to other processes for writing or graphing. """

    def __init__(self, host, port, motor):
        print "udp:", host, port
        self.udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM);
        self.host = host
        self.port = port
        self.packet_cnt = 0;
        self.gracefulStopRequest = False

        self.motor = motor
            


    def broadcast(self, motorStatusPackets):
        bytes = ""        
        for pkt in motorStatusPackets: 
            bytes += pkt.pack(self.packet_cnt) 
            self.packet_cnt+=1    
        if len(bytes) > 0:         
            verbose(["MotorUDPBroadcaster sent", len(bytes), "bytes"])    
            self.udp_socket.sendto(bytes, (self.host, self.port))
    
    
    
    def continuouslyBroadcast(self):
        while not self.gracefulStopRequest:    
            now = time.clock()
            queuedUp = []  
            lastStatusRead = self.motor.readMotorStatusTimestamped()
            while(time.clock() - now < .1):

                #Broadcast 10x per second spurts for better UDP performance:
                s = self.motor.readMotorStatusTimestamped()
                #Only add a new data packet if some chunk of our position or motor current data has been updated:
                if (lastStatusRead['posVelTimestamp'] != s['posVelTimestamp']) or (lastStatusRead['motCurrentTimestamp'] != s['motCurrentTimestamp']) or (lastStatusRead['motVoltageTimestamp'] !=  s['motVoltageTimestamp']):
                    lastStatusRead = s                    
                    pkt = CopleyMotorPacket(s.values())        
                    queuedUp.append(pkt)           
                self.broadcast(queuedUp)
        
        self.udp_socket.close()        
        return

    def __del__(self):
        self.gracefulStopRequest = True

class CopleyMotorPacket(universal_dataport.datasource_pkt):
    """ A data format designed to broadcast our load cell readings and time so that other processes can read and plot the data coming out. """
    fieldList = [k for k in dir(CopleyWrapperCpp.MotorStatus) if not k.startswith("__")] #All attributes of MotorStatus object automatically become data fields.

    def __init__(self, data): 
            i =0 
            for f in CopleyMotorPacket.fieldList:
                self[f] = data[i]
                i+=1        
            
    @staticmethod  
    def getTimestampLabel():
        return "posVelTimestamp"  #Use the latest reading of the position and velocity time as a timestamp for this whole packet. 
      
    def pack(self, packetIndex=0):
        values = [CopleyMotorDataParser.SYNC_CHARS[0], CopleyMotorDataParser.SYNC_CHARS[1], packetIndex] + [self[f] for f in CopleyMotorPacket.fieldList]
        return struct.pack(">cc"+(len(values)-2)*"d", *values)

    
class CopleyMotorDataParser(universal_dataport.datasource_parser):
    """A data parser implemented to follow the universal_dataport.py API so that other processes can parse load cell data """ 
    
    SYNC_CHARS = "!!"
    packet_len = 2 + 11*8 #2 sync char + packet index + just tread data points returned from Copley as if they are all signed 64bit, and there are 7 values + 3 timestamps each.
    last_valid = -1

    
    """Implementation of datasource_parser.eat_and_parse method that parses CopleyMotorPacket packet format. """ 
    def eat_and_parse(self, buffer):
        packets = []
        ndx = buffer.find(CopleyMotorDataParser.SYNC_CHARS, 0);
        if ndx == -1 or (len(buffer) < CopleyMotorDataParser.packet_len) :
            return [], buffer;
        while ndx != -1 and ndx + CopleyMotorDataParser.packet_len < len(buffer) :
            pkt = struct.unpack(">cc11d", buffer[ndx:ndx+ CopleyMotorDataParser.packet_len]);
            if self.last_valid == -1:
                self.last_valid = pkt[2]
            else:    
                #Packets are produced in order, warn if we are dropping packets:
                if self.last_valid - pkt[2] > 1: #TODO Add rollover handling
                    debug("DROPPED COPLEY MOTOR DATA PACKET.")
                self.last_valid = pkt[2]    
            
            p = CopleyMotorPacket(pkt[3:]) #remove sync chars and packet index
            packets.append(p) 
            
            buffer = buffer[ndx+self.packet_len:]           
            temp_ndx = buffer.find(CopleyMotorDataParser.SYNC_CHARS, 0);
            if temp_ndx != -1:
                ndx = temp_ndx #continue parsing
            else:
                break
        return packets, buffer



"""Sample demo code for broadcasting all information about a running motor over UDP."""
def broadCastMotorStatusDemo():
    global DEBUG_LEVEL 
    DEBUG_LEVEL = 5    
    motor = MotorController()
    motor.start(0)
    motorData = CopleyMotorUDPBroadcaster("localhost", 8888, motor)
    t = thread.start_new_thread ( motorData.continuouslyBroadcast, ())

    start = time.time()
    while (time.time() - start < 10): #Broadcast for 10seconds
        continue
    



"""Sample demo code for controlling the Copley motor using encoder position."""
def positionControlDemo():
    motor = MotorController()
    motor.start(85)
    while True:
        try:     
                distance = int(raw_input("Enter in value from +- 10000 to move that distance:"))
                print motor.readMotorPosition(), motor.readRawMotorPosition(), motor.zeroOffset
                print "Motor is moving?", motor.isMoving() 
                print "Motor is stopped?", motor.isStopped()   
                motor.moveRelativeRawPosition(distance, blocking = True, threshold = 1)
                
        except Exception as e:
                print e
                motor.stop()   

""" Get time values for how long it takes to get non-blocking comm from motor. """
def motorCommunicationTimeDemo():
    import inspect    
    motor = MotorController()
    motor.start(55)
    print motor.readMotorPosition(), motor.readRawMotorPosition(), motor.zeroOffset
             
    #Demonstrate slow read when getting amplifier settings:
    start = time.time()
    motor.amplifier.getPositionMotor()
    end = time.time()
    print (end - start) * 1000 , "milliseconds to get confirmed motor pos."
    lastTimestamp1 = 0
    lastTimestamp2 = 0
    lastTimestamp3 = 0
    
    while True:
        s = motor.readMotorStatusTimestamped()
        if (lastTimestamp1 != s['posVelTimestamp']):
            print (s['posVelTimestamp'] - lastTimestamp1)/1000.0, "milliseconds between Position/Velocity data updates."
            print s
            lastTimestamp1 = s['posVelTimestamp'] #Update whenever we get fresh data.

        if (lastTimestamp2 != s['motCurrentTimestamp']):
            print (s['motCurrentTimestamp'] - lastTimestamp2)/1000.0, "milliseconds between Current data updates."
            print s
            lastTimestamp2 = s['motCurrentTimestamp'] #Update whenever we get fresh data.

        if (lastTimestamp3 != s['motVoltageTimestamp']):
            print (s['motVoltageTimestamp'] - lastTimestamp3)/1000.0, "milliseconds between Voltage data updates."
            print s
            lastTimestamp3 = s['motVoltageTimestamp'] #Update whenever we get fresh data.



"""Expose available members and functions of CPP library."""
def printAPI():
    print "CopleyWrapperCpp:", [k for k in dir(CopleyWrapperCpp) if not k.startswith("__")]
    print "CopleyWrapperCpp.Amplifier:", [k for k in dir(CopleyWrapperCpp.Amplifier) if not k.startswith("__")]
    print "CopleyWrapperCpp.MotorStatus:", [k for k in dir(CopleyWrapperCpp.MotorStatus) if not k.startswith("__")]
    print "CopleyWrapperCpp.MotorOperationParams:", [k for k in dir(CopleyWrapperCpp.MotorOperationParams) if not k.startswith("__")]
    

def motorUpDownRepeatDemo():
    global DEBUG_LEVEL 
    DEBUG_LEVEL = 5
    motor = MotorController()
    motor.start(0)
    motor.runSawtoothProfile(speed = 50, goalStart = 0, goalFinish = -2000, passes = 3)
     

""" Example usage of most useful library functions. """
if __name__ == "__main__":
    positionControlDemo()
    #motorCommunicationTimeDemo()
    #printAPI()   
    #motorUpDownRepeatDemo()
    #broadCastMotorStatusDemo()
