import DataBorgLTF, CopleyWrapper, LoadcellJR3, LtfTrigGeomUtil
import thread, sys, time, collections
import comm_lib.universal_dataport as universal_dataport
import numpy
import matplotlib.pyplot as plt


#P2V:  Position to Voltage controller on the BiOM. Commands a motor encoder position and picks the voltage that will get us there. 
CMD_SETUP_TORQUE_CONTROLLER_TEST = [1599] #Set up Torque to P2V test. Uses Motor State 58. Uses Motor mode 9
CMD_COMMAND_TQ_NM_DELTA = [1607, 0]  #P2V torque to position to voltage instrumentation command. 1st variable is desired torque offset in milliNewton Meters. Defaults to 0 Nm
CMD_READOUT_P2V_CONTROLLER_SETTINGS = [1604] #Request present values of P2V controller: p2v.max_qVq, p2v.K_shift_right, p2v.max_i_err, p2v.max_k_err, p2v.max_err, p2v.Kp, p2v.Ki,  p2v.max_encoder,  p2v.min_encoder,  p2v.max_diff
    


"""
Commands BiOM into torque controller mode and commands it to move freely (0 torque offset).  Commands Copley to move in a "chirp" cycle (sinusoid of increasing frequency) to measure response.
"""
class BiOMTorqueControllerChirpTest(DataBorgLTF.LTFPlottableTest):
       
       
    UPD_CURRENT_SCALE_R300 = 866.0 #Motor current scale factor.
    
    varLabelsToPlot = [ "BiOM Joint Torque (Nm) from LoadCell", "BiOM Pos Err", "BiOM Joint Angle Deg","MC_encoder", "Copley Enc Pos", "BiOM qIq"] #"Copley Enc RPM", "BiOM qVq", ]  
    plotStyles = dict(zip(varLabelsToPlot, ['r','g','b','c','m', 'r-'])) 
    scaleFactor = dict(zip(varLabelsToPlot, [1.0, 1.0, 100.0,1.0, 1.0, 1000.0]))
    offsetFactor = dict(zip(varLabelsToPlot, [0,0,0,0,0,0]))
    
    
    def performTest(self, encoderStepMagnitude = 1000, encoderOffset = -4000,  startingVelRPM = 2500, accel = 200, freqRangeToTestHz = [1,2,3,4,5,6,7,8,9,10,15,20,25,30,35,40,45,50,60,70,80,90,100]): #Specify what frequency our chirp should start and end at
        
        
        self.motor.moveToZero(); #Hold Copley in position to start with. 
             
        self.prepBiOMForTrajectoryControl() #Disable normal BiOM operation but leave motor running. 
        
        try:
            print self.biomCommand([CMD_SETUP_TORQUE_CONTROLLER_TEST]) #Put BiOM in Zero-torque (frictionless) mode.
        except:
            print "WARNING: got no response back once BiOM set in torque mode..."        

        print "Setting desired torque of torque controller to: %d milli-NM" % CMD_COMMAND_TQ_NM_DELTA[1]
        
        
        #TODO: find a way to automate this: basically if BiOM didn't boot up with correct MC reading, the zero torque control doesn't work..."
        while (self.biomTelemetryHandler.peek_last_received() == None): 
            pass #Wait until the rest of the test is hooked up and ready before continuing.

        mc_encoder_now = self.biomTelemetryHandler.peek_last_received()["p2v_encoder_actual"]
        continueIfCorrectSetup = raw_input("Motor controller encoder reads: %d counts. Continue with test (y/n)?" % mc_encoder_now) 
        if continueIfCorrectSetup != 'y':
            return

        self.logData("init_settings", {"mc_encoder_at_startup":  mc_encoder_now , "copleyOffsetPos":encoderOffset, "copleyAmplPos": 2*encoderStepMagnitude, "copleyVelRPM":startingVelRPM, "freqRangeTested": "'" + ",".join([str(i) for i in freqRangeToTestHz]) + "'" })
        self.logData("p2v_controller_settings", dict(self.biomCommand([CMD_READOUT_P2V_CONTROLLER_SETTINGS])[0]))

        
        repsPerFreq = 10  #Change freq after 3 full square waves at each listed frequency.

        testingIndexDiv = 0
   
        #Startup where we choose
        self.motor.moveAbsoluteUsingProfile( encoderOffset, velocityRPM = startingVelRPM, blocking = True, accelRadPSec2 = accel, decelRadPSec2=accel, threshold = 1)

        while testingIndexDiv < len(freqRangeToTestHz)*repsPerFreq:
            try:   
                sqWavePeriod = 1.0/freqRangeToTestHz[testingIndexDiv/repsPerFreq]  
                testingIndexDiv +=1
                            
         
                time.sleep(sqWavePeriod/2.0)        
                self.motor.moveAbsoluteUsingProfile(-1*encoderStepMagnitude + encoderOffset, velocityRPM = startingVelRPM, blocking = False, accelRadPSec2 = accel, decelRadPSec2=accel, threshold = 1)
                time.sleep(sqWavePeriod/2.0)        
                self.motor.moveAbsoluteUsingProfile(encoderStepMagnitude + encoderOffset, velocityRPM = startingVelRPM, blocking = False, accelRadPSec2 = accel, decelRadPSec2=accel, threshold = 1)

                print sqWavePeriod
                

            except Exception as e:
                print e
                self.motor.moveToZero()
                break
            


    def ongoingDashboard(self):
       d = dict()
       if self.motor:
            d["Copley Enc Pos"] = self.motor.readMotorStatusTimestamped()['position']
            
       if self.loadCell:
            row =  self.loadCell.getOneTimestampedRowSIUnits(useZeroOffset= False)[1]        
            d2 = dict(zip(["FxNewtons", "FyNewtons", "FzNewtons" ,"Mx_Nm", "My_Nm", "Mz_Nm", 'v1', 'v2'], row))
            tqNM = LtfTrigGeomUtil.ConvertLoadCellForceBiOMJointTorque(d2["FzNewtons"], d2["FyNewtons"])
            d["BiOM Joint Torque (Nm) from LoadCell"] = tqNM
    
       if self.biomTelemetryHandler:
            data = self.biomTelemetryHandler.peek_last_received();
                        
            if data != None and "raw_hall_sensor" and "qIq" in data.keys():                
                d["BiOM Joint Angle Deg"] = data["raw_hall_sensor"]/8192.0 * 360.0
                d["BiOM qIq"] = data["qIq"]/BiOMTorqueControllerChirpTest.UPD_CURRENT_SCALE_R300 
                d["BiOM Pos Err"] = data["p2v_err"] 
                d["MC_encoder"] = data["p2v_encoder_actual"]
            elif data != None:  
                d = d + data #Just get whatever data is coming out of the BiOM.
            if data == None: 
                print "Wififast data not coming through."       

       d["endReadTimeSec"] =  DataBorgLTF.millis()/1000.0 #Put timestamp in our dictionary
       return d


       
def makePlot(label):
    plt.plot([])
    plt.ylabel(label)
        
    return plt


if __name__ == "__main__": 
    options =  DataBorgLTF.DataBorgOptions(sys.argv).values;
    myTest = BiOMTorqueControllerChirpTest(biomCommandComport = options.biom_com_port, biomTelemetryComport = options.telemetry_com_port, biomTelemetryDataportNum = 0, biomTelemetryVlist = "dataport0_posToVoltageController")
   
   
    myPlot = makePlot("JR3 and BiOM Torque Measurement")
    
    thread.start_new_thread(myTest.performTest, ())    
    thread.start_new_thread(myTest.dataReadThread, ())    
    thread.start_new_thread(myTest.plotUpdateThread, (myPlot,))

    myPlot.show()
    myTest.destroyFramework()

            
