import DataBorgLTF, CopleyWrapper, LoadcellJR3, LtfTrigGeomUtil
import thread, sys, time, collections
import comm_lib.universal_dataport as universal_dataport
import numpy
import matplotlib.pyplot as plt


       

"""Commands Copley to move in a square wave using a profile and watches the JR3 load cell torque response. Plots JR3 torque vs ankle angle encoder to show BiOM work loop.
""" 
class CopleyStepWorkLoopTest(DataBorgLTF.LTFPlottableTest):
    varLabelsToPlot = ["Joint Torque (Nm) from LoadCell", "BiOM Joint Torque (Nm) from BiOM"]  
    plotStyles = dict(zip(varLabelsToPlot, ['b-', 'g.'])) 
    scaleFactor = dict(zip(varLabelsToPlot, [1.0, 1.0]))
    offsetFactor = dict(zip(varLabelsToPlot, [0,0]))
    
    timeAxisLabel = "Ankle Joint Encoder Counts"
    timestampDataLabel = "Raw AAE"
        
    def __init__(self, biomCommandPort = None, biomTelemetryPort = None):
        DataBorgLTF.LTFPlottableTest.__init__(self, biomCommandComport = biomCommandPort, biomTelemetryComport = biomTelemetryPort)    


        self.timeAxisLabel = CopleyStepWorkLoopTest.timeAxisLabel
        self.timestampDataLabel = CopleyStepWorkLoopTest.timestampDataLabel

    
    def performTest(self, encoderStepMagnitude = 6000, startingVelRPM = 50, accel = 10):
        self.motor.moveToZero(); #Start at zero.        
        
        for i in range(1,10):
            try:
                #Move far into hardstop                
                self.motor.moveAbsoluteUsingProfile(abs(encoderStepMagnitude), velocityRPM = startingVelRPM, blocking = False, accelRadPSec2 = accel, decelRadPSec2=accel, threshold = 1)
                time.sleep(15.0)       
                #Move back out.  
                self.motor.moveAbsoluteUsingProfile(-500, velocityRPM = startingVelRPM, blocking = False, accelRadPSec2 = accel, decelRadPSec2=accel, threshold = 1) 
                time.sleep(15.0)        
                
            except Exception as e:
                print e
                self.motor.moveToZero()
                break
        self.finished = True

    
    def ongoingDashboard(self):
       d = dict() 
       d["startReadTimeSec"] =  DataBorgLTF.millis() #Put timestamp in our dictionary
       if self.motor:
            motor_d = self.motor.readMotorStatusTimestamped()
            d["Copley Encoder Pos"] = motor_d['position']
            d["Copley Encoder Vel Rpm"] = motor_d['velocity']
       if self.loadCell:
            row =  self.loadCell.getOneTimestampedRowSIUnits(useZeroOffset= False)[1]        
            d2 = dict(zip(["FxNewtons", "FyNewtons", "FzNewtons" ,"Mx_Nm", "My_Nm", "Mz_Nm", 'v1', 'v2'], row))
            tqNM = LtfTrigGeomUtil.ConvertLoadCellForceBiOMJointTorque(d2["FzNewtons"], d2["FyNewtons"])
            d["Joint Torque (Nm) from LoadCell"] = tqNM
    
       if self.biomTelemetryHandler:
            data = self.biomTelemetryHandler.peek_last_received();
            if data != None and "estimated_torque" and "raw_hall_sensor" in data.keys():                
                d["BiOM Joint Angle Deg"] = data["raw_hall_sensor"]/8192.0 * 360.0
                d["Raw AAE"] = data["raw_hall_sensor"]
                d["MC_encoder"] = data["MC_encoder"]
                d["BiOM Joint Torque (Nm) from BiOM"] = data["estimated_torque"]
       d["endReadTimeSec"] =  DataBorgLTF.millis() #Put timestamp in our dictionary 
       return d










def makePlot(label):
    plt.plot([])
    plt.ylabel(label)
        
    return plt




if __name__ == "__main__": 
    options =  DataBorgLTF.DataBorgOptions(sys.argv).values; 
    myTest = CopleyStepWorkLoopTest(biomCommandPort = options.biom_com_port, biomTelemetryPort = options.telemetry_com_port)
    
    myTest.prepBiOMForTestBench()
    
    
    myPlot = makePlot("Copley running into BiOM Hardstop vs JR3 Torque Measurements")
    
        
 
    thread.start_new_thread(myTest.performTest, (2000, 30, 10,))    
    thread.start_new_thread(myTest.dataReadThread, ())    
    thread.start_new_thread(myTest.plotUpdateThread, (myPlot,))

    myPlot.show()
    myTest.destroyFramework()




