import DataBorgLTF, CopleyWrapper, LoadcellJR3, LtfTrigGeomUtil
import thread, sys, time, collections
import comm_lib.universal_dataport as universal_dataport
import numpy
import matplotlib.pyplot as plt

class CycleCopleyTest(DataBorgLTF.LTFTestFramework):
    
    def performTest(self, repeatCnt):
        self.motor.runSawtoothProfile(speed = 50, goalStart = -5000, goalFinish = 2000, passes = repeatCnt)
        self.motor.moveToZero()




"""Commands a square wave of current from the BiOM and plots the resulting current vs commanded current and measured torques"""
class CurrentStepTest(DataBorgLTF.LTFPlottableTest):
    
    UPD_CURRENT_SCALE_R300 = 866 #Motor current scale factor.
    commandedCurrent = 0
    
    varLabelsToPlot = ["qIq", "BiOM Joint Torque (Nm) from LoadCell", "Copley Enc Pos", "BiOM Joint Torque (Nm) from BiOM"] #,"Copley Enc RPM" , commandedCurrent","MC_encoder",   
    plotStyles = dict(zip(varLabelsToPlot, ['r','b','c', 'm--'])) 
    scaleFactor = dict(zip(varLabelsToPlot, [1.0, 1.0,1, 1.0]))
    offsetFactor = dict(zip(varLabelsToPlot, [0,0,0,0]))
     
   

    def setBiOMCurrent(self, amps=0.0, mode =3 ) :
        raise Exception("NOT CONVERTED TO TANGO CODE YET!")
        self.commandedCurrent = amps        
        #print "Set mode %1d with %2.1f amps" % (mode, amps)
        v1 = int(amps * CurrentStepTest.UPD_CURRENT_SCALE_R300);
        result = self.biomCommand([[1115, v1, 0, 3, 0]]); # use special command 1115 to set motor mode and current
        
    def performTest(self, motorMagAmps = 2.0):
        self.motor.moveToZero(); #
        while True:
            self.setBiOMCurrent(4.0)
            time.sleep(1.0)
            self.setBiOMCurrent(10.0)
            time.sleep(1.0)
            #print "Commanded:", self.commandedCurrent

    maxBiOMTq = -800000000;
    maxJR3Tq = -8000000000;
    minBiOMTq = 800000000;
    minJR3Tq = 8000000000;
    
    
    def dataReadThread(self):
        self.stillLogging = True;
        while (self.stillLogging):
                d = self.ongoingDataCollection();
                self.logData("ALL_DATA", d) #log all data. Note that dataReadThread() can be overridden if we want more or less data. 
        self.stillLogging = False
        if hasattr(self, "logfile"): self.logfile.close()
    
    def ongoingDataCollection(self):
        d = dict() 
        d["startReadTimeMilliSec"] =  DataBorgLTF.millis() #Put timestamp in our dictionaryt 

        d["commandedCurrent"] = self.commandedCurrent        
        if self.motor:
            d["Copley Enc Pos"] = self.motor.readMotorStatusTimestamped()['position']
            d["Copley Enc RPM"] = self.motor.readMotorStatusTimestamped()['velocity']
                
        if self.loadCell:
            row =  self.loadCell.getOneTimestampedRowSIUnits(useZeroOffset= False)[1]        
            d2 = dict(zip(["FxNewtons", "FyNewtons", "FzNewtons" ,"Mx_Nm", "My_Nm", "Mz_Nm", 'v1', 'v2'], row))
            tqNM = LtfTrigGeomUtil.ConvertLoadCellForceBiOMJointTorque(d2["FzNewtons"], d2["FyNewtons"])
            d["BiOM Joint Torque (Nm) from LoadCell"] = tqNM
            self.maxJR3Tq = max(tqNM, self.maxJR3Tq)    
            self.minJR3Tq = min(tqNM, self.minJR3Tq) 
              

        if self.biomTelemetryHandler != None:
            data = self.biomTelemetryHandler.peek_last_received();
            if data != None and "estimated_torque" and "raw_hall_sensor" in data.keys():                
                d["BiOM Joint Angle Deg"] = data["raw_hall_sensor"]/8192.0 * 360.0
                d["BiOM Joint Torque (Nm) from BiOM"] = data["estimated_torque"]
                d["qIq"] = data["qIq"] 
                d["MC_encoder"] = data["MC_encoder"]
                self.maxBiOMTq = max(d["BiOM Joint Torque (Nm) from BiOM"], self.maxBiOMTq)    
                self.minBiOMTq = min(d["BiOM Joint Torque (Nm) from BiOM"], self.minBiOMTq) 
                
        
            d["Scaled BiOM Tq"] = ((d["BiOM Joint Torque (Nm) from BiOM"] + (self.maxJR3Tq + self.minJR3Tq)/2.0 - (self.maxBiOMTq + self.minBiOMTq)/2.0)* (self.maxJR3Tq - self.minJR3Tq))/(self.maxBiOMTq - self.minBiOMTq + .0000000000001)  #Make midpoints of both square waves the same, then scale both to same magnitude
              

        d["endReadTimeMilliSec"] =  DataBorgLTF.millis() #Put timestamp in our dictionaryt 
                    

        return d
        





"""Commands a ramp of current from the BiOM and plots the LoadCell torque response"""
class CurrentRampTest(CurrentStepTest):
    def performTest(self):
        self.motor.moveToZero(); #
                
        i = 0
        direction = 1
        stepSizeAmps = .020 #20 milli amp step * command rate @ 25 Hz => .5A per second rampup
        rampMagAmps = 2        
        last = DataBorgLTF.millis()

        while True:
            if DataBorgLTF.millis() - last > 40: #Ramp commands 25 per second       
                    last = DataBorgLTF.millis()
                    i = min(max(i, -5), 5) #Safety limit of +- 5A just in case our math/control below is wrong
                    self.setBiOMCurrent(i)
                    self.commandedCurrent = i                    
                    if direction == 1:
                        i = i + stepSizeAmps
                    else:
                        i = i - stepSizeAmps
                    
                    if abs(i) > abs(rampMagAmps): #Ramp back down
                        direction = -1*direction                    

                        
                        


"""Commands Copley to keep a specific torque profile (square wave) when hooked up to BiOM. 
Desired end result should be that commanded square wave of torque from copley can take BiOMs SEA torque into account and produce a clean sq wave.
10/3/13: Copley-side torque controller presently does not take SEA into model account and displays 10Hz wave.
"""
class CopleyCommandedTorqueTest(CurrentStepTest):

    UPD_CURRENT_SCALE_R300 = 866 #Motor current scale factor.
    commandedCurrent = 0
    
    varLabelsToPlot = [ "BiOM Joint Torque (Nm) from LoadCell", "Copley Enc Pos","Copley Enc RPM" ] #, "BiOM Joint Angle Deg","Copley Enc RPM" , "BiOM Joint Torque (Nm) from BiOM", commandedCurrent","MC_encoder",   
    plotStyles = dict(zip(varLabelsToPlot, ['r','b','c'])) 
    scaleFactor = dict(zip(varLabelsToPlot, [1.0, .001, .001]))
    offsetFactor = dict(zip(varLabelsToPlot, [0,0,0]))
    

     
        
    def performTest(self, TqAmpl = 100, Torque_Kp = 1, Torque_Ki = 0):#Use an outer PI loop torque controller linking JR3 torque to Motor Pos.
        self.motor.moveToZero(); #
             
        
        jr3Sample =  self.loadCell.getOneTimestampedRowSIUnits(useZeroOffset= False)[1]        
        d2 = dict(zip(["FxNewtons", "FyNewtons", "FzNewtons" ,"Mx_Nm", "My_Nm", "Mz_Nm", 'v1', 'v2'], jr3Sample))
        tqNow = LtfTrigGeomUtil.ConvertLoadCellForceBiOMJointTorque(d2["FzNewtons"], d2["FyNewtons"])
        TqOffset = 0 #tqNow #Lets us manually decide where to set our torque offset.       
    
        integratedError = 0
        timestep = .002 #Don't try to run faster than 500Hz since the motor can't be commanded faster than that anyway

        sqWavePeriod = 2.0
        

        last = DataBorgLTF.millis()
        
        tqSetPoint = 0 #TqOffset + TqAmpl/2.0

        while True:
            if DataBorgLTF.millis() - last > sqWavePeriod*1000:
                #if tqSetPoint > TqOffset:  tqSetPoint = TqOffset - TqAmpl/2.0
                #else: tqSetPoint = TqOffset + TqAmpl/2.0
                if tqSetPoint > 0: tqSetPoint = 0
                else: tqSetPoint = TqAmpl
                last = DataBorgLTF.millis()
            
            sample = DataBorgLTF.millis()
            tqLast = tqNow            
            jr3Sample =  self.loadCell.getOneTimestampedRowSIUnits(useZeroOffset= False)[1]        
            d2 = dict(zip(["FxNewtons", "FyNewtons", "FzNewtons" ,"Mx_Nm", "My_Nm", "Mz_Nm", 'v1', 'v2'], jr3Sample))
            tqNow = LtfTrigGeomUtil.ConvertLoadCellForceBiOMJointTorque(d2["FzNewtons"], d2["FyNewtons"])


            #biOMTorqueNow = self.biomTelemetryHandler.peek_last_received()["estimated_torque"] #BiOM spring is going to apply torque, use model inside BiOM to ignore it.
            
            integratedError += (tqSetPoint - tqNow)*timestep
            rpm = (Torque_Kp*(tqSetPoint - tqNow) + Torque_Ki * integratedError) * CopleyWrapper.COPLEY_ENCODER_COUNTS_P_REV / 60.0 / 15.0  #Aim to switch from units of Nm/sec to RPM:   60s=1minute, 1Nm ~ 15 encoder counts based on rough observation. 
            
            dist = rpm * CopleyWrapper.COPLEY_ENCODER_COUNTS_P_REV* timestep 

            rpm = abs(rpm) #RPM in trapezoidal profile has no direction, we spec it with distance param
            
            #print dist, rpm, integratedError, tqNow, tqSetPoint, tqSetPoint - tqNow , DataBorgLTF.millis()- sample
                
        
                    
            self.motor.moveRelativeUsingProfile(dist, velocityRPM = rpm, accelRadPSec2 = 30, decelRadPSec2=30, blocking = False)
            time.sleep(max(0, timestep - (DataBorgLTF.millis()- sample))) #even out sampling
            




"""Commands Copley to hold it's position as solidly as possible when hooked up to BiOM, while BiOM produces a step input of current. Test result will demonstrate how Copley motor position controller responds to step input from BiOM.  """
class BiOMCommandedMotorTorqueTest(CurrentStepTest):

    varLabelsToPlot = ["BiOM Joint Torque (Nm) from BiOM", "BiOM Joint Torque (Nm) from LoadCell", "Copley Enc Pos", "commandedTq (Nm)", "BiOM Joint Angle Deg"] #,"Copley Enc RPM" , commandedCurrent","MC_encoder",   
    plotStyles = dict(zip(varLabelsToPlot, ['r','b','c', 'm--', 'g'])) 
    scaleFactor = dict(zip(varLabelsToPlot, [1.0, 1.0, .001, 1.0, 1.0]))
    offsetFactor = dict(zip(varLabelsToPlot, [0,0,0,0, 0]))
    
    commandedTq = 0

    def setBiOMTorque(self, desTqNm =0.0, spring_mode =0 ) :
        raise Exception("NOT CONVERTED TO TANGO CODE YET!")

        self.commandedTq = round(desTqNm * 1000)        
        result = self.biomCommand([[1761, self.commandedTq, spring_mode]]); # use special command 1761 to set motor commanded torque
    
     
        
    def performTest(self, biomTorqueNm = 5, biomTorqueOffsetNm = 10):
        self.motor.moveToZero(); #Keep motor position roughly here to oppose motion.
        
        while True:
            self.setBiOMTorque(desTqNm = biomTorqueNm + biomTorqueOffsetNm )
            time.sleep(1.0)
            self.setBiOMTorque(desTqNm =  biomTorqueOffsetNm - biomTorqueNm)
            time.sleep(1.0)
            
    maxBiOMTq = -800000000;
    maxJR3Tq = -8000000000;
    minBiOMTq = 800000000;
    minJR3Tq = 8000000000;
    
    
    def dataReadThread(self):
        self.stillLogging = True;
        while (self.stillLogging):
                d = self.ongoingDataCollection();
                self.logData("ALL_DATA", d) #log all data. Note that dataReadThread() can be overridden if we want more or less data. 
        self.stillLogging = False
        if hasattr(self, "logfile"): self.logfile.close()
    
    def ongoingDataCollection(self):
        d = dict() 
        d["startReadTimeMilliSec"] =  DataBorgLTF.millis()  #Put timestamp in our dictionaryt 

        d["commandedTq (Nm)"] = self.commandedTq /1000.0
        if self.motor:
            d["Copley Enc Pos"] = self.motor.readMotorStatusTimestamped()['position']
            d["Copley Enc RPM"] = self.motor.readMotorStatusTimestamped()['velocity']
                
        if self.loadCell:
            row =  self.loadCell.getOneTimestampedRowSIUnits(useZeroOffset= False)[1]        
            d2 = dict(zip(["FxNewtons", "FyNewtons", "FzNewtons" ,"Mx_Nm", "My_Nm", "Mz_Nm", 'v1', 'v2'], row))
            tqNM = LtfTrigGeomUtil.ConvertLoadCellForceBiOMJointTorque(d2["FzNewtons"], d2["FyNewtons"])
            d["BiOM Joint Torque (Nm) from LoadCell"] = tqNM
            self.maxJR3Tq = max(tqNM, self.maxJR3Tq)    
            self.minJR3Tq = min(tqNM, self.minJR3Tq) 
              

        if self.biomTelemetryHandler:
            data = self.biomTelemetryHandler.peek_last_received();
            if data != None and "estimated_torque" and "raw_hall_sensor" in data.keys():                
                d["BiOM Joint Torque (Nm) from BiOM"] = data["estimated_torque"]
                d["qIq"] = data["qIq"] 
                d["MC_encoder"] = data["MC_encoder"]
                self.maxBiOMTq = max(d["BiOM Joint Torque (Nm) from BiOM"], self.maxBiOMTq)    
                self.minBiOMTq = min(d["BiOM Joint Torque (Nm) from BiOM"], self.minBiOMTq) 
                d["BiOM Joint Angle Deg"] = data["raw_hall_sensor"]/8192.0 * 360.0
                
        
            d["Scaled BiOM Tq"] = ((d["BiOM Joint Torque (Nm) from BiOM"] + (self.maxJR3Tq + self.minJR3Tq)/2.0 - (self.maxBiOMTq + self.minBiOMTq)/2.0)* (self.maxJR3Tq - self.minJR3Tq))/(self.maxBiOMTq - self.minBiOMTq + .0000000000001)  #Make midpoints of both square waves the same, then scale both to same magnitude
              

        d["endReadTimeMilliSec"] = DataBorgLTF.millis() #Put timestamp in our dictionaryt 
                    

        return d



       

"""Commands Copley to move in a square wave using a profile and watches the JR3 load cell torque response for ringing. 
- To detect ringing solely due to Copley internal controller, run this test with a BiOM shell or BiOM with detached actuator/spring pin.
- Running this test with a normal BiOM in current control makes Copley response hard/impossible to see.
""" 
class CopleyStepTest(DataBorgLTF.LTFPlottableTest):
    timeAxisLabel = "Seconds"
    varLabelsToPlot = ["Joint Torque (Nm) from LoadCell", "Copley Encoder Pos" ,"BiOM Joint Angle Deg", 'Copley Encoder Vel Rpm']      
    plotStyles = dict(zip(varLabelsToPlot, ['r','b','g', 'm'])) 
    scaleFactor = dict(zip(varLabelsToPlot, [1.0, .001, 1.0, .00001]))
    offsetFactor = dict(zip(varLabelsToPlot, [0,0,0, 0]))
        
    def performTest(self, encoderStepMagnitude = 10000, startingVelRPM = 2500, accel = 200):
        self.motor.moveToZero(); #Start at zero.        
        
        while True:
            try:
                time.sleep(2.0)        
                self.motor.moveAbsoluteUsingProfile(-1*encoderStepMagnitude, velocityRPM = startingVelRPM, blocking = True, accelRadPSec2 = accel, decelRadPSec2=accel, threshold = 1)
                time.sleep(2.0)        
                self.motor.moveAbsoluteUsingProfile(0, velocityRPM = startingVelRPM, blocking = True, accelRadPSec2 = accel, decelRadPSec2=accel, threshold = 1)

            except Exception as e:
                print e
                self.motor.moveToZero()
                break

    def dataReadThread(self):
        self.stillLogging = True;
        while (self.stillLogging):
                d = self.ongoingDataCollection();
                self.logData("ALL_DATA", d) #log all data. Note that dataReadThread() can be overridden if we want more or less data. 
        self.stillLogging = False
        if hasattr(self, "logfile"): self.logfile.close()
        
    def ongoingDataCollection(self):
        d = dict() 
        d["startReadTimeMilliSec"] =  DataBorgLTF.millis() #Put timestamp in our dictionaryt 
        if self.motor:
            motor_d = self.motor.readMotorStatusTimestamped()
            d["Copley Encoder Pos"] = motor_d['position']
            d["Copley Encoder Vel Rpm"] = motor_d['velocity']
        if self.loadCell:
            row =  self.loadCell.getOneTimestampedRowSIUnits(useZeroOffset= False)[1]        
            d2 = dict(zip(["FxNewtons", "FyNewtons", "FzNewtons" ,"Mx_Nm", "My_Nm", "Mz_Nm", 'v1', 'v2'], row))
            tqNM = LtfTrigGeomUtil.ConvertLoadCellForceBiOMJointTorque(d2["FzNewtons"], d2["FyNewtons"])
            d["Joint Torque (Nm) from LoadCell"] = tqNM
    
        if self.biomTelemetryHandler:
            data = self.biomTelemetryHandler.peek_last_received();
            if data != None and "estimated_torque" and "raw_hall_sensor" in data.keys():                
                d["BiOM Joint Angle Deg"] = data["raw_hall_sensor"]/8192.0 * 360.0
                d["MC_encoder"] = data["MC_encoder"]
       
        d["endReadTimeMilliSec"] =  DataBorgLTF.millis() #Put timestamp in our dictionary 
        return d


def makePlot(label):
    plt.plot([])
    plt.ylabel(label)
        
    return plt



def currentStepResponseDemo():
    options =  DataBorgLTF.DataBorgOptions(sys.argv).values; 
    myTest = CurrentStepTest(biomCommandComport = options.biom_com_port, biomTelemetryComport = options.telemetry_com_port, biomTelemetryVlist = "dataport3h")
    
    myTest.prepBiOMForTestBench()
        
    myPlot = makePlot("BiOM Current 6A Sq Wave vs BiOM, JR3 Torque Measurements")
    
    
 
    thread.start_new_thread(myTest.performTest, (6,))    
    thread.start_new_thread(myTest.dataReadThread, ())    
    thread.start_new_thread(myTest.plotUpdateThread, (myPlot,))

    myPlot.show()
    myTest.destroyFramework()
            
                           
                
def currentRampResponseDemo():
    options =  DataBorgLTF.DataBorgOptions(sys.argv).values; 
    myTest = CurrentRampTest(biomCommandComport = options.biom_com_port, biomTelemetryComport = options.telemetry_com_port, biomTelemetryVlist = "dataport3h")
    
    myTest.prepBiOMForTestBench()
       
 
    myPlot = makePlot("BiOM Current and BiOM Torque Measurement")
    
    thread.start_new_thread(myTest.performTest, ())    
    thread.start_new_thread(myTest.dataReadThread, ())    
    thread.start_new_thread(myTest.plotUpdateThread, (myPlot,))

    myPlot.show()
    myTest.destroyFramework()
        

def torqueSquareWaveDemo():
    options =  DataBorgLTF.DataBorgOptions(sys.argv).values; 
    myTest = BiOMCommandedMotorTorqueTest(biomCommandComport = options.biom_com_port, biomTelemetryComport = options.telemetry_com_port, biomTelemetryVlist = "dataport3h")
    
    myTest.prepBiOMForTorqueProfile() #Use this if we're going to command the BiOM to move.
    myPlot = makePlot("JR3 and BiOM Torque Measurement")
    
    thread.start_new_thread(myTest.performTest, ())    
    thread.start_new_thread(myTest.dataReadThread, ())    
    thread.start_new_thread(myTest.plotUpdateThread, (myPlot,))

    myPlot.show()
    myTest.destroyFramework()
        


def CopleyZeroTorqueControllerDemo():
    options =  DataBorgLTF.DataBorgOptions(sys.argv).values;
    myTest = CopleyZeroTorqueTest(biomCommandComport = None, biomTelemetryComport = None)
    
    #myTest.prepBiOMForTestBench()
       
 
    myPlot = makePlot("JR3 and BiOM Torque Measurement")
    
    thread.start_new_thread(myTest.performTest, ())    
    thread.start_new_thread(myTest.dataReadThread, ())    
    thread.start_new_thread(myTest.plotUpdateThread, (myPlot,))

    myPlot.show()
    myTest.destroyFramework()


def torqueToBiOMTorqueDemo():  
    options =  DataBorgLTF.DataBorgOptions(sys.argv).values; 
    myTest = CycleCopleyTest(biomCommandComport = options.biom_com_port, biomTelemetryComport = options.telemetry_com_port, biomTelemetryVlist = "dataport3h")
    now = DataBorgLTF.millis()
    myTest.prepBiOMForTestBench()

            
    while True:
        try:        
            if DataBorgLTF.millis() - now > 100:            
                d =  myTest.ongoingDataCollection()
                print d                
                now = DataBorgLTF.millis()       
                print len(myTest.biomTelemetryHandler.get_all()) #Just keep reading BiOM data even if we don't do anything with it.
           
                distance = int(raw_input("Enter in value from +- 10000 to move that distance:"))
                myTest.motor.moveAbsoluteUsingProfile(d['Copley Encoder Pos'] + distance, velocityRPM = 85, blocking = True, accelRadPSec2 = 10, decelRadPSec2=10, threshold = 5)
             
        
        except Exception as e:    
            print e
            print "Exiting..."
            myTest.destroyFramework()
            sys.exit(-1) 



def copleyStepResponseDemo():
    options =  DataBorgLTF.DataBorgOptions(sys.argv).values; 
    myTest = CopleyStepTest(biomCommandComport = options.biom_com_port, biomTelemetryComport = options.telemetry_com_port, biomTelemetryVlist = "dataport3h")
    
    myTest.prepBiOMForTestBench()
    
    myTest.logData("sqaure_wave_params", {"heightEncCnts":-3500, "velRpm":4000, "accelRadS2": 200}) #log all data regardless of whether it makes it to the plot window
                
    
    myPlot = makePlot("Copley running into BiOM Hardstop @4000RPM vs JR3 Torque Measurements")
    
        
 
    thread.start_new_thread(myTest.performTest, (-3500, 4000, 200,))    
    thread.start_new_thread(myTest.dataReadThread, ())    
    thread.start_new_thread(myTest.plotUpdateThread, (myPlot,))

    myPlot.show()
    myTest.destroyFramework()



if __name__ == "__main__":  
    #torqueToBiOMTorqueDemo()
    #currentStepResponseDemo()
    #currentRampResponseDemo()
    #biomFrequencyResponseDemo()
    #copleyStepResponseDemo()
    CopleyZeroTorqueControllerDemo()
    #torqueSquareWaveDemo()
