import DataBorgLTF, CopleyWrapper, LoadcellJR3, LtfTrigGeomUtil
import thread, sys, time, collections
import comm_lib.universal_dataport as universal_dataport
import numpy
import matplotlib.pyplot as plt



"""Commands Copley to keep 0 torque when hooked up to BiOM. End result should be that commanded square wave of torque from copley can take BiOMs SEA torque into account and produce a clean sq wave. """
class HardStopTorqueTest(DataBorgLTF.LTFPlottableTest):

    UPD_CURRENT_SCALE_R300 = 866 #Motor current scale factor.
    
    varLabelsToPlot = [ "BiOM Joint Torque (Nm) from LoadCell", "Copley Enc Pos","Copley Enc RPM","Raw AAE"] #, "BiOM Joint Angle Deg","Copley Enc RPM" , "BiOM Joint Torque (Nm) from BiOM", commandedCurrent","MC_encoder",   
    plotStyles = dict(zip(varLabelsToPlot, ['r','b','c', 'm'])) 
    scaleFactor = dict(zip(varLabelsToPlot, [1.0, .001, .001, .001]))
    offsetFactor = dict(zip(varLabelsToPlot, [0,0,0,0]))
    

    commandedTorque = 0
     
        
    def performTest(self, TqAmpl = 100, Torque_Kp = 10, Torque_Ki = 4, Torque_H_feedback = 1, period = 2.0):#Use an outer PI loop torque controller linking JR3 torque to Motor Pos.
        self.motor.moveToZero(); #
             
        
        jr3Sample =  self.loadCell.getOneTimestampedRowSIUnits(useZeroOffset= False)[1]        
        d2 = dict(zip(["FxNewtons", "FyNewtons", "FzNewtons" ,"Mx_Nm", "My_Nm", "Mz_Nm", 'v1', 'v2'], jr3Sample))
        tqNow = LtfTrigGeomUtil.ConvertLoadCellForceBiOMJointTorque(d2["FzNewtons"], d2["FyNewtons"])
        TqOffset = 0 #tqNow #Lets us manually decide where to set our torque offset.       
    
        integratedError = 0
        timestep = .002 #Don't try to run faster than 500Hz since the motor can't be commanded faster than that anyway

        sqWavePeriod = period
        

        last = time.clock()
        
        tqSetPoint = 0 #TqOffset + TqAmpl/2.0

        while True:
            if time.clock() - last > sqWavePeriod:
                #if tqSetPoint > TqOffset:  tqSetPoint = TqOffset - TqAmpl/2.0
                #else: tqSetPoint = TqOffset + TqAmpl/2.0
                if tqSetPoint > 0: tqSetPoint = 0
                else: tqSetPoint = TqAmpl
                self.commandedTorque = tqSetPoint
                last = time.clock()
            
            sample = time.clock()
            tqLast = tqNow            
            jr3Sample =  self.loadCell.getOneTimestampedRowSIUnits(useZeroOffset= False)[1]        
            d2 = dict(zip(["FxNewtons", "FyNewtons", "FzNewtons" ,"Mx_Nm", "My_Nm", "Mz_Nm", 'v1', 'v2'], jr3Sample))
            tqNow = Torque_H_feedback* LtfTrigGeomUtil.ConvertLoadCellForceBiOMJointTorque(d2["FzNewtons"], d2["FyNewtons"])


            
            integratedError += (tqSetPoint - tqNow)*timestep
            rpm = (Torque_Kp*(tqSetPoint - tqNow) + Torque_Ki * integratedError) * CopleyWrapper.COPLEY_ENCODER_COUNTS_P_REV / 60.0 / 15.0  #Aim to switch from units of Nm/sec to RPM:   60s=1minute, 1Nm ~ 15 encoder counts based on rough observation. 
            
            dist = rpm * CopleyWrapper.COPLEY_ENCODER_COUNTS_P_REV* timestep 

            rpm = abs(rpm) #RPM in trapezoidal profile has no direction, we spec it with distance param
            
            #print dist, rpm, integratedError, tqNow, tqSetPoint, tqSetPoint - tqNow , time.clock() - sample
                
        
                    
            self.motor.moveRelativeUsingProfile(dist, velocityRPM = rpm, accelRadPSec2 = 30, decelRadPSec2=30, blocking = False)
            time.sleep(max(0, timestep - (time.clock() - sample))) #even out sampling



    def ongoingDashboard(self):
       d = dict() 
       d["startReadTimeSec"] =  time.clock() #Put timestamp in our dictionaryt 

       d["commandedTorque"] = self.commandedTorque      
       if self.motor:
            d["Copley Enc Pos"] = self.motor.readMotorStatusTimestamped()['position']
            d["Copley Enc RPM"] = self.motor.readMotorStatusTimestamped()['velocity']
                
       if self.loadCell:
            row =  self.loadCell.getOneTimestampedRowSIUnits(useZeroOffset= False)[1]        
            d2 = dict(zip(["FxNewtons", "FyNewtons", "FzNewtons" ,"Mx_Nm", "My_Nm", "Mz_Nm", 'v1', 'v2'], row))
            tqNM = LtfTrigGeomUtil.ConvertLoadCellForceBiOMJointTorque(d2["FzNewtons"], d2["FyNewtons"])
            d["BiOM Joint Torque (Nm) from LoadCell"] = tqNM
              
       if self.biomTelemetryHandler != None:
            data = self.biomTelemetryHandler.peek_last_received();
            if data != None and "raw_hall_sensor" in data.keys():                
                d["Raw AAE"] = data["raw_hall_sensor"]
       
              
       d["endReadTimeSec"] =  time.clock() #Put timestamp in our dictionaryt 
                    

       return d

def makePlot(label):
    plt.plot([])
    plt.ylabel(label)
        
    return plt


if __name__ == "__main__": 
    options =  DataBorgLTF.DataBorgOptions(sys.argv).values;
    myTest = HardStopTorqueTest(biomCommandComport = options.biom_com_port, biomTelemetryComport = options.telemetry_com_port, biomTelemetryVlist = "dataport3h")
    myPlot = makePlot("JR3 and BiOM Torque Measurement")
    
    myTest.prepBiOMForTestBench()
    thread.start_new_thread(myTest.performTest, ())    
    thread.start_new_thread(myTest.dataReadThread, ())    
    thread.start_new_thread(myTest.plotUpdateThread, (myPlot,))

    myPlot.show()
    myTest.destroyFramework()

            
