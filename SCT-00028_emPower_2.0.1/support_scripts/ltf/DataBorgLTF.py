""" DataBorgLTF.py
    A class designed to assimilate, coordinate, and plot all data streams coming into the BiOM Life Test Fixture to make RnD and Manufacturing data easy to see. 
    Modules that get pulled in:
        LoadcellJR3.py is streaming load cell data from the LTF
"""

import sys, os, optparse
sys.path.insert(1, "..")
import python_paths_default
import python_paths
import socket, time, datetime, thread, collections, Queue #Queue is synchronized, so it can pass data between threads

import comm_lib.universal_dataport as universal_dataport
import comm_lib.pfcmd as pfcmd



import matplotlib, numpy
import matplotlib.pyplot as plt
    


try:
    import LoadcellJR3, CopleyWrapper, LtfTrigGeomUtil
except Exception as e:
    print e
    sys.exit(-1)


HOST_HOME = "localhost"
LOADCELL_CHANNEL = 5005
BIOM_TELEMETRY_CHANNEL = 9999
COPLEY_MOTOR_CHANNEL = 8888

class DataBorgOptions :
    
    def __init__(self, args) :
        self.args = args;
        usage = "usage: %prog [options]"
        parser=optparse.OptionParser(usage=usage);
        
        parser.add_option("-p", "--biom_com_port", dest="biom_com_port", type="int", default=1);
        parser.add_option("-t", "--telemetry_com_port", dest="telemetry_com_port", type="int", default=0);
        parser.add_option("-b", "--command_baud_rate", dest="command_baud_rate", type="int", default=115200, help="Baud rate to use when sending special commands to BiOM");
        parser.add_option("-l", "--load_cell_address", dest="load_cell_addr", type="string", default="/dev/comedi0", help="Linux mount of JR3 load cell. Eg. '-p3 /dev/comedi0'")
        parser.add_option("-C", "--copley_ethernet_address", dest="copley_ethercat", type="string", default = "eth1", help="Linux address of Ethercat card used to communicate with Copley motor amplifier. Eg. '-C eth1'") 


 
        (options, args) = parser.parse_args(args);
        d = parser.values.__dict__
        #print type(d)
        #print parser.values.__dict__
        self.values = parser.values
        self.parser = parser;        
        return 



#LTF Special Commands used by the BiOM. See firmware/StateController/Comm/MPD/tk_cmdset.c for all available commands. 
CMD_DSM_SET_IDLE_MODE =  [173, 77 , 7]     # demux SM idle mode
CMD_DSM_SET_NORMAL_MODE =  [173, 77 , 6]     # demux SM Normal mode

CMD_ENABLE_BENCHTEST = [170, 77 , 17]    # enable BENCHTEST mode (State Contoller WSM DO_NOTHING)
CMD_ENABLE_FET = [123, 1];  # turn on AC enable to power FET (solid green LED)
CMD_DISABLE_MOTOR_BACKDRIVE =  [1421, 8];         # self comreff (no back drive)
#    [1421, 10]);        # instant commref if available
CMD_MC_SET_IDLE_MODE =   [1421, 17];        # do nothing state on MC
CMD_MC_SET_IMP_MODE = [1421, 30]; #Torque/impedance control mode for MC
CMD_MC_SET_OPEN_MOTOR_LEADS = [1215, 0];          # use open leads
CMD_MC_SET_CLOSE_MOTOR_LEADS = [1215,1]; #close motor leads
CMD_DISABLE_IMU = [231, 0];          # turn off IMU
CMD_READ_THETA_BETA_VALUES = [1004, 0]  #Reads out lots of encoder values of the BiOM: 
#CMD_ZERO_ENCODER = [1323, 1, 0]
CMD_READ_ANKLE_ANGLE_ENC_OFFSET = [1322] #Reads the joint encoder offset at the hardstop (subtract this value from the raw joint encoder reading to get the hardstop 0 point.)
CMD_SETUP_DATAPORT =[1203, 3]  
        


millis_start_time = datetime.datetime.now()
# returns the elapsed milliseconds since the start of the program
def millis():
    global millis_start_time
    dt = datetime.datetime.now() - millis_start_time
    ms = (dt.days * 24 * 60 * 60 + dt.seconds) * 1000 + dt.microseconds / 1000.0
    return ms



""" Framework to do everything any test on the LTF is going to need: a reference to the copley amplifier, reference to the load cell, and command interface to the BiOM"""
class LTFTestFramework():
    def __init__(self, biomCommandComport = None, biomTelemetryComport = None, biomTelemetryVlist = "dataport3h", connectToLoadCell = True, connectToCopley=True, broadcastCopley = False, broadcastTelemetry = False, biomTelemetryDataportNum = 3, logFileNameOverride = None, cfg = None):
        if connectToLoadCell:
            self.loadCell = LoadcellJR3.LoadCellJR3();
            self.loadCell.changeFilterLevel(2)

            
        else:
            self.loadCell = None


        if logFileNameOverride != None:
            self.logfile = open(logFileNameOverride, 'w')
            self.recordTypes = dict()
    
        if biomCommandComport != None:
            self.biomConnection = LTFTestFramework.startBiOMCommandComport(biomCommandComport)
            #If we have a comport, we can change the dataport vlist output from here:
            if (biomTelemetryComport != None and biomTelemetryDataportNum != 3):
                CMD_SETUP_DATAPORT =[1203, biomTelemetryDataportNum]  
                print self.biomCommand([CMD_SETUP_DATAPORT])
            
        else:
            self.biomConnection = None
        


        if biomTelemetryComport != None: #Read in the data from the telemetry comport and rebroadcast that data so it can be plotted.
            if not biomTelemetryVlist.endswith('.vlist'):
                biomTelemetryVlist = biomTelemetryVlist+".vlist"        
            self.vlist = os.path.abspath(biomTelemetryVlist)
            self.biomTelemetryHandler = LTFTestFramework.startBiOMTelemetryBroadcastThread(biomTelemetryComport, self.vlist, udpBroadcast = broadcastTelemetry)
        else:
            self.biomTelemetryHandler = None
            self.vlist = None
            

        if connectToCopley: 
            self.motor = CopleyWrapper.MotorController()
            self.motor.start(0)
            
            if broadcastCopley:
                self.copleyDataBroadcaster = CopleyWrapper.CopleyMotorUDPBroadcaster(HOST_HOME, COPLEY_MOTOR_CHANNEL, self.motor)
            else:            
                self.copleyDataBroadcaster = None                    
        else:
            self.motor = None
            self.copleyDataBroadcaster = None                    
        
        #if cfg:
        self.cfg = cfg
        self.quitMessages = Queue.Queue()
    
    def shouldQuit(self):
        return (not self.quitMessages.empty())

    
    "Outputs all data from a test. Takes in a record type and the labeled data to write to the log file. If the record type has never been seen before, an output comment line with the header data will be generated."
    def logData(self, recordType, recordDict):
        if not hasattr(self, "logfile"):
            fname = self.__class__.__name__+ "_"+time.strftime("%d%m%y_%H%M.log", time.localtime(time.time()))
            self.logfile = open(fname, 'w')
            self.recordTypes = dict()
            
        if recordType not in self.recordTypes:
            recordDict.keys().sort()
            self.logfile.write(recordType+", " + (", ".join(recordDict.keys())+"\r\n"))
            self.recordTypes[recordType] = recordDict.keys()                    

        self.logfile.write(recordType+", " + (", ".join([str(recordDict[x]) for x in self.recordTypes[recordType]])+"\r\n"))

    def prepBiOMForTestBench(self):
        raise Exception("ERROR: FUNCTION NOT YET UPDATED FOR TANGO!")
        print self.biomCommand([CMD_DSM_SET_IDLE_MODE])
        print self.biomCommand([CMD_ENABLE_BENCHTEST])
        print self.biomCommand([CMD_ENABLE_FET])
        print self.biomCommand([CMD_DISABLE_MOTOR_BACKDRIVE])
        print self.biomCommand([CMD_MC_SET_IDLE_MODE])
        print self.biomCommand([CMD_MC_SET_OPEN_MOTOR_LEADS])
        print self.biomCommand([CMD_DISABLE_IMU])


    def prepBiOMForTorqueProfile(self):
        raise Exception("ERROR: FUNCTION NOT YET UPDATED FOR TANGO!")
        print self.biomCommand([CMD_DSM_SET_IDLE_MODE])
        print self.biomCommand([CMD_ENABLE_BENCHTEST])
        print self.biomCommand([CMD_ENABLE_FET])
        print self.biomCommand([CMD_DISABLE_MOTOR_BACKDRIVE])
        print self.biomCommand([CMD_MC_SET_IMP_MODE])
        print self.biomCommand([CMD_DISABLE_IMU])
    
    def prepBiOMForTrajectoryControl(self):
        raise Exception("ERROR: FUNCTION NOT YET UPDATED FOR TANGO!")
        print self.biomCommand([CMD_DSM_SET_IDLE_MODE])
        print self.biomCommand([CMD_ENABLE_BENCHTEST])
        print self.biomCommand([CMD_ENABLE_FET])
        print self.biomCommand([CMD_DISABLE_MOTOR_BACKDRIVE])
        print self.biomCommand([CMD_MC_SET_IDLE_MODE])
        print self.biomCommand([CMD_DISABLE_IMU])
    
    
    @staticmethod       
    def startBiOMCommandComport(biomCommandComport, baudrate = 115200):
        sc_conn = pfcmd.PFCmd (0,0)   
        sc_conn.connect(biomCommandComport, baudrate);
        sc_conn.quiet = True
        return sc_conn 
        
        
    def biomCommand(self, special_command_list, struct_str = None):
        r = []        
        for l in special_command_list:
            #resultRsp = self.biomConnection.optimized_do_command('special', l);
            if struct_str is None:
                resultRsp = self.biomConnection.do_command('special', l);
            else:
                resultRsp = self.biomConnection.do_command_w_struct_str('special', l, struct_str)
            r.append(zip(resultRsp['rsp_fields'], [resultRsp[k] for k in resultRsp['rsp_fields']]))
              
        return r
    @staticmethod       
    def startBiOMTelemetryBroadcastThread(biomTelemetryComport, biomTelemetryVlist, udpBroadcast = False):        
        biomTelemetryConnection = universal_dataport.datasource(connection_type = universal_dataport.datasource.SERIAL, baudrate = 1250000, port = biomTelemetryComport)
        biomTelemetryParser = universal_dataport.vlist_parser(biomTelemetryVlist,err_quiet = True)
        biomTelemetryHandler = universal_dataport.datasource_handler(biomTelemetryConnection, biomTelemetryParser)
        if udpBroadcast:
            biomTelemetryHandler.setup_rebroadcasting(HOST_HOME, BIOM_TELEMETRY_CHANNEL)
        
        biomTelemetryHandler.start()
        return biomTelemetryHandler

    def performTest(self):
        pass 
    
    """Prints out a standard set of things we're going to want on most tests. Can be overridden for any local test. Information is returned as a dictionary in order:
            Copley Encoder Position
            Calculated Joint Torque (LoadCell Measurement)
            Calculated Joint Torque (BiOM- if available)
            BiOM Joint Angle Deg (BiOM- if available) 
    """
    def dataReadThread(self):
        oldData = self.biomTelemetryHandler.peek_last_received()
        while (not self.shouldQuit()):
                d = dict() 
                if self.motor:
                    d["Copley Encoder Pos"] = self.motor.readMotorStatusTimestamped()['position']
                    
                if self.loadCell:
                    row =  self.loadCell.getOneTimestampedRowSIUnits(useZeroOffset= False)[1]        
                    d2 = dict(zip(["FxNewtons", "FyNewtons", "FzNewtons" ,"Mx_Nm", "My_Nm", "Mz_Nm", 'v1', 'v2'], row))
                    tqNM = LtfTrigGeomUtil.ConvertLoadCellForceBiOMJointTorque(d2["FzNewtons"], d2["FyNewtons"])
                    d["BiOM Joint Torque (Nm) from LoadCell"] = tqNM
            
                if self.biomTelemetryHandler:
                    data = self.biomTelemetryHandler.peek_last_received();
                    
                                
                    if data != None and "estimated_torque" and "raw_hall_sensor" in data.keys():                
                        d["BiOM Joint Angle Deg"] = data["raw_hall_sensor"]/8192.0 * 360.0
                        d["BiOM Joint Torque (Nm) from BiOM"] = data["estimated_torque"]
                         
                    elif data != None:            
                        for k in data.keys():
                            d[k] = data[k] #Just get whatever data is coming out of the BiOM.
                
                d["endReadTimeMilliSec"] =  millis() #Put timestamp in our dictionary
                if data != oldData:
                    self.logData("ALL_DATA", d) #log all data. Note that dataReadThread() can be overridden if we want more or less data. 
                oldData = data
                
        if hasattr(self, "logfile"): self.logfile.close()
        


    def destroyFramework(self):
            self.quitMessages.put("quit") 
            time.sleep(1)
            
            if (self.biomTelemetryHandler != None):  
                self.biomTelemetryHandler.stop()
                self.biomTelemetryHandler.join()
            
            if (self.copleyDataBroadcaster != None): self.copleyDataBroadcaster.gracefulStopRequest = True #End process reading from the Copley encoders
                   
            if (self.motor != None):
                self.motor.stop()
                self.motor.destroy()
            
            while not self.logfile.closed:#time.sleep(1)
                try: self.logfile.close()
                except Exception as e:print e

        



""" A Test Framework that has all capabilities of the LTFTestFramework, but adds 2 functions designed to be each run in their own Thread: dataReadThread and plotUpdateThread. These 2 functions handle ALL the plotting and graphics for live logging and viewing plots of test data. See LTFTestSuite.py:  LTFTestSuite.currentStepResponseDemo for a demonstration implementation of this class. """  
class LTFPlottableTest(LTFTestFramework):
    timestampDataLabel = "endReadTimeMilliSec"
            
    timeAxisLabel = "Seconds"
    varLabelsToPlot = ["qIq",  "BiOM Joint Torque (Nm) from BiOM", "BiOM Joint Torque (Nm) from LoadCell", "commandedCurrent","MC_encoder", "Copley Encoder Pos"]      
    plotStyles = dict(zip(varLabelsToPlot, ['r','b','g', 'm','c','m-'])) 
    scaleFactor = dict(zip(varLabelsToPlot, [1.0, 1.0, 1.0, 1.0, .001, 1.0]))
    offsetFactor = dict(zip(varLabelsToPlot, [0,0,0,0,0,0]))
 
    def dataReadThread(self):
        self.samplesToPlot = 3000
        self.dataQ = collections.deque([], self.samplesToPlot) #Ring buffer of data we want to plot 
        self.stillLogging = True

        self.readyToPlot = True
        self.stillLogging = True;
        while (self.stillLogging):
                d = dict() 
                if self.motor:
                    d["Copley Encoder Pos"] = self.motor.readMotorStatusTimestamped()['position']
                    
                if self.loadCell:
                    row =  self.loadCell.getOneTimestampedRowSIUnits(useZeroOffset= False)[1]        
                    d2 = dict(zip(["FxNewtons", "FyNewtons", "FzNewtons" ,"Mx_Nm", "My_Nm", "Mz_Nm", 'v1', 'v2'], row))
                    tqNM = LtfTrigGeomUtil.ConvertLoadCellForceBiOMJointTorque(d2["FzNewtons"], d2["FyNewtons"])
                    d["BiOM Joint Torque (Nm) from LoadCell"] = tqNM
            
                if self.biomTelemetryHandler:
                    data = self.biomTelemetryHandler.peek_last_received();
                                
                    if data != None and "estimated_torque" and "raw_hall_sensor" in data.keys():                
                        d["BiOM Joint Angle Deg"] = data["raw_hall_sensor"]/8192.0 * 360.0
                        d["BiOM Joint Torque (Nm) from BiOM"] = data["estimated_torque"]
                         
                    elif data != None:            
                        d = d + data #Just get whatever data is coming out of the BiOM.
                        
                    self.biomTelemetryHandler.flush() #Just keep reading BiOM data even if we don't do anything with it.
               
                d["endReadTimeMilliSec"] =  millis() #Put timestamp in our dictionary
                self.logData("ALL_DATA", d) #log all data. Note that dataReadThread() can be overridden if we want more or less data. 
                self.dataQ.append(d)
                
        self.stillLogging = False
        if hasattr(self, "logfile"): self.logfile.close()

    

    """Thread function responsible for plotting all data collected by dataReadThread into a matlab plot with legend and title. Also responsible for GUI surrounding plot, including callbacks and extra scale/offset windows. This fuction registers the following callbacks:  
        Pressing 'p' will pause and start the window plot. """
    def plotUpdateThread(self, myPlot):
        toPlot = dict()
                    
        for key in self.varLabelsToPlot:
            toPlot[key], = myPlot.plot([], [], self.plotStyles[key], label=key)
        
        #Deal with labels"
        ax = plt.gca()        
        handles, labels = ax.get_legend_handles_labels()
        ax.legend(handles, labels, bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=2, mode="expand", borderaxespad=0.)

        

        sampleTimes = []       
        
        now = millis() 

        self.liveplotting = True

        #Connect the myPlot window to a "pause/play" event when mouse clicked:
        def pause_on_p_press(event):
            if 'p' == event.key:
                self.liveplotting = not self.liveplotting
                if (self.liveplotting == True):
                    #reset all our axes:
                    ax = plt.gca()                    
                    ax.set_autoscale_on(True)
                    for label in self.varLabelsToPlot:                      
                        toPlot[label].set_ydata([])
                        toPlot[label].set_xdata([]) 
                    print "Resuming graphing."            
                else:
                    print "Pausing graphing. "

        myPlot.figure(1).canvas.mpl_connect('key_press_event', pause_on_p_press)
            

        
        while not hasattr(self, "readyToPlot"):
            continue; # Wait for read Thread to start producing data.
        while True:
            try:
                if self.liveplotting and millis() - now > 500:  #Update graph at rate of 1Hz. Data plotted is sampled at 500 Hz  
                        now = millis()
                                
                        while len(self.dataQ) > 0:
                            d = self.dataQ.popleft()              
                            
                            sampleTimes.append(d[self.timestampDataLabel] )
                            
                            if len(sampleTimes) > self.samplesToPlot:
                                sampleTimes = sampleTimes[-1*self.samplesToPlot:-1]
                                for label in self.varLabelsToPlot:                      
                                    valueToAppend = d[label]* self.scaleFactor[label] + self.offsetFactor[label]
                                    toPlot[label].set_ydata(numpy.append(toPlot[label].get_ydata(),valueToAppend)[-1*self.samplesToPlot:-1])     
                                
                            else:
                                for label in self.varLabelsToPlot:                
                                    valueToAppend = d[label]* self.scaleFactor[label] + self.offsetFactor[label]
                                    toPlot[label].set_ydata(numpy.append(toPlot[label].get_ydata(),valueToAppend))     
                                    
                            for label in toPlot.keys(): #All data has the same X axis
                                toPlot[label].set_xdata(sampleTimes)
                                                   
                        try: 
                                ax = plt.gca()
                                ax.relim() 
                                ax.autoscale_view()
                                myPlot.draw()
                        except Exception as e:
                                print "PLOT EXCEPTION:", e
                                
                        
                        
                else:
                    time.sleep(.05)        
            except Exception as e:
                print e








""" Example usage of most useful library functions. See LTFTestSuite.py for examples of using LTFPlottableTest class.  """
if __name__ == "__main__":    
    print sys.argv
    options =  DataBorgOptions(sys.argv).values;    
    #First make a test that doesn't do anything:
    myTest = LTFTestFramework(biomCommandComport = options.biom_com_port, biomTelemetryComport = options.telemetry_com_port, biomTelemetryVlist = "./vlist/calibrate.vlist", connectToCopley = True, connectToLoadCell = True, broadcastCopley = True)
    thread.start_new_thread(myTest.dataReadThread, ())    
    time.sleep(3) #collect for log file before we're done. 
    myTest.performTest()
    print "Exiting..."
    myTest.destroyFramework()
        

