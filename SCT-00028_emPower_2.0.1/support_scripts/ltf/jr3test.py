#!/usr/bin/env python
import struct
import time

from CopleyWrapperCpp import * #Compiled .so object. See http://svn.iwalk.com/svn/test_fixture/cml/readme.txt for how to generate LtfPy.so and where the source comes from.
# Set maximum number of records to collect
max_num_records = 100
sample_rate_hz = 500
copley_node_id = 1

# Initalize a paramater block
params = MotorOperationParams().create()
params.setParam("initial_position", 0)
params.setParam("final_position", 0)
params.setParam("accel", 10.0)
params.setParam("decel", 10.0)
params.setParam("rpm", 1000)
params.setParam("passes", 1000000)
params.setParam("mode", "p2p")

# Instantiate an object to control the Copley amplifier
ecat = MoveEcat("eth1", copley_node_id)

# Initialize the amp(s)
ecat.InitAmp()

# Get the amplifier being controlled
amp = ecat.getAmp(0)

# Start the backdrive
ecat.setParams(params)
ecat.start()

x = ReadLoadCell(max_num_records, 1000000 / sample_rate_hz)
# Create the buffer
buffer = chr(65) * x.get_header_length() + chr(66) * x.get_record_length() * max_num_records
# Start the collection
n = x.start()
if (n == 0):
    print "Collection started successfully: rate(hz)=%d, queue_size=%d" % (sample_rate_hz, max_num_records)

for iter in range(0,1000) :
    # Let's read some records
    num_records_requested = 2
    n = x.read(buffer, num_records_requested)

    if (n) :
        # header unpacking
        print x.get_header_data_labels()
        unpacked_header = struct.unpack(x.get_header_unpack_string(), buffer[:x.get_header_length()])
        print x.get_header_print_format_string() % unpacked_header

        # record unpacking
        print x.get_record_data_labels()

        # Process the records
        # Note: num_records_requested should NOT be used here, instead get the number of records
        # read from the header
        print "Number of records read:", num_records_requested
        for i in range(0, num_records_requested):
            record_offset = x.get_header_length() + i * x.get_record_length()
            unpacked_record = struct.unpack(x.get_record_unpack_string(),
                                            buffer[record_offset : record_offset + x.get_record_length()])
            print x.get_record_print_format_string() % unpacked_record

    time.sleep(0.005)

n = x.stop()
if (n == 0):
    print "Collection stopped successfully"

ecat.stop()
