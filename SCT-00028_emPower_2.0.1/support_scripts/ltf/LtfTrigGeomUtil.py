""" Class that stores LTF information taken from Solidworks and JR3 Datasheets to use in translating Copley motor torque and LoadCell JR3 measurements into torque applied at the BiOM ankle Joint. All calculations assume BiOM has been placed level and with pyramid flush to LTF. """ 

import math
import numpy
import LoadcellJR3


LDCELL_TO_BIOM_PIVOT_WIDTH_MM = 134.6211 
LDCELL_TO_BIOM_PIVOT_HEIGHT_MM = 121.580 + 106.513  #2 measurements- see trig drawings in TR-XXXX TODO RELEASE THIS TR. 
LDCELL_TO_TANGO_PIVOT_HEIGHT_MM = 121.580 + 106.513 + 4.3 # 4.3 is the hight difference between the BIOM T2 and the Tango prototypes  
LOAD_CELL_TO_BIOM_PIVOT_HYPO_MM = math.sqrt((LDCELL_TO_BIOM_PIVOT_WIDTH_MM*LDCELL_TO_BIOM_PIVOT_WIDTH_MM) + (LDCELL_TO_BIOM_PIVOT_HEIGHT_MM)*(LDCELL_TO_BIOM_PIVOT_HEIGHT_MM))
LOAD_CELL_TO_TANGO_PIVOT_HYPO_MM = math.sqrt((LDCELL_TO_BIOM_PIVOT_WIDTH_MM*LDCELL_TO_BIOM_PIVOT_WIDTH_MM) + (LDCELL_TO_TANGO_PIVOT_HEIGHT_MM)*(LDCELL_TO_TANGO_PIVOT_HEIGHT_MM))
LOAD_CELL_TO_BIOM_PIVOT_ANGLE_RAD = math.asin(LDCELL_TO_BIOM_PIVOT_HEIGHT_MM/LOAD_CELL_TO_BIOM_PIVOT_HYPO_MM)
LOAD_CELL_TO_TANGO_PIVOT_ANGLE_RAD = math.asin(LDCELL_TO_TANGO_PIVOT_HEIGHT_MM/LOAD_CELL_TO_TANGO_PIVOT_HYPO_MM)

#Torque = F x L = |F||L|* sin(theta)
#theta = 180 degrees - (angle between Force vector and level ground) - (angle between load_cell_to_biom_pivot vector and level ground) 
#Consider Fx the force parallel to level ground and pointing from the load cell origin to the LTF pyramid base that holds the BiOM.
#Consider Fz force perpendicular to level ground

"""Take Load Cell JR3 measurements and turn them into BiOM Torque measurements. Assumes attached Rigid AFRAME with no play. Input units: Newtons. Output units: Newton-meters"""
def ConvertLoadCellForceBiOMJointTorque(loadCellFz, loadCellFy):
    loadCellF_Magnitude_N =  math.sqrt((loadCellFy)*(loadCellFy) + (loadCellFz)*(loadCellFz))
    loadCellF_Direction = math.asin(loadCellFz/loadCellF_Magnitude_N)
    torqueNM = loadCellF_Magnitude_N * LOAD_CELL_TO_BIOM_PIVOT_HYPO_MM/1000.0 * math.sin(math.pi - LOAD_CELL_TO_BIOM_PIVOT_ANGLE_RAD - loadCellF_Direction)
    return torqueNM

def ConvertLoadCellForceTANGOJointTorque(loadCellFz, loadCellFy):
    loadCellF_Magnitude_N =  math.sqrt((loadCellFy)*(loadCellFy) + (loadCellFz)*(loadCellFz))
    loadCellF_Direction = math.asin(loadCellFz/loadCellF_Magnitude_N)
    torqueNM = loadCellF_Magnitude_N * LOAD_CELL_TO_TANGO_PIVOT_HYPO_MM/1000.0 * math.sin(math.pi - LOAD_CELL_TO_TANGO_PIVOT_ANGLE_RAD - loadCellF_Direction)
    return torqueNM


def readBiOMJointTorqueDemo():
    raw_input("WARNING: THIS DEMO IS SET UP FOR A v1 BIOM, NOT FOR A TANGO ANKLE. UPDATE TO GEOMETRY REQUIRED.")
    raw_input("WARNING: For this to output valid calculations, a BiOM must be attached properly to the Life Test fixture using the A-Frame. Press any key to continue.")
    
    loadCell = LoadcellJR3.LoadCellJR3();
    loadCell.changeFilterLevel(2) #filter with shortest time constant that gets rid of noise:
    while True:
            row =  loadCell.getOneTimestampedRowSIUnits()[1]        
            d = dict(zip(["FxNewtons", "FyNewtons", "FzNewtons" ,"Mx_Nm", "My_Nm", "Mz_Nm", 'v1', 'v2'], row))
            #print d            
            print "Calculated Torque at BiOM Joint:", ConvertLoadCellForceBiOMJointTorque(d["FzNewtons"], d["FyNewtons"]), "Newton-Meters"
    
""" Example usage of most useful library functions. """
if __name__ == "__main__":
    readBiOMJointTorqueDemo()

