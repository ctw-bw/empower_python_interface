#!/usr/bin/env python
# the new robo config

SVN_SUB_REV_STR = "$Revision: 12448 $"

import python_paths_default  # test for user's python_paths.py and update paths
import python_paths as pyenv

import os
import comm_lib.serial_port_helper as serial_port_helper
import time, argparse, sys
sys.pfcmd_do_not_import_support = True;

import comm_lib.pfcmd as pfcmd
import comm_lib.pfcmd_ctrl as pfcmd_ctrl
import comm_lib.robo_lib.key_value_file as key_value_file
import comm_lib.robo_lib.robo_table as robo_table

import testPrograms.ankle_commands as ankle_commands

from python_utils.class_to_str import *


class robo_config_comm:

    def __init__(self, pfcmd_connection, target_device = "SC"):
        self.pfcmd_conn = pfcmd_connection;
        self.ctrl = pfcmd_ctrl.controller(self.pfcmd_conn);
        self.target_device = target_device
        self.table_interface_conn = robo_table.robo_table(self.ctrl, self.target_device);

    def print_available_tables(self):
        self.table_interface_conn.show_avail();
        
    def reset_RAM_to_defaults(self, table_index_number):
        return self.pfcmd_conn.do_command(ankle_commands.s_command_type(),ankle_commands.s_mem_ram_to_defaults(table_index_number))    
        
    def save_RAM_to_flash(self,table_index_number):
        return self.pfcmd_conn.do_command(ankle_commands.s_command_type(),ankle_commands.s_mem_ram_to_flash(table_index_number))
        

        
    def read_all_tables(self, debug_level):
        self.table_interface_conn.read_all(debug_level = debug_level);
        return self.table_interface_conn.get_kvf() #This will return the KVF of what we just read.
        
    def read_tables(self, table_list, debug_level):
        for var_name in table_list :
            self.table_interface_conn.read_table(var_name, debug_level = debug_level);
                
        return self.table_interface_conn.get_kvf(); #tbl will store KVF for all tables we just read. 
        
    def restore_imu_status(self):
        self.pfcmd_conn.do_command('special',[231, self.imu_status_at_startup]); 
     
    def disable_imu(self):
        result = self.pfcmd_conn.do_command('special',[230]); 
        if result :
            self.imu_status_at_startup = result['v0'];
        else :
            self.imu_status_at_startup = 1;

        self.pfcmd_conn.do_command('special',[231, 0]);  # turn off IMU communcations
        
    def create_tables_from_input_files(self, table_file_list, debug_level):
        tbl = key_value_file.key_value_file();
        for t_fname in table_file_list :
            fin = open(t_fname,'r')
            tbl.read(fin, debug_level)
            fin.close();
        return tbl
    
    def write_all_tables_from_input_file(self, table_file_list, debug_level):
        tbl = self.create_tables_from_input_files(table_file_list, debug_level);
        tables = self.table_interface_conn.get_avail_tables()
        tableNames=[]
        for table in tables:
            tableNames.append(table.get('var_name'))
        for sec_name in tbl.get_section_list() :
            if sec_name in tableNames:
                print "writing : ", sec_name
                sec = tbl.get_section(sec_name)
                self.table_interface_conn.write_table(sec_name, sec, debug_level);
            else:
                print 'not writing : ', sec_name
    
       
    
    def write_tables_from_input_file(self, table_file_list, write_table_list, debug_level):
            tbl = self.create_tables_from_input_files(table_file_list, debug_level);
            for var_name in write_table_list :
                sec = tbl.get_section(var_name)
                if sec :
                    print "writing : ", sec.get_name()
                    self.table_interface_conn.write_table(var_name, sec,);
                else :
                    print
                    print "COULD NOT LOCATE %s in variable list:" %( var_name)
                    print
    
    def get_present_table_kvf(self):
        return self.table_interface_conn.get_kvf()
        
    

    def disconnect(self):
        self.pfcmd_conn.disconnect()
    
    def verify_all_tables(self,files_to_compare_against, debug_level,compare_fh):
        file_tbl = self.create_tables_from_input_files(files_to_compare_against, debug_level);
#         print file_tbl
        errors = 0
        for var_name in file_tbl.get_section_list() :
            print file_tbl.get_section_list()
            print var_name
            if var_name != 'workflow_status' and var_name != 'MFG_CONFIG' and var_name != 'EE_keys_param' and var_name != 'firmware_revision' and var_name != 'EE_TorqueVerify':
                if self.target_device == 'SC' and var_name != 'EE_imu_model_calibrationTable':
                    r_tbl = self.table_interface_conn.read_table(var_name, debug_level = debug_level);
                    print type(r_tbl), r_tbl.get_name();
                    errors += file_tbl.compare(r_tbl, var_name, 2,compare_fh=compare_fh);
                elif self.target_device == 'IMU' and var_name == 'EE_imu_model_calibrationTable':
                    r_tbl = self.table_interface_conn.read_table(var_name, debug_level = debug_level);
                    print type(r_tbl), r_tbl.get_name();
                    errors += file_tbl.compare(r_tbl, var_name, 2,compare_fh=compare_fh);
        print 'There were %d errors in the verify'%errors
        return errors
        
    def verify_all_tables_tango(self,files_to_compare_against, debug_level,compare_fh):
        file_tbl = self.create_tables_from_input_files(files_to_compare_against, debug_level);
#         print file_tbl
        errors = 0
        for var_name in file_tbl.get_section_list() :
            print
            print
            print file_tbl.get_section_list()
            print var_name
            if var_name != 'mfg_table':
                r_tbl = self.table_interface_conn.read_table(var_name, debug_level = debug_level);
                print r_tbl
                print type(r_tbl), r_tbl.get_name();
                errors += file_tbl.compare(r_tbl, var_name, 2,compare_fh=compare_fh);
        return errors
    
    def verify_tables(self, table_list, files_to_compare_against, debug_level,compare_fh):
        file_tbl = self.create_tables_from_input_files(files_to_compare_against, debug_level);
       
        for var_name in table_list :
            r_tbl = self.table_interface_conn.read_table(var_name, debug_level = debug_level);
            print type(r_tbl), r_tbl.get_name();
            file_tbl.compare(r_tbl, var_name, 2,compare_fh=compare_fh);
        # tbl = self.table_interface_conn.get_kvf();
        
    def dump_present_tables(self):
        self.table_interface_conn.dump_table();        
        fout = open("dump_table.txt",'w');
        self.table_interface_conn.dump_table(fout);
        fout.write('\n\n\n');
        self.table_interface_conn.dump_decode_table(fout);
        fout.close();
        
        

    """Attempts to update a single table line with a new value. Returns the updated value, whether or not the update was successful."""
    def single_param_update(self, table_name, table_item_name, new_value_list):
        table = self.table_interface_conn
        tbl = table.read_table(table_name, debug_level = 0);
        if (tbl == None):
            print 'ERROR! No table named \"'+table_name+'\" found!'
            return None
            
        typed_new_values = []    
        for i in range(0,len(tbl[table_item_name])):
            #Cast the string input argument to whatever the type of that item is SUPPOSED to be (note that this will deliberately fail if attempt to make something like "b" into a float occurs.)
            typed_new_values.append(type(tbl[table_item_name][i])(new_value_list[i]))
            print "Old tbl value:", table_item_name+"["+str(i)+"]",tbl[table_item_name][i]
            
        tbl[table_item_name] = typed_new_values
        table.write_table(table_name, tbl, debug_level=0);
        table.clear_kvf()  # need to clear the kvf object to avoid conflict when updating the KVF with an existing section
        read_back_tbl = table.read_table(table_name, debug_level =0);
        for i in range(0,len(read_back_tbl[table_item_name])):
            if  read_back_tbl[table_item_name][i] == typed_new_values[i]:
                passed = True
            else:
                passed = False
            print "%30s : read back %-10s  : PASSED=%s" % (read_back_tbl[table_item_name][i], typed_new_values[i], passed)
    
       
        return typed_new_values
    ##################################################

    
class struct :
    pass
  
#helper for options so we can send cmd line arguments as hex or binary if we want. 
def int_any_fmt(x):
        return int(x, 0)
  
class config_options :


    def __init__(self) :
        parser = argparse.ArgumentParser();
        parser.add_argument("--timeit", dest="timeit", action="store_true", default="False", help="display Round Trip Time of this table read/write to execute.")
    
        parser.add_argument("-i", "--ip_address", dest="ip_address", help="Connect to robo via TCP to specified address or DNS name");
        parser.add_argument("-p", "--com_port", dest="com_port", type=int_any_fmt, help="communcation port for serial communcation");

        parser.add_argument("-P", "--tcp_port", dest="tcp_port", type=int_any_fmt, default=10002, help="TCP port to use when using an IP address connection");
        parser.add_argument("-b", "--baud_rate", dest="baud_rate", type=int_any_fmt, default=115200, help="baudrate for serial communcation.  default is 115.2kbaud");
        parser.add_argument("--LIN", dest="LIN_mode", action="store_true", default=False, help="Use with LIN mode comm: ignores the local echo on the RX line of whatever command was just sent out.")
        
        #############                  
        parser.add_argument("-D", "--debug", dest="debug_level", type=int_any_fmt, default=0, help="set debug level, not necessarly consistently implmented or used");
        parser.add_argument("-Q", "--quiet", dest="quiet", action="store_true", default=False, help ="suppress auto matic display of tables.");

        parser.add_argument("--dump_table", dest="dump_table", action="store_true", default=False, help="read header string table from robot, decode it and save it to text file dump_table.txt");
        
        #############                  
        parser.add_argument("-l", "--list", dest="list_avail", action="store_true", default=False, help="List available shared variables and their addresses from robot");

        parser.add_argument("-Z", "--target_device", dest="target_device", default="SC", help="Which processor is the target of the application.  OPTIONS = SC, MC, IMU  (or the first letter of)");
        
        #############                  
        ### writing table options
        parser.add_argument("-T", "--table_file", dest="table_file_list", action="append", help="specify the table file used with the -W and -M options");
        parser.add_argument("-M", "--modify_table", dest="modify_table_list", action="append", help="specify which tables from calibration file may be modified.")
        
        parser.add_argument("-W", "--write_table", dest="write_table_list", action="append", help="specify which tables from calibration file may be written (applies to RAM and EEPROM only. Flash tables can only be written using --save_to_flash and a write to the corresponding RAM table).")
        parser.add_argument("--save_to_flash", dest="save_flash_list", action="append", help="If you write a RAM table that you want saved to flash, it must be saved using the SHARED table ID number. Example: 'robo_config.py -p 1 --save_to_flash 0' will save the contents of shared table '0' to flash. ");
        parser.add_argument("--WA", "--write_all", dest="write_all", action="store_true", default=False, help="(under development) if specified, then all sections in the KVF are written");
        parser.add_argument("-U", "--update_param", dest="updated_param", action="append", help="Update a single parameter using full table reference. Works only on NON FLASH tables.  Spaces must be surrounded by quotation marks. Valid Examples: -U \"sys_key_shared.run_level : 70\" -U sys_key_shared.run_level=70 -U calb_table_shared.encoder.theta_beta_poly=-88.580639,313.56,-0.042435" );
        

        
        #############                  
        ### reading table options
        parser.add_argument("-n", "--read_iterations", dest="read_iterations", default=1, type=int_any_fmt, help = "number of times to read.");
        parser.add_argument("-S", "--status", dest="get_status", action="store_true", default=False, help = "will attempt to read the boot_status_obj tlabe.");
        
        parser.add_argument("-s", "--stdout", dest="output_to_stdout", action="store_true", default=False, help = "when reading tables to a file, output will be to screen.");
        
        parser.add_argument("-O", "--output_file", dest="output_file_list", type=str, action="append", help="when reading table, they can be written to the file specified here.")
        parser.add_argument("-A", "--read_all", dest="read_all", action="store_true", default=False, help="read all tables in the robot");
        parser.add_argument("-R", "--read_table", action="append", dest="read_table_list", type=str, help="read specific tables from the robot.")

        parser.add_argument("-V", "--verify_table", dest="verify_table_list", action="append", help="specify which tables should be verified.")
        parser.add_argument("--VA", "--verify_all", dest="verify_all", action="store_true", help="all tables in the KVF are verified.");
        
        parser.add_argument("-C", "--compare_filename", default = None)
        
        self.args = parser.parse_args();
        
        # make a struct from the options dictionary
        opt = struct();
        for k,v in self.args.__dict__.iteritems() :
            opt.__dict__[k] = v
            # print k,v
            self.opt = opt;


        if not opt.output_file_list :
            self.opt.output_to_stdout = True
            
        if opt.quiet :
            self.opt.output_to_stdout = False


        return  

    def __str__(self) :
        return class_to_str(self)



if __name__ == "__main__" :

    opt_parser = config_options();
    opt = opt_parser.opt;

    ### validate options

        

    ###########################################################
    ### make a connection to the robot
    pf = pfcmd.PFCmd(0, 0);
    
    if opt.compare_filename :
        compare_fh = open(opt.compare_filename,'w')
    else :
        compare_fh = None
        
    if opt.LIN_mode:
        pf.ignore_rx_of_local_echo = True;
        pf.quiet = True
    
    if opt.com_port and opt.com_port < 100 :  # serial
        pf.connect(opt.com_port, opt.baud_rate, timeout_seconds=.5);
        # pf.set_serial_timeout(0.05);
    elif opt.ip_address :
        pf.tcp(opt.ip_address, port = opt.tcp_port);
    elif os.name == 'nt' :
        print "You must specifiy a COM Port or IP Address/Port."
        sys.exit(-1)
    else :
        # Discover the USB Serial Ports to use (ignore opt.port)
        sph = serial_port_helper.serial_port_helper()
        # Note: detects PSER movement from 0,1 to 2,3 (Linux)
        portNumStr = sph.get_port_to_use()
        if portNumStr == None :
            print 'USB Serial Port NOT FOUND... Exiting'
            exit(-1)
        else :
            portNum = int(portNumStr)
            MPD_COMMAND_SERIAL_PORT = portNum
            DATAPORT_SERIAL_PORT    = portNum + 1

        MPD_COMMAND_COM_PORT = sph.get_os_serial_port_name(MPD_COMMAND_SERIAL_PORT);
        print "Using MPD COMMAND SERIAL COM:", MPD_COMMAND_COM_PORT
        # Unused
        # DATAPORT_OS_COM_PORT = sph.get_os_serial_port_name(DATAPORT_SERIAL_PORT);
        # print "Using DATA SERIAL COM:"       , DATAPORT_OS_COM_PORT
        pf.connect(MPD_COMMAND_COM_PORT, opt.baud_rate, timeout_seconds=.5);

    ###
    
    configurator = robo_config_comm(pf, target_device = opt.target_device)
    configurator.disable_imu()
    
   

    ###########################################################
    ### read the 'table' file if specified on command line
    if opt.table_file_list :
        print "#READING KVF:"
        configurator.create_tables_from_input_files(opt.table_file_list, opt.debug_level)
        for t_fname in opt.table_file_list :
            print "#read file : %s " % t_fname
        
        
    option_start_time = time.clock()    
             
             
            
    ###########################################################
    ### process the write file
    if opt.write_table_list : 
            configurator.write_tables_from_input_file(opt.table_file_list, opt.write_table_list, opt.debug_level)
                    
    if opt.write_all:                
            print "WRITING ALL TABLES IN KVF:"
            configurator.write_all_tables_from_input_file(opt.table_file_list, opt.debug_level)
            
                            
            
        
    ###########################################################        
    ### process the write file
    if opt.modify_table_list :
        tbl = configurator.create_tables_from_input_file(opt.table_file_list)
        
        for var_name in opt.modify_table_list :
            sec = tbl.get_section(var_name)            

            if sec :
                print "read table (%s) from robot" % var_name
                table.read_table(var_name, debug_level=opt.debug_level);
                print "merging"
                print "writing : ", sec.get_name()
                table.write_table(write_tname, sec);
    
 
    if opt.updated_param:
        for string_arg in opt.updated_param:
            if (string_arg.count('=') + string_arg.count(':')) != 1:
                print "ERROR: '", string_arg, "' does not contain exactly 1 field separator ('=' or ':')"
                exit(-1)
            else:
                tname = ''
                if (string_arg.count('=') == 1):
                    tname = string_arg.split('=')[0]
                    args = string_arg.split('=')[1]
                elif (string_arg.count(':') == 1):
                    tname = string_arg.split(':')[0]
                    args = string_arg.split(':')[1]
                cleaned_args = [a.strip() for a in args.strip().split(',')]    
                configurator.single_param_update(tname.split('.')[0].strip(),".".join(tname.split('.')[1:]).strip(),cleaned_args)
                

    ####################### 
    ## save any flash tables AFTER the write/update
    
    if opt.save_flash_list:
            for table_id in opt.save_flash_list:
                configurator.save_RAM_to_flash(0)
        
    
    
    
    ###########################################################        
    ### process the read tables and read all table options
    if opt.get_status :
        if opt.read_table_list :            opt.read_table_list.append("boot_status_obj");
        else :            opt.read_table_list = ["boot_status_obj"]
        
        
    for i in range(0,opt.read_iterations):    
        if opt.read_all :
            returned_kvf = configurator.read_all_tables(opt.debug_level);   
            if i > 0: print returned_kvf
        
        elif opt.read_table_list :
            returned_kvf = configurator.read_tables(opt.read_table_list, opt.debug_level);   
            if i > 0: print returned_kvf
        
            



    ###########################################################        
    ### process the read tables and read all table options
    if opt.verify_all :
        print "VERIFY ALL TABLES:"
        configurator.verify_all_tables(opt.table_file_list, opt.debug_level,compare_fh)
#         raise Exception("option verify_all Never implemented.")
        
    elif opt.verify_table_list :
        print "VERIFY TABLES:"
        configurator.verify_tables(opt.verify_table_list, opt.table_file_list, opt.debug_level,compare_fh)
        
            

    ###########################################################
    ## display table KVF to the screen
    if opt.output_to_stdout:
        tbl = configurator.get_present_table_kvf()
        tbl.write(sys.stdout, PPRINT=True)

    ###########################################################
    ## write table KVF to one or more output files
    if opt.output_file_list:
        tbl = configurator.get_present_table_kvf()
        date_header_string = '\n# generated on %s\n\n' % time.strftime("%Y-%m-%d @ %H:%M:%S")
#        print(tbl)
#        for fname in opt.output_file_list :
#            fout = open(fname,'w');
#            tbl.write(fout, file_header = date_header_string, PPRINT=True)
#            fout.close();
        for fname in opt.output_file_list:
            tbl.write(open(fname, 'w'), PPRINT=True)
            tables_copied= 0
            for t in tbl.section_dict.iteritems():
                tables_copied+=1
            print "Wrote ",tables_copied, " tables to file '", fname, "'"
            
        
    ###########################################################
    ## list the available tables
    if opt.list_avail :
        configurator.print_available_tables()
        

    if opt.dump_table :
        configurator.dump_present_tables()
        

    if opt.timeit == True:
        s2 = time.clock() - option_start_time    
        print "Command took", s2*1000, " milliseconds."
        
        
    configurator.restore_imu_status()
