
#!/usr/bin/env python
SVN_SUB_REV_STR = "$Revision: 3108 $"

import python_paths_default  # test for user's python_paths.py and update paths
import python_paths as pyenv

import serial_port_helper
import sys, os;
import serial, time;
import comm_lib.pfcmd as pfcmd

trans_tbl = ' '*32 + ''.join(  ['%c' % chr(ndx) for ndx in range(32, 256)] );
    

import argparse
import struct

######################
class supress_duplicates :
    def __init__(self) :
        self.count = 0;
        self.last_lst = None;
        pass

    def next (self, lst) :
        brk = 0
        prt = True
        if lst == self.last_lst :
            if self.count :
                prt = False
            self.count += 1;
        else :
            self.last_lst = lst;
            if self.count :
                brk = self.count
            self.count = 0;
        return brk, prt





######################
class c_struct :
    pass

#helper for options so we can send cmd line arguments as hex or binary if we want. 
def int_any_fmt(x):
   return int(x, 0)    
    
 

class do_command_options :
    
    def __init__(self, args) :
        
        self.args = args
        
# allow special command data to be saved to find with command line redirection.
        try:
            arg_end = args.index(">")
            args = args[:arg_end];
            print i
        except:
            pass

        parser = argparse.ArgumentParser();
        
        parser.add_argument("-i", "--ip_address", dest="ip_address", help="make a TCP connection to the specified IP (instead of using serial)");
        parser.add_argument("-P", "--port", dest="port", type=int_any_fmt, default=10002, help="port number for TCP connections.");
        parser.add_argument("-p","-c", "--com_port", dest="com_port", type=int_any_fmt, help="COM PORT number for serial port connection");
        parser.add_argument("-B", dest="device_string", type=str, help="MAC or Linux device name");
        parser.add_argument("--btle_comport", dest="btle_comport", type=int_any_fmt, help = "COM PORT is a comport to a BLE Dongle.  Use BLE to find Bluetooth Low Energy radio connection and address.")

        parser.add_argument("--LIN", dest="LIN_mode", action="store_true", default=False, help="Use with LIN mode comm: ignores the local echo on the RX line of whatever command was just sent out.")
        parser.add_argument("-b", "--baud_rate", dest="baudrate", type=int_any_fmt, default=115200, help="baurdrate when using serial port (default is 115200)");
            
        parser.add_argument("-g", dest="get_type", type=str, help="get IO channels : options are [ai, di, ao, do]");
        parser.add_argument("--channel", dest="read_io_channel", type=int_any_fmt, help="which IO channel you want to read/write. Use option with -g to specify analog or digital pin")
        parser.add_argument("--set_channel_value", dest="set_channel_value", type=int_any_fmt, help="a value to write to an output pin. Usage: \"-g [ao, do] --channel N --set_channel_value M \"")
        
            
        parser.add_argument("--suffix", dest="special_command_suffix", default = "", type=str, help = "suffix for special commands (thus variations on the special commands can be used.)");
        parser.add_argument("-D", "--delay", dest="repeat_delay", type=int_any_fmt, default=30, help = "special command repeat delay in milliseconds");
        parser.add_argument("-v", "--version", dest="get_version", type=str, help = "execute the version command on each of the specified platforms.  Eg -v smi does all platforms.");
        parser.add_argument("--version_repeat", dest="version_repeat", type=int_any_fmt, default=1, help="execute the version commands specified this many times. defaults to 1. Eg -v smi --version_repeat 10 does all platforms 10x");
        parser.add_argument("-e", "--echo", dest="get_echo", type=str, help="execute the echo command \"12345\" on each of the specified platforms. Eg -v smi echoes 12345 back from all 3 platforms.")
        # parser.add_argument("-n", "--repeat_count", dest="repeat_count", action="append", default=1, help="number of times to repeat the supplied special command(s)");
        parser.add_argument("-n", "--repeat_count", dest="repeat_count", type=int_any_fmt, default=1, help="number of times to repeat the supplied special command(s)");
        
        parser.add_argument("-T", "--set_time", dest="set_time", action="store_true", default=False, help="use the PC time to set the robot clock");
        parser.add_argument("-t", "--timeout", dest="timeout", default=0.05, help="serial or TCP communcations timeout (sec)");
        parser.add_argument("--timeit", dest="timeit", action="store_true", default="False", help="display Round Trip Time of this special command to execute.")
        parser.add_argument("-X", "--hex_mode", dest="hex_output_mode", action="store_true", default=False, help="display the special command results in hex");
        parser.add_argument("--hex_output_file", dest="hex_output_file", type = str, help="Store resulting hex output in this binary data file");
        parser.add_argument("--hex_input_file", dest="hex_input_file", type = str, help="Write binary data file to specified memory location in this binary data file");

        parser.add_argument("-F", "--float_mode", dest="float_output_mode", action="store_true", default=False, help="interpret the data from the special command as floating point numbers");
        
        
        parser.add_argument("-f", "--file_name", dest="log_file_name", help="log the results to a file.");
        
        parser.add_argument("-S", "--struct_string", dest="struct_string", type=str, help="use struct.unpack(STRUT_STRING, data) to process the result of the special command.  Eg -S =10L");
        
        parser.add_argument("-R", "--read_address", dest="read_address", type=str, help = "read memory from address (int or hex)");
        
        parser.add_argument("-W", "--write_address", dest="write_address", type=str, help = "write data to address (int or hex)");
        
        parser.add_argument("-d", "--data_filename", dest="data_filename", type=str, help = "text file source for data that is written. file should contain numbers (UINT8_T) separated with a char from [,;:= ].  these numbers will be turned into a bytestream for use with memory write.");
        
        parser.add_argument("-L", "--read_length", dest="read_length", type=str, default = "0x100", help = "length of memory buffer to read");
        
        parser.add_argument("-M", "--memory_type", dest="memory_type", type=str, default=None, help="numeric code to specify the memory type and platform for read and write.  Use -H for a list.")
        
        parser.add_argument("-s", "--special", dest="special_cmd", type=int_any_fmt, nargs="+",action='append', metavar='SPECIAL_CMD', default=[], help = "-s N [arg1, ... ] issue special command N with arguments.  Args may be negative, and multiple commands may be issued by repeating the -s")
        
        parser.add_argument("-V", "--write_data", dest="write_data_list", nargs="+",action='append', help = "list of SPACE separated values available for memory write command.  Eg -V 1 2 3")        
        
        parser.add_argument("-H", "--memory_type_help", dest="memory_type_help", action="store_true", help="list the ID numbers for various robot memory types.");
        
        parser.add_argument("-Z", dest="disable_normal_robot", action="store_true", help="disable normal robot functions.  calls prepare_hardware");
        
        parser.add_argument("-C", "--char string", dest="char_input", type=str, default=None, help="used to provide a character string for the -W option.  eg ABCD.");
        
        # parser.disable_interspersed_args()
        args = parser.parse_args()
        
        # print options
        # print args
        
        # make a struct from the options dictionary
        opt = c_struct();
        for k,v in args.__dict__.iteritems() :
            opt.__dict__[k] = v
            #print k,v
            self.opt = opt;
        
        return  




                
if __name__ == "__main__":    
    
    parser = do_command_options(sys.argv)
    opt = parser.opt;
    # print "OPT = ", opt.__dict__
    
    
    def get_value(str) :
        u = str.upper()
        if u[0:2] == "0X" :
            v = int(u,16);
        else :
            v = int(u)
        return v
    
    
    
    
    
        
        
    
    
    
    
    
    ctrl = pfcmd.controller(None)
    if opt.memory_type_help or opt.memory_type and len(opt.memory_type) == 0 :
        ctrl.print_readwrite_mem_types();
    
    pf = pfcmd.PFCmd();
    if opt.device_string :
        dev = os.path.join("/dev", opt.device_string);
        print dev
        pf.connect(dev, opt.baudrate);
        pf.set_serial_timeout(opt.timeout);
    elif opt.com_port :
        print "SERIAL"
        pf.connect(opt.com_port, opt.baudrate);
        pf.set_serial_timeout(opt.timeout);
    elif opt.ip_address :
        pf.tcp(opt.ip_address, port = opt.port);
        pf.set_tcp_timeout(opt.timeout);
    elif opt.btle_comport:
        print "Using Bluetooth Low Energy"
        import comm_lib.bluetooth_support.bluetooth_low_energy_comport as ble_support
        import logging
        # Set up logging
        log_level = getattr(logging, "INFO", None)
        if not isinstance(log_level, int):
            raise ValueError('Invalid log level: {0}'.format(loglevel))
        logging.basicConfig(level=log_level, stream=sys.stdout)

    
        
        chosen_address = ble_support.scan("COM"+str(opt.btle_comport))
        emulated_serial = ble_support.BLESerialPortSocketWrapper(bluetooth_address=chosen_address, port = "COM"+str(opt.btle_comport), baudrate=115200, timeout=1.0)
        success = emulated_serial.open()
        if (success):
            pf.comm_device = emulated_serial;
        else:
            print "Bluetooth LE connection failed. Specify valid COMPORT."
            sys.exit()
    elif os.name != 'nt':
          print 'SERIAL LINUX'
          # Discover the USB Serial Ports to use
          sph = serial_port_helper.serial_port_helper()
          # Note: detects PSER movement from 0,1 to 2,3 (Linux)
          portNumStr = sph.get_port_to_use()
          if portNumStr == None :
            print('USB Serial Port NOT FOUND... Exiting')
            exit(-1)
          else :
            portNum = int(portNumStr)
            MPD_COMMAND_SERIAL_PORT = portNum
          
          MPD_COMMAND_COM_PORT = sph.get_os_serial_port_name(MPD_COMMAND_SERIAL_PORT);
          print("Using MPD COMMAND SERIAL COM:" + str(MPD_COMMAND_COM_PORT))
          port = MPD_COMMAND_COM_PORT
          pf.connect(port, opt.baudrate);
          pf.set_serial_timeout(opt.timeout);
    else:
        print "you must specify an IP address or COMPORT."
        sys.exit()
    
    
        
    
    if (os.name == 'nt') :
        import python_utils.keyboard;
        keybd =python_utils.keyboard.keyboard();
    else :
        keybd = None;
    
    ctrl = pfcmd.controller(pf)
    
    if opt.LIN_mode:
        pf.ignore_rx_of_local_echo = True;
    
    
    if opt.get_echo:
       if (  opt.get_echo.find('s')>=0) :
              ctrl.get_state_echo();
       if (  opt.get_echo.find('m')>=0) :
              ctrl.get_motor_echo();
       if (  opt.get_echo.find('i')>=0) :
              ctrl.get_imu_echo();
    
    
    if opt.get_version :
        for j in range(0, opt.version_repeat):
            if (  opt.get_version.find('s')>=0) :
                  ctrl.get_version( 0);
            if (  opt.get_version.find('m')>=0) :
                  ctrl.get_version( 1);
            if (  opt.get_version.find('i')>=0) :
                  ctrl.get_version( 2);
        
    if opt.get_type :
        import comm_lib.parse_iochan as parse_iochan;
        pio = parse_iochan.parse_iochan();
        
        if opt.read_io_channel :
            chan = [opt.read_io_channel]
        else :
            print "No channel specified, reading all channels."
            chan = []
            if (  opt.get_type.find('ai')>=0) :
                chan = [a['channel'] for a in pio.list_AI]
            if (  opt.get_type.find('di')>=0) :
                chan =[a['channel'] for a in pio.list_DI]
            if (  opt.get_type.find('ao')>=0) :
                chan =[a['channel'] for a in pio.list_AO]
            if (  opt.get_type.find('do')>=0) :
                chan =[a['channel'] for a in pio.list_DO]
            
        count = opt.repeat_count;
        for iter in xrange(0, count) :
            if (  opt.get_type.find('ai')>=0) :
                result = ctrl.read_analog_in(chan);
    
            if (  opt.get_type.find('di')>=0) :
                result = ctrl.read_digital_in(chan);
    
            if (  opt.get_type.find('ao')>=0) :
                #see if we should write new value before we read the output pin.
                if (opt.set_channel_value != None):
                    write_result = ctrl.write_analog_out(chan[0], opt.set_channel_value)
                result = ctrl.read_analog_out(chan);
    
            if (  opt.get_type.find('do')>=0) :
                #see if we should write new value before we read the output pin.
                if (opt.set_channel_value != None):
                    write_result = ctrl.write_digital_out(chan[0], opt.set_channel_value)
                result = ctrl.read_digital_out(chan);
                
            if result :
                if iter != 1 :
                    print "--------------------------------"
                
                for r in result :
                    if r.has_key('channel'):
                        name = pio.channel_to_name(r['channel']);
                        print  "%4d %6d   [%s]" % (r['channel'], r['value'], name)
                    else:
                        print "Error asking for channel:", [(k,r[k])  for k in r.keys()]
                
    if opt.set_time :
        d = ctrl.get_ankle_time()
        print "Before ", d
        ctrl.set_ankle_time(USE_UTC=True);
        time.sleep(0.5);  
        d = ctrl.get_ankle_time()
        print "After ", d
    
    
    if opt.char_input :
        #lst = [ chr(int(val)) for val in opt.char_input[0]]
        opt.bytestream = opt.char_input
    
    if opt.write_data_list :
        # print opt.write_data_list
        
        lst = [ chr(int(val)) for val in opt.write_data_list[0]]
        opt.bytestream = ''.join( lst)
    
    
    if opt.hex_input_file:
        print "reading bytestream from : ", opt.hex_input_file
        import re
    
        fin = open(opt.hex_input_file,'rb')
        opt.bytestream = fin.read()
        print "Will try to write ", len(opt.bytestream), "bytes to memory."
        fin.close()
        
    
    elif opt.data_filename :
        print "reading bytestream from : ", opt.data_filename
        import re
    
        fin = open(opt.data_filename,'r')
    
        lst = []
        for raw_ln in fin :
            ln = raw_ln.strip();
            if len(ln) == 0 or ln[0] == '#' or ln[0:2] == '//' :
                continue
            
            splt = re.findall("([-\d]+)[,;:= ]*", raw_ln);
            
            if splt :
                [lst.append( chr(int(val)) ) for val in splt]
        fin.close()
        opt.bytestream = ''.join( lst )
        print list(lst)
    
    
    
    if opt.write_address :
        addr = get_value(opt.write_address);    
        
        if opt.read_length:
            len_bs = len(opt.bytestream)*get_value(opt.read_length)
            opt.bytestream = opt.bytestream*get_value(opt.read_length)
        else:
            len_bs = len(opt.bytestream)
        
        if len_bs :
            result = ctrl.readwrite_mem('W', opt.memory_type, len_bs, addr, WRITE_BUFF=opt.bytestream, QUIET=False)        
    
        print result
            
    if opt.read_address :
        addr = get_value(opt.read_address);
        total_read_length  = get_value(opt.read_length);
        hex_output = None
        if (opt.hex_output_file):
            hex_output = open(opt.hex_output_file, 'wb');
        
        sd = supress_duplicates();
        
        block_length = 128;
        last_addr = 0;
        trans_str = ""
        str = ""
        for buf_ndx in range(0, total_read_length, block_length) :
            read_length = min(total_read_length-buf_ndx, block_length)
    
            try :
                result = ctrl.readwrite_mem('R', opt.memory_type, read_length, addr, QUIET=True)
            except :
                print "read error"
    
                
            if result and len(result) :
                if (hex_output != None):
                    hex_output.write(result[0]['buff'])
    
                if "ERROR" in result:
                    print
                    print "do_command:error:", result
                    print                
                elif "ecode" in result[0]: 
                    print "ecode", result[0]
                    buff_len = 0
                elif opt.char_input:
                     print result[0]['buff']
                elif opt.hex_output_mode:
                     print result[0]['buff']
                     
                else :
                    buff = result[0]['buff']
                    buff_len = len(buff)
                    
                    bytestream = ctrl.result_to_bytestream(result);
                    bytestream.print_addr = True;
                    bytestream.base_addr = addr;
                    
                    print bytestream
    
            
            
            if 0: 
                for ndx in range(0, buff_len, 16) :
                    lst = ["%2.2x" % ord(ch) for ch in buff[ndx:ndx+16] ]
                    str = ' '.join(lst);
                    trans_str = buff[ndx:ndx+16].translate(trans_tbl);
                    brk, prt = sd.next(lst);
                    if brk:
                        print "...."
                    if prt :
                        print "%3.3x : %s :: %s" % (addr, str, trans_str)
    
                    last_addr = addr;
                    addr += 16
                if buff_len : 
                    brk, prt = sd.next(None);
                    if brk:
                        print "...."
                    if prt :
                        print "%3.3x : %s :: %s" % (last_addr, str, trans_str)
            addr += read_length

            
        if (hex_output != None):
            hex_output.close();    
    opt.log_fout = None
    if opt.log_file_name :
        opt.log_fout = open(opt.log_file_name, 'w');
        
    
        
    """
    if opt.read_walking_param != None:
        import dspic_mem_to_struct;
        ds_mem = dspic_mem_to_struct.dspic_mem_to_struct()
    
        fname_list = ["c:/global.h", "c:/ceb/0svn/StateControl/global.h"];
        
        for fname in fname_list :    
            try :
                print "Trying ",fname
                ds_mem.parse_h_file(fname);
                break;
            except :
                pass
    
        obj = ds_mem.find_object("WALKING_STATEMACHINE_PARAM")
        
        # pf.debug_level = 4
    
        result = ctrl.read_walking_parameters(opt.read_walking_param, HEX=opt.hex_output_mode);
        data = ""
        # print type(result)
        for r in result :
            data = data + r['buff']
    
        s = time.strftime("%y%m%d_%H%M%S")
        fname= "%s_%s.bin" % ("walking_param", s);
        fout  =open(fname,"w");
        fout.write(data);
        fout.close();
        fname= "%s_%s.txt" % ("walking_param", s);
        txt = ds_mem.decode_object(obj, data);
        fout = open(fname,"w");
        fout.write(txt);
        fout.close();
    """
    
        
    
    
    
    if 0 and opt.motor_test : 
        mt = class_motor_test.motor_test(ctrl);
        if (  opt.motor_test.find('V')>=0) :
               mt.do_assending_voltage_test(cycles=10, period = 1.0, start=1.0, stop=5.0, step=0.5);
    
        if (  opt.motor_test.find('I')>=0) :
               mt.do_assending_current_test(cycles=10, period = 1.0, start=0.5, stop=5.0, step=0.5);
    
        if (  opt.motor_test.find('X')>=0) :
               mt.do_assending_current_test(cycles=10, period = 0.05, start=10.0, stop=15.0, step=0.5);
               mt.do_assending_current_test(cycles=10, period = 0.02, start=15.0, stop=25.0, step=1.0);
    
    
    if opt.disable_normal_robot :
        import benchtest_support
        benchtest_support.prepare_hardware(pf, ctrl);
    
        
    
    if opt.special_cmd :
        num_special_commands = len(opt.special_cmd)
        
        count = opt.repeat_count;
    
        pf.quiet = True;
        for iter in xrange(0, count) :
            if keybd :
                (raw_ch, esc, fun, ch) = keybd.getch_packed();
                if (ch == '\x1b') :
                    break;
    
            for cmd_num in range(0, num_special_commands) :
                cmd = opt.special_cmd[cmd_num]
                
                if opt.timeit == True: #Optimize a request when we are using timeit so we actually get an RTT
                    s = time.clock() #Use time.time() on posix, time.clock() on windows for best clock.
                    result = ctrl.pfcmd.optimized_do_command('special'+opt.special_command_suffix, cmd);
                    s2 = time.clock() - s    
                    print "Command took", s2*1000, " milliseconds."
                
                else:
                    result = ctrl.special(cmd[0], cmd[1:], secs=0.001 * opt.repeat_delay, suffix=opt.special_command_suffix);
                    
    
                if not result  :
                    continue
    
                if opt.struct_string :
                    # print result.keys()
                    # print len(result['rsp_string'])            
                    lst = struct.unpack(  opt.struct_string, result['rsp_string'][8:])
                    print lst
                    if opt.log_fout :
                        opt.log_fout.write(lst + '\n'); 
                elif opt.float_output_mode:
                    lst = struct.unpack( "=10f", result['rsp_string'][8:])
                    print lst
                    if opt.log_fout :
                        opt.log_fout.write(lst + '\n'); 
                else :
                    try :
                        field_list = result['rsp_fields'];
    
                        if iter < 1 and cmd_num < 1:
                            header_str = ' '.join( ["%-8s " % field for field in field_list] );
                            print header_str
                            if opt.log_fout :
                                opt.log_fout.write(header_str + '\n');                        
    
                        if (opt.hex_output_mode) :
                            dat_str = ' '.join( ["$%-8x " % result[field] for field in field_list] );
                        else :
                            dat_str = ' '.join( ["%-8s " % result[field] for field in field_list] );
                        print dat_str
                        if opt.log_fout :
                            opt.log_fout.write(dat_str + '\n');
    
                    except :
                        # print "NAK"
                        pass
    
    
    if opt.log_fout :
        opt.log_fout.close()
        
    
    
    
