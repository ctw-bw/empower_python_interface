#!/usr/bin/env python
'''
Created on Mar 31, 2016

@author: ekurz
'''
from __future__ import print_function
import python_paths_default
import python_paths
import optparse
import sys
import time
import pfcmd
import calb_springs
import ConfigParser
import ordereddict
import loggHandeler
import serial_port_helper
import robo_config

STARTUP_COMM_DELY_DUE_TO_BLUETOOTH_INIT = 5.0 ### Bluetooth init in Tango means that comm is inaccessible up to 5 seconds after power up of unit. 
COMM_NO_RESPONSE_TIMEOUT_SECONDS = .25
TIMEOUT_SECONDS_POWER_CYCLE = .25

if __name__ == '__main__':
    ankleCon = pfcmd.PFCmd(0,0)
    parser = optparse.OptionParser();
    parser.add_option('-f', '--data_file',  dest="data_file", help="data output file")
    parser.add_option("-b","--baudrate", type="int", default="115200", help="Baud rate for ankle commands. Defaults to 115200.")
    (opt, args) = parser.parse_args(sys.argv);
    
    # Discover the USB Serial Ports to use
    sph = serial_port_helper.serial_port_helper()
    # Note: detects PSER movement from 0,1 to 2,3 (Linux)
    portNumStr = sph.get_port_to_use()
    if portNumStr == None :
        print('USB Serial Port NOT FOUND... Exiting')
        exit(-1)
    else :
        portNum = int(portNumStr)
        MPD_COMMAND_SERIAL_PORT = portNum
        DATAPORT_SERIAL_PORT    = portNum + 1

    MPD_COMMAND_COM_PORT = sph.get_os_serial_port_name(MPD_COMMAND_SERIAL_PORT);
    print("Using MPD COMMAND SERIAL COM:" + str(MPD_COMMAND_COM_PORT))
    port = MPD_COMMAND_COM_PORT
    
    logHandler = loggHandeler.logger_class('test.txt')
    #Wait in case we just turned on ankle. 
    #print('Waiting for Ankle Bluetooth Comm to finish...',file = logHandler)
    #time.sleep(STARTUP_COMM_DELY_DUE_TO_BLUETOOTH_INIT)
    ankleCon.connect(portNum, opt.baudrate,COMM_NO_RESPONSE_TIMEOUT_SECONDS);  
    ankleCon.flush(); #Flush anything in comport from Radio
    
    rc = robo_config.robo_config_comm(ankleCon,'SC')
    result = rc.read_tables(['calb_table_shared','AAE_cfg_shared'], debug_level=0)
    
    cfg = ConfigParser.ConfigParser(dict_type=ordereddict.OrderedDict)
    cfg.optionxform=str
    cfg.add_section('mfg_table')
    cfg.add_section('calb_table_shared')
    cfg.set('calb_table_shared', 'encoder.aae_sensor_at_true_north', \
            result.section_dict['calb_table_shared']['encoder.aae_sensor_at_true_north'][0])
    cfg.add_section('AAE_cfg_shared')
    print(result.section_dict['AAE_cfg_shared']['joint_encoder_bits'][0])
    print(type(result.section_dict['AAE_cfg_shared']['joint_encoder_bits'][0]))
    cfg.set('AAE_cfg_shared','joint_encoder_bits', str(result.section_dict['AAE_cfg_shared']['joint_encoder_bits'][0]))
    print(cfg.get('AAE_cfg_shared','joint_encoder_bits'))
    
    spring = calb_springs.springClass(ankleCon, 1, cfg,opt.data_file, logHandler)
    spring.findk3()
    print('data collected')
    time.sleep(0.5)
    f = open(opt.data_file,'a')
    f.write('#data collection complete')
