'''
Created on Mar 19, 2014

@author: ekurz
'''
from Phidgets.Devices.InterfaceKit import InterfaceKit
from Phidgets.PhidgetException import PhidgetErrorCodes, PhidgetException

class relay():
    '''
    This class creates a interface with a robot electronics USB relay
    '''
    def __init__(self):
        self.rl = InterfaceKit()
        self.rl.openPhidget()
        self.rl.waitForAttach(10000)
        
    def get_states(self):
        '''
        Gets a binary from the relay witch represents which of the relays are turned on.
        '''
        count = self.rl.getOutputCount()
        status =[]
        for x in range(0,count):
            status.append(self.rl.getOutputState(x))
#         print status
        return status
        
    def turn_all_on(self):
        '''
        sends a command to the relay that turns on all of the relays
        '''
        num = self.rl.getOutputCount()
        for x in range(0,num):
            self.rl.setOutputState(x, True)
    
    def turn_on(self,relay):
        self.rl.setOutputState(relay, True)
        
    def turn_all_off(self):
        '''
        sends a command to the relay that turns all off all of the relays
        '''
        num = self.rl.getOutputCount()
        for x in range(0,num):
            self.rl.setOutputState(x, False)
    
    def turn_off(self, relay):
        self.rl.setOutputState(relay, False)

if __name__ == '__main__':
    """
    This is an example program of how to use this class
    """
    import time
#     port = 26
    rl = relay()
    rl.turn_all_on()
    print rl.get_states()
    time.sleep(2)
    rl.turn_all_off()
    print rl.get_states()
    time.sleep(2)
    rl.turn_on(1)
    print rl.get_states()
    time.sleep(2)
    rl.turn_all_off()
    time.sleep(1)
    print rl.get_states()