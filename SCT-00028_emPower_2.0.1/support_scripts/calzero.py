#!/usr/bin/env python

SVN_SUB_REV_STR = "$Revision: 6463 $"

#from ReadLoadCell import *
from LtfPy import *

import sys
import optparse

def calzero(args) :

    p = optparse.OptionParser()
    p.add_option ('-v', '--verbose', default = '1',)
    (opt, arg) = p.parse_args(args)
    verbose = int(opt.verbose)

    # Define which amp ID to use
    copley_node_id = 1
    
    # Instantiate an object to control the Copley amplifier
    ecat = MoveEcat("eth1", copley_node_id)
    
    # Initialize the amp(s)
    ecat.InitAmp()
    
    # Get the amplifier being controlled
    amp = ecat.getAmp(0)
    
    # Set te moor position to zero
    amp.SetPositionMotor(0)

    if verbose :
        # TODO - detect failure to zero the motor position
        print "The motor position has been zeroed"
    
    # Release the copley
    ecat.close()

if __name__ == '__main__' :
    calzero(sys.argv)
