

import os, sys


is_frozen = bool( getattr(sys, 'frozen', None) )
# Get path variable, attempt to automatically determinate the root location

if not is_frozen:
    whole_path = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))
else: 
    whole_path = os.path.abspath(os.path.dirname(sys.executable))

scripts_parent = whole_path.split(os.sep)
if 'support_scripts' not in scripts_parent:
#See if we are in a packaged py2exe file: if so, assume at the top level that we have all the modules we need stored in our library.zip file and ignore python_paths completely. Otherwise, throw exception.
    if is_frozen:
        BASE_PATH = whole_path
        FIRMWARE_BASE_PATH = BASE_PATH + os.sep + "firmware"
        SUPPORT_SCRIPTS_BASE = BASE_PATH + os.sep + "library.zip"
    else:
        print scripts_parent
        raise Exception("Error: Python script must be run under 'support_scripts' directory for automatic path support.")
else:
    BASE_PATH = os.sep.join(scripts_parent[0:scripts_parent.index('support_scripts')])
    SUPPORT_SCRIPTS_BASE = BASE_PATH + os.sep + "support_scripts"
    FIRMWARE_BASE_PATH = BASE_PATH + os.sep + "firmware"
    COM_BASE = SUPPORT_SCRIPTS_BASE+os.sep + 'comm_lib'
    pf_base = SUPPORT_SCRIPTS_BASE+os.sep + 'pf_python'
    ltf_base = SUPPORT_SCRIPTS_BASE+os.sep+'ltf'
    test_base = SUPPORT_SCRIPTS_BASE+os.sep+'testPrograms'
    calb_base = SUPPORT_SCRIPTS_BASE+os.sep+'testPrograms'+os.sep+'calb'
    sys.path.insert(0,BASE_PATH )
    sys.path.insert(0,SUPPORT_SCRIPTS_BASE)
    sys.path.insert(0,test_base)
    sys.path.insert(0, COM_BASE)
    sys.path.insert(0,pf_base)
    sys.path.insert(0, calb_base)
    sys.path.insert(0,ltf_base)

# specifiy the location of the project root directory:
# FIRMWARE_BASE_PATH = 'C:/src/this_project_root_directory';

# specify the location of critical files in the project
EEPROM_KEYS_RELATIVE_FNAME = FIRMWARE_BASE_PATH+ '/StateController/Comm/Config/eeprom_keys.h';
IOCHAN_RELATIVE_FNAME = FIRMWARE_BASE_PATH+ '/StateController/Meas/io_chan.h';
SC_GLOBAL_H_RELATIVE_FNAME = FIRMWARE_BASE_PATH+ '/StateController/Comm/Config/global.h'



