'''
Created on Oct 7, 2011

@author: ekurz
'''

# to use this build program you need to give it the parameter py2exe 

from distutils.core import setup
import sys
import py2exe
import os

DATA=[('imageformats',['C:\\Python27/Lib/site-packages/PyQt4/plugins/imageformats/qjpeg4.dll',
    'C:\\Python27/Lib/site-packages/PyQt4/plugins/imageformats/qgif4.dll',
    'C:\\Python27/Lib/site-packages/PyQt4/plugins/imageformats/qico4.dll',
    'C:\\Python27/Lib/site-packages/PyQt4/plugins/imageformats/qmng4.dll',
    'C:\\Python27/Lib/site-packages/PyQt4/plugins/imageformats/qsvg4.dll',
    'C:\\Python27/Lib/site-packages/PyQt4/plugins/imageformats/qtiff4.dll'
    ])]

#sys.path.append("C:\\Program Files\\Microsoft Visual Studio 9.0\\VC\\redist\\x86\\Microsoft.VC90.CRT")

#setup(console=['do_command.py'])

setup(
      options = {'py2exe': {'bundle_files':1,'compressed':True,}},
      console = ["do_command.py"],
      zipfile = None
      )

# setup(
#     options = {'py2exe': {'bundle_files': 1, 'compressed': True,}},#"includes":["sip"]}},
#     windows = [{'script': "do_command.py"}],
#     zipfile = None,
#     data_files = DATA,
# )
