'''
Created on Oct 30, 2014

@author: ekurz
'''
import python_paths_default
import python_paths
import serial_port_helper
import pfcmd
import ConfigParser
import ordereddict
import calb_springs
import ankle_commands
import time
import USB_RLY_Phidgets
import os
import wx
import datetime

def turnOn(rl,pf,cp, baudrate=115200):
    rl.turn_all_on()
    time.sleep(2)
    pf.connect(cp, baudrate);   # baudrate=115200

def powercycle(rl,pf,cp, baudrate=115200):
    pf.disconnect()
    rl.turn_all_off()
    time.sleep(0.5)
    turnOn(rl, pf, cp, baudrate = baudrate)

def workflowSetTS(cfgObj) :
    """set the datetime of the last update to the configuration"""
    d = datetime.datetime.now()
    cfgObj.set('mfg_table', 'last_update', d.strftime('%Y%m%d_%H%M%S'))

def commitConfigFileLocally(cfg,fn,logger) :
    """commit the current configuration to local file, including timestamp"""
    workflowSetTS(cfg)

    with open(fn,'w') as fh :
        cfg.write(fh)
        fh.flush()

if __name__ == '__main__':
    
    
    springFile = os.path.join(os.path.dirname(os.path.realpath(__file__)), time.strftime('spring_cal_%y%m%d_%H%M%S'))
    
    
    DATAPORT_SERIAL_BAUD_RATE = 1250000
    MPD_COMMAND_BAUD_RATE = 115200
    # Discover the USB Serial Ports to use
    sph = serial_port_helper.serial_port_helper()
    # Note: detects PSER movement from 0,1 to 2,3 (Linux)
    portNumStr = sph.get_port_to_use()
    if portNumStr == None :
        print 'USB Serial Port NOT FOUND... Exiting'
        exit(-1)
    else :
        portNum = int(portNumStr)
        MPD_COMMAND_SERIAL_PORT = portNum
        DATAPORT_SERIAL_PORT    = portNum + 1
    rl = USB_RLY_Phidgets.relay()
    app = wx.App(False)
    
    MPD_COMMAND_COM_PORT = sph.get_os_serial_port_name(MPD_COMMAND_SERIAL_PORT);
    print "Using MPD COMMAND SERIAL COM:", MPD_COMMAND_COM_PORT
#     folderName = "Spring"+time.strftime('_calibration_%y%m%d_%H%M%S')
#     os.mkdir(folderName)
    # Unused
    # DATAPORT_OS_COM_PORT = sph.get_os_serial_port_name(DATAPORT_SERIAL_PORT);
    # print "Using DATA SERIAL COM:"       , DATAPORT_OS_COM_PORT
    pf = pfcmd.PFCmd()
    pf.connect(MPD_COMMAND_COM_PORT, MPD_COMMAND_BAUD_RATE);
    cfg = ConfigParser.ConfigParser(dict_type=ordereddict.OrderedDict)
    cfg.read('template_ankle_tables.ini')
    
    turnOn(rl, pf,MPD_COMMAND_COM_PORT)
    time.sleep(1)
    runLevel = pf.do_command(ankle_commands.s_command_type(), ankle_commands.s_runlevel_get())
    print 'run level %d'%runLevel.get('v0')
    if runLevel.get('v0') != 20:
        print pf.do_command(ankle_commands.s_command_type(),ankle_commands.s_runlevel_set_ankle())
#         time.sleep(1)
#         print pf.do_command(ankle_commands.s_command_type(),ankle_commands.s_runlevel_save)
        time.sleep(1)
        powercycle(rl, pf, MPD_COMMAND_COM_PORT)
    pf.do_command(ankle_commands.s_command_type(), ankle_commands.s_set_system_idle())
    time.sleep(1)
    pf.do_command(ankle_commands.s_command_type(), ankle_commands.s_motor_open_leads())
    
#     springFile = os.path.join(os.path.dirname(os.path.realpath(__file__)), time.strftime('spring_cal_%y%m%d_%H%M%S'))
    
    spring = calb_springs.springClass(pf, 1, cfg,springFile)
    k2 = spring.findk2()
    k3 = spring.findk3()
    
    rl.turn_all_off()
    SFile = os.path.join(springFile, 'spring vals.txt')
    commitConfigFileLocally(cfg, SFile, None)