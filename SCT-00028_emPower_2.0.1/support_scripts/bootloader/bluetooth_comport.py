import sys
try:
    import bluetooth #Requires PyBluez library to be installed. 
except Exception as e:
    print e
    print "Python PyBluez not installed. Install it from here for Python 2.7:"
    print "http://svn.iwalk.com/svn/svn_engineering/PowerFoot/trunk/tools/PyBluez-0.19.beta.win32-py2.7.exe"
    sys.exit(-1)

import serial, select, time
from serial import *

possible_baud_settings = {1200: '5', 2400:'10', 4800:'20', 9600:'39', 19200:'79', 38400:'157', 57600:'236', 115200:'472', 230400:'944', 460800:'1887', 921600:'3775'}
possible_baud_readback = {'0005':1200, '000A': 2400, '0014':4800, '0027':9600, '004F': 19200, '009D':38400, '00ec': 57600, '01D8': 115200, '03B0': 230400, '075F': 460800, '0EBF': 921600}
   

""" Gives a BluetoothSocket the same basic interface as a typical PySerial com port for code interoperability """
class SerialPortSocketWrapper (serial.Serial):
 
     
    def __init__(self, bluetooth_address=None, port = 1, baudrate=9600, bytesize=EIGHTBITS, parity=PARITY_NONE, stopbits=STOPBITS_ONE, timeout=2.0, xonxoff=False, rtscts=False, writeTimeout=None, dsrdtr=False, interCharTimeout=None):
        super(self.__class__, self).__init__(None, baudrate, bytesize, parity, stopbits, timeout, xonxoff, rtscts, writeTimeout, dsrdtr, interCharTimeout)
        self.bluetooth_address = bluetooth_address
        self.client_port = port
        self.client_socket=bluetooth.BluetoothSocket( bluetooth.RFCOMM )
        self.baudrate = baudrate
        self.timeout = timeout
        
    def open(self):
        if (self.client_socket == None):
            self.client_socket=bluetooth.BluetoothSocket( bluetooth.RFCOMM )
        self.client_socket.connect((self.bluetooth_address, self.client_port)) 
        self.client_socket.setblocking(0)
        self.client_socket.settimeout(self.timeout)
        self.readBuffer = '';
        self.setBluetoothBaudrate(self.baudrate)
        self.local_bluetooth_address= self.client_socket.getsockname()[0] #getsockname() returns a tuple of (address, port)
        
        
    def close(self):
        self.client_socket.close()
        try:
            del(self.client_socket)
            self.client_socket = None
        except:
            pass
            
    def __del__(self):
        try:
            self.client_socket.close()
        except:
            pass
            
    def read(self,size=1):
        start = time.time() 
        toReturn = ''
        while len(self.readBuffer) < size:
            if time.time() - start > self.timeout:
                print "timeout on read."
                break;
            self.inWaiting(); #does the actual reading out
        toReturn = self.readBuffer[0:size]
        self.readBuffer = self.readBuffer[size:]
        return toReturn
        
    def write(self, data):
        return self.client_socket.send(data)
        
    def inWaiting(self):
        ready = select.select([self.client_socket], [], [], .001)
        #print ready
        if ready[0]:
            got = self.client_socket.recv(4096); #try reading up to 4096 bytes. 
            self.readBuffer = "".join([self.readBuffer, got])
        
        return len(self.readBuffer)
        
    def flush(self):
        got = ''
        toGet = self.inWaiting()
        while toGet > 0:      
            toGet = self.inWaiting()
            got = got + self.read(toGet)
           
        self.readBuffer = ''
        #print [ord(x) for x in got]
        
    def flushInput(self):
        readBuffer = ''

    def flushOutput(self):
        pass

    def setBluetoothInFastDataMode(self): #This disables command mode in favor of 
        """
        Sets bluetooth into fast data mode. Once this is set, the unit has to be power cycled to undo it. 
        See the following datasheet for how we're getting commands:
        https://www.sparkfun.com/datasheets/Wireless/Bluetooth/BlueRadios_ATMP_Commands_Rev_3.5.1.1.0.pdf
        """
        #Enter command mode:
        self.write("+++\r") #Wait 100 ms between each radio command. 
        time.sleep(.2)
        toRead = self.inWaiting()
        #print self.read(toRead) #May or may not get a response depending on if we've already called "+++" enter command mode. 
        self.write('ATMF\r')
        time.sleep(1.0)
        toRead = self.inWaiting()
        readout = self.read(toRead)
        

    def setBluetoothBaudrate(self, baudrate):
        """
        Sets bluetooth baudrate for whatever its connected to. See the following datasheet for how we're getting commands:
        https://www.sparkfun.com/datasheets/Wireless/Bluetooth/BlueRadios_ATMP_Commands_Rev_3.5.1.1.0.pdf
        """
        global possible_baud_settings,possible_baud_readback
        if baudrate not in possible_baud_settings:
            print 'ERROR:', baudrate, 'is not an allowed baud rate for this Bluetooth module.'
            raise Exception
        #Enter command mode:
        self.write("+++\r") #Wait 100 ms between each radio command. 
        time.sleep(.2)
        toRead = self.inWaiting()
        #print self.read(toRead) #May or may not get a response depending on if we've already called "+++" enter command mode. 
        
        self.write("ATSW20,"+possible_baud_settings[baudrate]+",0,0,1\r");  # 472 is for 115200 
        time.sleep(.2) #Does not return response
        
        self.write('ATSI,8\r')
        time.sleep(2.0)
        toRead = self.inWaiting()
        if toRead:
            readout = self.read(toRead).split('\n')
            #print readout
            readLastCmdResponse = readout[-2]
            if (readLastCmdResponse == None): return False        
            actual = (readLastCmdResponse.split(',')[0]) #In format: 0000,0000,0000  first number is hex corresponding to new baud.
            #print actual
            if actual in possible_baud_readback.keys():
                print 'Baudrate is:', possible_baud_readback[actual] 
                self.baudrate = possible_baud_readback[actual]
                print 'Port reopened at new baudrate ', self.baudrate
                return True
        
        return False 
 
def scanForSerialPortProfileBluetoothDevices():
#Figure out which port the BiOM will talk to us on:
    serial_port_uuid = "00001101-0000-1000-8000-00805F9B34FB" #SPP serial port profile used as default for most bluetooth devices
    service_matches = bluetooth.find_service( uuid = serial_port_uuid )
    for m in service_matches:
        print m
        if m["host"] == remote_address:
            port = m["port"]
            name = m["name"]
            host = m["host"]
   
            print "connecting to \"%s\" on %s" % (name, host)
            #Create the client socket
            client_socket=bluetooth.BluetoothSocket( bluetooth.RFCOMM )
            client_sock.connect((host, port))


 
if __name__ == "__main__":    
    test = SerialPortSocketWrapper(bluetooth_address="00:A0:96:2A:8B:30", port = 1, baudrate=230400, timeout=2.0)
    test.open()
    print "Opened."
    
    # test.write("ATSI,2\r") #Wait 100 ms between each radio command. 
    # time.sleep(.2)
    # toRead = test.inWaiting()
    # if toRead:
    #     print test.read(toRead)
    
    
    # test.write("ATSN,IW_BIOM_H314\r");  
    # time.sleep(.2)
    # toRead = test.inWaiting()
    # if toRead:
    #     print test.read(toRead)
    
    
    # test.write("ATSW20,472,0,0,1") #set baud to 115200
    # time.sleep(.2)
    # toRead = test.inWaiting()
    # if toRead:
    #     print test.read(toRead)
    
    