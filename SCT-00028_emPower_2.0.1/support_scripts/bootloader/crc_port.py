import sys, copy
import os
import os.path as paths
# Python port of C# Console program to calculate the CRC-32-IEEE for a Microchip dsPIC33 .hex file.
# Original program located at H22\support_scripts\hex_crc\HexCRC\Program.cs 
# Original program author: jstarr@iwalk.com


#Port of Mem24 Class:
class Mem24:    
    def __init__(self):
        self.used = False
        self.data = [0xFF, 0xFF, 0xFF]

    def SetData(self,newData, i):
       self.used = True;
       self.data[0:3] =  newData[i:i+3];
      

class Mem32:    
    def __init__(self):
        self.used = False
        self.data = [0xFF, 0xFF, 0xFF, 0xFF]

    def SetData(self,newData, i):
       self.used = True;
       self.data[0:4] =  newData[i:i+4];
      
 
#Port from C# of Mem16 class:
class Mem16:
    def __init__(self):
        self.used = False
        self.data = [0xFF, 0xFF]            
    
    def SetData(self, newData, i):
        self.used = True;
        self.data[0:2] =  newData[i:i+2];
 
#Port from C# of MemBlock: 
class MemBlock:
    def __init__(self, newStart):
        self.start = newStart
        self.end = newStart      
    
    def GetSummaryStr(self):
        values = {'a':start*2, 'b':end*2}
        return "FROM: %(a)06X TO: %(b)06X" % values;

instCount = 87552;   #// 256Kb part
eeWordCount = 2048;
cfgRegCount = 16;
pgmMemoryUpperLimit = instCount * 2;

stm_wordcount = (0x8100000 - 0x8000000);

        
class File_Contents:
    def __init__(self):
        self.pgmFlashImage32bit = [Mem32()] * stm_wordcount;#new Mem24[instCount];
        self.pgmFlashImage24bit = [Mem24()] * instCount;#new Mem24[instCount];
        self.eepromImage16bit = [Mem16()] * eeWordCount ; #len should be eeWordCount
        self.cfgRegImage16bit = [Mem16()] * cfgRegCount; #len should be cfgRegCount
        self.hexLineList = [] #new List<HexLine> { };
        self.pgmBlockList = [] #new List<MemBlock> { };
        self.eeBlockList = [] #new List<MemBlock> { };
        self.cfgBlockList = [] #new List<MemBlock> { };
        self.crcArray = [] #new ushort[18];
        #self.crc32 = 0xFFFFEEEE;


#No port for this method, mem init automatic in python. 
#         static void InitMaps()
#         {
#             for (int i = 0; i < instCount; ++i)
#                 pgmFlashImage[i] = new Mem24();
# 
#             for (int i = 0; i < eeWordCount; ++i)
#                 eepromImage[i] = new Mem16();
# 
#             for (int i = 0; i < cfgRegCount; ++i)
#                 cfgRegImage[i] = new Mem16();
#         }
def initMaps():
    pass
            
#Port below.
#             static void ParseHex(string hexFileName)
#         {
#             using (StreamReader sr = new StreamReader(hexFileName))
#             {
#                 uint currentULBA = 0;
#                 string inLine;
#                 while ((inLine = sr.ReadLine()) != null)
#                 {
#                     HexLine hl = new HexLine(inLine, currentULBA);
#                     if (hl.Type == HexLine.RecordType.REC_TYPE_EXT_ADDR)
#                         currentULBA = hl.ULBA;
#                     hexLineList.Add(hl);
#                 }
#             }
#         }
def parseHex(hexfile):
    global hexLineList
    with open(hexfile) as f:
        print "Parsing hex file '" + hexfile 
        currentULBA = 0;
        hexLineList = []
        for inLine in f:    
            hexLine = HexLine(inLine, currentULBA)
            if hexLine.r_type == RecordType.REC_TYPE_EXT_ADDR:
                currentULBA = hexLine.ULBA();
            hexLineList.append(hexLine)
        return hexLineList    
            
#port below            
#        static void ReportHexLineList()
#        {
#            foreach (HexLine hl in hexLineList)
#             {
#                 Console.WriteLine(hl.GetSummaryStr());
#             }
#         }

def reportHexLineList(hexList):
    for hl in hexList:
        print hl.getSummaryStr()


#Port located below:
#         static void AnalyzeMemory()
#         {
#             foreach (HexLine hl in hexLineList)
#             {
#                 if (hl.Type == HexLine.RecordType.REC_TYPE_DATA)
#                 {
#                     uint addr = hl.Addr;
#                     if (addr < pgmMemoryUpperLimit)
#                     {
#                         // In Program Flash
#                         for (int i = 0; i < hl.Len / 4; ++i)
#                         {
#                             pgmFlashImage[(hl.Addr / 2 + i)].SetData(hl.data, i * 4);
#                         }
#                     }
#                     else if (addr >= 0xF80000 && addr < 0xF80010)
#                     {
#                         // Config Registers
#                         for (int i = 0; i < hl.Len / 4; ++i)
#                         {
#                             cfgRegImage[((hl.Addr - 0xF80000) / 2 + i)].SetData(hl.data, i * 4);
#                         }
#                     }
#                     else if (addr >= 0x7FF000 && addr < 0x800000)
#                     {
#                         // EEPROM
#                         for (int i = 0; i < hl.Len / 4; ++i)
#                         {
#                             eepromImage[((hl.Addr - 0x7FF000) / 2 + i)].SetData(hl.data, i * 4);
#                         }
#                     }
#                     else
#                     {
#                         // Outside of any expected address range
#                         Console.WriteLine("Address out-of-range {0:X8}", addr);
#                     }
#                 }
#             }
# 
#             // Scan over all instructions, looking for blocks
#             MemBlock pgmBlock = null;
#             for (uint i = 0; i < instCount; ++i)
#             {
#                 Mem24 inst24 = pgmFlashImage[i];
#                 if (inst24.used)
#                 {
#                     if (pgmBlock != null)
#                     {
#                         pgmBlock.end = i;
#                     }
#                     else
#                     {
#                         pgmBlock = new MemBlock(i);
#                     }
# 
#                 }
#                 else if (pgmBlock != null)
#                 {
#                     pgmBlockList.Add(pgmBlock);
#                     pgmBlock = null;
#                 }
#             }
#             if (pgmBlock != null)
#             {
#                 pgmBlockList.Add(pgmBlock);
#                 pgmBlock = null;
#             }
# 
#             // Scan over all EEPROM, looking for blocks
#             MemBlock eeBlock = null;
#             for (uint i = 0; i < eeWordCount; ++i)
#             {
#                 Mem16 eeWord = eepromImage[i];
#                 if (eeWord.used)
#                 {
#                     if (eeBlock != null)
#                     {
#                         eeBlock.end = i;
#                     }
#                     else
#                     {
#                         eeBlock = new MemBlock(i);
#                     }
#                 }
#                 else if (eeBlock != null)
#                 {
#                     eeBlockList.Add(eeBlock);
#                     eeBlock = null;
#                 }
#             }
#             if (eeBlock != null)
#             {
#                 eeBlockList.Add(eeBlock);
#                 eeBlock = null;
#             }
# 
#             // Scan over all Config Registers, looking for blocks
#             MemBlock cfgBlock = null;
#             for (uint i = 0; i < cfgRegCount; ++i)
#             {
#                 Mem16 cfgReg = cfgRegImage[i];
#                 if (cfgReg.used)
#                 {
#                     if (cfgBlock != null)
#                     {
#                         cfgBlock.end = i;
#                     }
#                     else
#                     {
#                         cfgBlock = new MemBlock(i);
#                     }
#                 }
#                 else if (cfgBlock != null)
#                 {
#                     cfgBlockList.Add(cfgBlock);
#                     cfgBlock = null;
#                 }
#             }
#             if (cfgBlock != null)
#             {
#                 cfgBlockList.Add(cfgBlock);
#                 cfgBlock = null;
#             }
# 
#         }

def analyzeMemory(hexList, file):
    global pgmMemoryUpperLimit
    m = None
    for hl in hexList:
        if (hl.Type() == RecordType.REC_TYPE_DATA):
            addr = hl.Addr() 
            if (0x8000000 <= addr and addr < 0x8100000):
                #// STM32 range
                for i in range(0, hl.Len() / 4):
                    m = Mem32()
                    m.SetData(hl.data, i * 4);
                    file.pgmFlashImage32bit[((hl.Addr() - 0x8000000) / 4 + i)] = m
            elif (addr < pgmMemoryUpperLimit):
                # In Program Flash- take every 4 bytes of hex file and keep top 3 bytes
                for i in range(0, hl.Len()/4):
                    m = Mem24()
                    m.SetData(hl.data, i * 4)
                    file.pgmFlashImage24bit[(hl.Addr() / 2 + i)] = m;
                    
            elif (addr >= 0xF80000 and addr < 0xF80010):
                # Config Registers
                # Take 4 bytes of hex file and keep top 2 bytes as register address
                for i in range(0, hl.Len() / 4):
                    m = Mem16()
                    m.SetData(hl.data, i * 4)
                    file.cfgRegImage16bit[((hl.Addr() - 0xF80000) / 2 + i)] = m;

            elif (addr >= 0x7FF000 and addr < 0x800000):
                #// EEPROM
                for i in range(0, hl.Len() / 4):
                    m = Mem16()
                    m.SetData(hl.data, i * 4);
                    file.eepromImage16bit[((hl.Addr() - 0x7FF000) / 2 + i)] = m

            else:
                #// Outside of any expected address range
                print "Address out-of-range:", hex(hl.Addr())
           
    #// Scan over all instructions, looking for blocks
    pgmBlock = None;
    for i in range(0,instCount):
        inst24 = file.pgmFlashImage24bit[i];
        if (inst24.used):
            if (pgmBlock != None):
                pgmBlock.end = i;
            else:
                pgmBlock = MemBlock(i);
        elif (pgmBlock != None):
            file.pgmBlockList.append(pgmBlock);
            pgmBlock = None;

    if (pgmBlock != None):
      file.pgmBlockList.append(pgmBlock);
      pgmBlock = None;

    #// Scan over all EEPROM, looking for blocks
    eeBlock = None;
    for i in range(0, eeWordCount):
        eeWord = file.eepromImage16bit[i];
        if (eeWord.used):
            if (eeBlock != None):
                eeBlock.end = i;
            else:
                eeBlock = MemBlock(i);
        elif (eeBlock != None):
            file.eeBlockList.append(eeBlock);
            eeBlock = None;
       
    if (eeBlock != None):
        file.eeBlockList.append(eeBlock);
        eeBlock = None;
    #// Scan over all Config Registers, looking for blocks
    cfgBlock = None;
    for i in range(0, cfgRegCount):
        cfgReg = file.cfgRegImage16bit[i];
        if (cfgReg.used):
            if (cfgBlock != None):
                cfgBlock.end = i;
            else:
                cfgBlock = MemBlock(i);

        elif (cfgBlock != None):
            file.cfgBlockList.append(cfgBlock);
            cfgBlock = None;

    if (cfgBlock != None):
        file.cfgBlockList.append(cfgBlock);
        cfgBlock = None;


#Port of method located below this:

#         static void ReportMemBlock()
#         {
#             foreach (MemBlock mb in pgmBlockList)
#             {
#                 Console.WriteLine("Pgm: {0}", mb.GetSummaryStr());
#             }
# 
#             foreach (MemBlock mb in eeBlockList
#             {
#                 Console.WriteLine("EE: {0}", mb.GetSummaryStr());
#             }
# 
#             foreach (MemBlock mb in cfgBlockList)
#             {
#                 Console.WriteLine("Cfg: {0}", mb.GetSummaryStr());
#             }
#         }

def reportMemBlock(file):
    for mb in file.pgmBlockList:
        print "Pgm:", mb.GetSummaryStr()
    for mb in file.eeBlockList:
        print "EE:", mb.GetSummaryStr()
    for mb in file.cfgBlockList:
        print "Cfg:", mb.GetSummaryStr()

#Port of this method located below comments:        
#         static ushort continueCRC_16(ushort startCRC, byte[] data, int dataStart, int count)
#         {
#             ushort crc = startCRC;   // initial value for CRC
#             int i;
# 
#             // For each byte in the caller's data...
#             for (i = 0; i < count; ++i)
#             {
#                 crc = (ushort)(crc ^ data[dataStart + i]);     // xor next data byte into low crc byte
# 
#                 // For each bit in the byte...
#                 crc = (ushort)((crc >> 1) ^ (0xA001 * (crc & 1)));
#                 crc = (ushort)((crc >> 1) ^ (0xA001 * (crc & 1)));
#                 crc = (ushort)((crc >> 1) ^ (0xA001 * (crc & 1)));
#                 crc = (ushort)((crc >> 1) ^ (0xA001 * (crc & 1)));
#                 crc = (ushort)((crc >> 1) ^ (0xA001 * (crc & 1)));
#                 crc = (ushort)((crc >> 1) ^ (0xA001 * (crc & 1)));
#                 crc = (ushort)((crc >> 1) ^ (0xA001 * (crc & 1)));
#                 crc = (ushort)((crc >> 1) ^ (0xA001 * (crc & 1)));
#             }
# 
#             return crc;
#         }

def continueCRC_16(startCRC, data_string, dataStart, count):
            crc = startCRC;   #// initial value for CRC- This is a short
            
            #// For each byte in the caller's data...
            for i in range(0, count):
                crc = (crc ^ data_string[dataStart + i]);     #// xor next data byte into low crc byte

                #// For each bit in the byte...
                crc = ((crc >> 1) ^ (0xA001 * (crc & 1)));
                crc = ((crc >> 1) ^ (0xA001 * (crc & 1)));
                crc = ((crc >> 1) ^ (0xA001 * (crc & 1)));
                crc = ((crc >> 1) ^ (0xA001 * (crc & 1)));
                crc = ((crc >> 1) ^ (0xA001 * (crc & 1)));
                crc = ((crc >> 1) ^ (0xA001 * (crc & 1)));
                crc = ((crc >> 1) ^ (0xA001 * (crc & 1)));
                crc = ((crc >> 1) ^ (0xA001 * (crc & 1)));
            
            return crc;

#Port of this method located below:

#         // count is the number of bytes to copy.  Should be multiple of 3
#         static void _memcpy_p2d24(ref byte[] data, uint addr, int count)
#         {
#             int index = 0;
#             int instrCount = count / 3;
# 
#             for (uint a = addr; a < addr + (uint)(2 * instrCount); a += 2)
#             {
#                 Mem24 inst24 = pgmFlashImage[a / 2];
#                 data[index++] = inst24.data[0];
#                 data[index++] = inst24.data[1];
#                 data[index++] = inst24.data[2];
#             }
#         }
# 
# static int INSTRUCTION_COUNT = 1;

#    // count is the number of bytes to copy.  Should be multiple of 3
def _memcpy_p2d32(data_string, addr, count, file):
            
            index = 0;

            for a in range(addr, addr + count):
                inst32 = file.pgmFlashImage32bit[a];
                #print "%x %x %x %x" % inst32.data[0] % inst32.data[1] % inst32.data[2] % inst32.data[3]
                #print index, hex(addr), ":", hex(inst32.data[0]), hex(inst32.data[1]), hex(inst32.data[2]), hex(inst32.data[3])
                data_string[index] = inst32.data[0];
                data_string[index+1] = inst32.data[1];
                data_string[index+2] = inst32.data[2];
                data_string[index+3] = inst32.data[3];
                index = index + 4
                
INSTRUCTION_COUNT = 1;



#Port of this method located below:
# static void CalcApplProgramCRC()
# {
#     byte[] pgmData = new byte[3 * INSTRUCTION_COUNT];
# 
#     uint end = 0;
# 
#     // Calculate the CRC for addresses 0 - 0x200, which includes the Reset Vector and
#     // the two Interrupt Vector tables
#     uint crc = 0xFFFFFFFF;
#     for (uint addr = 0; addr < 0x000200; addr += (uint)(2 * INSTRUCTION_COUNT))
#     {
#         _memcpy_p2d24(ref pgmData, addr, 3 * INSTRUCTION_COUNT);
#         crc = updateCRC32(crc, pgmData, 0, 3 * INSTRUCTION_COUNT);
#     }
# 
#     // Note: Addresses 0x200 to 0x27E contain the ap_header_info structure and are
#     //       not included in the CRC calculation.
# 
#     // Find the highest address used in Program Memory
#     foreach (MemBlock mb in pgmBlockList)
#     {
#         if (mb.end > end) end = mb.end;
#     }
#     end = 2 * end;
# 
#     // Add to the CRC the instructions starting at address 0x280 all the way to the
#     // highest address used in the Program Memory.
#     for (uint addr = 0x280; addr <= end; addr += (uint)(2 * INSTRUCTION_COUNT))
#     {
#         _memcpy_p2d24(ref pgmData, addr, 3 * INSTRUCTION_COUNT);
#         crc = updateCRC32(crc, pgmData, 0, 3 * INSTRUCTION_COUNT);
#     }
# 
#     crc ^= 0xFFFFFFFF;
# 
#     crc32 = crc;
# 
# }

def CalcApplProgramCRC(file, app_start_addr, app_size_bytes, DEBUG_OUTPUT = 1):
    global INSTRUCTION_COUNT
    pgmData = [0] * (4*INSTRUCTION_COUNT) #new byte[3 * INSTRUCTION_COUNT];
    end = 0;
    crc = 0xFFFFFFFF;

    end = app_start_addr + app_size_bytes
   
    if DEBUG_OUTPUT > 0:
        print "CRC range (start byte addr, num bytes, end byte addr):", hex(app_start_addr), hex(app_size_bytes), hex(end)
    
    app_start_addr /= 4
    end /= 4

    initCRC32Table()
    for addr in range(app_start_addr, end, INSTRUCTION_COUNT):
        _memcpy_p2d32(pgmData, addr, INSTRUCTION_COUNT, file);
        if (DEBUG_OUTPUT > 2): print hex(addr), ":", pgmData
        crc = updateCRC32(crc, pgmData, 0, 4 * INSTRUCTION_COUNT);
    
    crc ^= 0xFFFFFFFF;
    crc32 = crc;
    if DEBUG_OUTPUT > 0:
        print "Calculated CRC:", hex(crc32) 
    return crc32

CRC32Table = [] #new uint[256];
CRC32TableInitd = False;
CRC_32_POLYNOMIAL = 0xEDB88320;
    
       
#Port of method located below this:        
# static void initCRC32Table()
# {
#     uint crc;
# 
#     for (int i = 0; i < 256; i++)
#     {
#         crc = (uint)i;
# 
#         for (int j = 0; j < 8; j++)
#         {
#             if ((crc & 0x00000001) != 0)
#                 crc = (crc >> 1) ^ CRC_32_POLYNOMIAL;
#             else
#                 crc = crc >> 1;
#         }
# 
#         CRC32Table[i] = crc;
#     }
# 
#     CRC32TableInitd = true;
# 
# }  // initCRC32Table


def initCRC32Table():
    global CRC32TableInitd, CRC32Table,CRC_32_POLYNOMIAL
    CRC32Table = [0] * 256
    crc = 0;
    for i in range(0, 256):
        crc = i;
        for j in range(0, 8):
            if ((crc & 0x00000001) != 0):
                crc = (crc >> 1) ^ CRC_32_POLYNOMIAL;
            else:
                crc = crc >> 1;
        CRC32Table[i] = crc;
    CRC32TableInitd = True;

#Port of method located below
# static uint updateCRC32(uint crc, byte[] data, int dataStart, int count)
# {
#     if (!CRC32TableInitd)
#     {
#         initCRC32Table();
#     }
# 
#     for (int i = 0; i < count; ++i)
#     {
#         crc = (crc >> 8) ^ CRC32Table[(crc ^ (uint)data[dataStart + i]) & 0xff];
#     }
# 
#     return crc;
# 
# }  // updateCRC32

def updateCRC32(crc, data_string, dataStart, count):
    global CRC32TableInitd, CRC32Table,CRC_32_POLYNOMIAL
    if (not CRC32TableInitd):
        initCRC32Table();
    for i in range(0, count):
         
        crc = (crc >> 8) ^ CRC32Table[(crc ^data_string[dataStart + i]) & 0xff];

    return crc;

#Port of method below    
# static void TestCRC()
# {
#     // Create the CRC-32 table if it hasn't been initialized yet
#     if (!CRC32TableInitd)
#     {
#         initCRC32Table();
#     }
# 
#     // Print the CRC32 table (to be cut and pasted into the firmware source)
#     for (int i = 0; i < 256; i += 4)
#     {
#         StringBuilder sb = new StringBuilder();
#         sb.Append("  ");
#         for (int j = 0; j < 4; ++j)
#         {
#             sb.AppendFormat("0x{0:X8}, ", CRC32Table[i + j]);
#         }
#         Console.WriteLine(sb.ToString());
#     }
# 
#     // Print the CRC32 for "123456789"  Expected value is 0xCBF43926
#     StringBuilder sb2 = new StringBuilder();
#     byte[] data = new byte[9];
#     for (byte i = 0; i < 9; ++i)
#         data[i] = (byte)(i + 0x31);
#     uint crc = updateCRC32(0xFFFFFFFF, data, 0, 9) ^ 0xFFFFFFFF;
#     sb2.AppendFormat("CRC-32={0:X8}", crc);
#     Console.WriteLine(sb2.ToString());
#     if (crc == 0xCBF43926) Console.WriteLine("CRC-32 calculation OK");
#     else Console.WriteLine("CRC-32 calculation is WRONG!");
# }


def TestCRC():
    global CRC32TableInitd, CRC32Table,CRC_32_POLYNOMIAL
    #// Create the CRC-32 table if it hasn't been initialized yet
    if (not CRC32TableInitd):
        initCRC32Table();
    
    #// Print the CRC32 table (to be cut and pasted into the firmware source)
    for i in range(0, 256, 4):
        sb = "  ";
        for j in range(0, 4):
            sb = sb + hex(CRC32Table[i + j]) + ", ";
        print sb;
    
    #// Print the CRC32 for "123456789"  Expected value is 0xCBF43926
    sb2 = ""
    data = [0] * 9;
    for i in range(0,9):
        data[i] = i + 0x31;
    crc = updateCRC32(0xFFFFFFFF, data, 0, 9) ^ 0xFFFFFFFF;
    sb2 = sb2 + "CRC-32=" + hex(crc);
    print sb2;
    if (crc == 0xCBF43926):
        print "CRC-32 calculation OK"
    else:
        print "CRC-32 calculation is WRONG!"

    # Second test: print crc32 for 123456789123456789. expected val is 0x6592B476 
    sb3 = ""
    data = [0] * 18;
    for i in range(0,9):
        data[i] = i + 0x31;
    for i in range(9,18):
        data[i] = i + 0x31;
        
    crc = updateCRC32(0xFFFFFFFF, data, 0, 18) ^ 0xFFFFFFFF;
    sb3 = sb3 + "CRC-32=" + hex(crc);
    print sb3;
    if (crc == 0x6592B476 ):
        print "CRC-32 calculation OK"
    else:
        print "CRC-32 calculation is WRONG!"    

#Port located below method:
# static int Main(string[] args)
# {
# string hexFileName;
# 
# // Write the CRC-32 table and test CRC-32 calculation with a known input
# //TestCRC();
# 
# if (args.Length == 1)
# {
#     // One command line argument, assume that it is the hex file name
#     hexFileName = args[0].Trim();
# }
# else
# {
#     Console.WriteLine("Bad Args");
#     PrintUsage();
#     return -1;
# }
# 
# if (Path.HasExtension(hexFileName))
# {
#     if (String.Compare(Path.GetExtension(hexFileName), ".hex", true) != 0)
#     {
#         //Console.WriteLine("The input file extension must be .hex");
#         //PrintUsage();
#         //return;
#         hexFileName = Path.ChangeExtension(hexFileName, "hex");
#     }
# }
# else
# {
#     // Append extension of ".hex"
#     hexFileName = Path.ChangeExtension(hexFileName, "hex");
# }
# Console.WriteLine("Input File: " + hexFileName);
# 
# try
# {
# 
#     ParseHex(hexFileName);
# }
# catch
# {
#     Console.WriteLine("Could not parse hex file {0}", hexFileName);
#     return -2;
# }
# 
# //ReportHexLineList();
# 
# InitMaps();
# 
# AnalyzeMemory();
# 
# //ReportMemBlock();
# 
# CalcApplProgramCRC();
# StringBuilder sb = new StringBuilder();
# sb.AppendFormat("CRC32 = {0:X8} ", crc32);
# Console.WriteLine(sb.ToString());
# 
# // 0x049C is the current address for ap_header_info.app_crc in the hex file.  This is double the actual dsPIC33 address of 0x024E.
# // Todo: get this address from a command line argument
# string hexmateArgs = MakeHexmateCrcCmdStr(hexFileName, 0x049C);
# Console.WriteLine(hexmateArgs);
# 
# //Console.WriteLine("Directory = {0}",Path.GetDirectoryName(hexFileName));
# //Console.WriteLine("Input File = {0}", Path.GetFileName(hexFileName));
# 
# //string wd = Directory.GetCurrentDirectory();
# //Console.WriteLine("Working Directory = {0}", wd);
# 
# int results = RunHexmate(hexmateArgs, 20000);
# if (results != 0)
# {
#     Console.WriteLine("hexmate failed = ({0})", results);
#     return results;
# }
# 
# return 0;
# }

def main2(args, CREATE_OUTPUT = True):
    global crc32
    hexFileName = args[1];
    # Write the CRC-32 table and test CRC-32 calculation with a known input
    # TestCRC();
    if (len(args) == 2):
        #// One command line argument, assume that it is the hex file name
        hexFileName = args[1].strip();
        
    else:
        print "Bad Args";
        sys.exit(-1);   
    
    
    if not (".hex" == paths.splitext(hexFileName)[1]):
        print "Appending extension of \".hex\". "
        hexFileName =  hexFileName + ".hex"
        print hexFileName
    print "Input File: " + hexFileName;

    hexLineList = None
    try:
        hexLineList = parseHex(hexFileName);
    except:
        print "Could not parse hex file", hexFileName;
        sys.exit(-2);


    #//ReportHexLineList();
    initMaps();
    analyzeMemory(hexLineList);
    #//ReportMemBlock();

    crc32 = CalcApplProgramCRC();
    print "CRC32 = ", crc32
    
    #// 0x049C is the current address for ap_header_info.app_crc in the hex file.  This is double the actual dsPIC33 address of 0x024E.
    #// Todo: get this address from a command line argument
    hexmateArgs = MakeHexmateCrcCmdStr(0x049C, hexFileName, crc32, hexFileName);
    print hexmateArgs

    print "Directory = ", paths.dirname(hexFileName)
    print "Input File = ", paths.abspath(hexFileName)

    #//string wd = Directory.GetCurrentDirectory();
    #//Console.WriteLine("Working Directory = {0}", wd);

    results = RunHexmate(hexmateArgs, 20000);
    if (results != 0):
        print "hexmate failed = ", results;
        return results;
    
    
    stream = '\n'.join([' '.join([`hex(num)` for num in l.data]) for l in hexLineList])
    
    if (CREATE_OUTPUT):
        bin = open(paths.dirname(hexFileName) + 'binary_from_hex','w')
        bin.write(stream)
    
    return stream

#Port located below method.
# static string MakeSERIALStr(ushort addr, ushort crcWord)
# {
#     byte lowByte = (byte)(crcWord & 0xFF);
#     byte highByte = (byte)((crcWord >> 8) & 0xFF);
#     return string.Format("+-SERIAL={0:X2}{1:X2}@{2:X4}", lowByte, highByte, addr);
# }

def MakeSERIALStr(addr, crcWord):
    lowByte = crcWord & 0xFF;
    highByte = (crcWord >> 8) & 0xFF
    values = {'a' : lowByte, 'b': highByte, 'c':addr }
    s = "+-SERIAL=%(a)02X%(b)02X@%(c)04X." % values
    return s


#port located below method:
# static string MakeHexmateCrcCmdStr(string inFile, ushort addr)
# {
#     ushort crcLowWord = (ushort)(crc32 & 0xFFFF);
#     ushort crcHighWord = (ushort)((crc32 >> 16) & 0xFFFF);
# 
#     string logFile = Path.ChangeExtension(inFile, "log");
#     return string.Format("{0} {1} {2} -o{3} -LOGFILE={4}",
#                          inFile,
#                          MakeSERIALStr(addr, crcLowWord),
#                          MakeSERIALStr((ushort)(addr + 4), crcHighWord),
#                          inFile, logFile);
# }

def MakeHexmateCrcCmdStr(addr, inFile, crc_to_insert, outfile):
    crcLowWord = (crc_to_insert & 0xFFFF);
    crcHighWord = ((crc_to_insert >> 16) & 0xFFFF);
    
    logFile = outfile + ".log"
    values = {'a':inFile, 'b':MakeSERIALStr(addr, crcLowWord), 'c':MakeSERIALStr(addr + 2, crcHighWord), 'd':outfile, 'e':logFile}
    s = "%(a)s %(b)s %(c)s -O%(d)s -LOGFILE=%(e)s" % values
    print s
    return s


#port located below method:
# static int RunHexmate(string cmdArgs, int timeout)
# {
#     int exitCode;
# 
#     string exeName = "..\\..\\support_scripts\\hex_crc\\hexmate.exe";
#     //string exeName = "C:\\Program Files\\HI-TECH Software\\PICC\\9.71a\\bin\\hexmate.exe";
#     ProcessStartInfo processInfo = new ProcessStartInfo(exeName, cmdArgs);
#     processInfo.CreateNoWindow = true;
#     processInfo.UseShellExecute = false;
# 
#     try
#     {
#         Process process = Process.Start(processInfo);
#         process.WaitForExit(timeout);
#         exitCode = process.ExitCode;
#         process.Close();
#     }
#     catch (Win32Exception w32e)
#     {
#         Console.WriteLine("Cannot find {0}", processInfo.FileName);
#         exitCode = w32e.NativeErrorCode;
#     }
# 
#     catch 
#     {
#         Console.WriteLine("Cannot run {0}", processInfo.FileName);
#         exitCode = -3;
#     }
# 
#     return exitCode;
# }

def RunHexmate(cmdArgs, timeout):
    #See http://forum.htsoft.com/all/showflat.php?Cat=0&Board=projects&Number=12986&page=0&fpart=all  for a Man page on how to use Hexmate.exe
    exitCode = 0;

    exeName = paths.dirname(paths.realpath(__file__))+ os.sep +"hexmate.exe " + cmdArgs ;
    
    try:
        import subprocess
        p=subprocess.call(exeName)
        if p != 0:
            print p
            exitCode = -1
        else:
            print "Hexmate was successful!"
        
    except Exception as e:
        print e
        print "Cannot run", exeName
        exitCode = -3;
    
    return exitCode;



#Replaced with native python library code.

# static int RunCommand(string cmd, int timeout)
# {
#     int exitCode;
#     ProcessStartInfo processInfo;
#     Process process;
# 
#     processInfo = new ProcessStartInfo("cmd.exe", "/C " + cmd);
#     processInfo.CreateNoWindow = true;
#     processInfo.UseShellExecute = false;
#     process = Process.Start(processInfo);
#     process.WaitForExit(timeout);
#     exitCode = process.ExitCode;
#     process.Close();
# 
#     return exitCode;
# }

    
#Port from C# of HexLine:
class RecordType:
        REC_TYPE_DATA = 0x00
        REC_TYPE_EOF = 0x01
        REC_TYPE_EXT_ADDR = 0x04

class HexLine:
    
        def Type(self):
            return self.r_type;
        

        def ULBA(self):
            return self.ulba;
        
        def Addr(self):
            return self.addr;
              
        def Len(self):
            return self.len
        

        def GetSummaryStr(self):
            global pgmMemoryUpperLimit
            sb = ""
            if (self.r_type == RecordType.REC_TYPE_EXT_ADDR):
                return "EXT_ADDR" + ( "%08X" % self.ulba )
            elif (self.r_type == RecordType.REC_TYPE_DATA):
                sb = "DATA" + str(self.len) + ("%06X" % self.addr)
                if (self.addr < pgmMemoryUpperLimit):
                    #// In Program Flash
                    sb = sb + "PGM ";
                    for i in range(0, (self.len/4)):
                        di = 4 * i;
                        val = self.data[di] + (self.data[di + 1] << 8) + (self.data[di + 2] << 16);
                        sb = sb + "%06X " % val;
                    
                elif (self.addr >= 0xF80000 and self.addr < 0xF80010) : 
                    #// Config Registers
                    sb = sb + "CFG ";
                    for i in range(0, self.len/4):
                        di = 4 * i;
                        val = hex(bytes(self.data));
                        sb = sb + "%04X " % val;
                elif (self.addr >= 0x7FF000 and self.addr < 0x800000):
                    sb = sb + "EE  "
                else:
                    #// Outside of any expected address range
                    sb = sb + "??? ";
                return sb
            else:
                return "EOF" + str(self.len);
        
        
        
        #// Constructor - parses a string into a line's constituent parts
        def __init__(self, line, currentULBA):
            self.raw = line.strip()
            self.len = int(line[1:3], 16)
            self.offset = int(line[3:7], 16)
            self.r_type = int(line[7:9], 16)
            self.data = [0] * self.len
            for i in range(0, self.len):
                ind = 9 + i * 2
                self.data[i] = int(line[ind:ind+2], 16)
                
            if self.r_type == RecordType.REC_TYPE_EXT_ADDR:
                
                self.ulba = (self.data[0] << 24) + (self.data[1] << 16);
                            
            elif self.r_type == RecordType.REC_TYPE_DATA: 
                self.ulba = currentULBA
            else:
                self.ulba = 0  
            
            
            self.addr = (self.ulba + self.offset) #/ 2
            
            self.chksum = int(line[9 + self.len *2: 9 + self.len *2 + 2], 16)
            

#binary_data = main(sys.argv) 
class HexfileMetadata:
    def __init__(self, hexFileName, DEBUG_LEVEL = 2, OFFSET = 0x8000000, APP_START_HEADER = 0x8010000, CRC_LOW_HDR_OFFSET = 0x18, START_ADDR_HDR_OFFSET = 0x10, REV_LOW_HDR_OFFSET = 0x08, SIZE_HDR_OFFSET = 0x14):
        self.file_data = File_Contents() 

        self.crc_app_calc = None
        self.crc_app_header = None
        
        if not (".hex" == paths.splitext(hexFileName)[1]):
            if (DEBUG_LEVEL > 1):      
                print "Appending extension of \".hex\". "
            hexFileName = hexFileName + ".hex"    

        if (DEBUG_LEVEL > 0):
            print "Input File: " + hexFileName;

        self.hexfilename =  hexFileName
        hexLineList = None
        try:
            hexLineList = parseHex(hexFileName);
        except:
            print "Could not parse hex file", hexFileName;
            return

        initMaps();
        analyzeMemory(hexLineList, self.file_data)
        
        self.binary_data32 = self.file_data.pgmFlashImage32bit

        #Read out data from header in the hex file, which contains the location of where the header stops and the application stops.
        app_start_loc = ((APP_START_HEADER + START_ADDR_HDR_OFFSET) - OFFSET)
        app_size_loc  = ((APP_START_HEADER + SIZE_HDR_OFFSET      ) - OFFSET)
        crc_location  = ((APP_START_HEADER + CRC_LOW_HDR_OFFSET   ) - OFFSET)
        rev_location  = ((APP_START_HEADER + REV_LOW_HDR_OFFSET   ) - OFFSET)

        self.CRC_LOW_ADDR = APP_START_HEADER + CRC_LOW_HDR_OFFSET
        self.header_size_bytes = 0x100

        self.start_prog_addr = (self.file_data.pgmFlashImage32bit[app_start_loc/4].data[3] << 24) + (self.file_data.pgmFlashImage32bit[app_start_loc/4].data[2] << 16) + (self.file_data.pgmFlashImage32bit[app_start_loc/4].data[1] << 8) +  (self.file_data.pgmFlashImage32bit[app_start_loc/4].data[0]) 
        self.prog_size_bytes = (self.file_data.pgmFlashImage32bit[app_size_loc/4].data[3]  << 24) + (self.file_data.pgmFlashImage32bit[app_size_loc/4].data[2] << 16) + (self.file_data.pgmFlashImage32bit[app_size_loc/4].data[1] << 8) + (self.file_data.pgmFlashImage32bit[app_size_loc/4].data[0])
        self.expected_crc32  = (self.file_data.pgmFlashImage32bit[crc_location/4].data[3]  << 24) + (self.file_data.pgmFlashImage32bit[crc_location/4].data[2] << 16) +  (self.file_data.pgmFlashImage32bit[crc_location/4].data[1] << 8) +  (self.file_data.pgmFlashImage32bit[crc_location/4].data[0]) 

        self.start_header_addr = (APP_START_HEADER - OFFSET); # We start flash writing from here
        self.start_abs_hex_addr = APP_START_HEADER
        self.hexLines = hexLineList;

        self.get_revision(rev_location)

        #Use header information to determine range of CRC
        self.crc_app_calc = CalcApplProgramCRC(self.file_data, (self.start_prog_addr - OFFSET + self.header_size_bytes), (self.prog_size_bytes - self.header_size_bytes));

        if (DEBUG_LEVEL > 1):
            print "CRC32 = ", hex(self.crc_app_calc)
            print "Read out CRC:", hex(self.expected_crc32)

        if (self.expected_crc32 != self.crc_app_calc):
            exception_info = "ERROR: Expected Hex file "+self.hexfilename+" to contain CRC32="+hex(self.expected_crc32)+", actually evaluates to "+hex(self.crc_app_calc)+" Abandoning upgrade attempt."
            print exception_info
            raise ValidFileException(exception_info)

    def get_revision(self, revision_address_low, DEBUG_LEVEL = 2):

        b = self.file_data.pgmFlashImage32bit[revision_address_low / 4]
        if (DEBUG_LEVEL > 2):
            print 'Rev Address:', hex(revision_address_low)
            print '%02X' % b.data[0], '%02X' %b.data[1], '%02X' %b.data[2], '%02X' %b.data[3]

        self.maj_hex_revision    = '%02X' % b.data[0]
        self.min_hex_revision    = '%02X' % b.data[1]
        self.bugfix_hex_revision = '%02X' % b.data[2]
        self.beta_revision       = '%02X' % b.data[3]

        self.revision =  str(int(self.maj_hex_revision, 16)) +"."+ str(int(self.min_hex_revision, 16)) +"." + str(int(self.bugfix_hex_revision, 16)) 

        #Follow iWalk SOP for creating a revision string - append beta number if non-zero
        if int(self.beta_revision, 16) != 0:
            self.revision = self.revision +  "b" + str(int(self.beta_revision, 16))

        if (DEBUG_LEVEL > 1):
            print self.revision
        return self.revision
        
    def binary_data32(self):
        return self.binary_data32
    def hex_line_data(self):
        return self.hexLines

class UnCrCdHexfileMetadata (HexfileMetadata):
    def __init__(self, hexFileName, DEBUG_LEVEL = 1):
    
        self.file_data = File_Contents() 
         
        self.ivt_crc32 = None
        self.app_crc32 = None
        self.app_hdr_crc32 = None
        
        if not (".hex" == paths.splitext(hexFileName)[1]):
            if (DEBUG_LEVEL > 0):      
                print "Appending extension of \".hex\". "
            hexFileName = hexFileName + ".hex"    
        if (DEBUG_LEVEL > 0):
            print "Input File: " + hexFileName;
    
        self.hexfilename =  hexFileName
        hexLineList = None
        try:
            hexLineList = parseHex(hexFileName);
        except Exception as e:
            print "Could not parse hex file", hexFileName;
            print e
            return

        initMaps();
        analyzeMemory(hexLineList, self.file_data);

        self.binary_data32 = self.file_data.pgmFlashImage32bit;
        self.hexLines = hexLineList


def calcCRC32(data):
    global CRC32TableInitd, CRC32Table,CRC_32_POLYNOMIAL
    #Create the CRC-32 table if it hasn't been initialized yet
    if (not CRC32TableInitd):
        initCRC32Table();

    for i in range(0, 256, 4):
        sb = "  ";
        for j in range(0, 4):
            sb = sb + hex(CRC32Table[i + j]) + ", ";
    
    crc = updateCRC32(0xFFFFFFFF, data, 0, len(data)) ^ 0xFFFFFFFF;
    return crc;

class ValidFileException(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)    

if __name__ == "__main__":              
    #TestCRC();
    #a = main2(sys.argv)  
    path_to_hexfile = sys.argv[1]
    hexfile = UnCrCdHexfileMetadata(path_to_hexfile, DEBUG_LEVEL = 3)
    print hex(hexfile.crc_app_calc)
    hexmateArgs = MakeHexmateCrcCmdStr(hexfile.CRC_LOW_ADDR, path_to_hexfile, hexfile.crc_app_calc, "dave.hex"); #Double CRC address to match correct byte address
    print hexmateArgs
    results = RunHexmate(hexmateArgs, 20000);
    if (results != 0):
        print "hexmate failed = ", results;
    else:
        print results