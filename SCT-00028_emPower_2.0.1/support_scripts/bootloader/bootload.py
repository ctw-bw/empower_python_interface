#import python_paths as pyenv
import time,datetime,optparse,sys, struct, zipfile, serial, timeit, os, thread, threading, StringIO

from sys import stdout

import python_paths_default     # test for user's python_paths.py and update paths
import python_paths as pyenv
import crc_port as crc_port
from crc_port import HexfileMetadata
from comm_lib import pfcmd
import bluetooth_comport as btSerial
from collections import deque
#import robo_update
local_log = None

import time

#Constants are all at top of file:
class cmds:
  BOOT_CMDS_TYPE_SEND_HEADER = 0
  BOOT_CMDS_TYPE_HEADER_DATA = 1
  BOOT_CMDS_TYPE_SEND_KEY = 2
  BOOT_CMDS_TYPE_KEY_DATA = 3
  BOOT_CMDS_TYPE_UNLOCK_MEM = 4
  BOOT_CMDS_TYPE_MEM_UNLOCKED = 5 
  BOOT_CMDS_TYPE_SEND_PROG_MEM = 6
  BOOT_CMDS_TYPE_PROG_MEM_DATA = 7
  BOOT_CMDS_TYPE_WRITE_PROG_MEM = 8
  BOOT_CMDS_TYPE_PROG_MEM_WRITTEN = 9
  BOOT_CMDS_TYPE_END_TRANSFER = 10
  BOOT_CMDS_TYPE_TRANSFER_ENDED = 11
  BOOT_CMDS_TYPE_ERROR_CODE = 12
  BOOT_CMDS_TYPE_ERASE = 13
  BOOT_CMDS_TYPE_ERASE_DONE = 14
  NUM_BOOT_CMDS_TYPE = 15

class cmd_dest:
  BOOT_CMDS_DEST_FUP = 0
  BOOT_CMDS_DEST_SC = 1   # V1 - obsolete
  BOOT_CMDS_DEST_MC = 2   # V1 - obsolete
  BOOT_CMDS_DEST_IMU = 3  # V1 - obsolete
  BOOT_CMDS_DEST_V2X = 4
  NUM_BOOT_CMDS_DEST = 5

#Timeout constants:
DEFAULT_CONNECTION_SECONDS = 20  
DEFAULT_BOOTLOADER_TIMEOUT_SECONDS = 3
BLUETOOTH_TIMEOUT_SECONDS = .5
PSER_TIMEOUT_SECONDS = 0.100  
WRITE_PACKET_RTT_SECONDS = .040 # 40 Milliseconds
LIN_TIMEOUT_SECONDS = 2

sequence_byte = 0 

# Debugging and packet traffic logging utility commands:
debug_level = 0     
DEBUG_FOR_DVEP_008 = 3    
DEBUG_TRAFFIC_LOG = -666
DEBUG_TEST_OUTPUT = -444
COLD_BOOT_DELAY = 0.001

def delay(DELAY = 0.001) :
    # Wait a period of time between messages
    delay_start = time.clock()
    while(time.clock() - delay_start < DELAY) :
        pass

class TrafficLogger:
    #Log quantity of packets sent and received at different times.
    def __init__(self):
        self.traffic_file = "traffic_file_"+str(int(time.time()))+".log"
        self.sent = [] #Each entry is tuple: (dest, time, qty_pckts_sent, queue_size, resend)
        self.rec = [] #Each entry is tuple: (src, time, window_end, queue_size)
        self.overall_rsp_buffer_size = [] #Entry is tuple: (time, bytes_rec, string_desc)
        self.overall_send_size = [] #Entry is tuple: (time, bytes_rec, string_desc)
        self.sent_i = 0
        self.rec_i = 0
        self.overall_rsp_buffer_size_i = 0
        self.overall_send_size_i = 0 

    def write(self):

        f = open(self.traffic_file, 'a')
        
        f.write("#Upload traffic sent: (dest, time, qty_pckts_sent, queue_size, resend)\n")
        for t in self.sent[self.sent_i:]:
            f.write("%d, %f, %d, %d, %d \n" % t)
        self.sent_i = len(self.sent)

        f.write("#Upload traffic rec: (src, time, window_end, queue_size) \n")
        for t in self.rec[self.rec_i:]:
            f.write("%d, %f, %d, %d\n" % t)
        self.rec_i = len(self.rec)

        f.write("#Overall response traffic:  (time, bytes_rec, string_desc)\n")
        for t in self.overall_rsp_buffer_size[self.overall_rsp_buffer_size_i:]:
            f.write("rsp, %f, %d, %s\n" % t)
        self.overall_rsp_buffer_size_i = len(self.overall_rsp_buffer_size)

        f.write("#Overall send traffic:  (time, bytes_rec, string_desc)\n")
        for t in self.overall_send_size[self.overall_send_size_i:]:
            f.write("rsp, %f, %d, %s\n" % t)
        self.overall_send_size_i = len(self.overall_send_size)

        f.close()

"""Boot-Loader Special Commands:"""
BOOTLOAD_SPECIAL_CMD = 4667
BOOTLOAD_CMD_OPT = 77
IN_BOOTLOADER_ECODE = -532
BOOTLOADER_SYNC = 0xAA55
BOOT_CMDS_SYNC_CODE  =   BOOTLOADER_SYNC
BOOT_CMDS_MAX_PAYLOAD_SIZE =   196
BOOT_CMDS_MAX_READ_DATA_SIZE = 192

"""Set the maximum packet window size that this upgrade utility can use to send packets asynchronously to the Bootloader."""
""" Max message bytes at any one time equals the sum of the data being sent in all packets at that time."""
""" This must be smaller than 1 Flash memory page in the the Bootloader\'s hardware RAM buffer, which is 0x600. """
MAX_TRANSIT_BYTES_SIZE = 0x600 #TODO: FIND OUT WHY WE CAN ONLY HAVE A BUFFER SIZE OF 192 right now!

"""Bootloader packet structure constants:"""
header_encoding = "BBBBH"
header_size = struct.calcsize('<'+header_encoding)
sync = BOOT_CMDS_SYNC_CODE
sync_size = 2
crc_encoding = 'L'
crc_size = struct.calcsize('<'+crc_encoding)

class send_header_opt: 
    BOOT_CMDS_HEADER_BL = 0
    BOOT_CMDS_HEADER_APP = 1

class payload_enc:
    SEND_HEADER = "H"
    VERSION_SIZE = 8
    HEADER_DATA = "HBBBBHHLLL"   # header code, rev_major, rev_minor, rev_bugfix, rev_beta, rev_build, rev_svn, app start, app_size, app crc32                                    // highest commit revision in svn
    KEY_SIZE = 8
    SEND_KEY = ""
    KEY_DATA = "B" * KEY_SIZE    
    UNLOCK_MEM = "B" * KEY_SIZE
    MEM_UNLOCKED = "B" * KEY_SIZE
    BOOT_CMDS_MAX_PROG_MEM_SIZE = BOOT_CMDS_MAX_PAYLOAD_SIZE - struct.calcsize("L")
    SEND_PROG_MEM = "LB" #address to start from, num of bytes to return (max 192)
    PROG_MEM_DATA = "L" + "B" * BOOT_CMDS_MAX_PROG_MEM_SIZE
    WRITE_PROG_MEM = "L" + "B" * BOOT_CMDS_MAX_PROG_MEM_SIZE
    PROG_MEM_WRITTEN = "L"
    END_TRANSFER = ""
    TRANSFER_ENDED = ""    
    ERROR_CODE = "H"
    ERASE = "L"
    ERASE_DONE = "L"

class ValidFileException(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

class OutdatedRevisionException(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)
 
class ValidVersionException(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)
        
class ValidPacketException(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)
        
class ConnectionFailureException(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

class WriteDataTimeoutException(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)
        
class bootload_parser_options :
    
    def __init__(self, args) :
        global app_baudrate, bootload_baudrate
        self.args = args;
        usage = "usage: %prog [options]"
        parser=optparse.OptionParser(usage=usage);
        parser.add_option("-p", "--com_port", dest="com_port", type="int");
        parser.add_option("-c", "--cold_boot", dest="cold_boot", action="store_true", default = False, help="Assume ankle is not already on when we start bootloading");
        parser.add_option("-a", "--app_baud_rate", dest="app_baud_rate", type="int", default=app_baudrate, help="Baud rate to use when sending special commands for warm booting");
        parser.add_option("--LIN", dest="use_LIN", action="store_true", default=False, help="Use with LIN mode comm: ignores the local echo on the RX line of whatever command was just sent out.")
        parser.add_option("-t", "--test", dest="test_command", type="int", help="Will send a test command of Command Type 'test'");
        parser.add_option("-D", "--debug", dest="debug_level", type="int", help="Set debug level. Default w/ no printouts is 0.");
        parser.add_option("-B", "--use_bluetooth", dest="use_bluetooth", action="store_true", default = False, help="Set if using Bluetooth to talk to ankle");
        parser.add_option("--use_remote_bluetooth_mac", dest="remote_bt_mac", help="The MAC address of a REMOTE Bluetooth device to dial out to. Use this option when using Bluetooth hardware instead of being connected directly to the ankle over a PSER.");
        parser.add_option("-b", "--baud_rate", dest="baud_rate", type="int", default=bootload_baudrate);
        parser.add_option("-S", "--state_controller", dest="hex_filename", type= "string", help= "Specify the image to be loaded into the ankle.");
        parser.add_option("-O", "--override_version_check", dest="override_version_check", type = "string")
# 
        (options, args) = parser.parse_args(args)
        d = parser.values.__dict__
        #print type(d)
        #print parser.values.__dict__
        self.values = parser.values
        self.parser = parser
        return 

"""Checks over the provided connection to see whether we are already in Bootload mode"""
def is_in_bootloader_mode(bl_ready, connection, remote_bt_addr=None, desired_comport=None, use_LIN = False):
    global USING_BLUETOOTH
    try:
        connection.close()
    except:
        print "connection already closed or null"
    #time.sleep(.5)
    connection = None
    #Try to reopen original connection to bootloader.
    if USING_BLUETOOTH:
        connection = config_bluetooth_serial_port(bootload_baudrate, BLUETOOTH_TIMEOUT_SECONDS, remote_address=remote_bt_addr)
        bootloader_timeout = .75
        
    else:
        
        chosen_timeout = PSER_TIMEOUT_SECONDS
        if use_LIN:
            chosen_timeout = LIN_TIMEOUT_SECONDS
        connection = serial.Serial(port=desired_comport, baudrate=bootload_baudrate, timeout=chosen_timeout, rtscts=0)
        bootloader_timeout = .2
    
    #Confirm that we've already gotten everything INTO bootload mode
    for  i in range(0, 30):
        if debug_level ==DEBUG_TEST_OUTPUT: 
            print "BOOTLOADER STATUS:", bl_ready

        if (bl_ready):
            break;

        try:
            if not bl_ready:
                sync, dest, src, msg_type, rec_seq_byte, msg_length, addr, payload  = send_cmd_expect_data(cmd_dest.BOOT_CMDS_DEST_V2X, cmd_dest.BOOT_CMDS_DEST_FUP, cmds.BOOT_CMDS_TYPE_SEND_HEADER, sequence_byte,2,[send_header_opt.BOOT_CMDS_HEADER_APP],payload_enc.SEND_HEADER, bootloader_timeout, connection, use_LIN = use_LIN)
                if msg_type == cmds.BOOT_CMDS_TYPE_HEADER_DATA and src == cmd_dest.BOOT_CMDS_DEST_V2X:
                    bl_ready = True

        except Exception  as e:
            if debug_level > 0: #== DEBUG_TEST_OUTPUT: 
                print 'Exception:', e

    return bl_ready, connection

"""Connects to bootloader over specified port assuming ankle is already powered-on"""   
""" Waits to see 3 messages from bootloader before either returning or timing out. A time-out in seconds will raise an exception that ends the transfer attempt."""
def warm_boot_connect(comport, bootload_baudrate, app_baudrate, TIMEOUT = 5, remote_bt_mac = None, use_LIN = False):

    print "\n------ Warm Boot Connect --------"
    print "Comport: " + str(comport)
    print "Bootload baudrate: " + str(bootload_baudrate)
    print "App baudrate: " + str(app_baudrate)
    print " "

    global debug_level, sequence_byte, PSER_TIMEOUT_SECONDS, BLUETOOTH_TIMEOUT_SECONDS, USING_BLUETOOTH
    dev_conn = pfcmd.PFCmd (0,0, quiet = True)   
    dev_conn.ignore_rx_of_local_echo = use_LIN;
    #Try an ankle special command to transfer control to bootloader. Deal with fact that app may be running at a different Baud rate than Bootloader:
    if USING_BLUETOOTH:
        dev_conn.USE_SERIAL_PORT = 1;
        dev_conn.comm_device = config_bluetooth_serial_port(app_baudrate, BLUETOOTH_TIMEOUT_SECONDS, remote_address=remote_bt_mac) 
        special_command_timeout = BLUETOOTH_TIMEOUT_SECONDS
    else:
        dev_conn.connect(comport, app_baudrate);
        dev_conn.set_serial_timeout(PSER_TIMEOUT_SECONDS);
        
    dev_conn.comm_device.flushOutput() #Flush to deal with error responses that may happen if you're already in the bootloader. 
    dev_conn.comm_device.flushInput()    
    
    dev_bl_ready = False

    if debug_level > 0:
        print "dev_application_comm_established"

    try:
        results_sc = dev_conn.special(BOOTLOAD_SPECIAL_CMD, values=[BOOTLOAD_CMD_OPT])

        if debug_level > 0:
            print "connect results: ", results_sc['cmd'], results_sc['v0']
            print "type of results_sc: " + str(type(results_sc))
        
        if results_sc['cmd'] == BOOTLOAD_SPECIAL_CMD and results_sc['v0'] == BOOTLOAD_CMD_OPT:
            dev_bl_ready = True
            # Pause for bootloader to start
            print "Waiting for Device..."
            time.sleep(2)

        else:
            # No result, but next section will check we actually got into BL mode.
            pass; #

    except Exception as e:
        if debug_level == DEBUG_TEST_OUTPUT:
            print 'Exception:', e       

    if debug_level == DEBUG_TEST_OUTPUT: 
        print "BOOT COMMAND STATUS:", dev_bl_ready
        
    if (comport): des_com = comport-1
    else: des_com = None

    # Now verify that we are into the boot loader
    dev_bl_ready, connection = is_in_bootloader_mode(False, dev_conn.comm_device,remote_bt_addr=remote_bt_mac, desired_comport=des_com)

    if debug_level == DEBUG_TEST_OUTPUT: 
        print "WARM BOOT CONNECT: ", dev_bl_ready

    if (dev_bl_ready):
        return connection
    else:
        try:   
            if (debug_level > 0):
                print "Special command failed, trying cold boot."

            return cold_boot_connect(connection, use_LIN= use_LIN)

        except: #Reset bluetooth baudrate to app baudrate before exiting
            if USING_BLUETOOTH:
                connection.close();
                connection = config_bluetooth_serial_port(app_baudrate, BLUETOOTH_TIMEOUT_SECONDS, remote_address=remote_bt_mac) 
            connection.close()

            raise ConnectionFailureException("Connection opened, but Ankle bootloader did not respond.")

"""Sends commands to exit bootloader and does not wait for a response."""
def force_exit(connection, timeout = 5.0, use_LIN = False):
    global debug_level, sequence_byte
    dev_conn = pfcmd.PFCmd()
    dev_conn.ignore_rx_of_local_echo = use_LIN;
    dev_conn.comm_device = connection
    dev_conn.USE_SERIAL_PORT = 1    

    payload_len = 0
    payload = []
    pl_fmt = ""

    start = time.clock()
    while (time.clock() - start < timeout*.25):   
        packed_msg = create_message(cmd_dest.BOOT_CMDS_DEST_V2X, cmd_dest.BOOT_CMDS_DEST_FUP, cmds.BOOT_CMDS_TYPE_END_TRANSFER, sequence_byte, payload_len,payload,pl_fmt)
        sequence_byte = (sequence_byte + 1) & 0xFF 
        send_message(connection, packed_msg)
        time.sleep(.1)

    #See where we got a response:
    rsp_byte_buffer = read_non_block(connection)
    start = rsp_byte_buffer.find(struct.pack('<H', BOOTLOADER_SYNC)) 
    while (start != -1 and len(rsp_byte_buffer) - start >= header_size + sync_size):
        sync, dest, src, msg_type, sequence_byte, msg_length = get_packet_header(rsp_byte_buffer[start:])
        if sync_size + header_size + msg_length + crc_size <= len(rsp_byte_buffer) - start:
            end = start + sync_size + header_size + msg_length + crc_size
            packet = rsp_byte_buffer[start:end]
            
            try:
                sync, dest, src, msg_type, sequence_byte, msg_length, addr, payload = receive_packet(packet)
                print "end tranfer rsp_msg dest:", dest, "src:", src, "type:", msg_type,cmds.BOOT_CMDS_TYPE_TRANSFER_ENDED 
                if msg_type == cmds.BOOT_CMDS_TYPE_TRANSFER_ENDED:
                    if src == cmd_dest.BOOT_CMDS_DEST_V2X:
                        self.bl_exited = True
                        self.bl_exit_time = time.clock() 

            except: #corrupted CRC
                pass 
            rsp_byte_buffer = rsp_byte_buffer[end:] + read_non_block(connection)
            start = rsp_byte_buffer.find(struct.pack('<H', BOOTLOADER_SYNC))
        else:
            break

"""Connects to bootloader over specified port assuming ankle may be powered off. This is the default connection method."""
""" Waits to see 3 messages from bootloader before either returning or timing out. A time-out will raise an exception that ends the transfer attempt."""
def cold_boot_connect(connection, TIMEOUT = DEFAULT_CONNECTION_SECONDS, remote_bt_mac = None, use_LIN = False):
    global sequence_byte, debug_level
    have_dev_ready = False
    connection.flushOutput()
    connection.flushInput()
    rsp_byte_buffer = ''
    print "Starting cold_boot. Power cycle now."

    start_sec = time.clock()
    while (time.clock() - start_sec < TIMEOUT and not (have_dev_ready)):
        
        if debug_level == DEBUG_TEST_OUTPUT: 
            print "BOOTLOADER STATUS:", have_dev_ready
       
        #Send request
        payload_len = 2
        payload = [send_header_opt.BOOT_CMDS_HEADER_APP]
        pl_fmt = payload_enc.SEND_HEADER

        if not have_dev_ready:
            if (debug_level > 0): print 'sent header req'
            packed_msg = create_message(cmd_dest.BOOT_CMDS_DEST_V2X, cmd_dest.BOOT_CMDS_DEST_FUP, cmds.BOOT_CMDS_TYPE_SEND_HEADER, sequence_byte, payload_len, payload, pl_fmt)
            if (send_message(connection, packed_msg)):
                sequence_byte = (sequence_byte + 1) & 0xFF
                delay(COLD_BOOT_DELAY);
            else:
                if debug_level > 0: print "Error sending packet:", packed   

        rsp_byte_buffer = rsp_byte_buffer + read_non_block(connection) 
            
        if len(rsp_byte_buffer) > 0 and  debug_level > 0:
            print"response buffer:", ' '.join(['%02X' % ord(x) for x in rsp_byte_buffer])
      
        start = rsp_byte_buffer.find(struct.pack('<H', BOOTLOADER_SYNC)) 

        while (start != -1  and len(rsp_byte_buffer) - start >= header_size + sync_size):
            #See if we can unpack length from what we have based on knowledge of header size
            sync, dest, src, msg_type, rec_seq_byte, msg_length = get_packet_header(rsp_byte_buffer[start:])
            if sync_size + header_size + msg_length + crc_size <= len(rsp_byte_buffer) - start:
                end = start + sync_size + header_size + msg_length + crc_size
                packet = rsp_byte_buffer[start:end]
                try:
                    sync, dest, src, msg_type, rec_seq_byte, msg_length, addr, payload = receive_packet(packet)
                    
                    if msg_type == cmds.BOOT_CMDS_TYPE_HEADER_DATA:
                        if src == cmd_dest.BOOT_CMDS_DEST_V2X:
                            have_dev_ready = True

                except Exception as e: #corrupted CRC
                    if debug_level > 0:
                        print e
                        print "failed parsing msg:", ' '.join(['%02X' % ord(x) for x in packet]) 
                    
                rsp_byte_buffer = rsp_byte_buffer[end:] + read_non_block(connection) 
                start = rsp_byte_buffer.find(struct.pack('<H', BOOTLOADER_SYNC))
            else:
                if debug_level > 0:
                    print 'processing response buffer:', ' '.join(['%02X' % ord(x) for x in rsp_byte_buffer])
                    print len(rsp_byte_buffer) - start, sync_size + header_size + msg_length + crc_size
                rsp_byte_buffer = rsp_byte_buffer + read_non_block(connection) 
                #break 
        
        #No sync char left in buffer means buffer has garbage to clear (possibly from special command responses):
        if (rsp_byte_buffer.find(struct.pack('<H', BOOTLOADER_SYNC)) == -1):
            rsp_byte_buffer = ''    
                    
    print 'Ending cold_boot attempt.'
    print "BOOTLOADER CONNECT:", have_dev_ready
                
    if (have_dev_ready):
        return connection
    else:
        #Try to exit back to apps if possible:
        force_exit(connection, use_LIN = use_LIN)             
        connection.close()
        if USING_BLUETOOTH: #Reset our baud rate back to app baud rate
            connection = config_bluetooth_serial_port(app_baudrate, BLUETOOTH_TIMEOUT_SECONDS, remote_address=remote_bt_mac) 
            connection.close()
        raise ConnectionFailureException("Connection opened, but Ankle bootloader did not respond.")
 
"""Verify an individual packet for corruption before we try sending it. If a packet is corrupt before we even send it, the hexfile or binary files are corrupt and cannot be used."""    
def verify_packet(packet_string, crc):
    print "TODO: Method not implemented"    
    pass      

"""Use parsed hexfile data to verify a binary string corruption before we try sending. Raises an exception if verification fails"""    
def verify_created_binary(parsed_hex):
    global debug_level 
    if (debug_level > 0):
        print parsed_hex
    i = 0
    str = ""
    for line in parsed_hex.hex_line_data():
        i = i + 1
        calcd = 0
        #Hex file checksum is intel Hex checksum: bytes in whole line of hexfile ignoring colon at beginning, then take least significant byte and 2s complement it
        #To get the checksum.
        for j in range(1, len(line.raw) - 2, 2):
            calcd = calcd + int(line.raw[j:j+2], 16)
        #Get 2s complement of least sig. byte: 
        calcd = (0x100 - (calcd & 0xFF)) & 0xFF
        
        str = str + " %X" % calcd
        if calcd != line.chksum:
            if (debug_level > 0):  print hex(calcd), hex(line.chksum)  
            print "\nERROR: Hex file "+parsed_hex.hexfilename
            print "Corrupted data in line %d:" % i
            print "\n>>> "+ line.raw 
            print "\nAbandoning upgrade attempt.\n"
            raise ValidFileException(-1)
    if (debug_level > 2):
        print str
    #Now verify entire CRC:     
    if (parsed_hex.crc_app_calc != parsed_hex.expected_crc32):
        if (debug_level > 0):  print hex(parsed_hex.crc_app_calc)   
        exception_info = "ERROR: Expected Hex file "+parsed_hex.hexfilename+" to contain CRC32="+hex(parsed_hex.expected_crc32)+", actually evaluates to "+hex(parsed_hex.crc_app_calc)+" Abandoning upgrade attempt."
        raise ValidFileException(exception_info)

"""Call this convenience method to send a single message and try to get a response from that 1 message. Throws an exception if the wrong packet type is returned."""
def send_cmd_expect_data(dest, src, msg_type, seq, msg_length, msg_bytes, msg_payload_format, timeout_sec, connection, traffic_log = '', use_LIN = False ):
    msg = create_message(dest, src, msg_type, seq, msg_length, msg_bytes, msg_payload_format)

    buf = ''

    if (msg_type == cmds.BOOT_CMDS_TYPE_SEND_HEADER):
        rsp_payload_fmt = payload_enc.HEADER_DATA
    elif (msg_type == cmds.BOOT_CMDS_TYPE_SEND_KEY):
        rsp_payload_fmt = payload_enc.KEY_DATA
    elif (msg_type == cmds.BOOT_CMDS_TYPE_UNLOCK_MEM):
        rsp_payload_fmt = payload_enc.MEM_UNLOCKED
    elif (msg_type == cmds.BOOT_CMDS_TYPE_SEND_PROG_MEM):
        rsp_payload_fmt = variable_payload_enc(msg_bytes[1])
    elif (msg_type == cmds.BOOT_CMDS_TYPE_WRITE_PROG_MEM):
        rsp_payload_fmt = payload_enc.PROG_MEM_WRITTEN
    elif (msg_type == cmds.BOOT_CMDS_TYPE_END_TRANSFER):
        rsp_payload_fmt = payload_enc.TRANSFER_ENDED
    elif (msg_type == cmds.BOOT_CMDS_TYPE_ERROR_CODE):
        rsp_payload_fmt = payload_enc.ERROR_CODE
    elif (msg_type == cmds.BOOT_CMDS_TYPE_ERASE):
        rsp_payload_fmt = payload_enc.ERASE_DONE

    #Clear out pipe:
    if (debug_level > 0):
        print 'read_non_block: '
    
    read_non_block(connection)
    
    if not (send_message(connection, msg)):

        print "Error sending packet:", msg
        
        if debug_level > 0: print "Error sending packet:", msg    
    
    starttime = time.clock()       
    
    
    while (time.clock() - starttime < timeout_sec ): #Wait up to TIMEOUT seconds for a response
        buf = buf + read_non_block(connection) 
        start = buf.find(struct.pack('<H', BOOTLOADER_SYNC)) 
        rsp_packet_size = header_size + sync_size + struct.calcsize('<'+rsp_payload_fmt) + crc_size
        if start != -1  and  (len(buf) - start) >= rsp_packet_size:
            packet = buf[start:start + rsp_packet_size]
            try:
                sync, dest, src, msg_type, sequence_byte, msg_length, addr, payload = receive_packet(packet)
                return  sync, dest, src, msg_type, sequence_byte, msg_length, addr, payload      
            except ValidPacketException as e: 
                print e.value
        else:
            pass
            #if(len(buf) > 0):
            #    print "response:", ['%02x' % ord(b) for b in buf],

            #if (debug_level > 5):
            #    print "response:", ['%02x' % ord(b) for b in buf]      

    return None

"""Call this method to get the struct encoding for a PROG_MEM payload that does not have the default max length."""
def variable_payload_enc(length):
    return "L" + "B" * length

"""Builds a message to be sent to bootloader. Returns a little-endian packed struct message to send."""
def create_message(dest, src, msg_type, seq, msg_length, msg_bytes, msg_payload_format):
    global debug_level, local_log
    #Based on the following C message structure: sync + header + variable payload +  crc 
    header = struct.pack('<H'+ header_encoding,  BOOT_CMDS_SYNC_CODE, dest & 0xFF, src & 0xFF, msg_type, seq, msg_length)
    body = struct.pack('<'+msg_payload_format, *msg_bytes)
    b_number = [ord(x) for x in (header + body)]
    crc = crc_port.calcCRC32(b_number);     
    if debug_level > 2:
        current_datetime = str(datetime.datetime.now())
        current_time_seconds = str(time.time())
        packet_record = 'Sent: '+current_datetime+' | '+current_time_seconds+' | '+' PL: ' + str([ '%02X' % ord(x) for x in  (header + body + struct.pack('<L', crc))])
        print str(packet_record)
        if local_log:
          local_log.write(str(packet_record))
    return header + body + struct.pack('<L', crc)

def LIN_send(connection, packet):
                connection.flush()
                written = connection.write(packet)
                #print 'trying to ignore LIN echo...'
                to_ignore = connection.read(len(packet)); #ignore what we just sent. 
                while(to_ignore < len(packet)):
                    to_ignore += connection.read(len(packet)); #ignore what we just sent. 
                if (to_ignore != packet):
                    print "LIN Echo matched message sent out:", (to_ignore == packet)
                    print "LIN Sent out:", len(packet), [hex(ord(x)) for x in packet]
                    print "LIN Ignored local echo:", len(to_ignore),[hex(ord(x)) for x in to_ignore]
                    #raise Exception("Error! Local LIN echo did not equal what was sent out!")
                    
                #print "Ignored local echo:", [hex(ord(x)) for x in to_ignore]
                return written
    
USING_BLUETOOTH = False    
    
total_msgs_sent = 0   
"""Sends a message to the bootloader. Does not wait for a response. A higher level algorithm is responsible for flow control."""     
def send_message(connection, packed_struct):

    # print "in send_message"
    
    
    global total_msgs_sent
    bytes_written = connection.write(packed_struct)

    # print 'bytes_written: '+str(bytes_written)

    if bytes_written == len(packed_struct):
        total_msgs_sent = total_msgs_sent + 1
        #All bytes written.
        # print 'all bytes written'
        return True
    else: #Port write issue:   
        print 'port write issue'
        return False

        
        
def receive_packet(packet):
    """Receives a packet, unpacks it, and parses it into the correct structure. Returns the address of the data written or -1 if the packet is of a different type. Raises an exception if there is a parsing or crc problem."""    

    global debug_level, local_log
    header_size = struct.calcsize('<H'+header_encoding)
    sync, dest, src, msg_type, rec_sequence_byte, msg_length = struct.unpack('<H'+header_encoding, packet[0:header_size])
    payload_bytes = packet[header_size:header_size + msg_length]
    expected_crc = crc_port.calcCRC32([ord(x) for x in packet[0:-4]]);    #Calc crc based on everything except last 4 bytes of old crc  
    crc = struct.unpack('<L', packet[-4:])[0]
    if debug_level > 2:
        current_timestamp = str(datetime.datetime.now()) 
        current_time_seconds = str(time.time())
        packet_record = 'Received: '+current_timestamp+' | '+current_time_seconds+' | '+' PL: '+ str([ '%02X' % ord(x) for x in packet])
        print packet_record
        if local_log:
          local_log.write(packet_record)
    
    if (crc != expected_crc):
        if (debug_level > 2):  
            print hex(crc), hex(expected_crc), ['%02X' % ord(x) for x in packet] 
        raise ValidPacketException("Received corrupted packet.")
    
    elif ( msg_type == cmds.BOOT_CMDS_TYPE_PROG_MEM_WRITTEN):
        addr = struct.unpack('<'+payload_enc.PROG_MEM_WRITTEN, payload_bytes)[0]
        return sync, dest, src, msg_type, rec_sequence_byte, msg_length, addr, -1
    
    elif ( msg_type == cmds.BOOT_CMDS_TYPE_PROG_MEM_DATA):
        addr = struct.unpack('<'+variable_payload_enc(msg_length-4), payload_bytes)[0]
        return sync, dest, src, msg_type, rec_sequence_byte, msg_length, addr, payload_bytes[4:]
    else:
        return sync, dest, src, msg_type, rec_sequence_byte, msg_length, -1, payload_bytes   


def get_packet_header(packet):
    """Unpacks a header and returns the length of the packet it should be attached to.""" 
    #Parse header:
    header_size = struct.calcsize('<H'+header_encoding)
    sync, dest, src, msg_type, rec_sequence_byte, msg_length = struct.unpack('<H'+header_encoding, packet[0:header_size])
    return sync, dest, src, msg_type, rec_sequence_byte, msg_length
    
    
def modify_packet_seq(packet, new_sequence_byte):
    """Quick convenience method that takes an already constructed packet and alters the sequence number so it can be re-sent:"""    

    #Already packed bytes have sequence_byte located at index  5 and recalc the crc in the last 4 bytes
    updated_packet = list(packet)[0:-4]
    updated_packet[5] = chr(new_sequence_byte & 0xFF)              
    b_number = [ord(x) for x in updated_packet]
    crc = crc_port.calcCRC32(b_number);     
    return ''.join(updated_packet) + struct.pack('<L', crc)         

    
    
#Main program:
def read_hex(connection):
    read_hex_image = ''
 
 
DEBUG_BT = -321    
    
       

class Upload: 
    """An Upload is a class designed to make it simple to manage many Uploads at once through a shared port. Each upload keeps track of its own sent and received
    messages in a buffer, sends messages over a shared port, and receives messages addressed to it from the shared port. Each upload is created from a hex file and can 
    be exported as a binary file."""
        
    def __init__(self, dest, hexfile, binary_file = None, traffic_logger = None):
        global sequence_byte    
        global header_encoding, header_size, sync, sync_size, crc_size, debug_level
        self.finished = False
        self.upload_time = -1
        if debug_level == DEBUG_TRAFFIC_LOG:
            self.traffic_log = traffic_logger 
        
        if (binary_file == None):
            self.dest = dest
            try:
                parsed_hex = HexfileMetadata(hexfile)
            except Exception as e:
                print "FILE-PARSE-ERROR"
                raise ValidFileException(e.value)
            
            self.revision_being_replaced = 'Unknown'

            self.fname = hexfile
            self.binary = parsed_hex.binary_data32
            verify_created_binary(parsed_hex)
            self.crc32 = parsed_hex.crc_app_calc

            self.maj_rev =  int(parsed_hex.maj_hex_revision, 16)
            self.min_rev =  int(parsed_hex.min_hex_revision, 16)
            self.bugfix_rev = int(parsed_hex.bugfix_hex_revision, 16)
            self.offset = parsed_hex.start_header_addr/4       # This offset is in 32-bit words
            self.total_bytes = parsed_hex.prog_size_bytes
            print "App start position:", hex(parsed_hex.start_abs_hex_addr)
            print "App size:", hex(self.total_bytes)

        else:
            self.create_from_binary(binary_file) 

        self.unconfirmed_msg_buffer = dict() #A List of addresses mapped to the message packets that were sent in packet form to the bootloader.
        self.unconfirmed_msg_send_time = dict() # A List of addresses mapped to the times the message packets were sent so that packet RTT time can be tracked and used as a way of
                                           #   knowing when we need to resend a packet.                                 
        self.window_size = MAX_TRANSIT_BYTES_SIZE
        self.percent_done = 0
        self.window_sent =  0
        self.repeat_msgs = 0
        self.repeat_acks = 0
        self.total_msgs = 0
        self.total_acks = 7
        self.corrupted_msg = 0
        self.start_send = time.clock()

    def export_binary(self, file_name):

        import pickle
        binary_info = { "app_type": self.dest, "crc32": self.crc32, "min_rev": self.min_rev,  "maj_rev": self.maj_rev, "binary": self.binary, "offset": self.offset, "total_bytes": self.total_bytes}  
        pickle.dump( binary_info, open( file_name, "wb" ) )    
        
    def create_from_binary(self, file_name):
        import pickle
        d = pickle.load( open( file_name ) )    
        self.dest = d["app_type"]
        self.min_rev = d["min_rev"]
        self.maj_rev = d["maj_rev"]
        self.fname = file_name
        self.binary = d["binary"]
        self.offset = d["offset"]
        self.total_bytes = d["total_bytes"]
        self.crc32 = d["crc32"]
        return self

    def send_next_write_msg(self,sp, use_LIN = False):
        global sequence_byte, WRITE_PACKET_RTT_SECONDS, header_encoding, header_size, sync, sync_size, crc_size, debug_level

        if self.finished:
            return 
        else:
            #Send next packets:
            should_send_more = True
            while (should_send_more):
                should_send_more = False
                addr_already_requested = self.offset + self.window_sent
                last_address_to_write = int(self.offset + self.total_bytes)
                if (debug_level > 1): print [hex(j) for j in self.unconfirmed_msg_buffer.keys()] 
                if len(self.unconfirmed_msg_buffer.keys()) == 0:
                    should_send_more = True
                else:
                    window_start = min(self.unconfirmed_msg_buffer.keys())
                    window_end = int(window_start + self.window_size)
                    
                    if (addr_already_requested < min(window_end, last_address_to_write)):
                        should_send_more = True

                    if (debug_level > 3):
                        print "Sending more?", should_send_more, "Addr_already_reqd:", hex(int(addr_already_requested)), "unconfd:", len(self.unconfirmed_msg_buffer.keys()), "st", hex(int(window_start)), "end", hex(int(window_end)), "sent:", self.window_sent, "total", self.total_bytes

                if (should_send_more and self.window_sent < self.total_bytes):
                    addr_len = 4;  #32 bit address.
                    payload_len = int(min(payload_enc.BOOT_CMDS_MAX_PROG_MEM_SIZE,  self.total_bytes - self.window_sent)) #Only send multiples of 4 bytes for 32 bit format.

                    addr = int(self.offset + self.window_sent) #Pic Hardware address only goes up by 4 for every 4 bytes sent.
                    payload = [addr]

                    # Assume we skip packet, then test in the following loop
                    skip_data_packet = 1
                    pl_fmt = variable_payload_enc(payload_len)
                    for x in range(0,payload_len/4):

                        # If any item in the packet is NOT all 0xFF, then we can't skip packet.
                        if (self.binary[addr/4 + x].data != [255,255,255,255]):
                            skip_data_packet = 0

                        payload = payload + self.binary[addr/4 + x].data   #binary data is 32 bit, divide addr by 4 to get correct addr.
                        #print self.binary[addr/4 + x].data

                    if (debug_level > 2): 
                        print payload_len
                        print self.dest,self.fname, ": ", hex(addr), ''.join(['%02X' % p for p in payload])
                        
                    if(skip_data_packet == 1):   
                        self.window_sent =  self.window_sent + payload_len
                        self.total_msgs = self.total_msgs + 1
                        if (debug_level > 1):
                            print "Skip send", hex(addr), self.window_sent, self.total_bytes
                        
                        if (self.window_sent >= self.total_bytes):
                            self.finished = True
                            should_send_more = False

                    else:                                            
                        packed = create_message(self.dest, cmd_dest.BOOT_CMDS_DEST_FUP, cmds.BOOT_CMDS_TYPE_WRITE_PROG_MEM, sequence_byte, addr_len + payload_len,payload,pl_fmt)
                    
                        if (send_message(sp, packed)):
                                    
                            if debug_level == DEBUG_TRAFFIC_LOG:
                                self.traffic_log.sent.append((self.dest, time.time(), 1, len(self.unconfirmed_msg_buffer.keys()), 0))
                                self.traffic_log.write()
                                
                            self.unconfirmed_msg_buffer[addr] = packed
                            self.unconfirmed_msg_send_time[addr]  = time.clock()
                            self.window_sent =  self.window_sent + payload_len
                            sequence_byte = (sequence_byte + 1) & 0xFF
                            self.total_msgs = self.total_msgs + 1
                            if debug_level > 1:
                                print "Sent Address:", hex(addr)
                            
                        else:
                             if debug_level > 0: print self.dest,self.fname, ": ","Error sending packet:"

                elif len(self.unconfirmed_msg_buffer.keys()) > 0:
                   
                    k = min(self.unconfirmed_msg_buffer.keys())
                    if (time.clock() - self.unconfirmed_msg_send_time[k] > WRITE_PACKET_RTT_SECONDS*len(self.unconfirmed_msg_buffer.keys())): 
                        # print "timed out!"
                        #Re ask for messages that haven't been answered after RTT has passed.
                        if (debug_level > 0):
                            print "Resending packets", hex(k), len(self.unconfirmed_msg_buffer.keys())
                            print [hex(j) for j in self.unconfirmed_msg_buffer.keys()]
                    
                        reused_packet = modify_packet_seq(self.unconfirmed_msg_buffer[k], sequence_byte)
                        send_message(sp, reused_packet)
                        if debug_level == DEBUG_TRAFFIC_LOG:
                            self.traffic_log.sent.append((self.dest, time.time(), 1, len(self.unconfirmed_msg_buffer.keys()), 1))
                            self.traffic_log.write()

                        self.unconfirmed_msg_buffer[k] = reused_packet
                        self.unconfirmed_msg_send_time[k]  = time.clock()  
                        sequence_byte = (sequence_byte + 1) & 0xFF
                        self.total_msgs = self.total_msgs + 1
                        self.repeat_msgs = self.repeat_msgs + 1
                    else:
                        if (debug_level > 3):   print self.dest,self.fname, ": Waiting on network latency before re-sending packet", hex(k)

                else: #Transfer complete when we have no unconfirmed msgs
                    self.finished = True
                    #print "Total distinct msgs sent", self.total_msgs-self.repeat_msgs       

                    if debug_level == DEBUG_TEST_OUTPUT:
                        print self.dest,self.fname, ": complete hex file written:",hex(self.window_sent), " bytes written to flash."
                        print "Corrupted:", 100 * self.corrupted_msg/(self.total_msgs * 1.0), "%  Lost:", 100.0*(1.0 - (1.0 * self.total_acks)/(self.total_msgs * 1.0)), "%  Rep:", 100*(self.repeat_msgs/(self.total_msgs * 1.0)), "% Rep.Acks (inefficient)", 100*(self.repeat_acks/(self.total_acks * 1.0))

    def receive_write_ack_packet(self,bytes):
        """Receives and processes acknowledgment packets that can further the progress of the Upload and keeps track of which packets have been acknowledged."""
    
        global sequence_byte, header_encoding, header_size, sync, sync_size, crc_size, debug_level

        try:
            sync, dest, src, msg_type, rec_sequence_byte, msg_length, addr_received, payload = receive_packet(bytes)
            
            #Account for received packet confirmation, remove it from list of unacknowledged:
            if (msg_type == cmds.BOOT_CMDS_TYPE_PROG_MEM_WRITTEN and addr_received != -1):
                self.total_acks = self.total_acks + 1
                if (debug_level > 1): 
                    print self.dest, ": Conf:" + hex(addr_received)+ " #Corr:" + str(self.corrupted_msg) + " %Lost:"+ str(100*(1 - self.total_acks/(self.total_msgs * 1.0))) + " %Rep:" + str(100*(self.repeat_msgs/(self.total_msgs * 1.0))), "% Rep.Acks (inefficient)", 100*(self.repeat_acks/(self.total_acks * 1.0))
                try:    
                    del self.unconfirmed_msg_buffer[addr_received]
                    del self.unconfirmed_msg_send_time[addr_received]
                    if debug_level == DEBUG_TRAFFIC_LOG:
                            self.traffic_log.rec.append((self.dest, time.time(), self.window_sent, len(self.unconfirmed_msg_buffer.keys())))
                            self.traffic_log.write()

                except:
                    if (addr_received != 0x1663a01): #Weird error code
                        self.repeat_acks = self.repeat_acks + 1 
                        if (debug_level > 0):  print self.dest, ": Received repeat write confirmation for ",hex(addr_received)
        except:
            #Deal with corrupted packet:
            self.corrupted_msg = self.corrupted_msg + 1
            if (debug_level > 0):   print self.dest,self.fname, ": Discarded corrupted packet."       
        
        if len(self.unconfirmed_msg_buffer.keys()) == 0 and self.window_sent == self.total_bytes:
            self.finished = True
            self.upload_time = time.clock() - self.start_send


app_baudrate = 115200
bootload_baudrate = 230400

def config_bluetooth_serial_port(desired_baud, bt_timeout, remote_address):
    """Configures BT Radio for Bootloader."""
    global WRITE_PACKET_RTT_SECONDS, radio 
    WRITE_PACKET_RTT_SECONDS = 0.200 # Bluetooth is slower than serial, so make RTT larger.
    
    emulated_comport = btSerial.SerialPortSocketWrapper(bluetooth_address=remote_address, port = 1, baudrate=desired_baud, timeout=bt_timeout)
    emulated_comport.open()
    emulated_comport.setBluetoothInFastDataMode();
    return emulated_comport

class BootloadProcess:
    """The BootloadProcess class is used to upload all files to the bootloader. It exists to encapsulate sets of uploaded Hex files and to provide process information to the Bootloader GUI representation."""            

    def __init__(self, opt, connect_to_all = False, connect_on_creation = True):
        global debug_level, USING_BLUETOOTH
        self.killed = False
        self.use_LIN = opt.use_LIN
        #First verify files, then connect.
        self.complete = False
        self.sp = None
        
        if (opt.debug_level):
            debug_level = opt.debug_level
            print "DEBUG LEVEL:", debug_level
        
        if debug_level == DEBUG_TRAFFIC_LOG:
            self.traffic_log = TrafficLogger() 
        else:
            self.traffic_log = None 
        
        if (opt.hex_filename):
            self.dev_upload = Upload(cmd_dest.BOOT_CMDS_DEST_V2X, opt.hex_filename, traffic_logger = self.traffic_log)
            if debug_level > 0:
                print 'finished loading file'
        else:
            raise ValidFileException("Must Specifiy a filename")

        print self.get_version_info()
           
        self.sp = None
        if (connect_on_creation):
            self.connect(opt)

        self.last_printed = 0
    
    def connect(self, opt):
        global debug_level, USING_BLUETOOTH
        
        #Create our own temporary com port if we're using a remote bluetooth radio:
        if (opt.use_bluetooth):
            USING_BLUETOOTH = True    
            self.remote_bt_mac= opt.remote_bt_mac
            
            if opt.cold_boot: #We think we're already in the bootloader state:
                self.sp = config_bluetooth_serial_port(opt.baud_rate, BLUETOOTH_TIMEOUT_SECONDS, remote_address=opt.remote_bt_mac)
                cold_boot_connect(self.sp, remote_bt_mac=opt.remote_bt_mac, use_LIN = False)
               
            else:
                
                if debug_level > 0:
                    print '---in BootloadProcess.connect, use_bluetooth = True'

                self.sp = warm_boot_connect(None ,opt.baud_rate,opt.app_baud_rate, remote_bt_mac = opt.remote_bt_mac, use_LIN= False)
                if debug_level > 0:
                    print 'Warm boot was successful!'
                    
        elif (opt.com_port and type(opt.baud_rate) == int) :
            if (opt.cold_boot):
                self.sp = serial.Serial(port=opt.com_port - 1, baudrate=opt.baud_rate, timeout=0, rtscts=0)
                cold_boot_connect(self.sp, use_LIN = opt.use_LIN)
                
            else:

                if debug_level > 0:
                    print '---in BootloadProcess.connect, use_bluetooth = False'

                self.sp = warm_boot_connect(opt.com_port,opt.baud_rate, opt.app_baud_rate, use_LIN = opt.use_LIN)
                
            self.sp.setRTS(1);# to enable power on BR        
            
        else:
            raise ConnectionFailureException("Must Specifiy a COMM port or IP address.")

        self.rsp_byte_buffer = ''  
    
    def write_bootloader_range(self, start_addr, end_addr, chunksize, data,write_to_location, TIMEOUT = .5):
        print "WARNING! THIS METHOD IS FOR DEVELOPMENT TESTING ONLY AND DOES NOT HAVE ANY BUFFERING OF WRITTEN DATA OR COMMUNICATION HANDLING."
        read_non_block(self.sp) #Flush before starting. 
        
        to_write_bytes = [0xff] * (end_addr - start_addr)*3/2
        to_write_bytes[0: (end_addr - start_addr)*3/2] = data[0: (end_addr - start_addr)*3/2]
        get_addr = start_addr
        seq_byte = 0
        payload_fmt = payload_enc.WRITE_PROG_MEM
        payload_len = min(end_addr - get_addr, BOOT_CMDS_MAX_READ_DATA_SIZE)
        addr_len = 4
        packed_msgs = ''
        rsp_byte_buffer = ''
        start_sec = time.clock()
  
        while get_addr < end_addr -1:
            addr_and_payload = [0] * len(payload_fmt)
            addr_and_payload[0] = get_addr
            addr_and_payload[1:len(data[(get_addr - start_addr)*3/2: (get_addr- start_addr)*3/2 + payload_len]) + 1] =  data[(get_addr - start_addr)*3/2: (get_addr - start_addr)*3/2 + payload_len]
            
            msg = create_message(write_to_location, cmd_dest.BOOT_CMDS_DEST_FUP, cmds.BOOT_CMDS_TYPE_WRITE_PROG_MEM, seq_byte, payload_len + addr_len, addr_and_payload,payload_fmt)
            send_message(self.sp, msg)

            start_sec = time.clock()
            while len(rsp_byte_buffer) == 0 and time.clock() - start_sec < TIMEOUT : #If a message can't be written, we won't get a response, so throw a write timeout exception
                rsp_byte_buffer = rsp_byte_buffer + read_non_block(self.sp) 
                
                seq_byte = (seq_byte + 1) & 0xFF

            if len(rsp_byte_buffer) < struct.calcsize('<H'): #Buffer does not even contain enough data for a sync character.
                raise WriteDataTimeoutException("Got no response and timed out while trying to write data to location "+str(write_to_location) +" and address " + hex(get_addr))
            else:
                rsp_byte_buffer = rsp_byte_buffer + read_non_block(self.sp) 
                print hex(get_addr), "Response:", " ".join("%02x" % ord(x) for x in rsp_byte_buffer)
                    
            
            start = rsp_byte_buffer.find(struct.pack('<H', BOOTLOADER_SYNC)) 
            rec = 0
            while (start != -1) and len(rsp_byte_buffer) > struct.calcsize('<H'+header_encoding):
                end = start
                #See if we can unpack length from what we have based on knowledge of header size
                sync, dest, src, msg_type, sequence_byte, msg_length = get_packet_header(rsp_byte_buffer[start:])
                if sync_size + header_size + msg_length + crc_size <= len(rsp_byte_buffer) - start:
                    end = start + sync_size + header_size + msg_length + crc_size
                    packet = rsp_byte_buffer[start:end]
                    sync, dest, src, msg_type, rec_sequence_byte, msg_length, addr_written, rsp_payload = receive_packet(packet)
                    get_addr = max(get_addr, addr_written + chunksize*2/3) #word to byte ratio is 2 to 3
                rsp_byte_buffer = rsp_byte_buffer[end:] + read_non_block(self.sp) #256 bytes is at least 1 messages worth of data.     
                start = rsp_byte_buffer.find(struct.pack('<H', BOOTLOADER_SYNC)) 
        return

    def exit_bootloaders(self, connection, TIMEOUT = DEFAULT_CONNECTION_SECONDS):    
        """Disconnects from the bootloaders Returns true if exited properly, false otherwise"""  
        #Try an ankle special command to transfer control to bootloader:
        global debug_level, sequence_byte
        dev_conn = pfcmd.PFCmd()
        dev_conn.ignore_rx_of_local_echo = self.use_LIN;
        dev_conn.comm_device = connection
        dev_conn.USE_SERIAL_PORT = 1
        results = None
        start_time = time.clock()
        self.bl_exited = False
        rsp_byte_buffer = ''

        #Send request: exit MC and IMU first, then SC.
        while (time.clock() - start_time < TIMEOUT and (not (self.bl_exited))):
            payload_len = 0
            payload = []
            pl_fmt = ""

            packed_msg = create_message(cmd_dest.BOOT_CMDS_DEST_V2X, cmd_dest.BOOT_CMDS_DEST_FUP, cmds.BOOT_CMDS_TYPE_END_TRANSFER, sequence_byte, payload_len, payload, pl_fmt)
            if (send_message(connection, packed_msg)):
                if (debug_level > 0):
                    print 'end transfer msg:', [ '%02X' % ord(x) for x in packed_msg]
                sequence_byte = (sequence_byte + 1) & 0xFF
            else:
                print "Error sending packet:", packed_msg

            rsp_byte_buffer = rsp_byte_buffer + read_non_block(connection)

            if (debug_level > 0):
                if len(rsp_byte_buffer) != 0:
                    print 'end transfer rsp:', [ '%02X' % ord(x) for x in rsp_byte_buffer] , len( rsp_byte_buffer), header_size + sync_size
                else :
                    print '.'

            start = rsp_byte_buffer.find(struct.pack('<H', BOOTLOADER_SYNC)) 
            while (start != -1 and len(rsp_byte_buffer) - start >= header_size + sync_size):
                sync, dest, src, msg_type, sequence_byte, msg_length = get_packet_header(rsp_byte_buffer[start:])
                if sync_size + header_size + msg_length + crc_size <= len(rsp_byte_buffer) - start:
                    end = start + sync_size + header_size + msg_length + crc_size
                    packet = rsp_byte_buffer[start:end]

                    try:
                        sync, dest, src, msg_type, sequence_byte, msg_length, addr, payload = receive_packet(packet)
                        if (debug_level > 0):
                            print "end tranfer rsp_msg dest:", dest, "src:", src, "type:", msg_type,cmds.BOOT_CMDS_TYPE_TRANSFER_ENDED 
                        if msg_type == cmds.BOOT_CMDS_TYPE_TRANSFER_ENDED:
                            if src == cmd_dest.BOOT_CMDS_DEST_V2X:
                                self.bl_exited = True
                                self.bl_exit_time = time.clock() 

                    except: #corrupted CRC
                        pass 
                    rsp_byte_buffer = rsp_byte_buffer[end:] + read_non_block(connection)
                    start = rsp_byte_buffer.find(struct.pack('<H', BOOTLOADER_SYNC))
                else:
                    break

        if self.bl_exited:
            if debug_level > 0:
                print 'Bootloaders exited.'
            return True
        else:
            #See if we missed the command:
            pf = pfcmd.PFCmd ()
            pf.ignore_rx_of_local_echo = self.use_LIN;
            connection.baudrate = app_baudrate   
            pf.comm_device = connection
            print pf.do_command('state_version',[]);
            return False

    def get_version_info(self):
        hex_fw_ver = "%s Firmware Hex Rev: %d.%d.%d CRC: %s" % (self.dev_upload.fname, self.dev_upload.maj_rev,self.dev_upload.min_rev, self.dev_upload.bugfix_rev, hex(self.dev_upload.crc32))
        return hex_fw_ver
           
    def get_old_version_info(self):
        dev_fw_ver = "Firmware Hex Rev: " + self.dev_upload.revision_being_replaced
        return dev_fw_ver
    
    def erase_device_application(self):
        try:
            sync, dest, src, msg_type, rec_seq_byte, msg_length, addr, payload = send_cmd_expect_data(cmd_dest.BOOT_CMDS_DEST_V2X, cmd_dest.BOOT_CMDS_DEST_FUP, cmds.BOOT_CMDS_TYPE_ERASE, sequence_byte,4,[send_header_opt.BOOT_CMDS_HEADER_APP],payload_enc.ERASE, 15, self.sp, use_LIN = self.use_LIN)
        except Exception as e:
            print e

    def verify_upload_revisions(self, stopIfOutdated):

        if debug_level > 0:
            print 'verify_upload_revisions'

        dev_read_majRev, dev_read_minRev, dev_read_app_crc, = -1, -1, -1
        global USING_BLUETOOTH 
        bootloader_timeout = 5
        if USING_BLUETOOTH:
            bootloader_timeout = .75

        #Double check revisions before starting:
        tries = 0
        while tries < 10:
            
            if debug_level > 0:
                print 'tries: ' + str(tries)
            payload = None
            try:
                # I think nothing is getting sent here in send_cmd_expect_data
                sync, dest, src, msg_type, rec_seq_byte, msg_length, addr, payload = send_cmd_expect_data(cmd_dest.BOOT_CMDS_DEST_V2X, cmd_dest.BOOT_CMDS_DEST_FUP, cmds.BOOT_CMDS_TYPE_SEND_HEADER, sequence_byte,2,[send_header_opt.BOOT_CMDS_HEADER_APP],payload_enc.SEND_HEADER, bootloader_timeout, self.sp, use_LIN = self.use_LIN)
            except Exception as e:
                if debug_level > 0:
                    print 'exception: ' + str(e)

            if payload != None:
                h_code, rev_major, rev_minor, rev_bugfix, rev_beta, rev_build, rev_svn, app_start, app_size, app_crc = struct.unpack('<'+payload_enc.HEADER_DATA, payload)
                currentHexMajRev =  rev_major
                currentHexMinRev =  rev_minor
                crcString = hex(app_crc)
                self.dev_upload.revision_being_replaced = ("%d.%d.%db%d" % ( currentHexMajRev,  currentHexMinRev, rev_bugfix, rev_beta)) + " CRC:" + crcString
                print "\nFirmware being replaced is at revision", self.dev_upload.revision_being_replaced
                
                if (stopIfOutdated):
                    if (currentHexMajRev > self.dev_upload.maj_rev):
                        self.exit_bootloaders(self.sp)
                        raise OutdatedRevisionException("Tried to upload version %d.%d.%d, device already at version %d.%d.%d" % ( self.dev_upload.maj_rev, self.dev_upload.min_rev,self.dev_upload.bugfix_rev, currentHexMajRev,  currentHexMinRev, rev_bugfix))
                    elif (currentHexMajRev == self.dev_upload.maj_rev):
                        if (currentHexMinRev > self.dev_upload.min_rev):
                            self.exit_bootloaders(self.sp)
                            raise OutdatedRevisionException("Tried to upload version %d.%d.%d, device already at version %d.%d.%d" % ( self.dev_upload.maj_rev, self.dev_upload.min_rev,self.dev_upload.bugfix_rev, currentHexMajRev,  currentHexMinRev, rev_bugfix))
                        elif (currentHexMinRev == self.dev_upload.min_rev):
                            if (rev_bugfix > self.dev_upload.bugfix_rev):
                                    self.exit_bootloaders()
                                    raise OutdatedRevisionException("Tried to upload version %d.%d.%d, device already at version %d.%d.%d" % ( self.dev_upload.maj_rev, self.dev_upload.min_rev,self.dev_upload.bugfix_rev, currentHexMajRev,  currentHexMinRev, rev_bugfix))
                            elif (rev_bugfix == self.dev_upload.bugfix_rev):
                                #If CRC values are the same, don't bother with upload, if they aren't, continue.
                                if app_crc == self.dev_upload.crc32:
                                    self.exit_bootloaders(self.sp)
                                    raise OutdatedRevisionException("Tried to upload version %d.%d.%d, device already at version %d.%d.%d" % ( self.dev_upload.maj_rev, self.dev_upload.min_rev,self.dev_upload.bugfix_rev, currentHexMajRev,  currentHexMinRev, rev_bugfix))
                dev_read_majRev = currentHexMajRev
                dev_read_minRev = currentHexMinRev
                dev_read_app_crc = app_crc
                break
                
            else:
                tries = tries + 1 
          
        if tries == 10:
            raise ConnectionFailureException("Could not contact Device about current code version" )

        return dev_read_majRev, dev_read_minRev, dev_read_app_crc
    
    def __del__(self):
        #Deal with fixing Bluetooth device on closing port if needed:
        if self.sp != None:
            if USING_BLUETOOTH:
                global app_baudrate
                self.sp.close()
                self.sp = config_bluetooth_serial_port(app_baudrate, BLUETOOTH_TIMEOUT_SECONDS, remote_address=self.remote_bt_mac)   
            self.sp.close()

    def start_uploading(self,opt):

        global debug_level 

        if debug_level > 0:
            print 'start_uploading'
            print 'pausing for bootloader to init'

        time.sleep(0.5)

        self.sp.flushOutput()
        self.sp.flushInput()

        if (opt.test_command):
            test(opt, self.sp)
            return

        if not opt.override_version_check == '1234':
            print 'version override_version_check failed'
            self.verify_upload_revisions(False) #First time through we record all the revisions, next time through we verify them
            self.verify_upload_revisions(True)
        else: #still readout revisions even if we aren't verifying them
            self.verify_upload_revisions(False)

        sequence_byte = 0 & 0xFF

        print "\nErasing Device Application - Do not interrupt power..."
        self.erase_device_application()

        print "\n  Downloading: v%d.%d.%d" % (self.dev_upload.maj_rev,self.dev_upload.min_rev, self.dev_upload.bugfix_rev)
        
        sc_done = False 

        while not sc_done and not self.killed:
                    
            self.rsp_byte_buffer = ''.join([self.rsp_byte_buffer, read_non_block(self.sp)]) #Read all available data- may not be this much. May be none 
            if debug_level == DEBUG_TRAFFIC_LOG:
                self.traffic_log.overall_rsp_buffer_size.append((time.time(), len(self.rsp_byte_buffer), "rcvd1"))
                self.traffic_log.write()
            if (len(self.rsp_byte_buffer) > 0):
                if (debug_level > 4):
                    print 'Buffer:', ' '.join(['%02X' % ord(p) for p in self.rsp_byte_buffer])        
            start = self.rsp_byte_buffer.find(struct.pack('<H', BOOTLOADER_SYNC)) 
            while (start != -1 and len(self.rsp_byte_buffer[start:]) >= header_size + 2): #Must have enough data for header + 2 byte sync character.
                    
                #See if we can unpack length from what we have based on knowledge of header size
                sync, dest, src, msg_type, rec_sequence_byte, msg_length = get_packet_header(self.rsp_byte_buffer[start:])
                if sync_size + header_size + msg_length + crc_size <= len(self.rsp_byte_buffer) - start:
                    end = start + sync_size + header_size + msg_length + crc_size
                    if (end <= len(self.rsp_byte_buffer)):
                        packet = self.rsp_byte_buffer[start:end]
                        
                        if src == cmd_dest.BOOT_CMDS_DEST_V2X and self.dev_upload != None:
                                self.dev_upload.receive_write_ack_packet(packet)
                                sc_done = self.dev_upload.finished

                        #remove processed piece from buffer:
                        self.rsp_byte_buffer = self.rsp_byte_buffer[end:] + read_non_block(self.sp) #Read all available data- may not be this much. May be none 
                        start = self.rsp_byte_buffer.find(struct.pack('<H', BOOTLOADER_SYNC)) 
                        if debug_level == DEBUG_TRAFFIC_LOG:
                            self.traffic_log.overall_rsp_buffer_size.append((time.time(), len(self.rsp_byte_buffer), "rcvd2"))
                            self.traffic_log.write()

                    else:
                        break
                else:
                    break

            if not (sc_done):
                self.dev_upload.send_next_write_msg(self.sp, use_LIN = self.use_LIN)
                sc_done = self.dev_upload.finished

            self.percent_done = self.dev_upload.window_sent/(1.0*self.dev_upload.total_bytes) * 100
            if ((self.percent_done - self.last_printed) > 1 or sc_done):
                stdout.write("\r     Progress: %02d%%  " % round(self.percent_done))
                stdout.flush()
                self.last_printed = self.percent_done

        #Flush read buffer of extra commands coming back:
        self.sp.flushOutput()
        self.sp.flushInput()
        #Send End transfer commands
        self.complete = self.exit_bootloaders(self.sp)

        try:
            if (opt.use_bluetooth): #Reset Bluetooth back to orginal app settings if necessary, then close serial ports:
                self.sp.close();
                self.sp = config_bluetooth_serial_port(opt.app_baud_rate, BLUETOOTH_TIMEOUT_SECONDS, remote_address=opt.remote_bt_mac)
                if self.sp != None:
                    self.sp.close()
                    time.sleep(2.0)
            else:
                self.sp.close()
        except:
            pass #Catch closed serial port exception.

        return

    def stop_bootload(self, opt):
        """Interrupt_bootload allows a bootload process to be killed from the outside while running """  
        self.killed = True
        if self.sp != None:
            self.sp.close()
        time.sleep(1)
        
        if (opt.use_bluetooth):
            self.sp = config_bluetooth_serial_port(opt.app_baud_rate, BLUETOOTH_TIMEOUT_SECONDS, remote_address=opt.remote_bt_mac)
            if self.sp != None:
                self.sp.close()
                time.sleep(2.0) 

def scan():
        """scan for available ports. return a list of tuples (num, name)"""
        available = []
        for i in range(256):
            try:
                s = serial.Serial(i)
                available.append((i, s.portstr))
                s.close()   # explicit close 'cause of delayed GC in java
            except serial.SerialException:
                pass
        return available

def test(opt, sp):
    global debug_level
    debug_level = 5
    packet = None
    rsp_byte_buffer = ''

    global sequence_byte
    global header_encoding, header_size, sync, sync_size, crc_size
    sequence_byte = 0 & 0xFF

    payload_len = 2
    payload = sequence_byte
    while 1 :
        valid = True
        number2 = -1
        number3 = -1
        number4 = -1
        number = raw_input("Type command type to send and press 'Enter':")
        try:    
            number = int(number) 
        except:
            valid = False
            print("Invalid command type. Must be integer. Press Ctrl-C to Quit.")
        if (number < 0 or number >= cmds.NUM_BOOT_CMDS_TYPE):
            print("Invalid command type. Must be between 0 and 13")
            valid = False
            pass
        #Pick a payload:
        if (number == cmds.BOOT_CMDS_TYPE_SEND_HEADER):
            payload_fmt = payload_enc.SEND_HEADER 
            number3 = raw_input("Type header code to send and press 'Enter':")
            try:    
                number3 = int(number3) 
            except:
                try:
                    number3 = int(number3, 16)
                except:     
                    valid = False
                    print("Invalid address. Must be integer. Press Ctrl-C to Quit.")
        elif (number == cmds.BOOT_CMDS_TYPE_HEADER_DATA):
            payload_fmt = payload_enc.HEADER_DATA
        elif (number == cmds.BOOT_CMDS_TYPE_SEND_KEY):
            payload_fmt = payload_enc.SEND_KEY
        elif (number == cmds.BOOT_CMDS_TYPE_KEY_DATA):
            payload_fmt = payload_enc.KEY_DATA
        elif (number == cmds.BOOT_CMDS_TYPE_UNLOCK_MEM):
            payload_fmt = payload_enc.UNLOCK_MEM
        elif (number == cmds.BOOT_CMDS_TYPE_MEM_UNLOCKED ):
            payload_fmt = payload_enc.MEM_UNLOCKED
        elif (number ==  cmds.BOOT_CMDS_TYPE_PROG_MEM_DATA):
            payload_fmt = payload_enc.PROG_MEM_DATA
        elif (number == cmds.BOOT_CMDS_TYPE_SEND_PROG_MEM):
            number2 = raw_input("Type base address to write to and press 'Enter':")
            try:    
                number2 = int(number2) 
            except:
                try:
                    number2 = int(number2, 16)
                except: 
                    valid = False    
                    print("Invalid address. Must be integer. Press Ctrl-C to Quit.")
            
            payload_fmt = payload_enc.SEND_PROG_MEM     
        elif (number == cmds.BOOT_CMDS_TYPE_WRITE_PROG_MEM):
            number2 = raw_input("Type base address to write to and press 'Enter':")
            try:    
                number2 = int(number2) 
            except:
                try:
                    number2 = int(number2, 16)
                except: 
                    valid = False    
                    print("Invalid address. Must be integer. Press Ctrl-C to Quit.")
            
            payload_fmt = payload_enc.WRITE_PROG_MEM
        elif (number == cmds.BOOT_CMDS_TYPE_PROG_MEM_WRITTEN):
            payload_fmt = payload_enc.PROG_MEM_WRITTEN
        elif (number == cmds.BOOT_CMDS_TYPE_END_TRANSFER):
            payload_fmt = payload_enc.END_TRANSFER
        elif (number == cmds.BOOT_CMDS_TYPE_TRANSFER_ENDED):
            payload_fmt = payload_enc.TRANSFER_ENDED
        elif (number == cmds.BOOT_CMDS_TYPE_ERROR_CODE):
            payload_fmt = payload_enc.ERROR_CODE
         
        number4 = raw_input("Type destination for command and press 'Enter' (FUP:0,SC:1,MC:2,IMU:3):")
        try:    
            number4 = int(number4) 
        except:
                try:
                    number4 = int(number4, 16)
                except: 
                    valid = False    
                    print("Invalid address. Must be integer. Press Ctrl-C to Quit.")        
               
        if (valid):    
            #Send packet:
            if (debug_level > 0):  print'WARNING: Sending command with state controller as dest and FUP as source.'
            payload_len = struct.calcsize('<'+payload_fmt)
            payload = [sequence_byte] * len(payload_fmt)
            if (number2 != -1):
                payload[0] = number2
            if (number3 != -1):
                payload[0] = number3
            if (debug_level > 0):  print payload_len, payload_fmt
            packed = create_message(number4, cmd_dest.BOOT_CMDS_DEST_FUP, number, sequence_byte, payload_len,payload,payload_fmt)
                    
            if (send_message(sp, packed)):
                if (debug_level > 0):  print "Sent packet:",
                if (debug_level > 0):  print ' '.join(['%02X' % ord(x) for x in packed])
                sequence_byte = (sequence_byte + 1) & 0xFF
            else:
                if (debug_level > 0):  print "Error sending packet."
                
            time.sleep(.5)    
            rsp_byte_buffer = ''.join([rsp_byte_buffer, read_non_block(sp)]) #Read all available data- may not be this much. May be none 
            
            if (debug_level > 0):  print "response buffer:", ' '.join(['%02X' % ord(x) for x in rsp_byte_buffer])
            
            start = rsp_byte_buffer.find(struct.pack('<H', BOOTLOADER_SYNC)) 
            while (start != -1):
                #See if we can unpack length from what we have based on knowledge of header size
                sync, dest, src, msg_type, rec_sequence_byte, msg_length = get_packet_header(rsp_byte_buffer[start:])
                if sync_size + header_size + msg_length + crc_size <= len(rsp_byte_buffer) - start:
                    end = start + sync_size + header_size + msg_length + crc_size
                    packet = rsp_byte_buffer[start:end]
                    if (debug_level > 2):  print start,sync_size,header_size,msg_length,crc_size, start, end, len(rsp_byte_buffer), ' '.join(['%02X' % ord(x) for x in packet])
                    
                    try:
                        sync, dest, src, msg_type, sequence_byte, msg_length, addr, payload = receive_packet(packet)
                        if (debug_level > 0):  print "Received packet:","sync:",sync, " dest:", dest, " src:", src," type:", msg_type, " seq:",rec_sequence_byte
                        if (debug_level > 0):  print "Payload:", ' '.join(['%02X' % ord(x) for x in payload])
                    except ValidPacketException as e: 
                        if (debug_level > 1): print e.value
                        
                    rsp_byte_buffer = ''.join([rsp_byte_buffer[end:], read_non_block(sp)])                    
                    start = rsp_byte_buffer.find(struct.pack('<H', BOOTLOADER_SYNC)) 
                else: #Waiting on more data. Break
                    break

def read_non_block(sp): # Optimized greedy read for our packet system- allow 1 skip if a byte isn't ready
    a = sp.read(sp.inWaiting())
    
    if (len(a) == 0):
        a  = ''
    else:
        pass
        #print ['%02X' % ord(x) for x in a],        
#     a = ''
#     skip = 0
#     while (skip < 2):
#         x = sp.read()
#         if (len(x) == 0):
#             skip = skip + 1
#             
#         else:
#             skip = 0
#             a = ''.join( [a, x]) #Read all available data- may not be this much. May be none 
    
    if (debug_level > 0):
        print a
    return a    
            
    
def test_bluetooth_unittest():
 
    print sys.argv
    parser =  bootload_parser_options(sys.argv); 
    opt = parser.values;
    
    # sp = serial.Serial(port=28, baudrate=115200, timeout=0.1, rtscts=0)
    sp = serial.Serial(port=28, baudrate=230400, timeout=0.1, rtscts=0)
    sp.setRTS(1)
    dev_conn = pfcmd.PFCmd (0,0) 
    dev_conn.USE_SERIAL_PORT = 1;
    dev_conn.comm_device = sp # config_bluetooth_serial_port(115200, BLUETOOTH_TIMEOUT_SECONDS, remote_address="00:A0:96:2A:8B:30") 
    
    robo_update.robo_update(dev_conn, "walking_state", "enable_imu", 0)      #Use robo update to enable and disable IMU before trying to use the bootloader. 
    
    print dev_conn.special(BOOTLOAD_SPECIAL_CMD, values=[BOOTLOAD_CMD_OPT], secs=0.5)
    dev_conn.comm_device.close()
    
    btSerialPort = serial.Serial(port=28, baudrate=230400, timeout=0.1, rtscts=0)  #config_bluetooth_serial_port(230400, BLUETOOTH_TIMEOUT_SECONDS, remote_address="00:A0:96:2A:8B:30") 
    btSerialPort.setRTS(1)
    test(opt, btSerialPort)
    sys.exit(-1)
    
    
def test_speed(opt):
    #Figure out where slow point is: is it python or chip?
    global debug_level
    
    sp = None
    packet = None
    rsp_byte_buffer = ''  
    global sequence_byte
    global header_encoding, header_size, sync, sync_size, crc_size
    sequence_byte = 0 & 0xFF
            
    if (opt.com_port and type(opt.baud_rate) == int) :
        try :
            sp = serial.Serial(port=opt.com_port - 1, baudrate=opt.baud_rate, timeout=0, rtscts=0)
            sp.setRTS(1);# to enable power on BR
        except :
            if (debug_level > 0):  print"Port Unavailable. Available ports:"
            for n,s in scan():
                if (debug_level > 0):  print"(%d) %s" % (n,s)
            val_str = raw_input("Choose one : ");
            try :
                opt.com_port = int(val_str);
                # sp = SyncReadSerial(COM_PORT-1, BAUD_RATE);
                sp = serial.Serial(port=opt.com_port - 1, baudrate=opt.baud_rate, timeout=0, rtscts=0)
                sp.setRTS(1);# to enable power on BR
            except :
                raise ConnectionFailureException("Error opening COM PORT"+str(opt.com_port))
    else:
        raise ConnectionFailureException("Must Specifiy a COMM port or IP address.")
    
    payload_len = 2
    payload = sequence_byte
    while 1 :
        number2 = -1
        number3 = -1
        number = raw_input("Type command type to send and press 'Enter':")
        try:    
            number = int(number) 
        except:
            print("Invalid command type. Must be integer. Press Ctrl-C to Quit.")
        if (number < 0 or number >= cmds.NUM_BOOT_CMDS_TYPE):
            print("Invalid command type. Must be between 0 and 13")
            pass
        #Pick a payload:
        if (number == cmds.BOOT_CMDS_TYPE_SEND_HEADER):
            payload_fmt = payload_enc.SEND_HEADER 
            number3 = raw_input("Type header code to send and press 'Enter':")
            try:    
                number3 = int(number3) 
            except:
                try:
                    number3 = int(number3, 16)
                except:     
                    print("Invalid address. Must be integer. Press Ctrl-C to Quit.")
        elif (number == cmds.BOOT_CMDS_TYPE_HEADER_DATA):
            payload_fmt = payload_enc.HEADER_DATA
        elif (number == cmds.BOOT_CMDS_TYPE_SEND_KEY):
            payload_fmt = payload_enc.SEND_KEY
        elif (number == cmds.BOOT_CMDS_TYPE_KEY_DATA):
            payload_fmt = payload_enc.KEY_DATA
        elif (number == cmds.BOOT_CMDS_TYPE_UNLOCK_MEM):
            payload_fmt = payload_enc.UNLOCK_MEM
        elif (number == cmds.BOOT_CMDS_TYPE_MEM_UNLOCKED ):
            payload_fmt = payload_enc.MEM_UNLOCKED
        elif (number ==  cmds.BOOT_CMDS_TYPE_PROG_MEM_DATA):
            payload_fmt = payload_enc.PROG_MEM_DATA
        elif (number == cmds.BOOT_CMDS_TYPE_SEND_PROG_MEM):
            number2 = raw_input("Type base address to write to and press 'Enter':")
            try:    
                number2 = int(number2) 
            except:
                try:
                    number2 = int(number2, 16)
                except:     
                    print("Invalid address. Must be integer. Press Ctrl-C to Quit.")
            
            payload_fmt = payload_enc.SEND_PROG_MEM     
        elif (number == cmds.BOOT_CMDS_TYPE_WRITE_PROG_MEM):
            number2 = raw_input("Type base address to write to and press 'Enter':")
            try:    
                number2 = int(number2) 
            except:
                try:
                    number2 = int(number2, 16)
                except:     
                    print("Invalid address. Must be integer. Press Ctrl-C to Quit.")
            
            payload_fmt = payload_enc.WRITE_PROG_MEM
        elif (number == cmds.BOOT_CMDS_TYPE_PROG_MEM_WRITTEN):
            payload_fmt = payload_enc.PROG_MEM_WRITTEN
        elif (number == cmds.BOOT_CMDS_TYPE_END_TRANSFER):
            payload_fmt = payload_enc.END_TRANSFER
        elif (number == cmds.BOOT_CMDS_TYPE_TRANSFER_ENDED):
            payload_fmt = payload_enc.TRANSFER_ENDED
        elif (number == cmds.BOOT_CMDS_TYPE_ERROR_CODE):
            payload_fmt = payload_enc.ERROR_CODE
        
        number4 = raw_input("Type in number of commands to send grouped and press 'Enter':")
        try:    
                number4 = int(number4) 
        except:
                try:
                    number4 = int(number4, 16)
                except:     
                    print("Invalid number of commands. Must be integer. Press Ctrl-C to Quit.")        

        #Send packet:
        if (debug_level > 0):  print'WARNING: Sending command with state controller as dest and FUP as source.'
        payload_len = struct.calcsize('<'+payload_fmt)
        
        
        group_packed = ''
        for y in range(0, number4):
            payload = [sequence_byte] * len(payload_fmt)
            if (number2 != -1):
                payload[0] = number2 + (y * payload_len - 4)
            if (number3 != -1):
                payload[0] = number3 + (y * payload_len - 4)
            if (debug_level > 0):  print payload_len, payload_fmt, payload

            packed = create_message(cmd_dest.BOOT_CMDS_DEST_V2X, cmd_dest.BOOT_CMDS_DEST_FUP, number, sequence_byte, payload_len,payload,payload_fmt)
            sequence_byte = (sequence_byte + 1) & 0xFF
            group_packed = group_packed + packed
        
        start_send = time.clock()        
        if (send_message(sp, group_packed)):
                start_send = time.clock()        
                print "Send took ",time.clock() - start_send, " sec"  
                if (debug_level > 0):
                    print "Sent packet(s):"
                    print ' '.join(['%02X' % ord(x) for x in group_packed])
                sequence_byte = (sequence_byte + 1) & 0xFF
        else:
                if (debug_level > 0):  print "Error sending packet:"

        rsp_byte_buffer = rsp_byte_buffer + read_non_block(sp) #512 bytes is at least 2 messages worth of data.
        if (debug_level > 0):  print "response buffer:", ' '.join(['%02X' % ord(x) for x in rsp_byte_buffer])
        start = rsp_byte_buffer.find(struct.pack('<H', BOOTLOADER_SYNC)) 
        rec = 0
        while (start != -1):
            #See if we can unpack length from what we have based on knowledge of header size
            sync, dest, src, msg_type, sequence_byte, msg_length = get_packet_header(rsp_byte_buffer[start:])
            if sync_size + header_size + msg_length + crc_size <= len(rsp_byte_buffer) - start:
                end = start + sync_size + header_size + msg_length + crc_size
                packet = rsp_byte_buffer[start:end]
                if (debug_level > 0):  print start,sync_size,header_size,msg_length,crc_size, start, end, len(rsp_byte_buffer), ' '.join(['%02X' % ord(x) for x in packet])
                sync, dest, src, msg_type, rec_sequence_byte, msg_length, addr, payload = receive_packet(packet)
                rec = rec + 1
                if (debug_level > 0):
                    print "Received packet:","sync:",sync, " dest:", dest, " src:", src," type:", msg_type, " seq:",rec_sequence_byte
                    print "Payload:", payload
                        
                rsp_byte_buffer = rsp_byte_buffer[end:] + read_non_block(sp) #256 bytes is at least 1 messages worth of data.
                start = rsp_byte_buffer.find(struct.pack('<H', BOOTLOADER_SYNC)) 
        print "Send + receive pkts took ",time.clock() - start_send, " sec"  
        print 'got ', 100*rec/(number4*1.0), '% of packets' 


if __name__ == "__main__":    

    #test_bluetooth_unittest();
    print "  "
    if debug_level > 0:
        print sys.argv
    
    parser =  bootload_parser_options(sys.argv); 
    opt = parser.values;

    if debug_level > 0:
        print '---opt: '+str(opt)
    
    os.system('cls')
    bp = BootloadProcess(opt)

    start_xfer = time.clock()
    if (opt.hex_filename):
        bp.start_uploading(opt)

    print
    print " Elapsed Time: %0.1f sec" % time.clock()
    print "Transfer Time: %0.1f sec" % (time.clock() - start_xfer)
    print 
    if (bp.complete):
        print "Upload Complete!"
    else:
        print "Upload complete but did not exit bootloader mode properly."
