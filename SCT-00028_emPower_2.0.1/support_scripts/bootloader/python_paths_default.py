import sys, os

#if sys.path[0] == 'c:/' :
#    print 'removing %s from path list' % sys.path[0]
#    sys.path.pop(0)





def output_instructions() :
    print """
    
Exception trying to load python_paths.py
    
Create the file 'python_paths.py' within the project
directory 'support_scripts'.  The file should contain:

############# CUT BELOW THIS LINE  ############# 

import os

# attempt to automatically determinate the root location
cwd = os.getcwd()
(head, tail) = os.path.split(cwd)
FIRMWARE_BASE_PATH = head;

# specifiy the location of the project root directory:
# FIRMWARE_BASE_PATH = 'C:/src/this_project_root_directory';

# specify the location of critical files in the project
EEPROM_KEYS_RELATIVE_FNAME = 'StateController/eeprom_keys.h';
IOCHAN_RELATIVE_FNAME = 'StateController/Infrastructure/io_chan-rev2x0.h';
SC_GLOBAL_H_RELATIVE_FNAME = 'StateController/Comm/Config/global.h''

############# CUT ABOVE THIS LINE  ############# 
    """
    sys.exit()
    



try :
    import python_paths
except :
    output_instructions();
    sys.exit(-1);


# add the relative paths to python library
sys.path.insert(0, python_paths.SUPPORT_SCRIPTS_BASE)

if 0 : 
    try :
        if len(python_paths.FIRMWARE_BASE_PATH) < 1 :
            raise()
        if len(python_paths.EEPROM_KEYS_RELATIVE_FNAME) < 1 :
            raise()
        if len(python_paths.IOCHAN_RELATIVE_FNAME) < 1 :
            raise()
        if len(python_paths.SC_GLOBAL_H_RELATIVE_FNAME) < 1 : 
            raise()    
    except :
        print "INVALID value for FIRMWARE_BASE_PATH "
        print "Edit your local python_paths.py file"
        print "Command : ", sys.argv[0]
        output_instructions();
        sys.exit(-2);
