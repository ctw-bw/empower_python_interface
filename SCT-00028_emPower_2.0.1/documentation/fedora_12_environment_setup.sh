#!/bin/bash

# Get our tools:
#svn co http://svn/svn/svn_engineering/PowerFoot/trunk/tools/Tango_AutoSupportSetup

cd Tango_AutoSupportSetup/fedora

# Install Python 2.7.4  (unicode option is required to use Python2.7 with our python Boost libraries.) (Prefix option is so we do NOT confuse our location with native Python 2.6)
tar xf Python-2.7.4.tar.bz2 
cd Python-2.7.4
./configure --prefix=/usr/local --enable-unicode=ucs4
make && make altinstall
cd ..

#Use get-pip script to get pip for python 2.7. We can use pip WITH VERSION numbers to install the dependencies we need, as well as to install SVN saved .whl archives of python libraries
cd ..
python2.7 get-pip.py
/usr/local/bin/pip2.7 install --upgrade pip
/usr/local/bin/pip2.7 install intelhex-2.0-py2.py3-none-any.whl


#Install some specialized libraries and versions from source files:

python2.7  -c "import zipfile; zf=zipfile.ZipFile('pycparser-2.13.zip'); zf.extractall();"
cd pycparser-2.13
python2.7  setup.py install
cd ..

python2.7  -c "import zipfile; zf=zipfile.ZipFile('ply-3.4.zip'); zf.extractall();"
cd ply-3.4
python2.7  setup.py install
cd ..

python2.7  -c "import zipfile; zf=zipfile.ZipFile('pycparserext-2013.zip'); zf.extractall();"
cd pycparserext-2013
python2.7 setup.py install
cd ..


#Install Fedora equivalent to Windows equivalent to following tools:
#wxPython2.8-win32-unicode-2.8.12.1-py27.exe scipy-0.11.0b1-win32-superpack-python2.7.exe 


cd fedora

#wxPython build instructions source: http://www.wxpython.org/BUILD.html
#Fedora dependencies:
yum install make automake gcc gcc-c++ kernel-devel gtk2-devel gtkglext-devel gstreamer-plugins-base-devel python-devel webkitgtk wxGTK-devel
tar -xvjf wxPython-src-3.0.2.0.tar.bz2
cd wxPython-src-3.0.2.0/wxPython
python2.7 build-wxpython.py --install --force_config
cd ..
#Apply a special patch that fixes a broken piece of wxPython 3.0.2:
# See http://trac.wxwidgets.org/ticket/16767#no1 for source of issue. 
cd ..
rm /usr/local/lib/python2.7/site-packages/wx-3.0-gtk2/wx/lib/plot.py
cp plot.py /usr/local/lib/python2.7/site-packages/wx-3.0-gtk2/wx/lib/plot.py
cd fedora


#Add local python2.7 library locations for shared objects to LD_LIBRARY config file:
echo '# added by fedora_12_environment_setup.sh to allow python2.7 to access wxPython install.' >> /etc/ld.so.conf
echo '/usr/local/lib/' >> /etc/ld.so.conf
ldconfig


#Boost and Boost-Python and Cython needed before we work with JR3 and Comedi
tar -xvzf Cython-0.23.4.tar.gz
cd Cython-0.23.4
python2.7 setup.py install
cd ..


tar -xvzf numpy-1.10.4.tar.gz 
cd numpy-1.10.4
python2.7 setup.py install
cd ..

#SciPy requires the native BLAS package. 
yum install blas lapack blas-devel lapack-devel
tar -xvzf scipy-0.17.0.tar.gz 
cd scipy-0.17.0
python2.7 setup.py install
cd ..

#Add matplot lib (which relies on having at least 1 backend)
tar -xvzf matplotlib-1.5.1.tar.gz
cd matplotlib-1.5.1
#Need the "configure" option so that we pick up the right Wx and Cython versions for our back end
python2.7 setup.py configure install 
cd ..


#Install the drivers needed to work with the JR3 (ALSO installs pyserial2.6, which we need)
./install_jr3_support.sh

#Reload the libraries we've just installed:
ldconfig 

#Install copley amplifier support 
./install_copley_amplifier_support.sh

#install support for phidgets relay:
cd PhidgetsPython
python2.7 setup.py install
cd ..

#back to top level directory
cd ..


