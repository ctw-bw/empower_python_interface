::Environment and support library setup for Tango Build and Test Scripts
::Configuration of stuff to install:
echo off
set python_executable_installers=wxPython3.0-win32-3.0.2.0-py27.exe pyserial-2.5.win32.exe scipy-0.11.0b1-win32-superpack-python2.7.exe intelhex-1.4.win32.exe PyBluez-0.19.beta.win32-py2.7.exe
set python_archived_libraries=pycparser-2.11.alpha pycparserext-2013 ply-3.4
set python_archived_wheel_files=numpy-1.10.4+mkl-cp27-none-win_amd64.whl numpy-1.10.4+mkl-cp27-none-win32.whl

::Start here
echo Getting necessary tools from SVN... 
svn co http://svn/svn/svn_engineering/PowerFoot/trunk/tools/Tango_AutoSupportSetup
echo Auto-Installing Python 2.7 and the needed support libraries...
cd Tango_AutoSupportSetup

::Add Python27 to the user's PATH if it isn't there:
if exist path_check.tmp (del path_check.tmp)
echo ;%PATH%; | find /C /I ";C:\Python27;">>path_check.tmp
set /p HAS_PYTHON_IN_PATH=<path_check.tmp
::Save new path for future:
IF %HAS_PYTHON_IN_PATH% NEQ 1 (setx PATH "%PATH%;C:\Python27;")
::Also update the local copy of PATH that the rest of this script is going to use so we don't have to restart cmd line. 
IF %HAS_PYTHON_IN_PATH% NEQ 1 (set PATH="%PATH%;C:\Python27;")
del path_check.tmp


::If we don't see Python where it's supposed to be, install it: 
echo Checking for Python 2.7 Installation:
C:\Python27\python.exe --version
if %ERRORLEVEL% neq 0 (
    msiexec.exe /i python-2.7.msi /QN /L*V "python27_install_attempt.log"
)
echo Validate that path to newly installed python exists... 
C:\Python27\python.exe --version 1>python_version_check_stdout.log 2>python_version_check.log
if %ERRORLEVEL% neq 0 (
        echo Python 2.7 was NOT installed. See python27_install_attempt.log for output. 
        exit /b %ERRORLEVEL%   
)

set /p PYTHON_VERSION=<python_version_check.log
echo %PYTHON_VERSION%
if NOT "%PYTHON_VERSION%"=="Python 2.7" (exit /b)


::Install all Python libraries that come with exe or msi installer files. 
For %%X in (%python_executable_installers%) do (
    %%X
    if %ERRORLEVEL% neq 0 (
        echo %%X was NOT installed.
        exit /b %ERRORLEVEL%   
    )  
)    

::Use Python itself to install the libraries that DON'T come with executable installers
For %%X in (%python_archived_libraries%) do (
    C:\Python27\python.exe -c "import zipfile; zf=zipfile.ZipFile('%%X.zip'); zf.extractall();"
    if %ERRORLEVEL% neq 0 (
        echo %%X was NOT installed.
        exit /b %ERRORLEVEL%   
    )
    cd %%X
    C:\Python27\python.exe setup.py install
    cd ..
)    

:: Deal with the face that wxPython release 3.0.2 executable for Windows has an annoying bug in it that we have to patch: 
move /Y plot.py C:\Python27\Lib\site-packages\wx-3.0-msw\wx\plot.py


::Install pip and the stuff that depends on it:
python get-pip.py

C:\Python27\Scripts\pip.exe install numpy-1.10.4+mkl-cp27-none-win32.whl
C:\Python27\Scripts\pip.exe install matplotlib-1.5.1-cp27-none-win32.whl
C:\Python27\Scripts\pip.exe install sounddevice