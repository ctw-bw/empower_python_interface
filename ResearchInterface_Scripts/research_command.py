# -*- coding: utf-8 -*-
"""
Created on Thu Aug 22 11:28:46 2019
Based on do_command.py
@adapted by: BeltranUllauri
"""


#!/usr/bin/env python

import sys;
import comm_lib.pfcmd as pfcmd

#trans_tbl = ' '*32 + ''.join(  ['%c' % chr(ndx) for ndx in range(32, 256)] );
    

import argparse
import struct


######################
class c_struct :
    pass

#helper for options so we can send cmd line arguments as hex or binary if we want. 
def int_any_fmt(x):
   return int(x, 0)    
    
 

class do_command_options :
    
    def __init__(self, args) :
        
        self.args = args
        
# allow special command data to be saved to find with command line redirection.
        try:
            arg_end = args.index(">")
            args = args[:arg_end];
            print i
        except:
            pass

        parser = argparse.ArgumentParser();
        
        parser.add_argument("-p","-c", "--com_port", dest="com_port", type=int_any_fmt, help="COM PORT number for serial port connection");
        parser.add_argument("-b", "--baud_rate", dest="baudrate", type=int_any_fmt, default=115200, help="baurdrate when using serial port (default is 115200)");
        parser.add_argument("-t", "--timeout", dest="timeout", default=0.05, help="serial or TCP communcations timeout (sec)");        
        parser.add_argument("-s", "--special", dest="special_cmd", type=int_any_fmt, nargs="+",action='append', metavar='SPECIAL_CMD', default=[], help = "-s N [arg1, ... ] issue special command N with arguments.  Args may be negative, and multiple commands may be issued by repeating the -s")
        parser.add_argument("-S", "--struct_string", dest="struct_string", type=str, help="use struct.unpack(STRUT_STRING, data) to process the result of the special command.  Eg -S =10L");
        parser.add_argument("-D", "--delay", dest="repeat_delay", type=int_any_fmt, default=30, help = "special command repeat delay in milliseconds");
        parser.add_argument("-F", "--float_mode", dest="float_output_mode", action="store_true", default=False, help="interpret the data from the special command as floating point numbers");
        parser.add_argument("-X", "--hex_mode", dest="hex_output_mode", action="store_true", default=False, help="display the special command results in hex");
        
        args = parser.parse_args()
        
        # print options
        # print args
        
        # make a struct from the options dictionary
        opt = c_struct();
        for k,v in args.__dict__.iteritems() :
            opt.__dict__[k] = v
            #print k,v
            self.opt = opt;
        
        return  


                
if __name__ == "__main__":    
    
    parser = do_command_options(sys.argv)
    opt = parser.opt;
   
    ctrl = pfcmd.controller(None)
    pf = pfcmd.PFCmd();

    if opt.com_port :
        print "SERIAL"
        pf.connect(opt.com_port, opt.baudrate);
        pf.set_serial_timeout(opt.timeout);

    else:
        print "you must specify a COMPORT."
        sys.exit()
    
    ctrl = pfcmd.controller(pf)
    
    if 0 and opt.motor_test : 
        mt = class_motor_test.motor_test(ctrl);
        if (  opt.motor_test.find('V')>=0) :
               mt.do_assending_voltage_test(cycles=10, period = 1.0, start=1.0, stop=5.0, step=0.5);
    
        if (  opt.motor_test.find('I')>=0) :
               mt.do_assending_current_test(cycles=10, period = 1.0, start=0.5, stop=5.0, step=0.5);
    
        if (  opt.motor_test.find('X')>=0) :
               mt.do_assending_current_test(cycles=10, period = 0.05, start=10.0, stop=15.0, step=0.5);
               mt.do_assending_current_test(cycles=10, period = 0.02, start=15.0, stop=25.0, step=1.0);
    
        
    
    if opt.special_cmd :
        num_special_commands = len(opt.special_cmd)
        
   
        pf.quiet = True;

            
        for cmd_num in range(0, num_special_commands) :
            cmd = opt.special_cmd[cmd_num]
            
            result = ctrl.special(cmd[0], cmd[1:], secs=0.001 * opt.repeat_delay);

            if not result  :
                continue

            if opt.struct_string :
                # print result.keys()
                # print len(result['rsp_string'])            
                lst = struct.unpack(  opt.struct_string, result['rsp_string'][8:])
                print lst
                if opt.log_fout :
                    opt.log_fout.write(lst + '\n'); 
            elif opt.float_output_mode:
                lst = struct.unpack( "=10f", result['rsp_string'][8:])
                print lst
                if opt.log_fout :
                    opt.log_fout.write(lst + '\n'); 
            else :
                try :
                    field_list = result['rsp_fields'];

                    if iter < 1 and cmd_num < 1:
                        header_str = ' '.join( ["%-8s " % field for field in field_list] );
                        print header_str
                        if opt.log_fout :
                            opt.log_fout.write(header_str + '\n');                        

                    if (opt.hex_output_mode) :
                        dat_str = ' '.join( ["$%-8x " % result[field] for field in field_list] );
                    else :
                        dat_str = ' '.join( ["%-8s " % result[field] for field in field_list] );
                    print dat_str


                except :
                    #print "NAK"
                    pass
    
    

        
    
    
    
