# -*- coding: utf-8 -*-
"""
Created on Thu Aug 22 11:28:46 2019
Based on do_command.py
@adapted by: BeltranUllauri
"""


#!/usr/bin/env python
import sys;
import comm_lib.pfcmd as pfcmd
import time
import argparse
import commands_empower as commands


quiet = False
def output(things):
    if not quiet:
        print things
        
#helper for options so we can send cmd line arguments as hex or binary if we want. 
def int_any_fmt(x):
   return int(x, 0)    


######################
class c_struct :
    pass
        
class CONTROL_MODES:
    CURRENT = 1
    VOLTAGE = 2
    
class ENDPOINT_MODES:    
    ANKLE_ANGLE = 1
    BALLNUT_POSITION = 2 

class do_command_options :
    
    def __init__(self, args) :
        
        self.args = args
        
        try:
            arg_end = args.index(">")
            args = args[:arg_end];
            print i
        except:
            pass

        parser = argparse.ArgumentParser();
        
        parser.add_argument("-p","-c", "--com_port", dest="com_port", type=int_any_fmt, help="COM PORT number for serial port connection");
        parser.add_argument("-b", "--baud_rate", dest="baudrate", type=int_any_fmt, default=115200, help="baurdrate when using serial port (default is 115200)");
        parser.add_argument("-t", "--timeout", dest="timeout", default=0.05, help="serial or TCP communcations timeout (sec)");        
        parser.add_argument("-s", "--special", dest="special_cmd", type=int_any_fmt, nargs="+",action='append', metavar='SPECIAL_CMD', default=[], help = "-s N [arg1, ... ] issue special command N with arguments.  Args may be negative, and multiple commands may be issued by repeating the -s")
        parser.add_argument("-S", "--struct_string", dest="struct_string", type=str, help="use struct.unpack(STRUT_STRING, data) to process the result of the special command.  Eg -S =10L");
        parser.add_argument("-D", "--delay", dest="repeat_delay", type=int_any_fmt, default=30, help = "special command repeat delay in milliseconds");
        parser.add_argument("-F", "--float_mode", dest="float_output_mode", action="store_true", default=False, help="interpret the data from the special command as floating point numbers");
        parser.add_argument("-X", "--hex_mode", dest="hex_output_mode", action="store_true", default=False, help="display the special command results in hex");
        
        '''for the control'''
        parser.add_argument("--start", dest="start", type=int_any_fmt, default=30, help="Starting encoder value for ANKLE exerciser");
        parser.add_argument("--end", dest="end", type=int_any_fmt, default=400, help="Ending encoder value for ANKLE exerciser");
        parser.add_argument("--encoder_choice", dest="encoder_choice", type=int_any_fmt, default=ENDPOINT_MODES.ANKLE_ANGLE, help="Control back and forth using Ankle encoder value (1) or Ball Nut Position (2). Defaults to Ankle Encoder.");
        parser.add_argument("--control_type", dest="control_mode", type=int_any_fmt, default=CONTROL_MODES.CURRENT, help="Control back and forth with Current (1) or Voltage (2). Defaults to Current.");
        
        parser.add_argument("-n", "--number_of_cycles", dest="cycles", type=int_any_fmt, default=10, help="Number of cycles to run. Defaults to 10.");
 
        
        args = parser.parse_args()
    
        # make a struct from the options dictionary
        opt = c_struct();
        for k,v in args.__dict__.iteritems() :
            opt.__dict__[k] = v
            self.opt = opt;
        
        return  


def back_and_forth(empower, control_mode, endpoint_mode, startpoint, endpoint,  input_volts=2.0, cycles = 1):
    cmd = commands.s_startup_run() 
    if (endpoint_mode is ENDPOINT_MODES.BALLNUT_POSITION):
        key = commands.startup_INC ()  # ballscrew position in encoder counts
    else:
        key = commands.startup_AAE ()  # AA raw - offset in encoder counts

        
    if control_mode is CONTROL_MODES.CURRENT:
        pos  =  empower.do_command('special', cmd).get(key)
        print pos
        posLast = pos
        sign = 1;
        
        posStuck = 0;
        output(empower.do_command('special',commands.s_motor_current_amp(1.0,0)))   
        
        mini_cycles = 1
        
        for input_amps in range(-50, 0, 1):
            output(input_amps)
            n_switch = 1
            while(n_switch < 2*mini_cycles):
                try: 
                    pos = empower.do_command('special', cmd).get(key)
                    if pos == posLast:
                        posStuck += 1
                    else: 
                        posStuck = 0;
                    
                    #if (pos < between1 or pos > between2)
                    if (posStuck > 5): #stuck or out of range. 
                        n_switch +=1 
                        sign = sign * -1
                        output(empower.do_command('special',commands.s_motor_current_amp(sign*input_amps/10.0,0)))
                    
                    posLast = pos
                    time.sleep(.05)
                except Exception as e:
                    print e
                    #set motor to not moving
                    output(empower.do_command('special',commands.s_motor_stop()))
                    break
        
        
        #set motor to not moving
        output(empower.do_command('special',commands.s_motor_stop()))
#        

                
if __name__ == "__main__":    
    
    parser = do_command_options(sys.argv)
    opt = parser.opt;
  
    ctrl = pfcmd.controller(None)
    empower_pf = pfcmd.PFCmd();

    if opt.com_port :
        print "SERIAL"
        empower_pf.connect(opt.com_port, opt.baudrate);
        empower_pf.set_serial_timeout(opt.timeout);

    else:
        print "you must specify a COMPORT."
        sys.exit()
    
#    prep_for_test(empower_pf);
    back_and_forth(empower_pf, opt.control_mode, opt.encoder_choice, opt.start, opt.end, cycles = opt.cycles)
    

    
    

        
    
    
    
