'''
RESEARCH INTERFACE
Commands for Empower
'''

'''Read sensors'''
def s_get_encoders (): return  [9009]
def ENC_INCREMENTAL (): return 'v0'
def ENC_ABSOLUTE  (): return 'v1'
def ENC_ANKLE_ANGLE (): return 'v2'

def s_get_IMU (): return [9210]
def IMU_acelX (): return 'v0'
def IMU_acelY (): return 'v1'
def IMU_acelZ (): return 'v2'
def IMU_gyroX (): return 'v3'
def IMU_gyroY (): return 'v4'
def IMU_gyroZ (): return 'v5'

def sf_torque_get(): return  [9200]
def TORQUE_AAE_FLOAT_RADIANS (): return  'v0'
TORQUE_AAE_FLOAT_RADIANS_IND = 1
def TORQUE_HARDSTOP_TORQUE_RATE_NMPERSEC (): return  'v1'
def TORQUE_HARDSTOP_TORQUE_NM (): return  'v2'
def TORQUE_SERIES_TORQUE_RATE_NMPERSEC (): return  'v4'
def TORQUE_SERIES_TORQUE_NM (): return  'v3'
def TORQUE_TOTAL_ANKLE_TORQUE_NM (): return  'v5'
def TORQUE_SERIES_DEFLECTION_RAD (): return 'v6'

'''Run levels
    RUN_LEVEL_VIRGIN,                       /*!< 0  Only basic hardware & communication initialized; appropriate for initially bring up the board. */
    RUN_LEVEL_POWER_UP_ERROR = 1,           /*!< 1  Run level is set here when there is a power-up issue.*/
    RUN_LEVEL_MOTOR_OPERATION = 10,         /*!< 10 Motor Control system is started, but left in a non-commanded state. */
    RUN_LEVEL_ANKLE_OPERATION = 20,         /*!< 20 Walking task started, Motor State Machine running, but Walking State Machine is held idle. */
    RUN_LEVEL_SPRING_PULL = 25,             /*!< 25 Walking task started, WSM held idle and Motor State Machine commanded to spring-pulls. */
    RUN_LEVEL_TRAJECTORY_TEST = 26,         /*!< 26 Walking task started, WSM held idle and Motor State Machine commanded to trajectory test. */
    RUN_LEVEL_WALKER_OPERATION = 70,        /*!< 70 Walking State Machine is allowed to run. */
'''
def s_runlevel_set_virgin (): return  [9001, 0]
def s_runlevel_set_motor (): return  [9001, 10]
def s_runlevel_set_ankle (): return  [9001, 20]
def s_runlevel_set_walk (): return  [9001, 70]
def s_runlevel_get (): return  [9002]
def s_set_system_idle (): return  [9003]

'''startup - read encoder data'''
def s_startup_run():return [9010]
def startup_AAE (): return'v0' #zeroed ankle angle in encoder counts (raw - offset)
def startup_ABS (): return'v1' #motor absolute encoder counts
def startup_INC (): return'v2' #calculated motor position (ball-screw position)
def startup_dif (): return'v3' #revolutions (x1000)

'''motor commands'''
def s_motor_stop (): return  [9100]      # TOCHECK
def s_motor_phase(qvd):return [9102, qvd]
def s_motor_voltage(qVq, qVd): return [9106, -1*qVq, qVd]
def s_motor_current_amp(iQ_amps, iD_amps):return [9103, -1*iQ_amps*1000, iD_amps*1000]#
def s_motor_current_milliamp(iQ_milliamps, iD_milliamps):return [9103, -1*iQ_milliamps, iD_milliamps]#
def s_motor_open_leads (): return  [9110]








