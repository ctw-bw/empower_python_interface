'''
RESEARCH INTERFACE
Commands for Biom
'''


'''Read sensors'''
def s_get_IMU (): return [2010]
def IMU_gyroX (): return 'v0'
def IMU_gyroY (): return 'v1'
def IMU_gyroZ (): return 'v2'
def IMU_acelX (): return 'v3'
def IMU_acelY (): return 'v4'
def IMU_acelZ (): return 'v5'

def sf_torque_get(): return  [104]
def TORQUE_TOTAL_ANKLE_TORQUE_NM (): return  'v8'


'''startup - read encoder data'''
def s_startup_run():return [1350]
def startup_AAE (): return'v9' # zeroed ankle angle in encoder counts (raw - offset)
def startup_INC (): return'v8' # ballscrew encoder counts


'''motor commands'''
def s_motor_stop (): return  [1109, 0]      # motor mode set to STOP_MODE (leads shorted)
def s_motor_phase(qvd):return [9102, qvd]
def s_motor_voltage(qVq, qVd): return [1114, -1*qVq, qVd]
def s_motor_open_leads (): return  [1109, 10] # motor mode set to STOP_MODE (open leads)
def s_motor_current(Iq): return[1115, Iq, 0, 3] # motor mode 3 is for current control
#








