class ByteStream(str) :
    # http://docs.python.org/reference/datamodel.html#additional-methods-for-emulation-of-sequence-types
    # http://stackoverflow.com/questions/835469/extending-pythons-builtin-str
    def __new__(cls, string) :
        ob = super(ByteStream, cls).__new__(cls, string)

        return ob

    def __init__(self,obj):
        self.WRAP_LEN = 32
        self.print_addr = False;
        self.base_addr = 0;



    def __call__(self, string) :
        # bytestream(str)
        # CB - not really sure if this is correct, but it seems to work
        ob = ByteStream.__new__(ByteStream, string)
        return ob

    def __str__ (self) :
        str = super(ByteStream, self).__getslice__(0, 299999)
        
        if str :

            str_list = []
                
            for ndx in range(0, len(str),self.WRAP_LEN) :
                
                if self.print_addr :
                    str_list.append ( "$%4.4X : " % (self.base_addr+ndx) )

                lst = ["%2.2x" % ord(ch) for ch in str[ndx: ndx+self.WRAP_LEN] ]
                str_list.append(  ' '.join(lst) )

                str_list.append('\r\n')


                
            return ''.join(str_list)

        else :
            return "EMPTY ByteStream"        

    def __repr__ (self) :
        self.__str__()




