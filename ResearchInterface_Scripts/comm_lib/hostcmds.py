class hostcmds :
    
    def __init__(self) :

        HEADER_FMT = "<BBBB";
        HEADER_FIELDS = ["crc","unit","cmd","seq"];
    

        ECHO_SEND_FIELDS = ["text"];
        ECHO_SEND_FMT = "<50s";
        ECHO_RSP_FIELDS = ["text"];
        ECHO_RSP_FMT = "<50s";
        
        VERSION_SEND_FIELDS = []
        VERSION_SEND_FMT = "";
        VERSION_RSP_FIELDS = ["other","fw_rev_and_CRC","fw_build_date","svn_revision","sn"];
        VERSION_RSP_FMT = "<20s 20s 20s h i ";

        SPECIAL_FIELDS = "cmd","v0", "v1", "v2", "v3", "v4", "v5", "v6", "v7", "v8", "v9"
        SPECIAL_DEFAULT = 0,  0,0,0,0,0,   0,0,0,0,0
        SPECIAL_FMT = "<11l";
        
        SPECIAL_L_FIELDS = "cmd","v0", "v1", "v2", "v3", "v4", "v5", "v6", "v7", "v8", "v9", "b0", "b1", "b2", "b3", "b4", "b5", "b6", "b7", "b8", "b9"
        SPECIAL_L_DEFAULT = 0,  0,0,0,0,0,   0,0,0,0,0,       0,0,0,0,0,   0,0,0,0,0;
        SPECIAL_L_FMT = "<21l";

        DIG_SEND_FIELDS = "channel",;
        DIG_SEND_FMT = "<h"
        DIG_OUT_SEND_FIELDS = "channel","value";
        DIG_OUT_SEND_FMT = "<hb"        
        DIG_RSP_FIELDS = "channel","value";
        DIG_RSP_FMT = "<hb"
        
        AN_OUT_SEND_FIELDS = "channel","value";
        AN_OUT_SEND_FMT = "<hh" 

        AN_SEND_FIELDS = "channel",;
        AN_SEND_FMT = "h"
        AN_RSP_FIELDS = "channel", "value";
        AN_RSP_FMT = "hh"
        
        #WRITE_WALKING_PARAM_SEND_FIELDS = "position", "data"
        #WRITE_WALKING_PARAM_SEND_FMT = "<b256b";
        #WRITE_WALKING_PARAM_RSP_FIELDS = "position",
        # 4 4 2 22 7 
        #WRITE_WALKING_PARAM_RSP_FMT = "<b"

        ECODE_RSP_FIELDS = "ecode";
        ECODE_RSP_FMT = "h";
        

        #READ_WALKING_PARAM_SEND_FIELDS = "position", "sector"
        #READ_WALKING_PARAM_SEND_FMT = "<bb";
        #READ_WALKING_PARAM_RSP_FIELDS = "position", "sector", "code","buff" 
        #READ_WALKING_PARAM_RSP_FMT = "<bbh64s"     
        #READ_WALKING_PARAM_RSP_FIELDS = "position", "magic", "save_idcode", "SVN revision", "STR", "data"
        #READ_WALKING_PARAM_RSP_FMT = "<bll22b170b" 

        READ_MEM_SEND_FIELDS   = "mem_type","len","address"
        READ_MEM_SEND_FMT      = "<BBL"
        
        READ_MEM_RSP_FIELDS   = "mem_type","len","address","buff"
        READ_MEM_RSP_FMT      = "<BBL128s"
        
        WRITE_MEM_SEND_FIELDS = "mem_type","len","address","buff"    
        WRITE_MEM_SEND_FMT    = "<BBL128s"



        GF_SEND_FIELDS = "filter_number",;
        GF_SEND_FMT = "<l"
        GF_RSP_FIELDS = "channel", "v_Pk", "v_Pi", "na", "v_Vmax", "pk2", "pi2", "na2", "v_max2"
        GF_RSP_FMT = "<9l"

        SF_SEND_FIELDS = "channel", "v_Pk", "v_Pi", "na", "v_Vmax", "na1", "na2", "na3", "na4"
        SF_SEND_FMT = "<9l"
        SF_RSP_FIELDS = ""
        SF_RSP_FMT = ""


        
        self.table = {
            'header' : {'fields':HEADER_FIELDS, 'format':HEADER_FMT},
            'state_echo' :     {'cmd_value':0, 'send_fields':ECHO_SEND_FIELDS, 'send_format':ECHO_SEND_FMT, 'rsp_fields':ECHO_RSP_FIELDS, 'rsp_format':ECHO_RSP_FMT },
            'motor_echo' :     {'cmd_value':1, 'send_fields':ECHO_SEND_FIELDS, 'send_format':ECHO_SEND_FMT, 'rsp_fields':ECHO_RSP_FIELDS, 'rsp_format':ECHO_RSP_FMT },
            'imu_echo' :       {'cmd_value':2, 'send_fields':ECHO_SEND_FIELDS, 'send_format':ECHO_SEND_FMT, 'rsp_fields':ECHO_RSP_FIELDS, 'rsp_format':ECHO_RSP_FMT },
            
            'state_version' :  {'cmd_value':3, 'send_fields':VERSION_SEND_FIELDS, 'send_format':VERSION_SEND_FMT, 'rsp_fields':VERSION_RSP_FIELDS, 'rsp_format':VERSION_RSP_FMT},
            'motor_version' :  {'cmd_value':4, 'send_fields':VERSION_SEND_FIELDS, 'send_format':VERSION_SEND_FMT, 'rsp_fields':VERSION_RSP_FIELDS, 'rsp_format':VERSION_RSP_FMT},
            'imu_version' :    {'cmd_value':5, 'send_fields':VERSION_SEND_FIELDS, 'send_format':VERSION_SEND_FMT, 'rsp_fields':VERSION_RSP_FIELDS, 'rsp_format':VERSION_RSP_FMT},

            'special' :        {'cmd_value':9,  'send_fields':SPECIAL_FIELDS,   'send_format':SPECIAL_FMT,   'rsp_fields':SPECIAL_FIELDS,   'rsp_format':SPECIAL_FMT,   'default':SPECIAL_DEFAULT},
            'special_n_l' :    {'cmd_value':94, 'send_fields':SPECIAL_FIELDS,   'send_format':SPECIAL_FMT,   'rsp_fields':SPECIAL_L_FIELDS, 'rsp_format':SPECIAL_L_FMT, 'default':SPECIAL_L_DEFAULT},
            'special_l_n' :    {'cmd_value':95, 'send_fields':SPECIAL_L_FIELDS, 'send_format':SPECIAL_L_FMT, 'rsp_fields':SPECIAL_FIELDS,   'rsp_format':SPECIAL_FMT,   'default':SPECIAL_DEFAULT},
            'special_l_l' :    {'cmd_value':96, 'send_fields':SPECIAL_L_FIELDS, 'send_format':SPECIAL_L_FMT, 'rsp_fields':SPECIAL_L_FIELDS, 'rsp_format':SPECIAL_L_FMT, 'default':SPECIAL_L_DEFAULT},
            
            
            'get_dig_in' :    {'cmd_value':10, 'send_fields':DIG_SEND_FIELDS, 'send_format':DIG_SEND_FMT, 'rsp_fields':DIG_RSP_FIELDS, 'rsp_format':DIG_RSP_FMT},
            'set_dig_out' :    {'cmd_value':11, 'send_fields':DIG_OUT_SEND_FIELDS, 'send_format':DIG_OUT_SEND_FMT, 'rsp_fields':'', 'rsp_format':''},
            'get_dig_out' :    {'cmd_value':12, 'send_fields':DIG_SEND_FIELDS, 'send_format':DIG_SEND_FMT, 'rsp_fields':DIG_RSP_FIELDS, 'rsp_format':DIG_RSP_FMT},

            'get_an_in' :    {'cmd_value':20, 'send_fields':AN_SEND_FIELDS, 'send_format':AN_SEND_FMT, 'rsp_fields':AN_RSP_FIELDS, 'rsp_format':AN_RSP_FMT}, 
            'set_an_out' :    {'cmd_value':22, 'send_fields':AN_OUT_SEND_FIELDS, 'send_format':AN_OUT_SEND_FMT, 'rsp_fields':'', 'rsp_format':''},
            'get_an_out' :    {'cmd_value':23, 'send_fields':AN_SEND_FIELDS, 'send_format':AN_SEND_FMT, 'rsp_fields':AN_RSP_FIELDS, 'rsp_format':AN_RSP_FMT},

            'ecode_rsp' :    {'cmd_value':42, 'send_fields':"", 'send_format':"", 'rsp_fields':ECODE_RSP_FIELDS, 'rsp_format':ECODE_RSP_FMT},

            
            'get_filter' :    {'cmd_value':51, 'send_fields':GF_SEND_FIELDS, 'send_format':GF_SEND_FMT, 'rsp_fields':GF_RSP_FIELDS, 'rsp_format':GF_RSP_FMT},
            'set_filter' :    {'cmd_value':50, 'send_fields':SF_SEND_FIELDS, 'send_format':SF_SEND_FMT, 'rsp_fields':SF_RSP_FIELDS, 'rsp_format':SF_RSP_FMT},            


            
            'write_mem' :     {'cmd_value':30, 'send_fields':WRITE_MEM_SEND_FIELDS,  'send_format':WRITE_MEM_SEND_FMT,  'rsp_fields':'' ,  'rsp_format':''},
            'read_mem' :     {'cmd_value':31, 'send_fields':READ_MEM_SEND_FIELDS,  'send_format':READ_MEM_SEND_FMT,  'rsp_fields':READ_MEM_RSP_FIELDS,  'rsp_format':READ_MEM_RSP_FMT},

            };


    def get_cmd_dict(self) :
        return self.table;

    def get_def(self,command) :
        return self.table[command];

    
