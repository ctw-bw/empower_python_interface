#!/usr/bin/env python


import sys, traceback, os, time

import comm_lib.crc8 as crc8
import struct;

import hostcmds;

from python_utils import printable_dict 
import python_utils.class_address_helper as class_address_helper

class SerialConnectException(Exception) :
    def __init__(self, value) :
        Exception.__init__(self)
        self.value = value;
        exc_type, exc_value, exc_traceback = sys.exc_info()
        print "class SerialConnectException(Exception) :"

    def __str__(self) :
        str = "\n\nIn class serial connect exception\n\n"
        str += repr(self.value)
        return 


try :
    from collections import deque
    import collections
    SKIP_DEQUE = False
except :
    SKIP_DEQUE = True

try :
    sys.pfcmd_do_not_import_support
    # print "sys.pfcmd_do_not_import_support", sys.pfcmd_do_not_import_support
    pass
except :

    """
    print
    print "PFCMD IS IMPORTING SUPPORT CLASSES"
    print "EDIT this script to support the split pfcmd.py"
    print "and set : sys.pfcmd_do_not_import_support = True"
    print
    """
    #from pfcmd_mc import *
    from pfcmd_ctrl import *
    #from pfcmd_bench_test import *
    #from pfcmd_eeprom_sfs import *

import socket        
import serial






def fast_exit() :
    print
    print "serial port could not be reconnected"
    print
    exc_type, exc_value, exc_traceback = sys.exc_info()    
    print repr(traceback.format_tb(exc_traceback))
    time.sleep(3.0)    
    sys.exit(0)
    
class serial_reconnect(serial.Serial) :

    def __init__(self, com_port, baudrate, timeout, rtscts) :
        # print "INIT CALLED"
        self.my_com_port = com_port;
        self.my_baudrate = baudrate;
        self.my_timeout = timeout;
        self.my_rtscts = rtscts;
        self.my_reconnect(wait_time = 0);


    def my_reconnect(self, wait_time = 0.1) :
        # print "reconnect called :"
        
        tend = time.time() + wait_time

        count = 0;
        sp = None
        tnext = time.time();
        while True :

            try :
                sp = serial.Serial.__init__(self, port=self.my_com_port, baudrate=self.my_baudrate, timeout=self.my_timeout, rtscts=self.my_rtscts)
                sp = True
                break;
            
            except Exception as e :
                if time.time() > tend :
                    raise SerialConnectException("reconnect")
                    break;

            count += 1                
            if time.time() > tnext : 
                print "serial port reconnect try #%5d, time remaining %3.0f sec" % (count, (tend -time.time()))
                tnext += 1.0
                

            time.sleep(0.01)
                

        # print "my_reconnect returns : ", sp

        return sp


    def read(self, msg_len) :
        while True : 
            try :
                buff = serial.Serial.read(self, msg_len);
                break;
            except serial.SerialException  :
                # print "caught SerialException - in read"
                rcd = self.my_reconnect(wait_time = 30);
                if not rcd : 
                    # fast_exit()
                    raise SerialConnectionException("read");


        return buff

    def write(self, msg) :
        while True :
            try :
                n = serial.Serial.write(self, msg)
                break;

            except serial.SerialTimeoutException  :
                # print "caught SerialTimeoutException - in write"
                rcd = self.my_reconnect(wait_time = 30);
                if not rcd :
                    # fast_exit();
                    raise SerialConnectionException("write");
                    
            except serial.SerialException  :
                # print "caught SerialException in write"
                rcd = self.my_reconnect(wait_time = 30);
                if not rcd : 
                    # fast_exit()
                    raise SerialConnectionException("write");




        return n

    def flush(self) :
        while True :
            try :
                n = serial.Serial.flush(self)
                break;
            except serial.SerialException  :
                # print "caught SerialException in flush"
                rcd = self.my_reconnect();
                if not rcd : 
                    fast_exit()            


        return n

        
    def inWaiting(self) :
        while True :
            try :
                n = serial.Serial.inWaiting(self)
                break;
            except serial.SerialException  :
                # print "caught SerialException in inWaiting"
                rcd = self.my_reconnect();
                if not rcd : 
                    fast_exit()            


        return n
        


class PFCmd (object):
    def __init__ (self, unit=0, device=0, quiet=False) :
        # print __name__, SVN_SUB_REV_STR
        self.quiet = quiet;
        self.unit = 1;
        self.seq = 0;
        self.crc = crc8.CRC8();
        self.comm_device = device;
        
        self.debug_level = 0;
        self.hostcmds = hostcmds.hostcmds()
        self.host_cmd_dict = self.hostcmds.get_cmd_dict();
        
        self.crc_error_runlength = 0
        
        # for k in self.host_cmd_dict.keys() :
        #    print k

        # build the tables used by translate
        # built a char array with elements 0-255
        args = range(0,256);
        args.insert(0,"256B");
        self.trans_tbl = apply(struct.pack, args);
        args = range(0, 32) + range(127, 256);        
        args.insert(0,"161B");
        self.trans_del_tbl = apply(struct.pack, args);


        #############
        d1 = ''.join( [chr(n) for n in range(0,10)])
        d2 = ''.join( [chr(n) for n in range(11,13)])
        d3 = ''.join( [chr(n) for n in range(14,32)])
        d4 = ''.join( [chr(n) for n in range(127,256)])        
        self.trans_del_tbl = d1+d2+d3+d4;
        self.trans_tbl = ''.join ( ['%c' % i for i in range(0,256)])

  
        self.ignore_rx_of_local_echo = False #Flag to route away local echo of send data in the case of using LIN mode. 


        
        
        self.USE_SERIAL_PORT = 1;
        self.USE_TCP = 0;        
        self.USE_UDP = 0;
        self.timeout_interval = 0.2;

        #Create buffer object for use with asynchronous command-response protocol:
        #Note: this protocal, while asynchronous, STILL MAKES ASSUMPTIONS ABOUT COMMAND ORDER. IT CANNOT HANDLE OUT OF RESPONSES.
        if not SKIP_DEQUE :
            self.async_cmd_buffer = deque()
        
    def find_serial_ports(self,start=1, stop=39) :
        port_list = [];
        for com_port in range(start, stop) :
            try :
                sp = serial.Serial(com_port-1, timeout=0.001)
                sp.close();
                port_list.append(com_port);

            except :
                pass
        return port_list;
            

        
        
    def connect(self, com_port_raw=None, baud_rate=115200, timeout_seconds =.02) :
        self.USE_SERIAL_PORT = 1;

        sp = None
        if os.name == "posix" :
            if type(com_port_raw) == type(1) :
                com_port = "/dev/ttyUSB%d" % com_port_raw
            else :
                com_port = com_port_raw;
        else :
            com_port = com_port_raw-1;

        start = time.time()    
        while time.time() - start < timeout_seconds: 
            try :
                sp = serial_reconnect(com_port, baudrate=baud_rate, timeout=timeout_seconds, rtscts=0);
                self.comm_device = sp;
                #print "connect : ", sp
                break;
                
            except :
                print "-" * 50
                traceback.print_exc()
                print
                # print "Can't open serial port ", com_port_raw, com_port
                print "Can't open serial port ", com_port_raw
                if os.name == "posix" :
                    print "Can't use this on Posix based operating system."
                    raise SerialConnectionException("Can't use this on Posix based operating system.")
                    break;
                else :
                    print "probing ",
                    sp_list = self.find_serial_ports(1, 99);
                    print "Found ", sp_list
                    val_str = raw_input("Choose one : ");
                    try :
                        com_port = int(val_str)-1;
                    except :
                        print "No comm port specified, exit in pfcmd.connect"
                        sys.exit()
                        break
                    
            
    def close(self) :
        self.comm_device.close();
        
    def open(self):
        self.comm_device.open();
        
    def tcp(self, ip_addr='10.0.0.10', port=10002) :
        self.timeout_interval = 1.0
        self.USE_SERIAL_PORT = 0;
        self.USE_TCP = 1;

        BASE_IP_ADDRESS = "192.168.192."
        AH = class_address_helper.address_helper(190,240, BASE_IP_ADDRESS);
        ip_addr = AH.get_address(ip_addr)
        

        print "Connecting to ", ip_addr, port

        sp = socket.socket(socket.AF_INET, socket.SOCK_STREAM);
        sp.settimeout(self.timeout_interval);

        # sp.connect((LANTRON_HOST,LANTRON_PORT));
        sp.connect( (ip_addr, port));
        self.comm_device = sp;


    def udp(self, ip_addr = '127.0.0.1', bind_port = 9989, send_to_port = 9990) :
        self.USE_SERIAL_PORT = 0;
        self.USE_UDP = 1;
        
        self.bind_addr = (ip_addr, bind_port);
        self.bind_soc = socket.socket(socket.AF_INET, socket.SOCK_DGRAM);
        self.bind_soc.bind(self.bind_addr);
        self.comm_device = self.bind_soc;
        
        self.udp_addr = (ip_addr, send_to_port);
        self.send_soc = socket.socket(socket.AF_INET, socket.SOCK_DGRAM);

    def set_tcp_timeout(self, val) :
        print "pfcmd: NOT COMPELTE"

        
    def set_serial_timeout(self, val) :
        # print "pfcmd: old / new timeout :", self.comm_device.timeout, val
        self.comm_device.timeout = float(val)

    def raw_read(self, val_len):
        return self.comm_device.read(val_len);   
         
    def read(self, val_len) :
        count = 0;
        if self.USE_SERIAL_PORT :
            ln = self.comm_device.read(val_len);
            
                
        else :
            self.read_level = 1;
            try : 
                ln = self.comm_device.recv(val_len);
                if (self.debug_level > 3) :
                    print "pf.read",len(ln)
            except :
                print "timeout", self.timeout_interval
                print "Read failed", len(ln)
                print "Got", [hex(ord(ch)) for ch in ln]
                raise SerialConnectionException("read");

            self.read_level = 2;                        

            while (len(ln) < val_len ):
                count = count + 1;
                self.read_level = 10+count;                                        
                
                ln2 = self.comm_device.recv(val_len - len(ln));

                ln = ln + ln2;
                if (count > 3) :  # try to read severla times
                    break;

        return ln;

    def fast_flush(self, TIMEOUT = 0.005, FLUSH_SIZE = 1000) :
        if self.USE_SERIAL_PORT :
            pass;
        else :
            self.comm_device.settimeout(TIMEOUT);
            try :
                ln = self.comm_device.recv(FLUSH_SIZE);
            except :
                pass
            self.comm_device.settimeout(self.timeout_interval);


    def flush(self, FLUSH_SIZE=100) :
        self.read(FLUSH_SIZE);
        
    def write(self, msg) :
        if self.USE_SERIAL_PORT :
            self.comm_device.flush()
            self.comm_device.write(msg)
            
            
        elif self.USE_TCP :
            self.comm_device.send(msg)
            # self.comm_device.flush()            
        elif self.USE_UDP :
            self.send_soc.sendto(msg, self.udp_addr)
        
        if (self.ignore_rx_of_local_echo):
                to_ignore = self.read(len(msg)); #ignore what we just sent. 
                if (to_ignore != msg):
                    print "Echo matched message sent out:", (to_ignore == msg)
                    print "Sent out:", [hex(ord(x)) for x in msg]
                    print "Ignored local echo:", [hex(ord(x)) for x in to_ignore]
                    #raise Exception("Error! Local LIN echo did not equal what was sent out!")
                elif not self.quiet:
                    print "Ignored local echo:", [hex(ord(x)) for x in to_ignore]
            
    def disconnect(self):
        if self.USE_SERIAL_PORT :
            self.comm_device.close()
            
        elif self.USE_TCP :
            self.comm_device.close()
        elif self.USE_UDP :
            self.send_soc.close()
            self.bind_soc.close()

    def build_msg(self, cmd, data) :
        header = struct.pack('BBB', self.unit, cmd, self.seq);


        self.seq = 1;
        if (self.seq >= 100) :
            self.seq = 0;

        str = header+data;
        
        crc = self.crc.upd_crc_string(str);
        # print len(str),crc,
        
        msg = struct.pack('B',crc) + header+data;
        return msg;

    def build_result_dict(self, fields, values) :
        d = {};
        try :
            for f,v in zip(fields, values) :
                d[f] = v;
        except KeyError :
            d = None
            
        return d;
    
    def get_header(self, data) :
        """
        return a dictionary with header field names and values
        """
        # header_fields = ["crc","unit","cmd","seq"];
        # values = struct.unpack("<BBBB",data[0:4]);
        fields = self.host_cmd_dict['header']['fields'];
        fmt = self.host_cmd_dict['header']['format'];
        len = struct.calcsize(fmt);
        values = struct.unpack(fmt, data[0:len]);
        
        header = self.build_result_dict(fields, values);
        return header



    def send_command(self,command, values) :
        # self.debug_level = 5;
        # print command
        cmd_def = self.hostcmds.get_def(command);
        
        if self.debug_level > 2:
            print "\n\nsend_command:cmd_def : ", command, cmd_def


        valid_len = struct.calcsize(cmd_def['send_format']);

        cmd = cmd_def['cmd_value'];

        if (self.debug_level > 2) :
            print "command %d, len %d " % (cmd, valid_len)
            print "cmd_def", cmd_def            
            print "values",values, len(values)

        
        """
        in_params = {};
        for f,v in zip(cmd_def['send_fields'], values) :
            in_params[f] = v;
        """
        
        send_format = cmd_def['send_format'];
        if (self.debug_level > 2) :
            print "send format", send_format

        # test that supplied argument count is enough,

        
        num_required_fields = len(cmd_def['send_fields']);
        if (self.debug_level > 2) :
            print "required fields",num_required_fields, cmd_def['send_fields']
        
        
        if (len(values) != num_required_fields) :
            if (self.debug_level > 2) :
                print "required fields error"
                print "pfcmd.send_command : ", len(values), num_required_fields, cmd_def['send_fields']
            default = cmd_def['default'];
            # if not then extend the list with the default values
            if (self.debug_level > 2) :
                print "extending", values
            
            for i in range(len(values) , num_required_fields) :
                values.append(default[i])


        d_len = struct.calcsize(send_format)
        if (self.debug_level > 2) :
            print "send len", d_len


        if (d_len > 0) :

            ## v = [int(x) for x in values];   # copy values by value, not reference!
            #print values
            v = list(values);

            v.insert(0,send_format);

            # print send_format
            data_str = apply( struct.pack, v);
        else :
            data_str = "";
                    

        msg = self.build_msg(cmd,data_str);
        self.write(msg);
       
    def receive_rsp(self,command, ASYNCHRONOUS = False) :
        MAX_CRC_ERROR_RUNLENGTH = 10        
        self.rsp_level = 1;
        header_size = 4;
        cmd_def = self.hostcmds.get_def(command);
        
        #print "receive_rsp:cmd_def : ", cmd_def['cmd_value']
        
        rsp_format = cmd_def['rsp_format'];

        d_len = struct.calcsize(rsp_format);
        
        #if not d_len :
            # a command with no response causes CRC errors without this...
            # return None
        
        valid_len = header_size + d_len;
        # print "expected length : ", valid_len, self.comm_device.inWaiting()

        # result = self.comm_device.read(valid_len);
        self.rsp_level = 11;

        result = None
        if not (ASYNCHRONOUS):
            result = self.read(valid_len);
        else:
            n = time.clock()
            self.comm_device.timeout = None #Asynchronous will wait until we get all bytes, then return
            result = self.comm_device.read(valid_len);
            n2 = time.clock() - n 
            print "Raw response read took:", n2*1000, "milliseconds"
            
        self.rsp_level = 2;
        lr = len(result)

        if lr != valid_len :
            if self.quiet == False or self.quiet == None:
                print "result length ", lr            
            if lr == 6 :
                # switch command to ecode rsp
                cmd_def = self.hostcmds.get_def("ecode_rsp");
                rsp_format = cmd_def['rsp_format'];
                d_len = struct.calcsize(rsp_format);
                valid_len = header_size + d_len;                
            else :
                #for r in result :
                #    print "ord(r)",ord(r),
                if self.quiet == False : 
                    print "length of response t:%d expected:%d (%s)" % (len(result), valid_len, command);
                    print ["%x " % ord(ch) for ch in result]


                return [];
            
        # print len(result)
        self.rsp_level = 3;        
        crc = self.crc.upd_crc_string(result[1:]);
        self.rsp_level = 4;        

        header = self.get_header(result);
        self.rsp_level = 5;

        # check for CRC error
        if (crc != header['crc']) :
            self.crc_error_runlength += 1
            print "res CRC error", crc, header
            
            #self.flush();


            if self.crc_error_runlength >= MAX_CRC_ERROR_RUNLENGTH :
                raise RuntimeError ("pfcmd.receive_rsp:maximum CRC error runlength reached.")
                
            return None
        else :
            self.crc_error_runlength = 0

        self.rsp_level = 51;


        # check that the unit ID matches
        if header['unit'] != self.unit :
            if not self.quiet :
                print "received response has UNIT ID mismatch.  Received %s, expected %s" % (header['unit'] , self.unit)        
            self.flush();
            # ideally we would raise an exception
            return None

        self.rsp_level = 52;

        
        # check that the header type matches what was sent
        if header['cmd'] != cmd_def['cmd_value'] :
            if not self.quiet :
                print "received COMMAND TYPE (%s) mismatch.  Received %s, expected %s" % (command, header['cmd'] ,  cmd_def['cmd_value']);
            self.flush();
            # ideally we would raise an exception
            return None


        self.rsp_level = 6;
        # print "RSP FMT : ", rsp_format
        values = struct.unpack(rsp_format, result[4:]);
        self.rsp_level = 7;
        
        fields = cmd_def['rsp_fields'];
        if type(fields) == type("") :
            fields = [fields]
            
        # print "RSP FIELDS: ",fields, values
        
        d = printable_dict.printable_dict({}) ;
        d.clear()  # wonder why I have to do this?
        
        self.rsp_level = 8;
        d['rsp_list'] = []

        for f,v in zip(fields, values) :
            d[f] = v
            d.set_printable(f);
            d['rsp_list'].append(v)
        self.rsp_level = 9;

        d['rsp_string'] = result;        self.rsp_level = 10;
        
        d['rsp_fields'] = fields;        self.rsp_level = 11;

        return d



    
    

    def cmd(self, command, values, OUTPUT=False) :
        result = self.do_command(command, values);

        s_list = [];
        for key in result['rsp_fields'] :

            str = "%3s=%3s " % (key, result[key])
            if (result[key] <=0) :
                str += ' ';
            s_list.append(str);
            if (OUTPUT) :
                print "cmd:", str,
        if (OUTPUT) :
            print
        return s_list
    

    def special(self, command=104, values=[], secs=0.001, suffix="") :
        values.insert(0,command);
        result = self.do_command('special'+suffix, values);
        time.sleep(secs);
        return result;

                   
    
    
    def do_command_w_struct_str(self, command, values, struct_str, field_list= ['v0','v1','v2','v3','v4','v5','v6','v7','v8','v9']) :
        result = self.do_command(command, values);
        if result :
            rsp = result['rsp_string'][8:]  # skip the header, and the command #
            l_ss = struct.calcsize(struct_str);
            if len(rsp) >= l_ss :
                result['user_values'] = struct.unpack(struct_str, rsp[: l_ss]);
                for val,key in zip(result['user_values'], field_list) :
                    result[key] = val; 

        return result
        
     
    def optimized_do_command(self, command, values):
        result = None;  # CEB 10-20-09 thinks this is a better null result
        cnt = 0;
        self.read_level = 0;
        try :
                self.send_command(command, values);
        except :
                return None
            
        try :
                result = self.receive_rsp(command, ASYNCHRONOUS = True);

        except :
                return None
        
        cmd_def = self.hostcmds.get_def(command);
        #try :
            
        first_send_field = cmd_def['send_fields'][0] 
        first_received_field = cmd_def['rsp_fields'][0]
        if first_send_field == first_received_field :
            first_send_value = values[0];
            print result
            first_received_value = result['rsp_list'][0]
            if first_send_value != first_received_value :
                str = "received response has mismatch in field (%s).  Received %d, expected %d" % (first_send_field, first_received_value, first_send_value);                             
                if not self.quiet :
                    print str
                
        #except :
        #    print cmd_def
        #    return None
            
        return result

    def do_command(self, command, values) :
        result = None; 
        cnt = 0;
        self.read_level = 0;
        self.fast_flush();
        # print command, values
        
        self.send_command(command, values);
        result = self.receive_rsp(command);
            
        if result == [] or result == None :
                pass
        else :
                    cmd_def = self.hostcmds.get_def(command);
                
                    if len(cmd_def['send_fields']) > 0:
                        first_send_field = cmd_def['send_fields'][0] 
                    else:
                        first_send_field = None
                    if len(cmd_def['rsp_fields']) > 0:
                        first_received_field = cmd_def['rsp_fields'][0]
                    else:
                        first_received_field = None
                    
                    if first_send_field == first_received_field :
                        first_send_value = values[0];
                        first_received_value = result['rsp_list'][0]
                        if first_send_value != first_received_value :
                            str = "received response has mismatch in field (%s).  Received %d, expected %d" % (first_send_field, first_received_value, first_send_value);                             
                            if not self.quiet :
                                print str
                            else :
                                # raise ValueError(str)                                
                                pass


                            return None
                

                
            
        cnt = cnt + 1;
  
            
        

        return result;

    # returns str with non-printable chars removed
    def strip_nonprintable(self, str) :
        
        s = str.translate(self.trans_tbl, self.trans_del_tbl);
        return s;


    def command(self, command, values=[], suffix="") :
        # execute a special command and return something from the result class
        if type(values) == type(1) :
            val = [values];
        else :
            val = list(values);
        val.insert(0,command);
        result_dict = self.do_command('special'+suffix, val);
        result = pfcmd_result(result_dict);
        return result




    def test_operational(self) :
        fail = 0;
        success = 0;
        max_count = 30;
        for ndx in range(0, max_count) :
            try : 
                result = self.do_command('special',[104]);
                if result and len(result) :
                    success += 1;
                else :
                    fail += 1                    
            except :
                fail += 1
            if fail > (success+10) :
                break

        rate = success/float(max_count)
        
        str = "The robot responded to %2.0f%% of pings." % (rate*100) 
        if success<fail :
            raise IOError(str)

        print str
        return rate


class pfcmd_result :
    def __init__ (self, result_dict=None) :
        self.values = [];
        if result_dict and type(result_dict) == type({}) :
            self.cmd = result_dict['cmd'];
            self.field_list = result_dict['rsp_fields'];

            # build a value dictionary
            self.value_dict = {};
            for f_name in self.field_list :
                self.value_dict[f_name] = result_dict[f_name];
            

    # helper function to just display the results
    def display_header(self, FORMAT="%-8s ") :
        for k in self.field_list :
            print FORMAT % k,
        print
        
    def display(self, FORMAT = "%-8s ") :

        for k in self.field_list :
            v = self.value_dict[k];
            if type(v) == type('str') :
                print "%s" % v
            else :
                print FORMAT % v,
        print
